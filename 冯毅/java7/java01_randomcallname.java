package java08;


import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class java01_randomcallname {
    public static void main(String[] args) {
			ArrayList<Student> students=new ArrayList<>();
			Scanner sc=new Scanner(System.in);
			addStudentName(students);
			System.out.println(students);
			System.out.println(randomStudentName(students));
    }
    public static  void printStudentName(ArrayList<Student> students){
        for(int i=0;i<students.size();i++){
        	Student s=students.get(i);
            System.out.println("第"+(i+1)+"个学生名称:"+s.getName()+"年龄"+s.getAge());
        }
    }
    public static void addStudentName(ArrayList<Student> students) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入要添加的学生数量：");
        int numStudents = sc.nextInt();
        sc.nextLine();

        for (int i = 0; i < numStudents; i++) {
            System.out.println("请输入第" + (i + 1) + "个学生的名称：");
            String name = sc.next();
            Student s = new Student();
            s.setName(name);
            System.out.println("请输入第" + (i + 1) + "个学生的年龄：");
            boolean a=true;
            int age=0;
            while(a){
            	try {
            		 age = sc.nextInt();
    				s.setAge(age);
    				a=false;
    			} catch (AgeException e) {
    				System.out.print("年龄必须在18-25,请重新输入：");
    				
            }catch(InputMismatchException e){
            	System.out.print("年龄必须输入数字，请重新输入：");
				sc.next();
            }
            }
            students.add(s);
        }
    }
    public static String randomStudentName(ArrayList<Student> students){
        int index= new Random().nextInt(students.size());
        Student s=students.get(index);
        return "幸运儿: 姓名："+s.getName()+"年龄："+s.getAge();
    }
}

