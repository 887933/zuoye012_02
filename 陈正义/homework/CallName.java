package homework;
import java.util.ArrayList;
/*8.随机点名案例

1.存储三个同学的姓名
2.查看所有同学的姓名
3.随机点名其中一人，打印到控制*/
import java.util.Random;
import java.util.Scanner;
public class CallName {
	public static void main(String args[]) {
		ArrayList<Student>list=new ArrayList<Student>();
		setStudentName(list);
		printStudentName(list);
		Student st=callName(list);
		System.out.println("姓名:"+st.name+" 年龄"+st.age+" id:"+st.id);
	}

	private static Student callName(ArrayList<Student> list) {
		Random r=new Random();
		int i=r.nextInt(list.size());
		return list.get(i);
		
	}

	private static void printStudentName(ArrayList<Student> list) {
		for(int i=0;i<list.size();i++) {
			Student st=list.get(i);
			System.out.println("学生"+(i+1)+"学生姓名:"+st.name+" 学生年龄"+st.age+" id:"+st.id);
		}
	}

	private static void setStudentName(ArrayList<Student> list) {
		for(int i=0;i<3;i++) {
			Student st= new Student();
			Scanner sc=new Scanner(System.in);
		    System.out.println("请输入学生信息");
		    st.name=sc.next();
		    st.age=sc.nextInt();
		    st.id=i+1;
			list.add(st);
			}
		
		
	}
	

}
