package homework;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.ToIntFunction;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//决策引擎
public class homework {
	public static void main(String[] args) throws Exception {
		BufferedReader br=new BufferedReader(new FileReader("决策引擎Java-data.txt"));
		//只能一个一个运行
		//wenti1(br);
		//wenti2(br);
		wenti3(br);
	}
	private static void wenti3(BufferedReader br) throws IOException {
		ArrayList<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("0~15");
        HashMap<String, Integer> map = new HashMap<>();
        String line;
        while ((line = br.readLine()) != null) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                list.add(line);
            }
        }
        String[] str=list.get(0).split(";");
        int i=1;
        for(String s:str){
        	String[] str1=s.split(",");
        	for(String str12:str1){
	        	String[] s1=str12.split(":");
	        	String[] ss1=s1[0].split("~");
	        	ss1[0] = ss1[0].trim();
	        	if(i<10){
	        		map.put("0"+i+String.valueOf(Integer.parseInt(ss1[0])+15), Integer.parseInt(s1[1]));
	        	}else{
	        		map.put(""+i+String.valueOf(Integer.parseInt(ss1[0])+15), Integer.parseInt(s1[1]));
	        	}	
        	}
        	i++;
        }
        
        List<Map.Entry<String, Integer>> list1 = new LinkedList<>(map.entrySet());
        System.out.println("15分钟:");
        for (Map.Entry<String, Integer> entry : list1) {
            System.out.print("<"+entry.getKey() + "," + entry.getValue()+">");
        }
        System.out.println("");
        
        Collections.sort(list1, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        System.out.println("排序后:");
        for (Map.Entry<String, Integer> entry : list1) {
            System.out.print("<"+entry.getKey() + "," + entry.getValue()+">");
        }
	}
	private static void wenti2(BufferedReader br) throws IOException {
        ArrayList<String> list = new ArrayList<>();
        Pattern pattern = Pattern.compile("0~30");
        HashMap<String, Integer> map = new HashMap<>();
        String line;
        while ((line = br.readLine()) != null) {
            Matcher matcher = pattern.matcher(line);
            if (matcher.find()) {
                list.add(line);
            }
        }
        String[] str=list.get(0).split(";");
        int i=1;
        for(String s:str){
        	String[] str1=s.split(",");
        	String[] s1=str1[0].split(":");
        	String[] ss1=s1[0].split("~");
        	String[] s2=str1[1].split(":");
        	String[] ss2=s2[0].split("-");
        	if(i<10){
        		map.put("0"+i+ss1[1], Integer.parseInt(s1[1]));
        	}else{
        		map.put(i+ss1[1], Integer.parseInt(s1[1]));
        	}
        	if(i<10){
        		map.put("0"+i+ss2[1], Integer.parseInt(s2[1]));
        	}else{
        		map.put(i+ss2[1], Integer.parseInt(s2[1]));
        	}
        	i++;
        }
        
        List<Map.Entry<String, Integer>> list1 = new LinkedList<>(map.entrySet());
        System.out.println("30分钟:");
        for (Map.Entry<String, Integer> entry : list1) {
            System.out.print("<"+entry.getKey() + "," + entry.getValue()+">");
        }
        System.out.println("");
        Collections.sort(list1, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o2.getValue().compareTo(o1.getValue());
            }
        });
        System.out.println("排序后:");
        for (Map.Entry<String, Integer> entry : list1) {
            System.out.print("<"+entry.getKey() + "," + entry.getValue()+">");
        }
	}
	private static void wenti1(BufferedReader br) throws IOException {
		String line=null;
		while((line=br.readLine())!=null){
			System.out.println(line);
		}
	}
	
}
