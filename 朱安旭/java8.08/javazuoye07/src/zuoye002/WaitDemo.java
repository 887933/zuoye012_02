package zuoye002;

public class WaitDemo {
    public static void main(String[] args) {
        Object lock = new Object();
        
        // 循环三次，重复执行子进程和主进程的过程
        for (int count = 0; count < 3; count++) {
            Thread threadA = new Thread(() -> {
                System.out.println("INFO:A等待锁");
                synchronized (lock) {
                    System.out.println("INFO:A得到了锁");
                    printZi("A");
                    try {
                        System.out.println("INFO:A准备进入等待状态，放弃锁lock的控制权");
                        lock.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            });

            Thread threadB = new Thread(() -> {
                System.out.println("INFO:B等待锁");
                synchronized (lock) {
                    System.out.println("INFO:B得到了锁");
                    printZhu("B");
                    System.out.println("INFO:B打印完毕，调用notify方法");
                    lock.notify();
                }
            });

            threadA.start();
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            threadB.start();

            // 等待子进程和主进程执行完毕
            try {
                threadA.join();
                threadB.join();
            } catch (InterruptedException e) {
               e.printStackTrace();
            }
        }
    }

    private static void printZhu(String threadName) {
        int i = 0;
        while (i++ < 5) {
            try {
                Thread.sleep(100);
                System.out.println("主进程：" + threadName + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static void printZi(String threadName) {
        int i = 0;
        while (i++ < 3) {
            try {
                Thread.sleep(100);
                System.out.println("子进程：" + threadName + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}