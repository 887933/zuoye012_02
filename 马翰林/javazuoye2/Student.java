package javazuoye2;

public class Student {
    private int id;
    private String name;
    private int age;
    public void eat()
    {
        System.out.println("我吃吃吃");
    }
    public void sleep()
    {
        System.out.println("我到處睡覺");
    }
    public int getId()
    {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean equals(javaday2.Student t) {
        if(this.getAge()==t.getAge()){
            return true;
        }
        else
            return false;
    }

    public Student(int id, String name, int age) {
        this.id = id;
        this.name=name;
        this.age=age;
    }
    public Student() {

    }


}
