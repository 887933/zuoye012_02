package HomeWork;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomCallName01 {
    public static void main(String[] args) {
        List<String> students = new ArrayList<>();
        try (BufferedReader br = new BufferedReader(new FileReader("src/HomeWork/rcn.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                students.add(line);
            }
        } catch (IOException e) {
            System.out.println("读取数据时出错");
            return;
        }

        Random random = new Random();
        int index = random.nextInt(students.size());
        String selectedStudent = students.get(index);

        String[] parts = selectedStudent.split("-");
        String name = parts[0];
        System.out.println("随机选择的学生是: " + name);
    }
}