package homework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class MapDistinct_7_24 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {3, 4, 5, 2};

        Map<Integer, Integer> map = new HashMap<>();
        for (int i : arr1) {
            map.merge(i, 1, Integer::sum);
        }

        for (int j : arr2) {
            map.merge(j, 1, Integer::sum);
        }

        System.out.println("Complete Map: " + map);

        ArrayList<Integer> arr3 = map.entrySet().stream()
                .filter(entry -> entry.getValue() == 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toCollection(ArrayList::new));

        System.out.println("Elements with frequency 1: " + arr3);
    }
}
