package java07;

public class Actors {
	private String name;
	private String sex;
	private int age;
	public Actors(String name, String sex, int age) {
		super();
		this.name = name;
		this.sex = sex;
		this.age = age;
	}
	public Actors() {
		super();
	}
	public Actors(String str) {
		String[] split = str.split("-");
		this.name=split[0];
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	@Override
	public String toString() {
		return "Actors [name=" + name + "]";
	}
}
