package demo1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.stream.Collectors;

public class java02 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<>();
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", 
		"张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35",
		"王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		//Stream流
		list.stream()
		.filter(s->"男".equals(s.split("-")[1]))
		.filter(s->s.split("-")[0].length()==3).limit(3)
		.forEach(System.out::println);
		
		list.stream()
		.filter(s->"女".equals(s.split("-")[1]))
		.filter(s->s.startsWith("林")).skip(1)
		.forEach(System.out::println);
		
		//方法引用
		list.stream().filter(java02::filterMan).limit(3).forEach(System.out::print);
		list.stream().filter(java02::filterWomen).skip(1).forEach(System.out::print);

	}
	public static boolean filterMan(String s){
		if("男".equals(s.split("-")[1]) && s.split("-")[0].length()==3){
			return true;
		}else{
			return false;
		}
		
	}
	public static boolean filterWomen(String s){
		if("女".equals(s.split("-")[1]) && s.startsWith("林")){
			return true;
		}else{
			return false;
		}
	}
	
}


