package homework;

import java.util.ArrayList;
import java.util.Random;

public class User {
	private String username;
	private String usertype;
	private int moeny;
	public User() {}
	
	public User(String username,String usertype,int money) {
		this.username = username;
		this.usertype = usertype;
		this.moeny = money;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public int getMoney() {
		return moeny;
	}
	public void setMoeny(int moeny) {
		this.moeny = moeny;
	}
	
	public void show(){
		System.out.println("用户"+this.username+"的余额为"+moeny);
	}
	
	public void receive(ArrayList<Integer> redList){
		int index = new Random().nextInt(redList.size());
		Integer delta = redList.remove(index);
		int money=this.getMoney()+delta;
		this.setMoeny(money);
	}
	
	public ArrayList<Integer> send(int totalMoney,int count){
		ArrayList<Integer>  redList=new ArrayList<Integer>();
		int leftMoney=this.getMoney();
		if(totalMoney>leftMoney){
			System.out.println("余额不足");
			return redList;
		}else {
			this.setMoeny(leftMoney-totalMoney);
			int m = totalMoney,avg,num;
			for(int i=0;i<count-1;i++) {
				avg = m/(count-i);
				num = new Random().nextInt(avg);
				redList.add(num);
				m -= num;
			}
			redList.add(m);
			for(int i=0;i<redList.size();i++) {
				System.out.println(redList.get(i)+" ");
			}
			return redList;
		}
	}
}
