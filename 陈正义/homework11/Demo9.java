package homework11;

//编写10个线程，第一个线程从1加到10，第二个线程从11加20…第十个线程从91加到100
//，最后再把10个线程结果相加。
public class Demo9 {
	static int sum=0;
	public static void main(String[] args) {
		Thread t1=new Thread(()->{
			for(int i=1;i<=10;i++) {
				sum=sum+i;
			}
		});
		Thread t2=new Thread(()->{
			for(int i=111;i<=20;i++) {
				sum=sum+i;
			}
		});
		Thread t3=new Thread(()->{
			for(int i=21;i<=30;i++) {
				sum=sum+i;
			}
		});
		Thread t4=new Thread(()->{
			for(int i=31;i<=40;i++) {
				sum=sum+i;
			}
		});
		Thread t5=new Thread(()->{
			for(int i=41;i<=50;i++) {
				sum=sum+i;
			}
		});
		Thread t6=new Thread(()->{
			for(int i=51;i<=60;i++) {
				sum=sum+i;
			}
		});
		Thread t7=new Thread(()->{
			for(int i=61;i<=70;i++) {
				sum=sum+i;
			}
		});
		Thread t8=new Thread(()->{
			for(int i=71;i<=80;i++) {
				sum=sum+i;
			}
		});
		Thread t9=new Thread(()->{
			for(int i=81;i<=90;i++) {
				sum=sum+i;
			}
		});
		Thread t10=new Thread(()->{
			for(int i=91;i<=100;i++) {
				sum=sum+i;
			}
		});
		t1.start();
		try {
			t1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t2.start();
		try {
			t2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t3.start();
		try {
			t3.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t4.start();
		try {
			t4.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t5.start();
		try {
			t5.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t6.start();
		try {
			t6.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t7.start();
		try {
			t7.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t8.start();
		try {
			t8.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t9.start();
		try {
			t9.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		t10.start();
		try {
			t10.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(sum);
		
	}
}
