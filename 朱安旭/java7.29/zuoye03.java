package demo01;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class zuoye03 {
	public static void main(String[] args) {
		File src=new File("E:\\1437970061");
		HashMap<String, Integer> map=new HashMap<>();
		findFile(src,map);
		
	}

	private static void findFile(File src, HashMap<String,Integer>map) {
		// TODO Auto-generated method stub
		File[] listFiles=src.listFiles();
		if(listFiles !=null)
		{
			for(File f : listFiles)
			{
				String name=f.getName();
				String lastname=getlastname(name);
				if(map.containsKey(lastname))
				{
					int count=map.get(lastname);
					map.put(lastname, count+1);
				}else
				{
					map.put(lastname, 1);
				}
			}
		}
		for(String type : map.keySet())
		{
			int count=map.get(type);
			System.out.println(type+":"+count);
		}
	}

	private static String getlastname(String name) {
		// TODO Auto-generated method stub
		int lastDotIndex = name.lastIndexOf('.');
		if(lastDotIndex >0)
			{
				return name.substring(lastDotIndex+1);
			}else{
				return "";
			}
	}

	
}
