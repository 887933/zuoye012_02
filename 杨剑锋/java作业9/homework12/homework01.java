package homework12;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

/*作业1：抽奖金额
集合：[10,6,20,50,100,200,500,800,2,80,300,700]

两个抽奖箱，抽奖箱1，抽奖箱2.
 打印控制台的格式：
  	每次抽出一个奖项随机打印一个只:
  抽奖箱1产生一个10元大奖
抽奖箱2产生一个500元大奖
抽奖箱1产生一个20元大奖
抽奖箱1产生一个200元大奖
抽奖箱2产生一个800元大奖
...*/

public class homework01 {
	public static void main(String[] args) {
		jackpot t1=new jackpot();
		jackpot t2=new jackpot();
		t1.setName("抽奖箱1");
		t2.setName("抽奖箱2");
		t1.start();
		t2.start();
	}
}
