package zuoye08;

import javax.management.InvalidApplicationException;
import java.io.*;
import java.util.Date;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/*
 作业2：
在文件中准备用户名和密码，user.txt
每个用户输入用户名和密码输入三次后，被锁1分钟，如果再次输入三次后错误，将被冻结
* */
public class zuoye02 {
     public static void main(String[] args) throws IOException, InterruptedException, InputMismatchException {
         HashMap<String,Integer> map=new HashMap<>();
           BufferedReader br = new BufferedReader(new FileReader("user.txt"));
           Scanner sc = new Scanner(System.in);
         String str=br.readLine();
           while(str!=null)
           {
               String[] arr=str.split("=");
               map.put(arr[0],Integer.parseInt(arr[1]));
               str=br.readLine();
           }
           System.out.println(map);
           int count=0;
           try {
               for (int i = 0; ; i++) {
                   if (count == 6) {
                       System.out.println("登录失败");
                       break;
                   }
                   String name = sc.nextLine();
                   if (map.containsKey(name)) {
                       System.out.println("用户名正确请输入密码");
                       int age = sc.nextInt();
                       if ((map.get(name) == age)) {
                           System.out.println("密码正确登陆成功");
                           break;
                       } else {
                           System.out.println("密码错误,请重新输入");
                           count++;
                       }
                   } else {
                       System.out.println("输入错误，请重新输入");
                       count++;
                   }
                   if (count == 3) {
                       Date date = new Date();
                       System.out.println("请等一分钟后重试");
                       TimeUnit.SECONDS.sleep(10);
                   }
               }

           }
           catch (InputMismatchException e) {
               System.out.println("请输入数字");
           }
           br.close();
     }
}
