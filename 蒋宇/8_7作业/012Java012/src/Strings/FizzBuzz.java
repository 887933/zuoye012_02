package Strings;

public class FizzBuzz extends Thread{
	int printFizzBuzz;
	
	public FizzBuzz(int printFizzBuzz) {
		super();
		this.printFizzBuzz = printFizzBuzz;
	}

	@Override
	public void run() {
		synchronized (Demo.class) {
			if(printFizzBuzz%3==0&&printFizzBuzz%5==0){
				System.out.println("fizzbuzz");
			}
			
		}
		
	}

}
