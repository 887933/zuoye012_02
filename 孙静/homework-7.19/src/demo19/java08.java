package demo19;

import java.util.HashMap;
import java.util.Map;

public class java08 {
    public static int str(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int n = s.length();
        int ans = 0;
        // 定义map存储字符到索引的映射
        Map<Character, Integer> map = new HashMap<>();

        // 用i和j定义当前窗口的起始和结束位置
        for (int j = 0, i = 0; j < n; j++) {
            // 如果字符在map中，更新i的位置
            if (map.containsKey(s.charAt(j))) {
                i = Math.max(map.get(s.charAt(j)) + 1, i);
            }
            // 更新答案和map中的索引
            ans = Math.max(ans, j - i + 1);
            map.put(s.charAt(j), j);
        }

        return ans;
    }

    public static void main(String[] args) {
        String input = "abcdacmfg";
        int length = str(input);
        System.out.println("is: " + length);
    }
}
