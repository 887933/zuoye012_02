package fd;

import java.util.ArrayList;
import java.util.Random;

public class Member implements USB{
	String username;
	int moeny;
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public int getMoeny() {
		return moeny;
	}

	public void setMoeny(int moeny) {
		this.moeny = moeny;
	}

	public Member(String username, int moeny) {
		super();
		this.username = username;
		this.moeny = moeny;
	}
	public Member() {
	}
	public void show(){
		System.out.println("我叫:"+username+",我有多少钱:"+moeny);
	}

	@Override
	public void receive(ArrayList<Integer> redList) {
		// TODO Auto-generated method stub
		int index = new Random().nextInt(redList.size());
		//根据索引  ，从集合中删除这个元素 ，得到删除得红包
		Integer delta = redList.remove(index);
		//当前成员自己本来有多少钱
		int money=this.getMoeny()+delta;
		this.setMoeny(money);
	}

	@Override
	public ArrayList<Integer> send(int totalMoeny, int count) {
		// TODO Auto-generated method stub
		ArrayList<Integer>  redList=new ArrayList<Integer>();
		Random sc=new Random();
		int leftMoney=this.moeny;
		if(totalMoeny>leftMoney){
			System.out.println("余额不足");
			return redList;//返回null
		}
		this.setMoeny(leftMoney-totalMoeny);
		int avg,mod;
		for(int i=count;i>0;i--){
			if(i==1){
				redList.add(totalMoeny);
				break;
			}
			mod=totalMoeny/i*2;
			avg=sc.nextInt(mod)+1;
			redList.add(avg);
			totalMoeny-=avg;
		}
		return redList;
	}
}
