package homework;

import java.util.Random;
import java.util.Scanner;

//求1-100之间的奇数和，并把求和的结果在控制台打印
public class homework02 {
	public static void main(String[] args){
		for01();
	}
	//求1-100之间的奇数和，并把求和的结果在控制台打印
	public static void while01(){
		int sum=0;
		for(int i=1;i<=100;i++){
			if(i%2==1){
				sum+=i;
			}
		}
		System.out.println(sum);
	}
	//输入三个数，求最大值
		public static void while03(){
			Scanner sc=new Scanner(System.in);
			int[] arr=new int[3];
			int max;
			int j=0;
			for(int i=0;i<3;i++){
				arr[i]=sc.nextInt();
			}
			for(int i=0;i<3;i++){
				if(arr[j]<arr[i]){
					j=i;
				}
			}
			max=arr[j];
			System.out.println(max);
		}
	
	public static void while02(){
		int i=0;
		double think=0.001;
		while(think<8000){
			think=think*2;
			i++;
		}
		System.out.println("折叠了："+i+"次");
	}
	
	//输出所有两位数，三位数，四位数，五位数的回文数      12321 是回文  10101 是   12345不是
	public static void for01(){
		for(int i=10;i<=99999;i++){
			int orig=i;
			int comp=0;
			while(orig!=0){
				int digit=orig%10;
				comp=comp*10+digit;
				orig/=10;
			}
			if(i==comp){
				System.out.println(i);
			}
		}
	}
	// 逢7过， 个位7，十位7或者7的倍数     1-100  满足条件的打印出来
	public static void for02(){
		int i;
		for(i=1;i<100;i++){
			int a=i/10;
			int b=i%10;
			if(a==7||b==7||i%7==0){
				System.out.println(i);
			}
		}
	}
	
	
	//循环作业3：  求平方根
	/*
	 * 键盘输入一个数字， 求平方根
	 * 
	 * 	16的平方根4
	 * 4的平方根是2
	 * 
	 * 10的平方根：
	 * 	3*3=9；
	 * 4*4=16；
	 *  10的平方根是3-4之间
	 *  
	 *  20的平方根4-5之间
	 *  
	 *  
	 *  循环作业4：  输入一个(1-100)数字，判断是否为质数
	 *  质数：
	 *  只能1和本身整除
	 *  7 质数
	 *  8 和数
	 *  		
	 */
	public static void for03(){
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		int i=0;
		while(i*i!=num&&i*i<num){
			i++;
		}
		if(i*i!=num){
			System.out.println(num+"的根是"+(i-1)+"-"+i+"之间");
		}
		else{
			System.out.println(num+"的根是"+i);
		}
	}

	
	//循环作业4：  输入一个(1-100)数字，判断是否为质数
	public static void for04(){
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		int i,j=0;
		for(i=2;i<num;i++){
			if(num%i==0){
				System.out.println("不是质数");
				j=1;
				break;
			}
		}
		if(j==0){
			System.out.println("是质数");
		}
	}
	
	
	/*
	 * 作业1：
	 *  你妈妈每天给你2.5元，你都会存起来，但是当这一天是
	 *  存前的第五天或者是5的倍数的时候，你就会去花6元钱，
	 *  多久你能存到100元
	 */
	public static void for05(){
		double money=0;
		int i=0;
		while(money<100){
			i++;
			money+=2.5;
			if(i%5==0){
				money=money-6;
			}
		}
		System.out.println("存100元用了"+i+"天");
	}
	
	//作1：   int [] arr={1,2,3,4,5};  要求打乱顺序
	public static void array01(){
		int[] arr={1,2,3,4,5};
		Random random=new Random();
		for(int i=0;i<arr.length;i++){
			int j=random.nextInt(i+1);
			int temp=arr[i];
			arr[i]=arr[j];
			arr[j]=temp;
		}
		for(int i=0;i<arr.length;i++){
			System.out.println(arr[i]);
		}
	}
	
	
	 /* 作业：定义方法
	  * 1.判断数组当中某一个数是否存在，如果存在，则返回true,否则返回false
	  * 2. 定义方法复制数组当中的任意位置的内容，转化为新的数组
	  * 	源数组：int[] arr={1,2,3,4,5,6,7,8,9};
	  * 	复制2，5这个位置的数组的内容
	  * 	int[] arr={3,4,5};
	 */
	public static void way(){
		int[] arr={1,2,3,4,5};
		int num=9;
		System.out.println(judge(arr,num));
		int[] arrs={1,2,3,4,5,6,7,8,9};
		int[] lo={2,3,4};
		int[] tab=new int[lo.length];
		tab=copy(arrs,lo);
		for(int i=0;i<tab.length;i++){
			System.out.println(tab[i]);
		}
	}
	
	public static boolean judge(int[] arr,int num){
		for(int i=0;i<arr.length;i++){
			if(num==arr[i]){
				return true;
			}
		}
		return false;
	}
	public static int[] copy(int[] arr,int[] lo){
		int[] num=new int[lo.length];
		for(int i=0;i<lo.length;i++){
			num[i]=arr[lo[i]];
		}
		return num;
	}
}


