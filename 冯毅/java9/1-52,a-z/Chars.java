package java11;

public class Chars extends Thread{
	@Override
	public void run() {
		while(true){
			synchronized (Tools.lock) {
				if(Tools.count==53){
					Tools.lock.notify();
					break;
				}else{
					if(!Tools.flag){
						char c=(char)('A'+Tools.charcount);
						System.out.print(c+" ");
						Tools.charcount++;
						Tools.flag=true;
						Tools.lock.notify();
					}else{
						try {
							Tools.lock.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
