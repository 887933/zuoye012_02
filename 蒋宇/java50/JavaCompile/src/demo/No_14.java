package demo;

import java.util.Scanner;

public class No_14 {
	public static void main(String[] args) {
//		输入某年某月某日，判断这一天是这一年的第几天？
//		程序分析：以3月5日为例，应该先把前两个月的加起来，
//		然后再加上5天即本年的第几天，特殊情况，闰年且输入月份大于3时需考虑多加一天。
		int[] pin={31,28,31,30,31,30,31,31,30,31,30,31};
		int[] run={31,29,31,30,31,30,31,31,30,31,30,31};
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入年");
		int year=sc.nextInt();
		System.out.println("请输入月");
		int month=sc.nextInt();
		System.out.println("请输入日");
		int day=sc.nextInt();
		int days=0;
		if(isLeapYear(year)){
			for(int i=0;i<month-1;i++){
				days+=run[i];
			}
			System.out.println("这天是本年的第"+(days+day)+"天");
		}else{
			for(int i=0;i<month-1;i++){
				days+=pin[i];
			}
			System.out.println("这天是本年的第"+(days+day)+"天");
		}

	}

	public static boolean isLeapYear(int year) {

		if (year % 4 == 0) {

			if (year % 100 == 0) {

				if (year % 400 == 0) {

					return true; // 能够被400整除，是闰年

				} else {

					return false; // 能够被100整除但不能被400整除，不是闰年

				}

			} else {

				return true; // 能够被4整除但不能被100整除，是闰年

			}

		} else {

			return false; // 不能够被4整除，不是闰年

		}

	}

}
