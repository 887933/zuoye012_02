package java50practice;

import java.util.Scanner;

public class java07 {
	public static void main(String[] args) {
Scanner scanner = new Scanner(System.in);
        
        System.out.println("请输入一行字符:");
        String input = scanner.nextLine();
        
        int letters = 0;
        int digits = 0;
        int spaces = 0;
        int others = 0;

        for (char ch : input.toCharArray()) {
            if (Character.isLetter(ch)) {
                letters++;
            } else if (Character.isDigit(ch)) {
                digits++;
            } else if (ch == ' ') {
                spaces++;
            } else {
                others++;
            }
        }
        System.out.println("英文字母有 " + letters + " 个");
        System.out.println("数字有 " + digits + " 个");
        System.out.println("空格有 " + spaces + " 个");
        System.out.println("其他字符有 " + others + " 个");
	}
}
