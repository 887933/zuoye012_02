package java9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Scanner;

public class user_login {
	public static void main(String[] args) throws Exception {
		BufferedReader br=new BufferedReader(new FileReader("user"));
		String line;
		ArrayList<User> list=new ArrayList<>();
		while((line=br.readLine())!=null){
			String[] arr=line.split("=");
			list.add(new User(arr[0],arr[1]));
		}
		br.close();
		int count=0;
		long currenttime=System.currentTimeMillis();
		while(true){
			try{
				Scanner sc=new Scanner(System.in);
				System.out.print("请输入用户名：");
				String username=sc.next();


				System.out.println(" ");
				System.out.print("请输入密码：");


				String password=sc.next();
				System.out.println(" ");


				for(User u:list){
					boolean UserNameErro = u.getUsername().equals(username);
					boolean PasswordErro = u.getPassword().equals(password);
					if(!UserNameErro || !PasswordErro){
						count++;
						throw new IllegalUserLoginException("用户名或密码错误，请重新输入。"); 
					}
					break;
				}
				System.out.println("登陆成功");
				break;
			}catch(IllegalUserLoginException e){
				System.out.println("第"+ count +"次输入错误");
				if(count<3){
					System.out.printf("如果再输入错误%d次将被冻结一分钟\n",3-count);
				}
				if(count==3){
					System.out.println("请一分钟后再次输入");
					Thread.sleep(3600);
				}
				if(count>3 && count<6){
					System.out.printf("如果再输入错误%d次将被永久冻结\n",6-count);
				}
				if(count==6){
					System.out.println("该用户已被冻结");
					break;
				}
			}
		}

	}
}
