package demo01;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class zuoye01 {
	public static void main(String[] args) {
		ArrayList<String> arr=new ArrayList();
		Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		 arr.stream().map(new Function<String, String>() {

			@Override
			public String apply(String t) {
				// TODO Auto-generated method stub
				String [] arr=t.split(",");
				String name=arr[0];
				return name;
//				return (t.split(",")[0]);
			}
		}); new Function<String, Integer>() {

			@Override
			public Integer apply(String t) {
				// TODO Auto-generated method stub
				String [] arr=t.split(",");
				String ageString=arr[1];
				int age=Integer.parseInt(ageString);
				return age;
//				return Integer.parseInt(t.split(",")[1]);
			}
		};
		System.out.println(arr);
		Map<String, Integer> collect = arr.stream().filter(t->Integer.parseInt(t.split(",")[1])>24).collect(Collectors.toMap(s->s.split(",")[0],
				s->Integer.parseInt(s.split(",")[1])
				));
		System.out.println(collect);
	}
}
