package demo1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class java04 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入需要加密的字符串：");
		String s = sc.nextLine();
		char[] ch = s.toCharArray();
		
		ArrayList<Character> list = new ArrayList<>();
		for(char c:ch){
			list.add(c);
		}
		//stream
		list.stream().map(c-> {
				if(c>='a'&&c<='o'){
					return (char)((c-97)/3+50);
				}else if(c>='p'&&c<='s'){
					return (char)(55);
				}else if(c>='t'&&c<='v'){
					return (char)(56);
				}else if(c>='w'&&c<='z'){
					return (char)(57);
				}	
				else if(c>='A'&&c<='Z'){
					return (char)(c+33);
					
				}else{
					return c;
				}
			}).forEach(System.out::print);
		System.out.println("");
		//方法引用
		list.stream().map(java04::judge).forEach(System.out::print);
		
	}
	
	public static Character judge(Character c){
		if(c>='a'&&c<='o'){
			return (char)((c-97)/3+50);
		}else if(c>='p'&&c<='s'){
			return (char)(55);
		}else if(c>='t'&&c<='v'){
			return (char)(56);
		}else if(c>='w'&&c<='z'){
			return (char)(57);
		}	
		else if(c>='A'&&c<='Z'){
			return (char)(c+33);
			
		}else{
			return c;
		}
	}
}
