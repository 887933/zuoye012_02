package java9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class CallStudent05 {
	public static void main(String[] args) throws Exception, IOException {
		String line;
		int runtime=0;

		//读取程序运行次数
		BufferedReader brruntimes=new BufferedReader(new FileReader("runtimes.txt"));
		while((line=brruntimes.readLine())!=null){
			String[] str=line.split("=");
			runtime=Integer.parseInt(str[1]);
		}
		brruntimes.close();
		
		
		ArrayList<Student> removedstudentarray=new ArrayList<Student>();
		
		//如果程序已经运行了十次，再次运行第十一次
		if(runtime%10==0 && runtime!=0){
			//将已经移除的学生添加回studentlast文件
			BufferedReader removedstudent=new BufferedReader(new FileReader("TenStudent"));
			while((line=removedstudent.readLine())!=null){
				String[] str=line.split("-");
				removedstudentarray.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
			}
			removedstudent.close();

			BufferedWriter copystudent=new BufferedWriter(new FileWriter("studentlast.txt"));
			for(Student std:removedstudentarray){
				copystudent.write(std.getName()+"-"+std.getSex()+"-"+std.getAge());
				copystudent.newLine();
			}
			copystudent.close();
			
			System.out.println("开始重复运行");
		}



		//开始点名
		
		BufferedWriter bwruntimes=new BufferedWriter(new FileWriter("runtimes.txt"));
		bwruntimes.write("runtimes="+ (runtime+1));//程序运行次数+1
		System.out.println("运行次数"+ (runtime+1));
		bwruntimes.close();



		//获取点名后剩下的学生数据
		ArrayList<Student> studentlast=new ArrayList<Student>();
		BufferedReader brstudentlast=new BufferedReader(new FileReader("studentlast.txt"));
		while((line=brstudentlast.readLine())!=null){
			String[] str=line.split("-");
			studentlast.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
		}
		brstudentlast.close();


		Random rd=new Random();


		int num=rd.nextInt(studentlast.size());
		System.out.println(studentlast.get(num).getName());
		studentlast.remove(num);

		BufferedWriter bwstudentlast=new BufferedWriter(new FileWriter("studentlast.txt"));
		for(Student std:studentlast){
			bwstudentlast.write(std.getName()+"-"+std.getSex()+"-"+std.getAge());
			bwstudentlast.newLine();
		}
		bwstudentlast.close();

	}
}

