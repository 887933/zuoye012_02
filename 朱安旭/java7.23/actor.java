package javazuoye03;

public class actor {
	String sex;
	int age;
	public String getSex() {
		return sex;
	}
	public void setSex(String sex) {
		this.sex = sex;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public actor(String sex, int age) {
		super();
		this.sex = sex;
		this.age = age;
	}
	public actor() {
		super();
	}
	@Override
	public String toString() {
		return "actor [sex=" + sex + ", age=" + age + "]";
	}
	
}
