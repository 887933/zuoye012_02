package java9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class sort {
public static void main(String[] args) throws Exception {
	BufferedReader b1=new BufferedReader(new FileReader("sort"));
	ArrayList<String> arr=new ArrayList<String>();
	String line;
	while((line=b1.readLine())!=null){
		arr.add(line);
	}
	b1.close();
	Collections.sort(arr,(o1,o2)-> o1.charAt(0)-o2.charAt(0));
		
	BufferedWriter out=new BufferedWriter(new FileWriter("sort"));
	for(String s:arr){
		out.write(s);
		out.newLine();
	}
	out.close();
	BufferedReader b2=new BufferedReader(new FileReader("sort"));
	while((line=b2.readLine())!=null){
		System.out.println(line);
		System.out.println(" ");
	}
	
	b2.close();
}
}
