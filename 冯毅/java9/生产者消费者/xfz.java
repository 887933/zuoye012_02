package java11;

public class xfz extends Thread{
	public void run() {
		while(true){
			synchronized(Desk.lock){
				if(Desk.count==0){
					break;
				}else{
					if(Desk.flag){
						System.out.println("买者正在吃小笼包");
						Desk.flag=false;
						Desk.lock.notify();
						Desk.count--;
					}else{
						try {
							Desk.lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

}
