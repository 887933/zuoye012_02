package demo;

public class HomeWork06 {
	public static void main(String[] args){
//		输出所有两位数，三位数，四位数，五位数
//		的回文数12321 是回文  10101 是   12345
//		不是
		for(int i=10;i<100000;i++){
			if(i>=10&&i<=99){
				if(i%10==i/10){
					System.out.println(i);
				}
			}
			else if(i>=100&&i<=999){
				if(i%10==i/100){
					System.out.println(i);
				}
			}
			else if(i>=1000&&i<=9999){
				if(i%10==i/1000&&(i/10%10)==(i/100%10)){
					System.out.println(i);
				}
			}
			else {
				if(i%10==i/10000&&(i/10%10)==(i/1000%10)){
					System.out.println(i);
				}
			}
		}
	}

}
