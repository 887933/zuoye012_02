package javazuoye03;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;

public class zuoye03 {
	public static void main(String[] args) {
		HashMap<Character,Integer> arr=new HashMap<>();
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一串字符串");
		String str;
		str = sc.next();
		char[] ch =str.toCharArray();
		for(char c:ch)
		{
			if(arr.containsKey(c))
			{
				int value=arr.get(c);
				arr.put(c, value+1);
			}else
			{
				arr.put(c, 1);
			}
		}
		System.out.println("请输入想要查询出现了几次的字符");
		int number = sc.nextInt();
		for(Map.Entry<Character, Integer> entry : arr.entrySet())
		{
			Character key=entry.getKey();
			Integer value=entry.getValue();
			if(value == number) {System.out.println(key);}
	}
	}
}
