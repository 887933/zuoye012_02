package homework;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class homework {
	public static void main(String[] args) {
		//作业，插入排序和希尔排序
		//insertionSortHomework();
//		shellSortHomework();
	
		//重构随机点名案例
//		randomCallNameHomework();
		
		//拼手气红包案例
		//randomRedEnvelope();
	}


	private static void shellSortHomework() {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		Integer[] a= {5,7,10,1,3,2,8,4,9,6}; 
		for(int i=0;i<a.length;i++) {
			arr.add(a[i]);
		}
		shellSort(arr);
		System.out.println(arr.toString());
	}


	private static void shellSort(ArrayList<Integer> arr) {
		int n=arr.size()-1;
		for(int gap=arr.size()/2;gap>0;gap/=2) {
			for(int i=gap;i<arr.size();i++) {
				int last=0;
				for(int j=i;j>=gap;j-=gap) {	
					if(arr.get(j)<arr.get(j-gap)) {
						swap(arr,j,j-gap);
						last = j;
					}
				}
				if(n==0) {
					break;
				}
			}
		}
		
	}


	private static void randomCallNameHomework() {
		ArrayList<Student> arr = new ArrayList<Student>();
		CallName(arr);
	}


	private static void insertionSortHomework() {
		ArrayList<Integer> arr = new ArrayList<Integer>();
		Integer[] a= {5,7,1,3,2,8,4,9}; 
		for(int i=0;i<a.length;i++) {
			arr.add(a[i]);
		}
		insertSort(arr);
		System.out.println(arr.toString());
	}


	private static void insertSort(ArrayList<Integer> arr) {
		for(int i=1;i<arr.size();i++) {
			for(int j=i;j>0;j--) {
				if(arr.get(j)<arr.get(j-1)) {
					swap(arr,j,j-1);
				}else {
					break;
				}
			}
		}
		
	}


	private static void swap(ArrayList<Integer> arr, int i, int j) {
		int temp = arr.get(i);
		arr.set(i, arr.get(j));
		arr.set(j, temp);
	}


	private static void randomRedEnvelope() {
		User u0 = new User("aaa","manager",300);
		double[] darr = u0.send(200,3);
		User u1 = new User("bbb","member",50);
		User u2 = new User("ccc","member",50);		
		User u3 = new User("ddd","member",50);
		darr = u1.recieve(darr);
		darr = u2.recieve(darr);
		darr = u3.recieve(darr);
		System.out.println(u0.getUsertype()+" "+u0.getUsername()+"的余额为："+u0.getMoney());
		System.out.println(u1.getUsertype()+" "+u1.getUsername()+"的余额为："+u1.getMoney());
		System.out.println(u2.getUsertype()+" "+u2.getUsername()+"的余额为："+u2.getMoney());
		System.out.println(u3.getUsertype()+" "+u3.getUsername()+"的余额为："+u3.getMoney());
	}

	private static void CallName(ArrayList<Student> list) {
		addStudent(list);
		printStudent(list);
		randomStudentName(list);
	}

	private static void randomStudentName(ArrayList<Student> list) {
		int index = new Random().nextInt(list.size());
		System.out.println("幸运儿是:"+list.get(index).getName());
	}

	private static void printStudent(ArrayList<Student> list) {
		for(int i=0;i<list.size();i++) {
			System.out.println("第"+(i+1)+"个学生学号是"+list.get(i).getId()+"，姓名是"+list.get(i).getName()+"，年龄是"+list.get(i).getAge());
		}
	}

	private static void addStudent(ArrayList<Student> list) {
		Scanner sc = new Scanner(System.in);
		for(int i=1;i<=3;i++) {	
			Student s = new Student();
			s.setId(i);
			System.out.println("请输入学生姓名：");
			s.setName(sc.next());
			System.out.println("请输入学生年龄：");
			s.setAge(sc.nextInt());
			list.add(s);
		}
	}
}
	