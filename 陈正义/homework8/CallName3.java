package homework8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Random;

public class CallName3 {
	public static void main(String[] args) throws IOException {
		Random r=new Random();
		int n=22;
		ArrayList<Student> list=new ArrayList<Student>();
		BufferedReader br=new BufferedReader(new FileReader("names.txt"));
		String line;
		
		while((line=br.readLine())!=null){
			String[] arr = line.split("-");
			list.add(new Student(arr[0],arr[1],Integer.parseInt(arr[2].toString())));
		}
		callName(n,list);
	}

	private static void callName(int n, ArrayList<Student> list) throws IOException {
		// TODO Auto-generated method stub
		Random s=new Random();
		int m=s.nextInt(100);
		BufferedReader br=new BufferedReader(new FileReader("count2.txt"));
		ArrayList<String> list2=new ArrayList<String>();
		String line;
		while((line=br.readLine())!=null){
			list2.add(line);
		}
		System.out.println(list2.get(0));
		int num=Integer.parseInt(list2.get(0));
		System.out.println(num);
		if(Integer.parseInt(list2.get(0))%3==0) {
			System.out.println(list.get(n).getName());
			list2.set(0,String.valueOf(num+1));
		}else {
			System.out.println(list.get(m).getName());
			list2.set(0,String.valueOf(num+1));
		}
		System.out.println(list2.get(0));
		try (FileOutputStream fos = new FileOutputStream("count2.txt");
	             OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
	             BufferedWriter bw = new BufferedWriter(osw)) {

	            for (String mf : list2) {
	                bw.write(mf);
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}
}
