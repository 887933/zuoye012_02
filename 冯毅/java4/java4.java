package java05;

import java.util.Scanner;

public class java4 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入第一个字符串：");
		String s1=sc.next();
		System.out.print("请输入第二个字符串：");
		String s2=sc.next();
		String s=longestSame(s1, s2);
		System.out.println(s);
	}
    public static String longestSame(String str1, String str2) {
        int[][] dp = new int[str1.length() + 1][str2.length() + 1];
        int max = 0;
        int end = -1;

        for (int i = 1; i <= str1.length(); i++) {
            for (int j = 1; j <= str2.length(); j++) {
                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
                    dp[i][j] = dp[i - 1][j - 1] + 1;
                    if (dp[i][j] > max) {
                        max = dp[i][j];
                        end = i;
                    }
                } else {
                    dp[i][j] = 0;
                }
            }
        }

        if (max > 0) {
            return str1.substring(end - max, end);
        }
        return "没有相同的部分";
    }
}
