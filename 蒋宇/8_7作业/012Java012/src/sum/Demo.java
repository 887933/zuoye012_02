package sum;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.FutureTask;

public class Demo {
	private static final Object lock = new Object();
    
	public static void main(String[] args) throws InterruptedException, ExecutionException {
		No_1 s1=new No_1();
		No_2 s2=new No_2();
		No_3 s3=new No_3();
		No_4 s4=new No_4();
		No_5 s5=new No_5();
		No_6 s6=new No_6();
		No_7 s7=new No_7();
		No_8 s8=new No_8();
		No_9 s9=new No_9();
		No_10 s10=new No_10();
		FutureTask<Integer> f1=new FutureTask<>(s1);
		FutureTask<Integer> f2=new FutureTask<>(s2);
		FutureTask<Integer> f3=new FutureTask<>(s3);
		FutureTask<Integer> f4=new FutureTask<>(s4);
		FutureTask<Integer> f5=new FutureTask<>(s5);
		FutureTask<Integer> f6=new FutureTask<>(s6);
		FutureTask<Integer> f7=new FutureTask<>(s7);
		FutureTask<Integer> f8=new FutureTask<>(s8);
		FutureTask<Integer> f9=new FutureTask<>(s9);
		FutureTask<Integer> f10=new FutureTask<>(s10);
		Thread t1=new Thread(f1);
		Thread t2=new Thread(f2);
		Thread t3=new Thread(f3);
		Thread t4=new Thread(f4);
		Thread t5=new Thread(f5);
		Thread t6=new Thread(f6);
		Thread t7=new Thread(f7);
		Thread t8=new Thread(f8);
		Thread t9=new Thread(f9);
		Thread t10=new Thread(f10);
		t1.start();
		t2.start();
		t3.start();
		t4.start();
		t5.start();
		t6.start();
		t7.start();
		t8.start();
		t9.start();
		t10.start();
		System.out.println(f1.get()+f2.get()+f3.get()+f4.get()+f5.get()+f6.get()+f7.get()+f8.get()+f9.get()+f10.get());
		
	}

}
