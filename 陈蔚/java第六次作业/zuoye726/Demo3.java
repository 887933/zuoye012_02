package zuoye726;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/* 统计任意一个文件夹中（包含子文件夹），每种文件的个数并打印
打印格式如下：
Txt:50个
Doc:20个
Java:50个
Class:50个
Png:10个*/
public class Demo3 {
	public static void main(String[] args) {
		File src=new File("D:\\zuoye726Test");
	   findAll(src);
		
	/*	File file=new File("haha.txt");
		String name=file.getName();
		int lastIndexOf = name.lastIndexOf('.');
		String substring = name.substring(lastIndexOf+1);
		System.out.println(substring);*/
		
	}
	
public static void findAll(File src){
	HashMap<String,Integer> map=new HashMap<>();
	File[] listFiles = src.listFiles();
	if(listFiles!=null){
		for(File file: listFiles){
			if(file.isFile()){
				String name=file.getName();
				int lastIndexOf = name.lastIndexOf('.');
				String filehouzhui= name.substring(lastIndexOf+1);
				if(!map.containsKey(filehouzhui)){
					map.put(filehouzhui, 0);
				}
				if(map.containsKey(filehouzhui)){
					int value=map.get(filehouzhui);
					map.put(filehouzhui, value+1);	
				}	
			}else{
				findAll(file);
			}
				
		}
	 for (Map.Entry<String, Integer> entry : map.entrySet()) {  
	     System.out.println(entry.getKey() + ":" + entry.getValue() + "个");  
	     }  
	}
}
}
