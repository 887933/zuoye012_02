package demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DouDiZhu2 {
	public static void main(String[] args) {
		//准备牌
		ArrayList<String> poker = zhunBei();
		//洗牌
		Collections.shuffle(poker);
		System.out.println(poker);
		Player p1=new Player();p1.setName("老王");
		Player p2=new Player();p2.setName("老李");
		Player p3=new Player();p3.setName("老六");
		//摸牌
		ArrayList<String> dipai = mopai(poker, p1, p2, p3);
		//stream流
		List<String> p1Poker =p1.getPoker().stream().sorted((String o1, String o2) -> o1.charAt(1) - o2.charAt(1)).collect(Collectors.toList());
		List<String> p2Poker = p2.getPoker().stream().sorted((String o1, String o2) -> o1.charAt(1) - o2.charAt(1)).collect(Collectors.toList());
		List<String> p3Poker = p3.getPoker().stream().sorted((String o1, String o2) -> o1.charAt(1) - o2.charAt(1)).collect(Collectors.toList());
		//方法引用
		List<String> p4Poker =p1.getPoker().stream().sorted(DouDiZhu2::subtraction).collect(Collectors.toList());
		List<String> p5Poker =p2.getPoker().stream().sorted(DouDiZhu2::subtraction).collect(Collectors.toList());
		List<String> p6Poker =p3.getPoker().stream().sorted(DouDiZhu2::subtraction).collect(Collectors.toList());
		
		System.out.println(p1.getName()+"的牌:"+p1Poker);
		System.out.println(p2.getName()+"的牌:"+p2Poker);
		System.out.println(p3.getName()+"的牌:"+p3Poker);
		
		System.out.println(p1.getName()+"的牌:"+p4Poker);
		System.out.println(p2.getName()+"的牌:"+p5Poker);
		System.out.println(p3.getName()+"的牌:"+p6Poker);
		System.out.println("底牌:"+dipai);
	}



	public static ArrayList<String> zhunBei() {
		ArrayList<String> poker=new ArrayList<String>();
		String[] colors={"♠","♣","♦","♥"};
		String[] numbers={"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("大王");
		poker.add("小王");
		
		for(String number:numbers){
			for(String color:colors){
				poker.add(color+number);
			}
		}
		System.out.println(poker);
		return poker;
	}

	
	
	public static ArrayList<String> mopai(ArrayList<String> poker, Player p1, Player p2, Player p3) {
		ArrayList<String> dipai=new ArrayList<String>();
		//摸牌
		for(int i=0;i<poker.size();i++){
			//获取每一张的牌
			String p = poker.get(i);
			if(i>=51){
				dipai.add(p);
			}else if(i%3==0){
				p1.getPoker().add(p);
			}else if(i%3==1){
				p2.getPoker().add(p);
			}else if(i%3==2){
				p3.getPoker().add(p);
			}
		}
		return dipai;
	}
	
	
	public static int subtraction(String o1, String o2){
		return o1.charAt(1)-o2.charAt(1);
		
	}
}
