package homework04;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class gather06 {
	public static void main(String[] args) {
        int[] a = {1, 2, 3};
        int[] b = {3, 4, 5, 2};
        
        Set<Integer> setA = new HashSet<>(Arrays.stream(a).boxed().collect(Collectors.toList()));
        Set<Integer> setB = new HashSet<>(Arrays.stream(b).boxed().collect(Collectors.toList()));
        
        Set<Integer> result = new HashSet<>(setA);
        result.addAll(setB); 
        result.removeAll(setA.stream()
                             .filter(setB::contains)
                             .collect(Collectors.toSet()));
        
        System.out.println(result);
    }
}
