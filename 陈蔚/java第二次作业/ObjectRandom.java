package ObjectDemo;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class ObjectRandom {
public static void main(String[] args) {
	ArrayList<Student> liststudent=new ArrayList<>();
	Scanner sc=new Scanner(System.in);
	Student students=new Student();
	addStudent(liststudent);
	print(liststudent);
	randomStudent(liststudent);
	
}
private static void addStudent(ArrayList<Student> liststudent) {
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入要存储学生的个数:");
	int number=sc.nextInt();
	for(int i=0;i<number;i++){
		Student student=new Student();
		System.out.println("请输入第"+(i+1)+"个学生的名称:");
		student.setName(sc.next());
		System.out.println("请输入第"+(i+1)+"个学生的年龄");
		student.setAge(sc.nextInt());
		student.setId(i);
		liststudent.add(student);
	}
	
}

private static void print(ArrayList<Student> liststudent) {
	for(int i=0;i<liststudent.size();i++){
		
	    Student student=liststudent.get(i);
		System.out.println("第"+(i+1)+"个学生的名称为"+student.getName());
		System.out.println("第"+(i+1)+"个学生的年龄为"+student.getAge());
		
	}
	
}

private static void randomStudent(ArrayList<Student> liststudent) {
	Random r=new Random();
	int num=r.nextInt(3);
	System.out.println("幸运儿是"+liststudent.get(num).getName());
}
}
