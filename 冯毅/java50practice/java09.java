package java50practice;

import java.util.ArrayList;

public class java09 {
	public static void main(String[] args) {
		for(int i=1;i<=1000;i++){
			ArrayList<Integer> a=new ArrayList<>();
			for(int j=1;j<i;j++){
				if(i%j==0){
					a.add(j);
				}
			}
			int sum=0;
			for(Integer in:a){
				sum+=in;
			}
			if(sum==i){
				System.out.println(i);
			}
		}
	}
}
