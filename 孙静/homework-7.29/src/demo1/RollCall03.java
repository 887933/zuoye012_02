package demo1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;



public class RollCall03 {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException {
		ArrayList<String> list = new ArrayList<>();
		readFile(list,"names.txt");
		
		Random random = new Random();
		int index = random.nextInt(100);
		
		BufferedWriter bw=new BufferedWriter(new FileWriter(("name.txt"),true));
		bw.write(list.get(index));
		bw.newLine();
		bw.close();
		ArrayList<String> list1 = new ArrayList<>();
		list1 = readFile(list1,"name.txt");
		for(String s:list1){
			System.out.println(s);
		}
		System.out.println("本次幸运者是："+list.get(index));
	}

	public static ArrayList<String>  readFile(ArrayList<String> list,String string) throws FileNotFoundException, IOException {
		BufferedReader br = new BufferedReader(new FileReader(string));
		String line;
		while((line=br.readLine())!=null){
			list.add(line);
		}
		br.close();
		return list;
	}
	

}
