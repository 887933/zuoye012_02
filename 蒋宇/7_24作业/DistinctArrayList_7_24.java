package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.IntFunction;
import java.util.stream.Collectors;

public class DistinctArrayList_7_24 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {3, 4, 5, 2};

        List<Integer> list = new ArrayList<Integer>();

        list = Arrays.stream(arr2).mapToObj(new IntFunction<Integer>() {
        	@Override
        	public Integer apply(int value) {
        		// TODO Auto-generated method stub
        		return Integer.valueOf(value);
        	}
		})
                //.mapToObj(Integer::valueOf)
                .collect(Collectors.toCollection(ArrayList::new));

        List<Integer> finalList = list.stream()
                .collect(Collectors.groupingBy(i -> i, Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() % 2 != 0)
                .map(entry -> entry.getKey())
                .collect(Collectors.toList());

        System.out.println(finalList);
    }
}

