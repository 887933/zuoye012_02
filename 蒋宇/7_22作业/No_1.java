package homework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class No_1 {
	public static void main(String[] args) {
		/*
		 * 创建一个arraList集合,并添加以下字符串,字符串中前面是姓名 ,后面是年龄 "zhangsn,23" "lisi,24"
		 * "wangwu,25" 保留年龄大于24岁的,并将结果收集到map集合中,姓名为建,年龄为值
		 */

		List<String> list = new ArrayList<>();
		list.add("zhangsn,23");
		list.add("lisi,24");
		list.add("wangwu,25");

		HashMap<String, Integer> resultMap = new HashMap<>();
		for(String a:list){
			String[] split = a.split(",");
			if(Integer.parseInt(split[1])>24){
				resultMap.put(split[0], Integer.parseInt(split[1]));
			}
		}

		System.out.println("Filtered Map: " + resultMap);

		// ArrayList<String> aa= new ArrayList<String>();
		// HashMap<String,String> map=new HashMap<String,String>();
		// aa.add("zhangsn,23");
		// aa.add("lisi,24");
		// aa.add("wangwu,25");
		// String string = aa.toString();
		// String substring = string.substring(1, string.length()-1);
		//
		// String[] split = substring.split(",");
		// for(int i=0;i<split.length;i=i+2){
		// map.put(split[i],split[i+1]);
		// }
		// Set<String,String> entrySet = map.keySet();
		// for(<String,String> c :entrySet){
		// c.
		// }
		// ListIterator<String> listIterator = aa.listIterator();
		// while(listIterator.hasNext()){
		// String str = listIterator.next();
		// if(str.equals("老王")){
		// listIterator.add("隔壁老王");
		// }
		// }

	}
}
