package hom05;

import java.io.File;
import java.util.HashMap;
/*作业3：
统计任意一个文件夹中（包含子文件夹），每种文件的个数并打印
打印格式如下：
Txt:50个
Doc:20个
Java:50个
Class:50个
Png:10个
.....*/

public class homework04 {
	public static void main(String[] args) {
		File file = new File("d:\\1");
		HashMap<String, Integer> map=countFiles(file);
		map.entrySet().stream().forEach(a->System.out.println(a.getKey()+":"+a.getValue()+"个"));
	}

	private static HashMap<String, Integer> countFiles(File file) {
		File[] files=file.listFiles();
		HashMap<String, Integer> hashMap = new HashMap<String, Integer>(); 
		if(files!=null){
			for(File f1:files){
				if(f1.isFile()){
					String extension = getFileExtension(f1);
					if(hashMap.containsKey(extension)){
						int value=hashMap.get(extension)+1;
						hashMap.put(extension, value);
					}else{
						hashMap.put(extension, 1);
					}
				}else{
					hashMap.putAll(countFiles(f1));
				}
			}
		}
		return hashMap;
	}

	private static String getFileExtension(File f1) {
		String filename=f1.getName();
		if(filename.lastIndexOf(".")!=-1&&filename.lastIndexOf(".")!=0){
			return filename.substring(filename.lastIndexOf(".")+1);
		}
		return null;
	}
}
