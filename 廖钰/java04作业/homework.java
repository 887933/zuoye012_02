package homework;

import java.util.ArrayList;
import java.util.Scanner;

public class homework {
	public static void main(String[] args) {
		//面试题作业：String、StringBuffer、StringBuilder之间的区别
		//面试题作业：为什么String的长度是final的？不加final有什么影响？
		
//		
//		  作业：输入1-100以内的整数并添加到集合中，直到集合数据总和超过200为止
//		ArrayList<Integer> arr = new ArrayList<Integer>();
//		ArrayTill200(arr);
		
//		  作业：输入字符串转化为整数
//		Scanner sc = new Scanner(System.in);
//		System.out.print("put in a number: ");
//		String str = sc.nextLine();
//		int num = String_to_Int(str);
//		System.out.println(num);
		
//		  作业：输入字符串判断是否是回文
//		Scanner sc=new Scanner(System.in);
//		System.out.println("put in a number:");
//		String str = sc.nextLine();
//		System.out.println(isPalindrome(str));
		
//		  作业：找出两个字符串中相同的最长的字符串
//		String string1 = "abcdef";
//		String string2 = "acef";
//		findLongestSameString(string1,string2);
		
//		  作业：将一个字符串反转，或指定部分反转
//		String string = "abcde";
//		int begin = 0;
//		int end = 3;
//		System.out.println(turnStringOpposite(string,begin,end));
		
//		  作业：删除字符串中所有指定字符串
//		String string = "abcdef";
//		String deleted = "cde";
//		System.out.println(deleteString(string,deleted));
		
//		  作业：十进制转二进制（即写Integer.toBinaryString（）的实现）
//		Scanner sc=new Scanner(System.in);
//		System.out.println("put in a number:");
//		int decimal = sc.nextInt();
//		System.out.println(to_binary(decimal));

//		  作业：找出不含有重复（字母）的最长的字符串
//		String string1 = "aabcabcc";
//		String string2 = "abbc";
//		findDistinctLongestString(string1,string2);
//		 
	}



	private static void findDistinctLongestString(String string, String string2) {
		String compare = "";
		int flag = 0;
		for(int length = string2.length();length>0;length--) {
			for(int begin=0;begin<=string2.length()-length;begin++) {
				compare = string2.substring(begin,begin+length);
				if(isDistindct(compare)&&findSameString(string,compare)) {
					flag = 1;
					break;
				}
			}
			if(flag==1) {
				System.out.println(compare);
				break;
			}
		}
	}



	private static boolean isDistindct(String compare) {
		for(int i=0;i<compare.length();i++) {
			for(int j=i+1;j<compare.length();j++) {
				if(compare.charAt(i)==compare.charAt(j)) {
					return false;
				}
			}
		}
		return true;
	}



	private static void findLongestSameString(String string,String string2) {
		String compare = "";
		int flag = 0;
		for(int length = string2.length();length>0;length--) {
			for(int begin=0;begin<=string2.length()-length;begin++) {
				compare = string2.substring(begin,begin+length);
				if(findSameString(string,compare)) {
					flag = 1;
					break;
				}
			}
			if(flag==1) {
				System.out.println(compare);
				break;
			}
		}
	}

	private static boolean findSameString(String string, String compare) {
		char[] arr1=string.toCharArray(),arr2=compare.toCharArray();
		for(int i=0;i<arr1.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				if(arr2[j]!=arr1[i+j]) {
					break;
				}else if(j==arr2.length-1){
					return true;
				}else{
					continue;
				}	
			}
		}
		return false;
	}



	private static String deleteString(String string, String deleted) {
		char[] arr1=string.toCharArray(),arr2=deleted.toCharArray();
		String returnString = "";
		int flag = 0;
		for(int i=0;i<arr1.length;i++) {
			for(int j=0;j<arr2.length;j++) {
				if(arr2[j]!=arr1[i+j]) {
					break;
				}else if(j==arr2.length-1){
					flag = 1;
				}else{
					continue;
				}	
			}
			if(flag==1) {
				for(int j = 0;j<arr2.length;j++) {
					if(i+j+arr2.length>=arr1.length) {
						break;
					}
					returnString += arr1[i+j+arr2.length];
				}
				i += arr2.length;
			}else {
				returnString += arr1[i];
			}
		}
		return returnString;
	}



	private static String turnStringOpposite(String string, int begin, int end) {
		char[] arr = new char[string.length()];
		String returnString = "";
		for(int i=0;i<string.length();i++) {
			if(i>=begin&&i<=end) {
				arr[end+begin-i] = string.charAt(i);
			}else {
				arr[i] = string.charAt(i);
			}
		}
		for(int i=0;i<arr.length;i++) {
			returnString += arr[i];
		}
		return returnString;
	}



	private static String to_binary(int decimal) {
		String binary = "";
		int devided = decimal;
		while(true) {
			binary+= (devided%2);
			devided/=2;
			if(devided==0||devided==1) {
				binary += "1";
				break;
			}
		}
		String returnString = turnStringOpposite(binary, 0, binary.length()-1);
		return returnString;
	}



	private static boolean isPalindrome(String str) {
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)<'0'||str.charAt(i)>'9') {
				System.out.println("含有非法字符！请输入一个数字！");
				return false;
			}
		}
		for(int i=0;i<str.length()/2;i++) {
			if(str.charAt(i)!=str.charAt(str.length()-1-i)) {
				return false;
			}
		}
		return true;
	}



	private static int String_to_Int(String str) {
		for(int i=0;i<str.length();i++) {
			if(str.charAt(i)<'0'||str.charAt(i)>'9') {
				System.out.println("含有非法字符！请输入一个数字！");
				return 0;
			}
		}
		int sum=0;
		char c;
		for(int i=0;i<str.length();i++) {
			c = str.charAt(i);
			sum = sum*10+(int)(c-'0');
		}
		return sum;
	}



	private static void ArrayTill200(ArrayList<Integer> arr) {
		Scanner sc = new Scanner(System.in);
		int sum = 0,num = 0;
		while(sum<=200) {
			System.out.print("put in a number(1-100): ");
			num = sc.nextInt();
			if(num > 100 || num < 1) {
				continue;
			}
			arr.add(num);
			sum += num;
			System.out.println(" current array:"+arr.toString());
			System.out.println("current sum = "+sum);
		}
		System.out.println("==== method end ====");
	}
}
