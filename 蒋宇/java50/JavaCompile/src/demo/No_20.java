package demo;

public class No_20 {
	public static void main(String[] args) {
//		有一分数序列：2/1，3/2，5/3，8/5，13/8，21/13...
//		求出这个数列的前20项之和。
//		程序分析：请抓住分子与分母的变化规律。
		double num=0.0;
		for(int i=1;i<=20;i++){
			num=num+(getNumUp(i)/getNumDo(i));
		}
		System.out.println(num);

	}

	private static double getNumUp(double x) {
		// TODO Auto-generated method stub
		if(x==1){
			return 2;
		}else if(x==2){
			return 3;
		}else{
			return getNumUp(x-1)+getNumUp(x-2);
		}
	}
	private static double getNumDo(double x) {
		// TODO Auto-generated method stub
		if(x==1){
			return 1;
		}else if(x==2){
			return 2;
		}else{
			return getNumDo(x-1)+getNumDo(x-2);
		}
	}

}
