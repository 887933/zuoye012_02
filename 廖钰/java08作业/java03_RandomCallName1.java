package homework;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class java03_RandomCallName1 {
	public static void main(String[] args) throws IOException, ParseException {
		/*
		 * 
		 * 
		 * 作业1：随机点名
		 * 使用爬虫生成的50个男生、50个女生姓名的文件，每行姓名占用一行
		 * 格式 姓名-性别-年龄
		 * 
		 * 每次运行都打印一个同学：只显示姓名
		 * 
		 */
		randomCallName1("names.txt");
	}

	private static void randomCallName1(String string) throws IOException {
		FileReader fr = new FileReader(string);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		ArrayList<String> list = new ArrayList<String>();
		while((line = br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		Random r = new Random();
		int num = r.nextInt(list.size());
		String name = list.get(num);
		System.out.println(name.split("-")[0]);
	}


}
