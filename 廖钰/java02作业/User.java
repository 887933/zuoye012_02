package homework;

import java.util.Random;

public class User {
	private String username;
	private String usertype;
	private double money;
	
	public User() {}
	public User(String username,String usertype,double money) {
		this.username = username;
		this.usertype = usertype;
		this.money = money;
	}
	
	public double[] send(double totalMoney,int count) {
		double[] a = new double[3];
		if(totalMoney>this.money) {
			System.out.println("余额不足！");
			return a;
		}else {
			this.money -= totalMoney;
			double m = totalMoney;
			for(int i=0;i<count-1;i++) {
				double amount = new Random().nextDouble()*m;
				a[i] = amount-(amount%0.01);
				m = m-a[i];
			}
			a[2] = m;
			a[2] = a[2]-a[2]%0.01;
			
			System.out.print("红包分别为：[");
			for(int i=0;i<a.length;i++) {
				System.out.print(a[i]);
				if(i!=a.length-1) {
					System.out.print(", ");
				}
			}
			System.out.println("]");
			return a;
		}
	}
	
	public double[] recieve(double[] a) {
		double[] aa = new double[a.length-1];
		int index = new Random().nextInt(a.length);
		this.money += a[index];
		for(int i=0,j=0;i<a.length;i++) {
			if(i==index) {
				continue;
			}else {
				aa[j] = a[i];
				j++;
			}
		}
		if(aa.length!=0) {
			System.out.print("红包剩余为：[");
			for(int i=0;i<aa.length;i++) {
				System.out.print(aa[i]);
				if(i!=aa.length-1) {
					System.out.print(", ");
				}
			}
			System.out.println("]");
		}else {
			System.out.println("红包已领完！");
			aa = null;
		}
		return aa;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getUsertype() {
		return usertype;
	}
	public void setUsertype(String usertype) {
		this.usertype = usertype;
	}
	public double getMoney() {
		return money;
	}
	public void setMoney(double money) {
		this.money = money;
	}
}
