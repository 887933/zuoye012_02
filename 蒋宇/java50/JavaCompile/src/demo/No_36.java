package demo;

import java.util.Arrays;
import java.util.Scanner;

//equals方法  
public class No_36 {
	public static void main(String[] args) {
		// 有n个整数，使其前面各数顺序向后移m个位置，最后m个数变成最前面的m个数

		int[] a = { 1, 2, 3, 5, 9, 8, 4, 3 };
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入m(1-" + a.length + ")");
		int sign = sc.nextInt();
		String s = "";
		for (int i = 0; i < a.length-sign; i++) {
			s += a[i];
		}
		for(int i=a.length-1;i>=a.length-sign;i--){
			s=a[i]+s;
		}
		char[] charArray = s.toCharArray();
		System.out.println(Arrays.toString(charArray));

	}

}
