package java09;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class java01 {
	public static void main(String[] args) throws Exception {
		BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream("sort.txt"),"utf-8"));
		String s;
		TreeMap<Integer, String> tm=new TreeMap<>();
		while((s=br.readLine())!=null){
			String[] split = s.split("\\.");
			tm.put(Integer.parseInt(split[0]), split[1]);
		}
		OutputStreamWriter osw=new OutputStreamWriter(new FileOutputStream("newsort.txt"),"utf-8");
		Set<Entry<Integer, String>> entrySet = tm.entrySet();
		for(Entry<Integer, String> e:entrySet){
			String sss=e.getKey()+"."+e.getValue();
			osw.write(sss);
			osw.write("\n");
		}
		br.close();
		osw.close();
	}
}
