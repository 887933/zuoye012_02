package homework;

import java.util.ArrayList;
import java.util.HashMap;

public class MoreThan24Age_7_24 {

    public static void main(String[] args) {
        ArrayList<String> arr = new ArrayList<>();
        arr.add("张三,21");
        arr.add("李四,24");
        arr.add("丁真,26");
        arr.add("珍珠,27");
        arr.add("锐刻五,30");

        HashMap<String, Integer> map = arr.stream()
            .map(entry -> entry.split(","))
            .filter(es -> Integer.parseInt(es[1]) > 24)
            .collect(HashMap::new,
                (m, es) -> m.put(es[0], Integer.parseInt(es[1])),
                HashMap::putAll);

        System.out.println("年龄大于24岁的有：");
        map.forEach((name, age) -> System.out.println(name + ":" + age));
    }
}
