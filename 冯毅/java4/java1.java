package java05;

import java.util.ArrayList;
import java.util.Scanner;

public class java1 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int sum=0;
		ArrayList<Integer> a=new ArrayList<Integer>();
		while(sum<200){
			System.out.print("请输入数字：");
			int n=sc.nextInt();
			if(n<0||n>100){
				System.out.println("输入不合法，请重新输入！");
			}
			else{
				a.add(n);
				sum+=n;
			}
		}
		System.out.println(a);
	}
}
