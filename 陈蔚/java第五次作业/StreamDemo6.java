package zuoye724;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class StreamDemo6 {
	public static void main(String[] args) {
		 int []arr1 = {1,2,3};
	     int []arr2= {3,4,5,2};
	     List<Integer> collect1 = Arrays.stream(arr1).boxed().collect(Collectors.toList());
	      List<Integer> collect2 = Arrays.stream(arr2).boxed().collect(Collectors.toList());
		 List<Integer> result =new ArrayList<>();
	        Stream<Integer> stream1 = collect1.stream().filter(t->!collect2.contains(t));
	        Stream<Integer> stream2 = collect2.stream().filter(t->!collect1.contains(t));
	        result=Stream.concat(stream1, stream2).collect(Collectors.toList());
	        System.out.println(result);
	}

	
}
