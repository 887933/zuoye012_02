package zuoye11;

import java.util.TreeMap;

public class zuoye06 {
public static void main(String[] args) {
    FizzBuzz m1=new FizzBuzz(16);
    Thread a1=new Thread(m1::buzz);
    Thread a2=new Thread(m1::fizz);
    Thread a3=new Thread(m1::fizzbuzz);
    Thread a4=new Thread(m1::number);
    a1.start();
    a2.start();
    a3.start();
    a4.start();
}
}
