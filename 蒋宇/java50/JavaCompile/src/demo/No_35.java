package demo;

import java.util.Arrays;
import java.util.Scanner;

public class No_35 {
	public static void main(String[] args) {
		//输入数组，最大的与第一个元素交换，
		//最小的与最后一个元素交换，输出数组。
		Scanner sc = new Scanner(System.in);
		int[] a=new int[5];
		for(int i=0;i<a.length;i++){
			a[i]=sc.nextInt();
		}
		System.out.println(Arrays.toString(a));
		int j=0;
		int k=0;
		for(int i=0;i<a.length;i++){
			
			if(a[i]>a[j]){
				j=i;
			}else if(a[i]<a[k]){
				k=i;
			}
		}
		swap(a,j,0);
		swap(a,k,a.length-1);
		System.out.println(Arrays.toString(a));
	}
	private static void swap(int[] a, int i, int j) {
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

}
