package copy;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileCopyWithProgress {

    public static void main(String[] args) {
        // 假设要复制的文件列表
        String[] filesToCopy = {"2.png", "4.png"};
        String targetFolder = "E:\\"; // 目标文件夹路径
        
        for (String file : filesToCopy) {
            Thread copyThread = new Thread(new FileCopyTask(file, targetFolder));
            copyThread.start();
        }
    }
}

class FileCopyTask implements Runnable {
    private String fileName;
    private String targetFolder;

    public FileCopyTask(String fileName, String targetFolder) {
        this.fileName = fileName;
        this.targetFolder = targetFolder;
    }

    @Override
    public void run() {
        File sourceFile = new File(fileName);
        File destinationFile = new File(targetFolder + sourceFile.getName());
        
        try (FileInputStream fis = new FileInputStream(sourceFile);
             FileOutputStream fos = new FileOutputStream(destinationFile)) {
             
            long fileSize = sourceFile.length();
            byte[] buffer = new byte[4096];
            long totalBytesRead = 0;
            int bytesRead;
            
            while ((bytesRead = fis.read(buffer)) != -1) {
                fos.write(buffer, 0, bytesRead);
                totalBytesRead += bytesRead;
                
                // 计算并显示进度
                int progress = (int) ((totalBytesRead * 100) / fileSize);
                System.out.printf("%s文件已复制%d%%\n", sourceFile.getName(), progress);
            }
            
            System.out.printf("%s文件复制完成！\n", sourceFile.getName());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

