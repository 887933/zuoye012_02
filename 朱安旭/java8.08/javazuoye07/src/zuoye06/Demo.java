package zuoye06;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/*使用多线程实现多个文件同步复制功能，并在控制台显示复制的进度，进度以百分比表示。例如：把文件A复制到E盘某文件夹下，在控制台上显示“XXX文件已复制10%”，“XXX文件已复制20%”……“XXX文件已复制100%”，“XXX复制完成！”
*/
public class Demo {
	public static void main(String[] args) {
		File sourceDir =new File("E:\\copy");
		File destinationDir=new File("E:\\newcopy");
		if(!sourceDir.exists() || !sourceDir.isDirectory()) {
            System.err.println("源目录不存在或不是一个目录");
            return;
        }
        // 创建目标目录
        if (!destinationDir.exists()) {
            destinationDir.mkdirs();
        }
        File[] files=sourceDir.listFiles();
        List<FileCopy> tasks = new ArrayList<>();
        for (File file : files) {
            File destFile = new File(destinationDir, file.getName());
            tasks.add(new FileCopy(file, destFile));
        }
        for(FileCopy t: tasks)
        {
        	Thread thread = new Thread(t);
        	thread.start();
        }
	}
}
