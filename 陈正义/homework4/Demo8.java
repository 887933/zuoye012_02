package homework4;

import java.util.ArrayList;
import java.util.Scanner;

public class Demo8 {
    public static void main(String[] args) {
        System.out.println("请输入一个字符串：");
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        longest(str);
        sc.close();
    }

    private static void longest(String str) {
        int max = 0;
        ArrayList<Character> list = new ArrayList<>();
        ArrayList<Character> list2 = new ArrayList<>();

        for (int i = 0; i < str.length(); i++) {
            list.clear(); 
            for (int j = 0; j <= i; j++) { 
                if (!list.contains(str.charAt(j))) {
                    list.add(str.charAt(j));
                    if (j == i) { 
                        int longest = list.size();
                        if (longest > max) {
                            max = longest;
                            list2.clear();
                            list2.addAll(list); 
                        }
                    }
                }
            }
        }

        for (int m = 0; m < list2.size(); m++) {
            System.out.print(list2.get(m));
        }
        System.out.println(" 最长不含重复字符的子串长度为: " + max);
    }
}