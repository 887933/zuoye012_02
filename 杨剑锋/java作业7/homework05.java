package hom05;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;

/*作业1：  拷贝一个文件夹，考虑子文件
File src=new File(“d:\\aaa\\src”);
File desc=new FILE(“aaa\\dess”）；*/

public class homework05 {
	public static void main(String[] args) {
		File src=new File("d:\\aaa\\src");
		File dest=new File("d:\\aaa\\dess");
		dest.mkdir();
		copyFolder(src, dest);
	}

	private static void copyFolder(File src, File dest) {
		if(src.isDirectory()){
			String[] files=src.list();
			if(!dest.exists()){
				dest.mkdir();
			}
			for(String file:files){
				File srcFile=new File(src,file);
				File dastFile=new File(dest,file);
				copyFolder(srcFile, dastFile);
			}
		}else{
			copyFile(src, dest);
		}
	}

	private static void copyFile(File src, File dest) {
		try {
			FileInputStream reader = new FileInputStream(src);
			FileOutputStream writer = new FileOutputStream(dest);
			byte[] b=new byte[1024];
			int ch=0;
			while((ch=reader.read(b))!=-1){
				writer.write(b,0,ch);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
