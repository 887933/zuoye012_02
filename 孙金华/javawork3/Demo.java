package homework3;

import java.util.ArrayList;

public class Demo {
	public static void main(String[] args) {
		Manager m=new Manager("群主", 200);
		Member m1=new Member("甲", 0);
		Member m2=new Member("乙", 0);
		Member m3=new Member("丙", 0);
		Member m4=new Member("张三",0);
		Member m5=new Member("李四",0);
		m.show();
		m1.show();
		m2.show();
		m3.show();
		m4.show();
		m5.show();
		ArrayList<Integer> send = m.send(50, 5);
		m1.receive(send);
		m2.receive(send);
		m3.receive(send);
		m4.receive(send);
		m5.receive(send);
		m.show();
		m1.show();
		m2.show();
		m3.show();
		m4.show();
		m5.show();
	}
}
