package zuoye726;

import java.io.File;
import java.io.IOException;

//在当前模块下创建文件夹abc，再在 abc文件中创建文件a.txt
public class Demo1 {
	public static void main(String[] args) throws IOException {
		File file1=new File("abc\\");
		boolean mkdir = file1.mkdir();
		System.out.println(mkdir);
		File file2=new File("abc\\a.txt");
		boolean createNewFile = file2.createNewFile();
		System.out.println(createNewFile);
	}

}
