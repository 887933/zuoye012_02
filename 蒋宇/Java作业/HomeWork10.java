package demo;

public class HomeWork10 {
	/*
	 * 作业1：
	 *  你妈妈每天给你2.5元，你都会存起来，但是当这一天是
	 *  存前的第五天或者是5的倍数的时候，你就会去花6元钱，
	 *  多久你能存到100元
	 */

	public static void main(String[] args){
		double money=0;
		int count=0;
		while(money<100){
			money+=2.5;
			count++;
			if(count%5==0){
				money-=6;
			}
		}
		System.out.println(count);
	}
	
}
