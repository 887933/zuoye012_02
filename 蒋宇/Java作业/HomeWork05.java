package demo;

public class HomeWork05 {
	public static void main(String[] args){
//		作业2：高度8000m，有一张足够大的纸
//		，厚度为0.001m，
//		请问，折叠多少次，不低于8000m?
		double hight=8000;
		double num=0.001;
		int count=0;
		while(num<=8000){
			count++;
			num*=2;
		}
		System.out.println(count+"次");
	}

}
