package homework7;

import java.io.File;
import java.io.IOException;
//在当前模块下创建文件夹abc，再在 abc文件中创建文件a.txt
public class CreateFile {
	public static void main(String[] args) {
        File folder = new File("abc");
        if (!folder.exists()) {
            boolean isCreated = folder.mkdir();
            if (isCreated) {
                System.out.println("文件夹创建成功！");
            } else {
                System.out.println("文件夹创建失败！");
            }
        } else {
            System.out.println("文件夹已存在！");
        }     
        File file = new File(folder, "a.txt");
        try {
            if (!file.exists()) {
                boolean isCreated = file.createNewFile();
                if (isCreated) {
                    System.out.println("文件创建成功！");
                } else {
                    System.out.println("文件创建失败！");
                }
            } else {
                System.out.println("文件已存在！");
            }
        } catch (IOException e) {
            System.out.println("创建文件时发生错误：" + e.getMessage());
        }
  }
}
