package demo03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Player {
	String name;
	ArrayList<String> poker=new ArrayList<String>();
	public Player(String name, ArrayList<String> poker) {
		super();
		this.name = name;
		this.poker = poker;
	}
	public Player() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getPoker() {
		return poker;
	}
	public void setPoker(ArrayList<String> poker) {
		this.poker = poker;
	}
	public void sortPoker() {
		// TODO Auto-generated method stub
		Collections.sort(poker, new Comparator<String>() {

			@Override
			public int compare(String poker1, String poker2 ) {
				// TODO Auto-generated method stub
				return compareCards(poker1, poker2);
			}
		});
	}
			private int compareCards(String poker1, String poker2) {
				// TODO Auto-generated method stub
				 String rankOrder = "����С��2AKQJ109876543";
				 char rank1=poker1.charAt(1);
				 char rank2=poker2.charAt(1);
				 return rankOrder.indexOf(rank1) - rankOrder.indexOf(rank2);
			}
			
}
