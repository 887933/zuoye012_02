package homework;

import java.util.ArrayList;
import java.util.Random;

public class No_2 extends Thread{
    ArrayList<Integer> a;
	
	public No_2(ArrayList<Integer> a) {
		super();
		this.a = a;
	}

	@Override
	public void run() {
		ArrayList<Integer> b=new ArrayList<>();
		int max=0;
		int sum=0;
		while(true){
			synchronized (No_2.class) {
				
				if(a.size()==0){
					System.out.println(getName()+"总共产生了"+b.size()+"个大奖，最高金额为"+max+"元，总计金额为"+sum+"元");
					break;
				}else{
					int nextInt = new Random().nextInt(a.size());
					if(a.get(nextInt)>max){
						max=a.get(nextInt);
					}
					sum+=a.get(nextInt);
					System.out.println(getName()+"产生了一个"+a.get(nextInt)+"元大奖");
					b.add(a.get(nextInt));
					a.remove(nextInt);
					
				}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
