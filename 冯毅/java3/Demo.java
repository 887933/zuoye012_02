package java04;

import java.util.ArrayList;

public class Demo {
	public static void main(String[] args) {
		Manager m=new Manager("王思聪", 200);
		Member m1=new Member("老王1", 0);
		Member m2=new Member("老王2", 0);
		Member m3=new Member("老王3", 0);
		m.show();
		m1.show();
		m2.show();
		m3.show();
		ArrayList<Integer> send = m.send(100, 3);
		m1.receive(send);
		m2.receive(send);
		m3.receive(send);
		m.show();
		m1.show();
		m2.show();
		m3.show();
	}
}
