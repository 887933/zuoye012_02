package zuoye724;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class StreamDemo7 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入第一个字符串:");
		String str1=sc.nextLine();
		System.out.println("请输入第二个字符串:");
		String str2=sc.nextLine();
		String regex = "[0-9a-zA-Z]+";
		boolean matches1=str1.matches(regex);
    	boolean matches2=str2.matches(regex);
    	ArrayList<Character> list=new ArrayList<>();
		HashMap<Character,Integer> map=new HashMap<>();
    	boolean isCover=true;
    	if(matches1==false&&matches2==false){
    		System.out.println("输入的字符串不符合规则!");
    		return;
    	}
		for(char ch1:str1.toCharArray()){
			list.add(ch1);
		}
		list.stream().filter(t->!map.containsKey(t))
		.forEach(t->map.put(t, 0));	
		list.stream().filter(t->map.containsKey(t))
		 .forEach(t->map.put(t, map.get(t)+1));
		Set<Character> keySet = map.keySet();
		for(char ch2:str2.toCharArray()){		
			 keySet.stream().filter(t->t==ch2).forEach(t->map.put(t, map.get(t)-1));
			}
			List<Character> collect = keySet.stream().filter(t->map.get(t)<0).collect(Collectors.toList());	
			
			if(!collect.isEmpty()){
				System.out.println("NO");
	}else{
		int sum = map.keySet().stream().mapToInt(t->map.get(t)).sum();
		System.out.println("Yes "+sum);
	}
	}
	}

