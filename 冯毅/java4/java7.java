package java05;

import java.util.ArrayList;
import java.util.Collections;

public class java7 {
    public static void main(String[] args) {
		int i=12;
		System.out.println(intToBinary(i));
	}
    public static ArrayList intToBinary(int i){
    	ArrayList<Integer> a=new ArrayList<Integer>();
    	int y=i%2;
    	a.add(y);
    	int b=i/2;
    	while(b!=0){
    		y=b%2;
    		a.add(y);
    		b/=2;
    	}
    	Collections.reverse(a);
    	return a;
    }
}
