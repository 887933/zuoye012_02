package demo;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;


public class HomeWork14 {//修改callname为arraylist
	public static void main(String[] args){
        ArrayList<Student> students=new ArrayList<Student>();
		addStudentName(students,3);
		printStudentsName(students);
		String randName=randStudentName(students);
		System.out.println("幸运儿"+randName);
	}
      private static void printStudentsName(ArrayList<Student> students) {
    	  for(int i=0;i<students.size();i++){
  			System.out.println("第"+(i+1)+"个学生名称："+students.get(i).name);
  		}
		
	}
	public static void addStudentName(ArrayList<Student> students,int x){//加入一个参数表示要添加多少个学生
    	  Scanner sc=new Scanner(System.in);
    	  
  		for(int i=0;i<x;i++){
  			Student s=new Student();
  			System.out.println("存储第"+(i+1)+"个");
  			s.id=i+1;
  			s.name=sc.next();
  			s.age=sc.nextInt();
  			students.add(s);
  		}
	}
	public static String randStudentName(ArrayList<Student> students){
		int index=new Random().nextInt(students.size());
		return students.get(index).name;
	}

}
class Student {
	int id;
	String name;
	int age;

}

