package demo;

import java.io.File;
import java.util.ArrayList;
import java.util.Scanner;

import cn.hutool.core.io.FileUtil;

public class No_50 {
	public static void main(String[] args) {
		ArrayList<Student> arr=new ArrayList<>();
		File file=new File("stud.txt");
		
		Scanner sc=new Scanner(System.in);
		for(int i=1;i<6;i++){
			System.out.println("请输入第"+i+"个学生的姓名，学生号，语文成绩，数学成绩，英语成绩");
			Student s=new Student(sc.next(), sc.nextInt(),sc.nextInt(),sc.nextInt(),sc.nextInt());
			arr.add(s);
		}
		for(int i=0;i<5;i++){
			double avg=(double)(arr.get(i).getScore1()+arr.get(i).getScore2()+arr.get(i).getScore3())/3;
			arr.get(i).setAvg(avg);
		}
		FileUtil.writeLines(arr, file,"GBK");
		
	}

}
/*
 * 请输入第1个学生的姓名，学生号，语文成绩，数学成绩，英语成绩
jy
15
92
99
89
请输入第2个学生的姓名，学生号，语文成绩，数学成绩，英语成绩
lhy
16
93
100
85
请输入第3个学生的姓名，学生号，语文成绩，数学成绩，英语成绩
yh
17
94
100
94
请输入第4个学生的姓名，学生号，语文成绩，数学成绩，英语成绩
wct
18
89
95
88
请输入第5个学生的姓名，学生号，语文成绩，数学成绩，英语成绩
fjh
19
95
100
93
 * */
