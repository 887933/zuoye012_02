package javazuoye02;

import java.util.HashMap;
import java.util.Scanner;

public class zuoye08 {
	public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入一串字符串");
        String str = sc.nextLine();
        
        HashMap<Character, Integer> map = new HashMap<>();
        int maxlength = 0;
        int linshi = 0;
        
        for (int i = 0; i < str.length(); i++) {
            char temp = str.charAt(i);
            if (map.containsKey(temp)) {
                linshi = Math.max(linshi, map.get(temp) + 1);
            }
            map.put(temp, i);
            maxlength = Math.max(maxlength, i - linshi + 1);
        }
        
        System.out.println("最长不含重复字符子串的长度是：" + maxlength);
    }
}
