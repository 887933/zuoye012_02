package zuoye11;

import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class zuoye02 {
    public static void main(String[] args) throws InterruptedException {
      Object lock = new Object();
        final int[] turn = {0};
      ArrayList<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(6);
        list.add(500);
        list.add(2);
        list.add(80);
        list.add(300);
        list.add(20);
        list.add(50);
        list.add(200);
        list.add(800);
AtomicInteger sum1 = new AtomicInteger();
      Thread t1 = new Thread(()->{
          int sum = 0;
          int max=0;
          int n=0;
          synchronized (lock) {
              Random rc = new Random();
              int i = 0;
              if(turn[0] ==0) {
                  while (true) {

                      if (list.size() >=1) {

                          i = rc.nextInt(list.size());
                          if (list.get(i) != null) {
                              n++;
                              System.out.println(Thread.currentThread().getName() + " 产生了一个" + list.get(i) + "元大奖");
                              if (list.get(i) > max) {
                                  max = list.get(i);
                              }
                              sum += list.get(i);
                              sum1.set(sum);

                              list.remove(i);
                              turn[0] = 1;
                              lock.notifyAll();
                              try {
                                  lock.wait();
                              } catch (InterruptedException e) {
                                  throw new RuntimeException(e);
                              }
                          }
                      }else {
                              System.out.println(Thread.currentThread().getName() + "总共获得了" + n + "个大奖,最高金额为" + max + "总计为" + sum + "元");
                              turn[0] = 0;
                              break;
                          }
                  }
             lock.notifyAll(); }
          }
      });
      t1.setName("抽奖箱1");
      t1.start();
        Thread t2 = new Thread(()->{
            int n=0;
            int sum=0;
            int max=0;
            synchronized (lock) {
                int i = 0;
                if (turn[0] == 1) {
                    while (true) {
                        Random rc = new Random();
                        if (list.size() >= 1) {
                            i = rc.nextInt(list.size());
                            if (list.get(i) != null) {
                                n++;
                                System.out.println(Thread.currentThread().getName() + " 产生了一个" + list.get(i) + "元大奖");
                                if (list.get(i) > max) {
                                    max = list.get(i);
                                }
                                sum += list.get(i);
                                list.remove(i);
                                turn[0] = 0;
                                lock.notifyAll();
                                try {
                                    lock.wait();
                                } catch (InterruptedException e) {
                                    throw new RuntimeException(e);
                                }
                            }
                            } else {
                                System.out.println(Thread.currentThread().getName() + "总共获得了" + n + "个大奖,最高金额为" + max + "总计为" + sum + "元");
                                turn[0] = 1;
                                break;
                            }

                    }
                lock.notifyAll();}
            }
        });
        t2.setName("抽奖箱2");
        t2.start();
    }
}
