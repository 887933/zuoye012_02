package java06;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Map.Entry;
import java.util.Set;

public class java6 {
	public static void main(String[] args) {
		Integer[] n1={1,5,6,7,2,9,3};
		Integer[] n2={1,2,3,4,5};
		HashMap<Integer[], Integer[]> nn=new HashMap<Integer[], Integer[]>();
		nn.put(n1, n2);
		Set<Entry<Integer[], Integer[]>> entrySet = nn.entrySet();
		ArrayList<Integer> k=new ArrayList<Integer>();
		ArrayList<Integer> v=new ArrayList<Integer>();
		for(Entry<Integer[], Integer[]> e:entrySet){
			Integer[] kk=e.getKey();
			for(Integer i:kk){
				k.add(i);
			}
			Integer[] vv=e.getValue();
			for(Integer i:vv){
				v.add(i);
			}
		}
		ArrayList<Integer> sa=new ArrayList<Integer>();
		for(Integer i:v){
			if(k.contains(i)){
				k.remove(i);
				sa.add(i);
			}
		}
		for(Integer i:sa){
			v.remove(i);
		}
		k.addAll(v);
		k.sort(null);
		System.out.println(k);
	}
}
