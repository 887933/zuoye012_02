package homework12;

public class procedure08 extends Thread{
	private int key;

	public procedure08(int key) {
		super();
		this.key = key;
	}
	@Override
	public void run() {
		int sum=0;
		for(int i=0;i<10;i++){
			sum+=key+i;
		}
		synchronized (target08.lock) {
			target08.setCount(target08.getCount()+1);
			target08.setSum(target08.getSum()+sum);
			if(target08.getCount()==10){
				System.out.println(target08.getSum());
			}
		}
	}
}
