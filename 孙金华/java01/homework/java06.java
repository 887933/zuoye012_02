package homework;

import java.util.Scanner;

public class java06 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("输入一个数字");
		int a=sc.nextInt();
		for(int i=1;i<=(a/2)+1;i++) {
			if(i*i==a) {
				System.out.println(a+"的平方根是"+i);
				break;
			}
			if(i*i>a) {
				System.out.println(a+"的平方根在"+(i-1)+"和"+i+"之间");
				break;
			}
		}
	}
}
