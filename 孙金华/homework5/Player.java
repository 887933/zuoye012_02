package homework5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;

public class Player {
	private String name;
	private ArrayList<Integer> poker=new ArrayList<Integer>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Integer> getPoker() {
		Collections.sort(poker);
		return poker;
	}
	public void setPoker(ArrayList<Integer> poker) {
		this.poker = poker;
	}
	public Player() {
		super();
	}
	public Player(String name, ArrayList<Integer> poker) {
		super();
		this.name = name;
		this.poker = poker;
	}
	@Override
	public int hashCode() {
		return Objects.hash(name, poker);
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		return Objects.equals(name, other.name) && Objects.equals(poker, other.poker);
	}
}
