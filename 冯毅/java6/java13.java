package java07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.TreeSet;

public class java13 {
	public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		Collections.addAll(a, 1,5,1,6,4,9,5,5,3,6,4);
		ListToTreeSetFunction f=java13::kickSameNum;
		TreeSet<Integer> apply = f.apply(a);
		System.out.println(apply);
	}
	public static TreeSet<Integer> kickSameNum(ArrayList<Integer> a){
		TreeSet<Integer> ti=new TreeSet<Integer>();
		for(Integer i:a){
			ti.add(i);
		}
		return ti;
	}
	interface ListToTreeSetFunction {
		TreeSet<Integer> apply(ArrayList<Integer> arr);
	}
}
