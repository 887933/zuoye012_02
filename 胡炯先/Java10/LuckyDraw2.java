package java10;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class LuckyDraw2 implements Runnable{
	private static int[] arr={10,6,20,50,100,200,500,800,2,80,300,700};
	private static ArrayList<Integer> list;  
	static {  
		list = new ArrayList<>(); 
		for (int i : arr) {  
			list.add(i);  
		}  
	}  
	public LuckyDraw2(){

	}
	public LuckyDraw2(ArrayList<Integer> list) {
		super();
		this.list = list;
	}

	public ArrayList<Integer> getList() {
		return list;
	}

	public int choujiang(){
		Random rd=new Random();
		int index=rd.nextInt(list.size());
		int prize=this.list.get(index);
		this.list.remove(index);
		return prize;
	}

	private  int max = 0;  
    private  int total = 0;  
    private  int count = 0;
    private  String choujiangxiang =" ";
	@Override
	public void run() {
		while(true){
			synchronized (LuckyDraw2.class) {
				if(list.size()!=0){
					count++;
					int prize=choujiang();
					this.max=this.max>=prize?this.max:prize;
					total+=prize;
					if(prize==800){
						choujiangxiang =Thread.currentThread().getName();
					}
					System.out.println
					(Thread.currentThread().getName()
							+"抽到了"+prize+"元");
					try {
						Thread.sleep(100);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else{
					System.out.println
					(Thread.currentThread().getName()+
							"抽到了"+count+"个大奖，最高金额为："+
							max+"元，总计金额为："+total+"元");
					if(Thread.currentThread().getName().equals(choujiangxiang)){
						System.out.println("在此抽奖过程中，"+choujiangxiang+"产生最大奖项为800元.");
					}
					break;
				}
			}
		}
	}
	public int getMax() {
		return max;
	}
	public void setMax(int max) {
		this.max = max;
	}
}

