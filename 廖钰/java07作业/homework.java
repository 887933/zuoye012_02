package homework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;
import java.util.function.BiConsumer;

public class homework {
	public static void main(String[] args) throws IOException {
		/*
		 * 键盘录入学生姓名与年龄，年龄在18-25岁，超出这个范围则抛出一场，需要重新录入，一直到录入正确为止。
		 * 比如：年龄超出范围，如果年龄录入为abc，也要抛出异常
		 * 
		 */
//		ArrayList<Student> arr = new ArrayList<Student>();
//		CallName(arr);
		
		/*
		 * 作业1：
		 * 在当前模块下创建abc文件夹，在abc文件夹中创建a.txt文件
		 * 
		 */
//		createArchive("abc/bbb/cde/3.txt");
		
		/*
		 * 作业2：
		 * 删除一个多级文件夹
		 * 
		 */
	
		//创建abc/bbb/ccc
//		File abc = new File("abc/bbb/cde");
//		abc.mkdirs();
		
		//删除abc下所有文件和文件夹
//		File root = new File("abc");
//		deleteDirs(root);
		
		/*
		 * 作业2：
		 * 文本文件中有2-1-9-4-7-8
		 * 读取排序后写回去
		 * 
		 */
//		sortFileContent();
		
		
		/*
		 * 作业3：
		 * 生成50个男生的名字，50个女生的名字
		 * 其中男生年龄应该在20-30
		 * 女生年龄应该在18-25
		 * 
		 * 作业3：
		 * 统计任意一个文件夹中，包括子文件夹，每种文件的个数并打印
		 * 打印格式如下：
		 * txt：50个
		 * Doc：20个
		 * Java：50个
		 * class：10个
		 * png：10个
		 * ……
		 * 
		 */
//		HashMap<String, Integer> map = new HashMap<String, Integer>();
//		File dir = new File("D://dvlp");
//		countFiles(map,dir);
//		map.forEach((s,i)->System.out.println(s+"："+i+"个"));
		
		/*
		 * 作业1：  拷贝一个文件夹，考虑子文件
		 * File src=new File(“d:\\aaa\\src”);
		 * File desc=new FILE(“aaa\\dess”）；
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
//		File src=new File("abc\\bbb");
//		File dest=new File("abc\\ddd");
//		
//		copy(src,dest);
		
	}

	private static void CallName(ArrayList<Student> list) {
		addStudent(list);
		printStudent(list);
		randomStudentName(list);
	}
	
	private static void randomStudentName(ArrayList<Student> list) {
		int index = new Random().nextInt(list.size());
		System.out.println("幸运儿是:"+list.get(index).getName());
	}

	private static void printStudent(ArrayList<Student> list) {
		for(int i=0;i<list.size();i++) {
			System.out.println("第"+(i+1)+"个学生学号是"+list.get(i).getId()+"，姓名是"+list.get(i).getName()+"，年龄是"+list.get(i).getAge());
		}
	}


	private static void addStudent(ArrayList<Student> list) {
		Scanner sc = new Scanner(System.in);
		for(int i=1;i<=3;i++) {	
			Student s = new Student();
			s.setId(i);
			System.out.println("请输入学生姓名：");
			s.setName(sc.next());
			System.out.println("请输入学生年龄：");
			try {
				String age = sc.next();
				for(char c:age.toCharArray()) {
					if(c<'0'||c>'9') {
						throw new OutOfStudentAgeException("请输入数字！");
					}
				}
				if(Integer.parseInt(age)>25||Integer.parseInt(age)<18) {
					throw new OutOfStudentAgeException("超出18岁到25岁的范围！");
				}
				s.setAge(Integer.parseInt(age));
			} catch (OutOfStudentAgeException e) {
				e.printStackTrace();
				System.out.println("重新输入该学生信息");
				i--;
				continue;
			}
			list.add(s);
		}
	}
	
	private static void copy(File src, File dest) throws IOException {
		File[] listFiles = src.listFiles();
		for(File f:listFiles) {
			
			if(f.isDirectory()) {
				String[] strings = f.toString().split("\\\\");
				File dir = new File(dest.toString()+"\\"+strings[strings.length-1]);
				dir.mkdir();
				copy(f,dir);
			}else if(f.isFile()) {
				String file = f.toString();
				String[] strings = file.split("\\\\");
				FileInputStream fis = new FileInputStream(f);
				FileOutputStream fos = new FileOutputStream(dest.toString()+"\\"+strings[strings.length-1]);
				byte[] buf = new byte[32];
				int len=0;
				String content = "";
				while((len=fis.read(buf))!=-1) {
					fos.write(buf,0,len);
				}
				fos.close();
				fis.close();
			}
		}
		
	}

	private static void countFiles(HashMap<String, Integer> map, File dir) {
		File[] files = dir.listFiles();
		if(files!=null) {
			for(File f:files) {
				if(f.isFile()) {
					String[] strings = f.toString().split("\\.");
					String type = strings[strings.length-1];
					if(type.contains("\\")) {
						type = "NoTypeFile";
					}
					if(map.get(type)==null) {
						map.put(type, 1);
					}else {
						int count = map.get(type);
						map.put(type, count+1);
					}
				}else if(f.isDirectory()) {
					countFiles(map, f);
				}
			}
		}else if(dir.isFile()){
			String[] strings = dir.toString().split("\\.");
			String type = strings[strings.length-1];
			if(map.get(type)==null) {
				map.put(type, 1);
			}else {
				int count = map.get(type);
				map.put(type, count+1);
			}
		}
	}	

	private static void sortFileContent() throws FileNotFoundException, IOException {
		File file = new File("cn.txt");
		FileInputStream fis = new FileInputStream(file);
		byte[] buf = new byte[1024];
		int len = 0;
		String string = "";
		while((len = fis.read(buf))!=-1) {
			string += new String(buf,0,len);
		}
		
		String[] strings = string.split("-");
		ArrayList<Integer> list = new ArrayList<Integer>();
		for(String s:strings) {
			list.add(Integer.parseInt(s));
		}
		list.sort((o1,o2)->o1-o2);
		string = "";
		int j = 0;
		for(Integer i:list) {
			string += i;
			j++;
			if(j!=list.size()) {
				string+="-";
			}
		}
		
		FileOutputStream fos = new FileOutputStream(file);
		char[] chars = string.toCharArray();
		for(char c:chars) {
			fos.write((int)c);
		}
		fos.close();
		fis.close();
	}

	private static void deleteDirs(File src) {
		File dir = src;
		File[] listFiles = dir.listFiles();
		if(listFiles!=null) {
			for(File file:listFiles) {
				deleteDirs(file);
			}
		}
		dir.delete();	
	}

	private static void createArchive(String string) throws IOException {
		String[] strings = string.split("/");
		File dir = new File(strings[0]);
		File file = new File(string);
		dir.mkdir();
		file.createNewFile();
	}
}
