package homework6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class java2 {
    public static void main(String[] args) {
        List<String> list = new ArrayList<>();
        Collections.addAll(list,"张无忌-男-15","周芷若-女-14", 
        "赵敏-女-13","张强-男-20","张三丰-男-100","张翠山-男-40",
        "张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
        List<String> males=male(list);
        List<String> females=female(list);
        List<String> combined = new ArrayList<>();
        combined.addAll(males);
        combined.addAll(females);
        System.out.println("过滤后的演员名单：" + combined);
    }

    private static List<String> male(List<String> list) {
        List<String> male=list.stream()
        .filter(s->s.split("-")[1].equals("男")&&s.split("-")[0].length()==3)
        .limit(3).collect(Collectors.toList());
        return male;
    }

    private static List<String> female(List<String> list) {
        List<String> female=list.stream()
        .filter(s->s.split("-")[1].equals("女")&&s.split("-")[0].startsWith("林"))
        .skip(1).collect(Collectors.toList());
        return female;
}
}