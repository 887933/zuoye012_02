package java08;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

public class java06 {
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("number.txt");
		ArrayList<Integer> nums = fileToArrays(fis);		
		String news="";
		for(int i=0;i<nums.size();i++){
			if(i<nums.size()-1){
				news+=String.valueOf(nums.get(i))+"-";
			}
			else{
				news+=String.valueOf(nums.get(i));
			}
		}
		FileOutputStream fos=new FileOutputStream("number.txt");
		byte[] b=news.getBytes();
		fos.write(b);
		fos.close();
		System.out.println("Success!");
	}
	public static ArrayList<Integer> fileToArrays(FileInputStream fis) throws IOException{
		byte[] b=new byte[1024];
		int len=0;
		String s=null;
		while((len=fis.read(b))!=-1){
		    s=new String(b,0,len);
		}
		fis.close();
		String[] sp = s.split("-");
		ArrayList<Integer> num=new ArrayList<>();
		for(String ss:sp){
			num.add(Integer.parseInt(ss));
		}
		num.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o1-o2;
			}
		});
		return num;
	}
}
