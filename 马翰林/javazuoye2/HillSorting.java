package javazuoye2;

import java.util.Arrays;

import static javazuoye2.InsertionSort.sort;

public class HillSorting {
 public static void main(String[] args) {
    int[] a={9,5,64,3,8,7,98,22,66,44,13};
   HillSort(a);
   System.out.println(Arrays.toString(a));
 }
public static void HillSort(int[] a) {
     int n=0;
     for(int group=a.length/2;group>0;group/=2){
         n++;
         System.out.println("第"+n+"组");
        for(int i=0;i<group;i++){
            for(int j=i+group;j<a.length;j+=group){
                int x=0;
                int temp=a[j];
              for( x=j-group;x>=0&&a[x]>temp;x-=group){
                  a[x+group]=a[x];
              }
          a[x+group]=temp;
            }
        }
         System.out.println(Arrays.toString(a));
     }

}
}
/*
* 先取数组的一半作为增量，作为第一组，然后每组增量除以取半，遍历数组取a[i],通过增量获得每组的所有值再通过上一道题的插入排序对每组进行排序
* 重复上述步骤（增量减半）
* */