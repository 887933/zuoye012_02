package zuoye05;
/*编写10个线程，第一个线程从1加到10，第二个线程从11加20…第十个线程从91加到100，最后再把10个线程结果相加。
*/
public class Demo {
	public static void main(String[] args) {
		Thread[] threads = new Thread[10];
		int[] results=new int[10];
		for(int i=0;i<10;i++){
			int index=i;
			threads[i]=new Thread(()->{
				int start=index*10+1;
				int end=start+9;
				int sum=0;
				for(int j=start;j<=end;j++)
				{
					sum +=j;
				}
				results[index]=sum;
			});
			threads[i].start();
		}
		 try {
	            for (int i = 0; i < 10; i++) {
	                threads[i].join();
	            }
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
		int totalsum=0;
		for(int result:results)
		{
			totalsum +=result;
			System.out.println(totalsum);
		}
		System.out.println("十个线程的结果相加为："+totalsum);
	}
}
