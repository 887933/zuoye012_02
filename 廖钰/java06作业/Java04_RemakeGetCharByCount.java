package homework;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class Java04_RemakeGetCharByCount {
	public static void main(String[] args) {
		 /*  3.   
		 *  "dasdsawqeqw"  1 
		 *   输出:
		 *   e 
		 *   "dasdsawqeqw"  2
		 *   输出:
		 *   d
		 *   a
		 *   s
		 *   w
		 *   q
		 */
		
		HashMap<Character,Integer> map = new HashMap<Character,Integer>();
		
		char[] input = "dshfadfnykabssahd".toCharArray();
		int count = 3;
		
		for(char c:input) {
			if(map.get(c)==null) {
				map.put(c, 1);
			}else if(map.get(c)!=null) {
				map.put(c, map.get(c)+1);
			}
		}
		System.out.println(map);
		map.keySet().stream().filter(s->map.get(s)==count).forEach(System.out::println);
	}
}
