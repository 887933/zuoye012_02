package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.stream.Collectors;

public class Java01_RemakeDouDiZhu {
	//作业1：斗地主优化：看牌有序

	public static void main(String[] args) {
		ArrayList<String> poker=new ArrayList<String>();
		String[] colors= {"♥","♠","♦","♣"};
		String[] numbers = {"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("a大王");
		poker.add("a小王");
		char c='a';
		for(String number:numbers) {
			c = (char)(c+1);
			for(String color:colors) {
				poker.add(c+color+number);
			}
		}
		Collections.shuffle(poker);
		Player p1 = new Player();p1.setName("Wang");
		Player p2 = new Player();p2.setName("Li");
		Player p3 = new Player();p3.setName("Liu");
		ArrayList<String> dipai = new ArrayList<String>();
		//摸牌
		for(int i=0;i<poker.size();i++) {
			String p = poker.get(i);
			if(i>=51) {
				dipai.add(p);
			}else if(i%3==0) {
				p1.getPoker().add(p);
			}else if(i%3==1) {
				p2.getPoker().add(p);
			}else if(i%3==2) {
				p3.getPoker().add(p);
			}
		}
		
		//使用hashmap键值对排序
		HashMap<String,Integer> map = new HashMap<>();
		for(int i=1;i<=numbers.length;i++) {
			map.put(numbers[i-1], i);
		}
		//看牌前排序
		Collections.sort(p1.getPoker(), Java01_RemakeDouDiZhu::DouDiZhuSort);
		Collections.sort(p2.getPoker(), Java01_RemakeDouDiZhu::DouDiZhuSort);
		Collections.sort(p3.getPoker(), Java01_RemakeDouDiZhu::DouDiZhuSort);
		Collections.sort(dipai, Java01_RemakeDouDiZhu::DouDiZhuSort);
		ArrayList<String> arr1 = p1.getPoker(),arr2 = p2.getPoker(), arr3 = p3.getPoker(), arr4 = dipai;
		p1.setPoker((ArrayList)(arr1.stream().map(s->s.substring(1)).collect(Collectors.toList())));
		p2.setPoker((ArrayList)(arr2.stream().map(s->s.substring(1)).collect(Collectors.toList())));
		p3.setPoker((ArrayList)(arr3.stream().map(s->s.substring(1)).collect(Collectors.toList())));
		dipai = (ArrayList)(arr4.stream().map(s->s.substring(1)).collect(Collectors.toList()));
		//看牌
		System.out.println(p1.getPoker());
		System.out.println(p2.getPoker());
		System.out.println(p3.getPoker());
		System.out.println("底牌："+dipai);
	}
	
	public static int DouDiZhuSort(String o1, String o2){
		if(o1.charAt(0)!=o2.charAt(0)) {
			return o1.charAt(0)-o2.charAt(0);
		}else {
			return o1.charAt(1)-o2.charAt(1);
		}
	}
}
