package zuoye719;

import java.util.ArrayList;
import java.util.Scanner;
//键盘录入1-100之间的整数，并添加到集合中，直到集合数据总和超过200为止
public class demo1 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	ArrayList<Integer> list=new ArrayList<>();
	int s;
	for(s=0;s<=200;){
		System.out.println("请输入:");
		int num=sc.nextInt();
		if(num<1||num>100){
			System.out.println("请输入1-100之间的整数!");
			break;
		}
		s+=num;
		list.add(num);	
	}
	System.out.println("总和为:"+s);
	System.out.println("集合为:"+list);
	
}
}
