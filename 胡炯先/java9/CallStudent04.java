package java9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

public class CallStudent04 {
	public static void main(String[] args) throws Exception {
		BufferedReader b=new BufferedReader(new FileReader("student.txt"));
		ArrayList<Student> student=new ArrayList<Student>();
		String line;
		while((line=b.readLine())!=null){
			String[] str=line.split("-");
			student.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
		}
		b.close();
		Random rd=new Random();
		int num=rd.nextInt(student.size());
		int count = 1;
		BufferedReader brcount=new BufferedReader(new FileReader("studentcount03.txt"));
		while((line=brcount.readLine())!=null){
			String[] str=line.split("=");
			count=Integer.parseInt(str[1]);
		}
		brcount.close();
		if(count%3!=0){
			System.out.println("第"+ count +"次打印,学生是："+student.get(num).getName());
		}
		if(count%3==0){
			if(count==3){
				BufferedWriter bwname=new BufferedWriter(new FileWriter("studentflage.txt"));
				bwname.write(student.get(count).getName());  
				bwname.close();
			}
			BufferedReader brname=new BufferedReader(new FileReader("studentflage.txt"));
			while((line=brname.readLine())!=null){
				System.out.println("第"+ count +"次打印,学生是：" +line);
			}
			brname.close();
		}
		BufferedWriter bwcount=new BufferedWriter(new FileWriter("studentcount03.txt"));
		bwcount.write("count="+ (count+1));
		bwcount.close();
	}
}