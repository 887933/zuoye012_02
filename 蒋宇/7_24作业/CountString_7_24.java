package homework;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CountString_7_24 {
    public static void main(String[] args) {
        System.out.println("输入字符串:");
        Scanner s = new Scanner(System.in);
        String str = s.next();
        System.out.println("输入出现次数:");
        int count = s.nextInt();
        printCharacters(str, count);
    }

    public static void printCharacters(String inputString, int i) {
        HashMap<Character, Integer> map = new HashMap<>();
        
        inputString.chars().mapToObj(c -> (char) c)
        		.forEach(c -> map.merge(c, 1, Integer::sum));
        
        map.entrySet().stream()
        		.filter(entry -> entry.getValue() == i)
        		.map(Map.Entry::getKey)
        		.forEach(System.out::println);
    }
}
