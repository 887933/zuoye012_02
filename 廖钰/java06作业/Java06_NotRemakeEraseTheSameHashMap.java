package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Java06_NotRemakeEraseTheSameHashMap {
	public static void main(String[] args) {
		/*
		5. hashmap去重  
		合并 输出数组两个数组中都有元素则去除
		[1,2,3]
		[3,4,5,2]
		得到[1,4,5]
		
	 */
		int[] arr1 = {1,2,3}, arr2 = {3,4,5,2,6};
		System.out.println(eraseTheSame(arr1,arr2));
	}
	
	private static Set<Integer> eraseTheSame(int[] arr1, int[] arr2) {
		Map<Integer,Integer> map = new HashMap<Integer,Integer>();
		int j=0;
		for(int i:arr1) {
			if(map.get(i)==null) {
				map.put(i, 1);
			}else {
				j = map.get(i)+1;
				map.put(i, j);
			}
		}
		for(int i:arr2) {
			if(map.get(i)==null) {
				map.put(i, 1);
			}else {
				j = map.get(i)+1;
				map.put(i, j);
			}
		}
		Set<Integer> set = map.keySet();
		ArrayList<Integer> remove = new ArrayList<>();
		for(int i:set) {
			if(map.get(i)>1) {
				remove.add(i);
			}
		}
		for(int i:remove) {
			map.remove(i);
		}
		Set<Integer> returnSet = map.keySet();
		return returnSet;
	}

}
