package homework2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

//Alt+/ 自动补全
//Ctrl+1  自动改错；自动补全变量
/*1.在排序之前选定一个间隔大小gap（一般为数组大小的一半）
 *2.然后把数组以gap个元素为一组，分为若干组。每组元素按位置一一对应
 *3.在组内从左往右进行比较，小的数放左边，使得每组中的元素按从小到大的顺序排列。
 *4.排序完成后，gap=gap/2，重复以上操作，在gap=1时，执行最后一次排序。
 */

public class HillSort {
	public static void main(String[] args) {
		ArrayList<Integer> list =new ArrayList<Integer>();
		System.out.println("请输入数组长度：");
		Scanner sc=new Scanner(System.in);
		int number =sc.nextInt();
		Random r=new Random();
		for(int i=0;i<number;i++) {
			int digit=r.nextInt(100);
			list.add(digit);
		}
		sc.close();
		System.out.println("排序前：");
		for(int i=0;i<list.size();i++) {
			System.out.print(list.get(i)+"  ");
		}
		int gap=list.size();
		while(gap>1) {
			gap=gap/2;
			for(int j=0;j<gap;j++) {
				for(int i=j+gap;i<list.size();i+=gap) {
					int t=list.get(i);
					int n=i-gap;
					while(n>=0&&list.get(n)>t) {
						list.set(n+gap, list.get(n));
						n-=gap;
					}
					list.set(n+gap, t);
				}
			}
		}
		
			System.out.println(" ");
			System.out.println("排序后：");
			for(int i=0;i<list.size();i++) {
				System.out.print(list.get(i)+"  ");
			}
		
	}
}
