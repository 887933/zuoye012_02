package homework;
/*
 * 作业1：
 *  你妈妈每天给你2.5元，你都会存起来，但是当这一天是
 *  存前的第五天或者是5的倍数的时候，你就会去花6元钱，
 *  多久你能存到100元
 */
public class While3 {
	public static void main(String args[]) {
		double sum=0;
		int days=0;
		while(sum<100) {
			days++;
			sum+=2.5;
			if(days%5==0) {
				sum-=6;
			}
		}
		System.out.println("需要"+days+"天才能存够100元");
	
	}
}
