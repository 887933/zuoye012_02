package homework04;
/*3.   
*  "dasdsawqeqw"  1 
*   输出:
*   e 
*   "dasdsawqeqw"  2
*   输出:
*   d
*   a
*   s
*   w
*   q*/


import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class gather03 {
	public static void main(String[] args) {
        String arr = "dasdsawqeqw";
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入出现次数：");
        int num = sc.nextInt();
        
        Map<Character, Long> charCountMap = arr.codePoints().mapToObj(s->(char) s)
               .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
        
        List<Character> result = charCountMap.entrySet().stream()
               .filter(entry -> entry.getValue() == num)
               .map(Map.Entry::getKey)
               .collect(Collectors.toList());
        
        result.forEach(System.out::println);
    }
}
