package java08;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class java05 {
	public static void main(String[] args) throws IOException {
		File src=new File("D:\\abc\\bca\\aaa.txt");
		File descdir=new File("abc\\bca");
		if(!descdir.exists()){
			descdir.mkdirs();
		}
		File desc=new File("abc\\bca\\copy.txt");
		FileInputStream fis=new FileInputStream(src);
		FileOutputStream fos=new FileOutputStream(desc);
		byte[] b=new byte[1024];
		int ch=0;
		while((ch=fis.read(b))!=-1){
			fos.write(b,0,ch);
		}
		fis.close();
		fos.close();
		System.out.println("Success!");
	}
}
