package homework;

import java.util.Scanner;

/*2. 定义方法复制数组当中的任意位置的内容，转化为新的数组
* 	源数组：int[] arr={1,2,3,4,5,6,7,8,9};
* 			复制2，5这个位置的数组的内容
* 			int[] arr={3,4,5};*/
public class Way2 {
	public static void main(String args[]) {
		int[] arr={1,2,3,4,5,6,7,8,9};
		copy();
	}

	private static void copy() {
		int n=-1,i=0;
		int[] arr={1,2,3,4,5,6,7,8,9};
		int[] arr2=new int[9];
		while(n!=-2) {
			Scanner sc=new Scanner(System.in);
			System.out.println("请输入第"+(i+1)+"个位置,输入-2结束输入");
			int loc=sc.nextInt();
			if(loc>8) {
				System.out.println("输入错误！");
			}
			else if(loc<=8&&loc!=-2){
			arr2[i]=arr[loc];
			i++;}
			n=loc;

			}
			for(i=0;i<arr2.length;i++) {
                if(arr2[i]==0) {
				System.out.print("");
                }
                else {

					System.out.print(arr2[i]);
                }
			}
	}

}
