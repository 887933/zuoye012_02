package demo;

public class No_8 {
	public static void main(String[] args) {
        String s = "zahdsab";
        String longestSubstring = findLongestSubstring(s);
        int length = longestSubstring.length();
        System.out.println("最长无重复字符子串的长度是: " + length);
        System.out.println("最长无重复字符子串是: " + longestSubstring);
    }

    public static String findLongestSubstring(String s) {
        if (s == null || s.length() == 0) return "";

        int maxLength = 0;
        int start = 0;
        int end = 0;
        int[] charIndex = new int[256]; 
        for (int j = 0; j < 256; j++) {
            charIndex[j] = -1; 
        }

        String longestSubstring = "";

        while (end < s.length()) {
            int index = s.charAt(end);
            if (charIndex[index] >= start) {
                start = charIndex[index] + 1;
            }
            maxLength = Math.max(maxLength, end - start + 1);
            charIndex[index] = end;

            
            if (end - start + 1 == maxLength) {
                longestSubstring = s.substring(start, end + 1);
            }

            end++;
        }

        return longestSubstring;
    }
}
