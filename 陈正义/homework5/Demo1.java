//1.创建一个arraList集合,并添加以下字符串,字符串中前面是姓名 ,后面是年龄
//	 *  "zhangsn,23"
//	 *  "lisi,24"
//	 *  "wangwu,25"
//	 *  保留年龄大于24岁的,并将结果收集到map集合中,姓名为建,年龄为值  
//
//ArrayList<String> arr=new ArrayList();
//Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");

package homework5;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class Student {
    private String name;
    private int age;

    public Student(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}

public class Demo1 {
    public static void main(String[] args) {
        ArrayList<Student> list = new ArrayList<Student>();
        Student s1 = new Student("zhangsan", 23);
        Student s2 = new Student("lisi", 24);
        Student s3 = new Student("wangwu", 25);
        list.add(s1);
        list.add(s2);
        list.add(s3);

        // 使用迭代器遍历并删除年龄小于等于24岁的学生
        Iterator<Student> iterator = list.iterator();
        while (iterator.hasNext()) {
            Student student = iterator.next();
            if (student.getAge() <= 24) {
                iterator.remove();
            }
        }

        Map<String, Integer> map = new HashMap<String, Integer>();
        for (Student student : list) {
            map.put(student.getName(), student.getAge());
        }

        System.out.println(map);
    }
}