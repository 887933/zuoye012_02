package zuoye11;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.TreeMap;

public class Filelcopy extends Thread {
  public static String source;
   public static String destination;
    public Filelcopy(String start, String end) {
        Filelcopy.source= start;
        Filelcopy.destination = end;
    }

    public void run() {
        try (FileInputStream fis = new FileInputStream(source);
             FileOutputStream fos = new FileOutputStream(destination)) {
            byte[] buffer = new byte[1024];
            int totalRead = 0;
            long fileSize = new File(source).length();
            int len;
            while ((len = fis.read(buffer)) > 0) {
                totalRead += len;
                fos.write(buffer, 0, len);
                double percent = (double) totalRead / fileSize * 100;
                int per = (int) Math.round(percent);
                if (per % 10 == 0) {
                    System.out.printf("%s: 已复制%d%%\n", getName(), per);
                }
            }
            System.out.printf("%s: 复制完成\n", getName());
        } catch (FileNotFoundException e) {
            System.err.printf("%s: 文件未找到：%s\n", getName(), e.getMessage());
        } catch (IOException e) {
            System.err.printf("%s: 复制过程中出现异常：%s\n", getName(), e.getMessage());
        }
    }
}
