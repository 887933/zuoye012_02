package dailyProgram;

import java.util.Scanner;

public class commonDivisorAndMultiple {
	public static void main(String[] args) {
		System.out.println("请输入两个整数：");
		Scanner sc= new Scanner(System.in);
		int num1 = sc.nextInt();
		int num2 = sc.nextInt();
		System.out.println("最大公因数为"+maxCommonDivisor(num1,num2));
		System.out.println("最小公倍数为"+minCommonMultiple(num1,num2));
		sc.close();
	}

	private static int minCommonMultiple(int num1, int num2) {
		int times = maxCommonDivisor(num1, num2);
		return num1*num2/times;
	}

	private static int maxCommonDivisor(int num1, int num2) {
		int a=num1>num2?num1:num2,b=num1>num2?num2:num1,divided;
		while(a%b!=0) {
			divided=a;
			a=b;
			b=divided%a;
		}
		return b;
	}
}
