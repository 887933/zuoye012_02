package zuoye724;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/*
 *  一个集合中
	 *  分别存储了6个男演员
	 *  和6名女演员.
	 *  男演员只要名字为3个子的前面三人
	 *  女演员只要姓林的,并且不要第一个
	 *  把过滤后的男演员和女演员姓名结合在一起   
		ArrayList<String> list = new ArrayList<String>();
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-男-41","林婷-女-22","林立-女-23");
 */
public class StreamDemo2 {
	public static void main(String[] args) {
	ArrayList<String> list = new ArrayList<String>();
	Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100"
	, "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-男-41","林婷-女-22","林立-女-23");
	
	List<String> collect1=list.stream().filter(t->"男".equals(t.split("-")[1])&&t.split("-")[0].length()==3)
	.limit(3).map(t->t.split("-")[0])
	.collect(Collectors.toList());
	System.out.println(collect1);
    
	List<String> collect2 = list.stream().filter(t->"女".equals(t.split("-")[1])&&t.split("-")[0].charAt(0)=='林')
			.skip(1)
			.map(t->t.split("-")[0]).collect(Collectors.toList());
	
	System.out.println(collect2);	
	ArrayList<String> newlist=new ArrayList<>(collect1);
	newlist.addAll(collect2);
	System.out.println(newlist);
	
	}

}
