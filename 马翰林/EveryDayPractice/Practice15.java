package EveryDayPractice;

import java.util.Scanner;

public class Practice15 {
     public static void main(String[] args) {
         Scanner sc = new Scanner(System.in);
         int x = sc.nextInt();
         int y = sc.nextInt();
         int z = sc.nextInt();
         int temp=0;
         if(x>y)
         {
             temp=x;
             x=y;
             y=temp;
         }
         if(x>z)
         {
             temp=z;
             z=x;
             x=temp;
         }
         if(y>z)
         {
             temp=z;
             z=y;
             y=temp;
         }
         System.out.println("排序后为"+x+y+z);
     }
}
