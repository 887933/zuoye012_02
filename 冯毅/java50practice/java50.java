package java50practice;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.stream.Stream;

public class java50 {
	public static void main(String[] args) throws Exception {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入学生学号：");
		String id=sc.next();
		System.out.print("输入学生姓名：");
		String name=sc.next();
		double count=0;
		ArrayList<Integer> c=new ArrayList<>();
		for(int i=1;i<=3;i++){
			System.out.print("输入第"+i+"门课的成绩：");
			int score=sc.nextInt();
			c.add(score);
			count+=score;
		}
		double avgsc=count/3;
		OutputStreamWriter osw=new OutputStreamWriter(new  FileOutputStream("stud"));
		osw.write("学生学号："+id);
		osw.write("\n");
		osw.write("学生姓名："+name);
		osw.write("\n");
		osw.write("学生第一门课成绩："+c.get(0));
		osw.write("\n");
		osw.write("学生第二门课成绩："+c.get(1));
		osw.write("\n");
		osw.write("学生第三门课成绩："+c.get(2));
		osw.write("\n");
		osw.write("学生平均成绩："+avgsc);
		osw.close();
		System.out.println("导出成功！");
	}
}
