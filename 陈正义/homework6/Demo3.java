//3.   
//	 *  "dasdsawqeqw"  1 
//	 *   输出:
//	 *   e 
//	 *   "dasdsawqeqw"  2
//	 *   输出:
//	 *   d
//	 *   a
//	 *   s
//	 *   w
//	 *   q

package homework6;

import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.stream.Collectors;

public class Demo3 {
	public static void main(String[] args) {
		System.out.println("请输入一个字符串：");
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		System.out.println("请输入出现次数：");
		int num=sc.nextInt();
		String[] strArray = str.split("", -1); 
        Map<String, Long> map = Arrays.stream(strArray)
        .collect(Collectors.groupingBy(s->s, Collectors.counting()));
        Set<String> set=  map.entrySet().stream().filter(s->s.getValue()==num)
        .map(s->s.getKey()).collect(Collectors.toSet());
	    System.out.println(set);
	}
	
}
