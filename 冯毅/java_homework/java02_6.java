package dame;

import java.util.Scanner;

public class java02_6 {
    public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一个数字：");
		int m=sc.nextInt();
		for(int i=1;i<m;i++){
			if(m==i*i){
				System.out.println(m+"的平方根是"+i);
				break;
			}
			else if(m<i*i){
				System.out.println(m+"的平方根在"+(i-1)+"和"+i+"之间");
				break;
			}
		}
	}
}
