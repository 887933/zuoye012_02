package java07;

public class java14 {
	public static void main(String[] args) {
		String s="YrR8RrY";
		String s1="ppRYYGrrYBR2258";
		String s2="ppRYYGrrYB225";
		StringToIntFunction f=java7::isRight;
		int a = f.apply(s, s1);
		if(a==1){
			System.out.println("yes    "+(s1.length()-s.length()));
		}else{
			System.out.println("no");
		}
		int a2 = f.apply(s, s2);
		if(a2==1){
			System.out.println("yes"+(s2.length()-s.length()));
		}else{
			System.out.println("no");
		}
	}
	interface StringToIntFunction {
	    int apply(String s, String s2);
	}
}
