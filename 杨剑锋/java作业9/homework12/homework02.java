package homework12;


/*作业2：在上面基础上，统计每个线程的最大值.
抽奖箱1：总共产生了6个大奖，最高金额为400元，总计金额为800元
抽奖箱2：总共产生了4个大奖，，最高金额为600元，总计金额为1000元
*/

public class homework02 {
	public static void main(String[] args) {
		jackpot1 t1=new jackpot1();
		jackpot1 t2=new jackpot1();
		t1.setName("抽奖箱1");
		t2.setName("抽奖箱2");
		t1.start();
		t2.start();
	}
}
