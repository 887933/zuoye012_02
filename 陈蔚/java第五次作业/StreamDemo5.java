package zuoye724;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class StreamDemo5 {
	public static void main(String[] args) {
		HashMap<Integer,Integer> map=new HashMap<>();
	    int[] arr1={1,2,3};
	    int[] arr2={3,4,5,2};
	    int[] newarr=new int[arr1.length+arr2.length];
	    System.arraycopy(arr1, 0, newarr, 0, arr1.length);
	    System.arraycopy(arr2, 0, newarr, arr1.length, arr2.length);
		ArrayList<Integer> list=new ArrayList<>();
	    for(int narr:newarr){
	    	list.add(narr);
	    }
	    
	   System.out.println(list);
       
	   list.stream().filter(t->!map.containsKey(t))
		.forEach(t->map.put(t, 0));
		
		list.stream().filter(t->map.containsKey(t))
	    .forEach(t->map.put(t, map.get(t)+1));
		
		Set<Integer> keySet = map.keySet();
		keySet.stream().filter(t->map.get(t)==1).forEach(t->System.out.print(t+" "));
		
		
		
	}

}
