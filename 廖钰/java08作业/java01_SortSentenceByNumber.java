package homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class java01_SortSentenceByNumber {
	public static void main(String[] args) throws FileNotFoundException, IOException {
		/*
		 * 作业1：根据序号排序诗句
		 * 
		 */
		
		//读取内容
		FileReader fr = new FileReader("sort.txt");
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		ArrayList<String> list= new ArrayList<String>(); 
		while((line=br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		br.close();
		
		//排序
		list = (ArrayList<String>)list.stream().sorted((o1,o2)->o1.charAt(0)-o2.charAt(0)).collect(Collectors.toList());
		
		//写回文件
		FileWriter fw = new FileWriter("sort.txt");
		BufferedWriter out = new BufferedWriter(fw);
		for(int i=0;i<list.size();i++) {
			out.write(list.get(i));
			out.newLine();
		}
		out.close();
		
	}
	
}
