package java50practice;

import java.util.Scanner;

public class java45 {
	public static void main(String[] args) {
		System.out.print("输入一个质数：");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		boolean prime = isPrime(n);
		if(prime){
			int count=0;
			while(true){
				if(n/9>0){
					count++;
					n=n/9;
				}else{
					System.out.println("最多被"+count+"个9整除");
					break;
				}
			}
		}else{
			System.out.println("该数不是质数");
		}
	}
	private static boolean isPrime(int number) {
        if (number <= 1) return false;
        if (number == 2) return true;
        if (number % 2 == 0) return false;

        for (int i = 3; i * i <= number; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
