package java9;

public class IllegalUserLoginException extends Exception {
	public IllegalUserLoginException() {  
        super();  
    }  
  
    // 带详细信息的构造函数  
    public IllegalUserLoginException(String message) {  
        super(message);  
    }  
  
    // 带详细信息和原因的构造函数  
    public IllegalUserLoginException(String message, Throwable cause) {  
        super(message, cause);  
    }  
}
