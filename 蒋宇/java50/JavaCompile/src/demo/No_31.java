package demo;

public class No_31 {
	//将一个数组逆序输出。
    //程序分析：用第一个与最后一个交换。
	public static void main(String[] args) {
		int[] a={1,3,5,6,8,9};
		for(int i=0,j=a.length-1;i<j;i++,j--){
			swap(a,i,j);
		}
		for(int i=0;i<a.length;i++){
			System.out.print(a[i]);
			System.out.print(" ");
		}
	}
	private static void swap(int[] a, int i, int j) {
		int t=a[i];
		a[i]=a[j];
		a[j]=t;
	}
	

}
