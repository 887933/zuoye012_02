package homework4;

import java.util.Scanner;

public class Demo3 {
	public static void main(String[] args) {
		System.out.println("请输入一段字符串：");
		Scanner sc=new Scanner(System.in);
		String str=sc.nextLine();
		boolean c=false;
		if(str.length()%2==0) {
			for(int i=0;i<str.length()/2;i++) {
				if(str.charAt(str.length()/2-i-1)!=str.charAt(str.length()/2+i)) {
					System.out.println("不是回文数");
					c=false;
					break;
				}
				else {
					c=true;
				}
			}
		}
		if(str.length()%2!=0) {
			for(int i=0;i<str.length()/2;i++) {
				if(str.charAt(str.length()/2-i-1)!=str.charAt(str.length()/2+i+1)) {
					System.out.println("不是回文数");
					c=false;
					break;
				}
				else {
					c=true;
				}
			}
		}
		if(c) {
			System.out.println("是回文数");
		}
	}
}
