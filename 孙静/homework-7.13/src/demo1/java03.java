package demo1;
//高度8000m，有一张足够大的纸，厚度为0.001m，请问，折叠多少次，不低于8000m?
public class java03 {
	public static void main(String[] args) {
		double thickness=0.001;
		int count = 0;
		while(thickness<=8000){
			count++;
			thickness = thickness*2;
		}
		System.out.println(count);
	}
}
