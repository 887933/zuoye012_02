package zuoye724;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class FunctionDemo1 {
	public static void main(String[] args) {
	 ArrayList<String> list=new ArrayList();
	
	 Collections.addAll(list, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		
		Map<String, Integer> collect = list.stream().filter(new FunctionDemo1()::ageFilter)
		.collect(Collectors.toMap(t->t.split(",")[0],t->Integer.parseInt(t.split(",")[1])));
		
		System.out.println(collect);
	}
   public boolean ageFilter(String t){
	return Integer.parseInt(t.split(",")[1])>24;
}
}
