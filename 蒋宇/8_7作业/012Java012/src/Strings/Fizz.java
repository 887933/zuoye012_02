package Strings;

public class Fizz extends Thread{
	int printFizz;
	
	public Fizz(int printFizz) {
		super();
		this.printFizz = printFizz;
	}

	@Override
	public void run() {
		synchronized (Demo.class) {
			if(printFizz%3==0&&printFizz%5!=0){
				System.out.println("fizz");
			}
			
		}
	}

}
