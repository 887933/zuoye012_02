package demo01;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.IntStream;

public class zuoye06 {
		public static void main(String[] args) {
			ArrayList<Integer> list=new ArrayList<Integer>();
			int[] arr1={1,2,3};
			int[] arr2={3,4,5,2};
			Arrays.stream(arr1).filter(e -> IntStream.of(arr2).noneMatch(e2 ->e==e2))
			.forEach(list::add);
			Arrays.stream(arr2).filter(e -> IntStream.of(arr1).noneMatch(e1 ->e==e1)).forEach(list::add);
			System.out.println(list);
			}
}
