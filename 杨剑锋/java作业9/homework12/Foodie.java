package homework12;

public class Foodie extends Thread{
	@Override
	public void run() {
		while(true){
			synchronized (Desk.lock) {

				if(Desk.count==0){
					break;
				}
				if(Desk.flag){
					System.out.println("消费者吃掉一个汉堡");
					Desk.flag=false;
					Desk.count--;
					Desk.lock.notify();
				}else{
					try {
						Desk.lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
