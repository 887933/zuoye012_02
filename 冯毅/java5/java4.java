package java06;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;

public class java4 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入字符串：");
		String s=sc.next();
		char[] c=s.toCharArray();
	    TreeMap<Character,Integer> lm=new TreeMap<Character,Integer>();
	    for(char cc:c){
			if(lm.containsKey(cc)){
				int value=lm.get(cc);
				lm.put(cc, value+1);
			}
			else{
				lm.put(cc, 1);
			}
	    }
	    Set<Entry<Character, Integer>> entrySet = lm.entrySet();
	    int maxcount=0;
	    ArrayList<String> s1=new ArrayList<String>();
		ArrayList<String> s2=new ArrayList<String>();
		ArrayList<String> s3=new ArrayList<String>();
		ArrayList<String> s4=new ArrayList<String>();
		ArrayList<String> s5=new ArrayList<String>();
		ArrayList<String> s6=new ArrayList<String>();
		for(Entry<Character, Integer> e : entrySet){
			
			if(maxcount<e.getValue()){
				maxcount=e.getValue();
			}
			if(e.getValue()==1){
				
				s1.add(Character.toString(e.getKey()));
			}
			if(e.getValue()==2){
				
				s2.add(Character.toString(e.getKey()));
			}
			if(e.getValue()==3){
				
				s3.add(Character.toString(e.getKey()));
			}
			if(e.getValue()==4){
				
				s4.add(Character.toString(e.getKey()));
			}
			if(e.getValue()==5){
				
				s5.add(Character.toString(e.getKey()));
			}
			if(e.getValue()==6){
				
				s6.add(Character.toString(e.getKey()));
			}
			
		}
		if(maxcount==1){
			System.out.println("出现一次的字符有："+s1);
		}
		if(maxcount==2){
			System.out.println("出现一次的字符有："+s1);
			System.out.println("出现2次的字符有："+s2);

		}
		if(maxcount==3){
			System.out.println("出现一次的字符有："+s1);
			System.out.println("出现2次的字符有："+s2);
			System.out.println("出现3次的字符有："+s3);
			
		}
		if(maxcount==4){
			System.out.println("出现一次的字符有："+s1);
			System.out.println("出现2次的字符有："+s2);
			System.out.println("出现3次的字符有："+s3);
			System.out.println("出现4次的字符有："+s4);
		}
		if(maxcount==5){
			System.out.println("出现一次的字符有："+s1);
			System.out.println("出现2次的字符有："+s2);
			System.out.println("出现3次的字符有："+s3);
			System.out.println("出现4次的字符有："+s4);
			System.out.println("出现5次的字符有："+s5);
		}
		if(maxcount==6){
			System.out.println("出现一次的字符有："+s1);
			System.out.println("出现2次的字符有："+s2);
			System.out.println("出现3次的字符有："+s3);
			System.out.println("出现4次的字符有："+s4);
			System.out.println("出现5次的字符有："+s5);
			System.out.println("出现6次的字符有："+s6);
		}
		

	}
}
