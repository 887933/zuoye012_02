package java02;

import java.util.Scanner;

public class java02_1 {
    public static void main(String[] args){
        //while作业1： 求1-100之间的奇数和，并把求和的结果在控制台打印
        //while作业2：高度8000m，有一张足够大的纸，厚度为0.001m，请问，折叠多少次，不低于8000m?
       int sum=0;
       int x=0;
       int i=1;
       while(i<=100) {
           if (i % 2 != 0) {
               sum += i;
           }
           i++;
       }
       System.out.println("奇数和:"+sum);
    //作业2
        double c=0.0;
        double h=0.001;
  while(h<8000){
    h=h*2;
    x++;
}
System.out.println("最少要:"+x+"次");
    }
}
