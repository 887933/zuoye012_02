package zuoye03;

public class FizzBuzz extends Thread{
	private int n;
    private int current = 1;
    private final Object lock = new Object();
    public FizzBuzz(int n) {
        this.n = n;    }
    public void fizz() { 
    	synchronized (lock) {
			while (current <= n) {
				if (current % 3 == 0 && current%5!=0) {
                System.out.println("fizz:"+current);
                current++;
                lock.notifyAll();
            } else {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            	 
			}		
    }
		}
    public void buzz() { 
    	synchronized (lock) {
			while (current <= n) {
				if (current % 5 == 0 && current % 3!=0) {
                System.out.println("buzz:"+current);
                current++;
                lock.notifyAll();
            } else {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            	 
			}
			 
    }
		}
    public void fizzbuzz() { 
    	synchronized (lock) {
			while (current <= n) {
			
				if (current % 3 == 0 && current % 5==0) {
                System.out.println("fizzbuzz:"+current);
                current++;
                lock.notifyAll();
            } else {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
			}
    }
		}
    public  void number() {
        synchronized (lock) {
			
    	while (current <= n) {
            if (current % 3 != 0 && current % 5 != 0) {
                System.out.println(current);
                current++;
                lock.notifyAll();
            } else {
                try {
                    lock.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        }
    }
    	
}
