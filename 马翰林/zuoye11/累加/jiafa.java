package zuoye11;

import java.util.concurrent.atomic.AtomicIntegerArray;

public class jiafa extends  Thread{
    private int start;
    private int end;
    private AtomicIntegerArray results;
    private int index;

    public jiafa(int start, int end, AtomicIntegerArray results, int index) {
        this.start = start;
        this.end = end;
        this.results = results;
        this.index = index;
    }

    @Override
    public void run() {
        int sum = 0;
        for (int i = start; i <= end; i++) {
            sum += i;
        }
        results.set(index, sum);
    }
}
