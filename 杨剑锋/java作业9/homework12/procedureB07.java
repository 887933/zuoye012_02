package homework12;

public class procedureB07 extends Thread{
	@Override
	public void run() {
		char ma='A';
		int i=0;
		while(true){
			synchronized (procedureA07.lock) {
				if(ma+i-1=='Z'){
					break;
				}
				if(procedureA07.num==2){
					System.out.print((char)(ma+i));
					i++;
					procedureA07.num=0;
					procedureA07.lock.notify();
				}else{
					try {
						procedureA07.lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
