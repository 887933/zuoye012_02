package homework8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class work2 {
	public static void main(String[] args) throws IOException{
		FileInputStream fis=new FileInputStream("user.txt");
		BufferedReader br=new BufferedReader(new InputStreamReader(fis,"utf-8"));
		ArrayList<String> arr=new ArrayList<String>();
		String line;
		while((line=br.readLine())!=null) {
			if(!line.isEmpty())
				arr.add(line);
		}
		ArrayList<String[]> result=new ArrayList<String[]>();
		for(String i:arr) {
			String[] temp=i.split("=");
			result.add(temp);
		}
		br.close();
		String name,pwd;
		Scanner sc=new Scanner(System.in);
		System.out.print("输入用户名：");
		name=sc.next();
		System.out.print("输入密码：");
		pwd=sc.next();
		sc.close();
		boolean b=false;
		for(int i=0;i<result.size();i++) {
			if(result.get(i)[0].equals(name)) {
				b=true;
				break;
			}
		}
		if(b) {
			long ban=-1;
			int count=-1;
			String password=null;
			for(int i=0;i<result.size();i++) {
				if(result.get(i)[0].equals(name)) {
					count=Integer.valueOf(result.get(i)[2]);
					password=result.get(i)[1];
					ban=Long.valueOf(result.get(i)[3]);
					break;
				}
			}
			if(pwd.equals(password)) {
				long current=System.currentTimeMillis();
				if(count==6) {
					System.out.println("该用户已被冻结");
				}
				else if(count==3&&current<ban) {
						System.out.println("当前禁止登录,请在"+(ban-current)/1000+"秒后登录");
				}
				else {
					System.out.println("登录成功，欢迎"+name);
				}
			}
			else {
				if(count<2) {
					++count;
					System.out.println("密码错误，若再输入错误"+(3-count)+"次，将禁止登录30秒");
					writeback(result,name,count,-1);
				}
				else if(count==2) {
					++count;
					System.out.println("密码输入错误达到3次，将禁止登录30秒");
					long current=System.currentTimeMillis();
					writeback(result,name,count,current+30*1000);
				}
				else if(count==3) {
					long current=System.currentTimeMillis();
					if(current<ban) {
						System.out.println("当前禁止登录");
					}
					else {
						++count;
						System.out.println("密码错误，若再输入错误"+(6-count)+"次，将冻结当前用户");
						writeback(result,name,count,-1);
					}
				}
				else if(count<5) {
					++count;
					System.out.println("密码错误，若再输入错误"+(6-count)+"次，将冻结当前用户");
					writeback(result,name,count,-1);
				}
				else if(count==5) {
					++count;
					System.out.println("密码输入错误达到6次，当前用户已被冻结");
					writeback(result,name,count,-1);
				}
			}
			
		}
		else {
			System.out.println("该用户不存在");
		}
		}
	private static void writeback(ArrayList<String[]> result,String name,int count,long ban) throws IOException {
		ArrayList<String> arr=new ArrayList<String>();
		FileOutputStream fos=new FileOutputStream("user.txt");
		BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(fos,"utf-8"));
		for(int i=0;i<result.size();i++) {
			if(result.get(i)[0].equals(name)) {
				result.get(i)[2]=String.valueOf(count);
				result.get(i)[3]=String.valueOf(ban);
				break;
			}
		}
		for(int i=0;i<result.size();i++) {
			String temp=result.get(i)[0]+"="+result.get(i)[1]+"="+result.get(i)[2]+"="+result.get(i)[3];
			arr.add(temp);
		}
		for(String i:arr) {
			bw.write(i);
			bw.newLine();
		}
		bw.close();
	}
}