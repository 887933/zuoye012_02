package Hwork;

import java.io.File;
import java.util.List;

import cn.hutool.core.io.FileUtil;

public class No_1 {
	public static void main(String[] args) throws Exception {
		
	        File file=new File("sort.txt");

	        List<String> readLines = FileUtil.readLines(file, "GBK");
	        for (int i = 0; i < readLines.size(); i++) {
	            for (int j = i + 1; j < readLines.size(); j++) { // 修改这里，条件改为 readLines.size()
	                if (Integer.parseInt(readLines.get(i).split("\\.")[0]) > Integer.parseInt(readLines.get(j).split("\\.")[0])) { // 修改这里
	                    String temp = readLines.get(i);
	                    readLines.set(i, readLines.get(j));
	                    readLines.set(j, temp);
	                }
	            }
	        }
	        FileUtil.writeLines(readLines, file, "GBK");
	    }

}
