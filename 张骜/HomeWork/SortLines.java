package HomeWork;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class SortLines {
        public static void main(String[] args) throws IOException {
            String Filepath = "src/HomeWork/sort.txt";

            List<String> lines;
            lines = Files.readAllLines(Paths.get(Filepath));
            lines.sort(String::compareTo);
            BufferedWriter writer = Files.newBufferedWriter(Paths.get("sorted_sort.txt"));
            for (String line : lines) {
                writer.write(line);
                writer.newLine();
            }
            writer.close();
        }
}
