package java50practice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.TreeSet;

public class java15 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入第一个数字：");
		int num1 = sc.nextInt();
		System.out.print("请输入第二个数字：");
		int num2 = sc.nextInt();
		System.out.print("请输入第三个数字：");
		int num3 = sc.nextInt();
		LinkedList<Integer> nums=new LinkedList<>();
		nums.add(num1);
		nums.add(num2);
		nums.add(num3);
		nums.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				
				return o1-o2;
			}
		});
		System.out.println("从小到大的顺序是："+nums.get(0)+","+nums.get(1)+","+nums.get(2));
	}
}
