package homework6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
public class Demo8 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> arr=new ArrayList<String>();
		Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		
		Map<String, Integer> map= arr.stream()
		.filter(s->Integer.parseInt(s.split(",")[1])>24)
		.collect(Collectors.toMap(s->s.split(",")[0], s->Integer.parseInt(s.split(",")[1])));
		System.out.println(map);
		List<People> collect = arr.stream().filter(s->Integer.parseInt(s.split(",")[1])>24).map(People::new).collect(Collectors.toList());
		System.out.println(collect);
	}
}


 class People{
	private String name;
	private int age;
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	@Override
	public String toString() {
		return "People [name=" + name + ", age=" + age + "]";
	}
	public People() {
	
	}
	public People(String str) {
		String[] split = str.split(",");
		this.name=split[0];
		this.age=Integer.parseInt(split[1]);
	}
}