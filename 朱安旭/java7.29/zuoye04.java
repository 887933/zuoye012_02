package demo01;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.temporal.IsoFields;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

public class zuoye04 {

	public static void main(String[] args) throws IOException {
		File f1 =new File("E:\\copy");
		boolean mkdir = f1.mkdirs();
		System.out.println(mkdir);
		File file=new File("E:\\abc");
		 try {
	            copyFile(file, f1);
	            System.out.println("文件夹复制成功！");
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
		
	}

	private static void copyFile(File file,File f1) throws IOException {
		// TODO Auto-generated method stub
		File[] listFiles=file.listFiles();
		byte[] b=new byte[1024];
		int len=0;
		if(listFiles !=null)
		{
			for(File f :listFiles)
			{
				String name=f.getName();
				if(f.isDirectory())
				{
					copyFile(f,new File(f1,f.getName()));
				}else{
					try(
					FileInputStream fis = new FileInputStream(f);
	                FileOutputStream fos = new FileOutputStream(new File(f1, f.getName()));){
					while ((len = fis.read(b)) != -1) {
						fos.write(b,0,len);
					}
					}catch (FileNotFoundException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
				}
			}
		}
	}
	}
	}
