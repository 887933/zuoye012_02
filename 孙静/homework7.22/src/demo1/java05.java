package demo1;

import java.security.KeyStore.SecretKeyEntry;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class java05 {
	

	public static void main(String[] args) {
		HashMap<Integer, Integer> hashMap = new HashMap<>();
		int[] arr1 = new int[]{2,3,4,5};
		int[] arr2 = new int[]{1,2,3};
//		System.out.println(arr1.toString());
		for(Integer a:arr1){
			if(hashMap.containsKey(a)){
				int value = hashMap.get(a);
				hashMap.put(a, value+1);
			}else{
				hashMap.put(a, 1);
			}
		}
		for(Integer a:arr2){
			if(hashMap.containsKey(a)){
				int value = hashMap.get(a);
				hashMap.put(a, value+1);
			}else{
				hashMap.put(a, 1);
			}
		}
		int count=0;

		System.out.print("[");
		Set<Entry<Integer, Integer>> entrySet = hashMap.entrySet();
		for(Entry<Integer, Integer> a:entrySet){
			if(a.getValue()==1){
				if(count==0){
					System.out.print(a.getKey());
				}else{
					System.out.print(","+a.getKey());
				}
				
				count++;
			}
		}
		System.out.print("]");
		
	}

}
