package homework;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class students {
	private int id;
	private String name;
	private int age;

	public students(int id, String name, int age) {
		this.id = id;
		this.name = name;
		this.age = age;
	}
	public students(){
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public static void printStuInfo(ArrayList<students> list) {
		if (list.isEmpty()) {
			System.out.println("没有学生信息可显示。");
			return;
		}
		Random r = new Random();
		int i = r.nextInt(list.size());
		System.out.println("抽到的是第" + (i + 1) + "个学生");  //
		System.out.println("学号:" + list.get(i).getId() + " 姓名:" + list.get(i).getName() + " 年龄:" + list.get(i).getAge());
	}

	public static void addStudent() {
		ArrayList<students> list = new ArrayList<>();
		try (Scanner sc = new Scanner(System.in)) {
			for (int i = 1; i <= 3; i++) {
				System.out.println("请输入第" + i + "个学生的学号,姓名,年龄:");
				list.add(new students(sc.nextInt(), sc.next(), sc.nextInt()));
			}
		}
		printStuInfo(list);
	}
}
