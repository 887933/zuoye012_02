package dailyProgram02;

public class ballRebounceFrom100 {
	public static void main(String[] args) {
		int height = 100, count = 10;
		System.out.println("球从"+height+"米落下，反弹"+count+"次后的弹起的高度为"+ballRebounce(height,count)+"米");
	}

	private static double ballRebounce(int height, int count) {
		double rebounceHeight = height;
		for(int i=0;i<count;i++) {
			rebounceHeight /= 2;
		}
		return rebounceHeight;
	}
}
