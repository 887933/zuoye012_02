package demo;

import java.util.Scanner;

public class HomeWork08 {
	public static void main(String[] agrs){
		//循环作业3：  求平方根
		/*
		 * 键盘输入一个数字， 求平方根
		 * 
		 * 	16的平方根4
		 * 4的平方根是2
		 * 
		 * 10的平方根：
		 * 	3*3=9；
		 * 4*4=16；
		 *  10的平方根是3-4之间
		 *  
		 *  20的平方根4-5之间
		 */
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		for(int i=1;i<num;i++){
			  if(i*i==num){
				  System.out.println("平方根是"+i);
			  }else if(i*i<num&&(i+1)*(i+1)>num){
				  System.out.println(num+"的平方根在"+i+"和"+(i+1)+"之间");
			  }
		}

	}

}
