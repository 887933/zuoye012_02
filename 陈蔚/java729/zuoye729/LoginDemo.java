package zuoye729;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Scanner;

public class LoginDemo {
	public static void main(String[] args) throws Exception {
		File file=new File("blacklist.txt");
		if(!file.exists()){
			file.createNewFile();
		}
		BufferedReader readfile=new BufferedReader(new FileReader("user.txt"));
		ArrayList<String> list=new ArrayList<>();
		String line=null;
		while((line=readfile.readLine())!=null){
			//System.out.println(line);
			list.add(line);
		}
		 readfile.close();
		 //System.out.println(list);
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入用户名:");
		String username=sc.nextLine();
		System.out.println("请输入密码:");
		String passwd=sc.nextLine();
		if(!login(username)){
			return;
		}
		for(String s:list){
			if((username.equals(s.split("=")[0]))&&(passwd.equals(s.split("=")[1]))){
				System.out.println("登录成功!");
				clear(username);
				break;
			}else if(s.equals(list.get(list.size()-1))){
				System.out.println("登录失败!");
				blackList(username);
			}	
		}	
	}
	private static void clear(String username) throws Exception {
		ArrayList<String> list=new ArrayList<>();
		BufferedReader br=new BufferedReader(new FileReader("blacklist.txt"));
		String line=null;
		while((line=br.readLine())!=null){
			  list.add(line);
		}
		br.close();
		//System.out.println(list);
		long count = list.stream().filter(t->t.split("-")[0].equals(username)).count();
		if(count<6){
			for(int i=0;i<list.size();i++){
				if(list.get(i).split("-")[0].equals(username)){
					list.remove(i);
					i--;
				}
			}
			System.out.println(list);
		}
		BufferedWriter bw=new BufferedWriter(new FileWriter("blacklist.txt"));
		for(String s:list){
			bw.write(s);
			bw.newLine();
			bw.flush();
		}
		bw.close();
	}
	private static boolean login(String username) throws Exception {
        ArrayList<String> list=new ArrayList<>();
		BufferedReader br=new BufferedReader(new FileReader("blacklist.txt"));
		String line=null;
		 String lastlogin=null;
		while((line=br.readLine())!=null){
			list.add(line);
		}
		long count = list.stream().filter(t->t.split("-")[0].equals(username)).count();
		if(count==3){
			for(int i=0;i<list.size();i++){
				if(list.get(i).split("-")[0].equals(username)) {
				  lastlogin = list.get(i).split("-")[1].toString();
				}
			}
			LocalTime now = LocalTime.now();
		    int sum=now.getMinute()*60+now.getSecond()-Integer.parseInt(lastlogin);
		    if(sum<=15){
		    	System.out.println("现在不能登陆，请"+(15-sum)+"秒后重试");
		    	System.out.println("提示:若您再错误3次，则该账号会被永久冻结!");
		    	return false;
		    }
					
		}
		if(count==6){
			System.out.println("该账号已被永久冻结!");
			return false;
		}
		return true;
	}

	private static void blackList(String username) throws Exception {
          LocalTime now = LocalTime.now();
          int minute = now.getMinute();
          int second = now.getSecond();
          BufferedWriter bw=new BufferedWriter(new FileWriter("blacklist.txt",true));
          StringBuilder sb=new StringBuilder();
          sb.append(username).append("-").append(minute*60+second);
          bw.write(sb.toString());
          bw.newLine();
          bw.close();
	}

}
