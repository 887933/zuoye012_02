package zuoye06;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.text.DecimalFormat;

public class FileCopy implements Runnable{
	File source;
	File destinationFolder;
	public FileCopy(File source, File destinationFolder) {
		super();
		this.source = source;
		this.destinationFolder = destinationFolder;
	}
	public FileCopy() {
		super();
	}
	public static void copyFile(){ }
	@Override
	public void run() {
            try {
				copyFile(source, destinationFolder);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            System.out.println(source.getName() + " 文件已复制完成！");

	}
	private void copyFile(File source, File destinationFolder) throws Exception  {
		// TODO Auto-generated method stub
		FileInputStream fis = new FileInputStream(source);
        FileOutputStream fos = new FileOutputStream(destinationFolder);
//        FileChannel sourceChannel = fis.getChannel();
//        FileChannel destChannel = fos.getChannel();
        int length=0;
        byte[] b=new byte[1024];
        long len=source.length();
        double temp=0;
        DecimalFormat Format = new DecimalFormat("##.##%");
        try {
            while((length=fis.read(b))!=-1){
                fos.write(b, 0, length);
                temp += length;
                 double d = (double)temp / len;
                 System.out.println(source.getName()+"已复制"+Format.format(d));
            }
            System.out.println(source.getName()+"文件已复制完成");
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }finally{
            if(fis!=null){
                try {
                    fis.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
        }
    } 
    }
}
