package demo;

public class No_13 {
	public static void main(String[] args) {
//		一个整数，它加上100后是一个完全平方数，再加上168又是一个完全平方数，
//		请问该数是多少？程序分析：在10万以内判断，先将该数加上100后再开方，
//		再将该数加上268后再开方，如果开方后的结果满足如下条件，即是结果。
		for(double i=1;i<100000;i++){
			if(judge(i+100)&&judge(i+168)){
				System.out.println(i);
			}
		}

	}
	public static boolean judge(double x){
	    if(judgeInt(Math.sqrt(x))){
	    	return true;
	    }
	    return false;
	}
	private static boolean judgeInt(double sqrt) {
		if(sqrt%1==0){
			return true;
		}
		return false;
	}

}
