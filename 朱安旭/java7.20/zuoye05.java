package javazuoye02;

import java.util.Scanner;

public class zuoye05 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一串字符串");
		String a1 = sc.nextLine();
		System.out.println("请输入要翻转的部分位置");
		int start = sc.nextInt();
		int end = sc.nextInt();
		 if (start < 0 || end >= a1.length() || start >= end) {
	            System.out.println("输入的翻转范围不合法！");
	            return;
	        }
		 char[] chars = a1.toCharArray();
		for(int i=start;i<end;i++)
		{
			for(int j=end;j>start;j--)
			{
				char temp = chars[i];
				chars[i] = chars[j];
				chars[j] = temp;
				if(i == j)
				{
					break;
				}
				i++;
			}
		}
		String reversedString = new String(chars);
        System.out.println("翻转后的结果为：" + reversedString);
	}
}
