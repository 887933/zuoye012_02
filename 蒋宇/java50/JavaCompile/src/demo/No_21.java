package demo;

public class No_21 {
//	求1+2!+3!+...+20!的和 
//	程序分析：此程序只是把累加变成了累乘。
	public static void main(String[] args) {
		long sum=0;
		for(int i=1;i<=20;i++){
			sum+=getMutiply(i);
		}
		System.out.println(sum);
	}
	public static long getMutiply(int x){
		if(x==1){
			return 1;
		}
		return x*getMutiply(x-1);
	}

}
