package HomeWork;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class RandomCallName04 {
    private List<String> students;
    private List<String> calledStudents;
    private int round;

    public RandomCallName04(String filePath) throws IOException {
        students = Files.readAllLines(Paths.get(filePath));
        calledStudents = new ArrayList<>();
        round = 1;
    }

    public String pickStudent() {
        if (calledStudents.size() == students.size()) {
            calledStudents.clear();
            round++;
            System.out.println("开始第" + round + "轮");
        }

        List<String> remainingStudents = new ArrayList<>(students);
        remainingStudents.removeAll(calledStudents);
        String student = remainingStudents.get(new Random().nextInt(remainingStudents.size()));
        calledStudents.add(student);
        return student;
    }

    public static void main(String[] args) {
        try {
            RandomCallName04 callName = new RandomCallName04("src/HomeWork/rcn.txt");
            Scanner scanner = new Scanner(System.in);
            System.out.println("输入回车开始点名");
            while (true) {
                scanner.nextLine();
                String student = callName.pickStudent();
                System.out.print("点到的学生是: " + student);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
