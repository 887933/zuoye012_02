package homework5;

import java.util.HashMap;
import java.util.Set;

public class java7 {
	public static void main(String[] args) {
		String a="ppRYYGrrYBR2258";
		String b="YrR8RrY";
		show(a,b);
	}

	private static void show(String a, String b) {
		char[] aa=a.toCharArray();
		char[] ab=b.toCharArray();
		HashMap<Character,Integer> ra=new HashMap<Character, Integer>();
		HashMap<Character,Integer> rb=new HashMap<Character, Integer>();
		for(char i:aa) {
			if(ra.containsKey(i))
				ra.put(i,ra.get(i)+1);
			else
				ra.put(i,1);
		}
		for(char i:ab) {
			if(rb.containsKey(i))
				rb.put(i,rb.get(i)+1);
			else
				rb.put(i,1);
		}
		Set<Character> set=rb.keySet();
		for(char i:set) {
			if(ra.containsKey(i)) {
				if(ra.get(i)<rb.get(i)) {
					System.out.println("No");
					return;
				}
				else if(ra.get(i)==rb.get(i)){
					ra.remove(i);
				}
				else {
					ra.put(i,ra.get(i)-rb.get(i));
				}
			}
			else {
				System.out.println("No");
				return;
			}
		}
		set=ra.keySet();
		int sum=0;
		for(char i:set)
			sum+=ra.get(i);
		System.out.println("Yes "+sum);
	}
}
