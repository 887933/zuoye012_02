package homework11;
import java.io.*;
//7.使用多线程实现多个文件同步复制功能，并在控制台显示复制的进度，进度以百分比表示。
//例如：把文件A复制到E盘某文件夹下，在控制台上显示“XXX文件已复制10%”，
//“XXX文件已复制20%”……“XXX文件已复制100%”，“XXX复制完成！”
//A文件复制了10%
//B文件复制了10%
//B文件复制了20%
//B文件复制了30%
//A文件复制20%
//B文件复制了40%
//B文件复制了50%
//B文件复制了60%
//A文件复制30%
//B文件复制了70%
//B文件复制了80%
//B文件复制了90%
//A文件复制40%
//B文件复制完毕
public class Demo10 {
	 private static class CopyThread extends Thread {
	        private final File src;
	        private final File dest;
	        private final String fileName;

	        public CopyThread(String fileName, File src, File dest) {
	            this.fileName = fileName;
	            this.src = src;
	            this.dest = dest;
	        }

	        @Override
	        public void run() {
	            try (FileInputStream fis = new FileInputStream(src);
	                 FileOutputStream fos = new FileOutputStream(dest)) {
	                long fileSize = src.length();
	                long totalBytesRead = 0;
	                byte[] bytes = new byte[1024];

	                int bytesRead;
	                while ((bytesRead = fis.read(bytes)) != -1) {
	                    fos.write(bytes, 0, bytesRead);
	                    totalBytesRead += bytesRead;

	                    
	                    int progress = (int) ((totalBytesRead * 100) / fileSize);
	                    System.out.println(fileName + "文件已复制" + progress + "%");

	                    
	                    Thread.sleep(400);
	                }
	                System.out.println(fileName + "复制完毕");
	            } catch (IOException | InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    }
	 private static class CopyThread2 extends Thread {
	        private final File src2;
	        private final File dest2;
	        private final String fileName2;

	        public CopyThread2(String fileName2, File src2, File dest2) {
	            this.fileName2 = fileName2;
	            this.src2 = src2;
	            this.dest2 = dest2;
	        }

	        @Override
	        public void run() {
	            try (FileInputStream fis = new FileInputStream(src2);
	                 FileOutputStream fos = new FileOutputStream(dest2)) {
	                long fileSize = src2.length();
	                long totalBytesRead = 0;
	                byte[] bytes = new byte[4096];

	                int bytesRead;
	                while ((bytesRead = fis.read(bytes)) != -1) {
	                    fos.write(bytes, 0, bytesRead);
	                    totalBytesRead += bytesRead;

	                   
	                    int progress = (int) ((totalBytesRead * 100) / fileSize);
	                    System.out.println(fileName2 + "文件已复制" + progress + "%");

	                   
	                    Thread.sleep(200);
	                }
	                System.out.println(fileName2 + "复制完毕");
	            } catch (IOException | InterruptedException e) {
	                e.printStackTrace();
	            }
	        }
	    }

	    public static void main(String[] args) {

	    	String sourcePath = "copyliuy.jpg";
	        String destPath = "copyliuy_copy3.jpg"; 
	        String sourcePath2 = "sdfg.jpg";
	        String destPath2 = "sdfg_copy2.jpg";
	        
	        
	        File srcFile = new File(sourcePath);
	        File destFile = new File(destPath);
	        String fileName = srcFile.getName();
	        File srcFile2 = new File(sourcePath2);
	        File destFile2 = new File(destPath2);
	        String fileName2 = srcFile2.getName();
	        
	        CopyThread thread1 =  new CopyThread(fileName, srcFile, destFile);
	        CopyThread2 thread2 =  new CopyThread2(fileName2, srcFile2, destFile2);
	        thread1.start();
	        thread2.start();
	        try {
	            thread1.join();
	            thread2.join();
	        } catch (InterruptedException e) {
	            e.printStackTrace();
	        }
	    }
}
