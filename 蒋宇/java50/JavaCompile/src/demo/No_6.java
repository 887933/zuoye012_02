package demo;

import java.util.Scanner;

public class No_6 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入一个较大数");
		int x=sc.nextInt();
		System.out.println("请输入一个较小数");
		int y=sc.nextInt();
		System.out.println(maxNumber(x, y));

	}

	public static int maxNumber(int x, int y) {
		if (y > x) {
			System.out.println("请把大数放在前面");
		}
		if (x > y) {
			while (true) {
				x = x % y;
				if (x == 0) {
					return y;
				} else if (y == 0) {
					return x;
				} else if (x == 1 || y == 1) {
					System.out.println("这两个数是互质数");
					return 1;
				}
				y = y % x;
				if (x == 0) {
					return y;
				} else if (y == 0) {
					return x;
				} else if (x == 1 || y == 1) {
					System.out.println("这两个数是互质数");
					return 1;
				}

			}
		}
		return -1;
	}

}
