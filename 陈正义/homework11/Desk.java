package homework11;

public class Desk {
	public  boolean flag;
	
	public  int count;
	public Desk() {
		this(false,10);
	}
	public Desk(boolean flag, int count) {
		super();
		this.flag = flag;
		this.count = count;
	}
	//������
	public  final Object lock=new Object();
	
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Object getLock() {
		return lock;
	}
}
