package EveryDayPractice;

import java.util.Scanner;

public class Practice44 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a=sc.nextInt();
        if(a%2==0)
        {
            for(int i=1;i<=a/2;i++)
            {
                if(isPrime(i)&&isPrime(a-i))
                {
                    System.out.println(a+"可以表示为两个素数之和");
                    break;
                }
                else {
                    System.out.println("不行");
                }
            }
        }
    }

    private static boolean isPrime(int i) {
        for(int j=2;j<i;j++)
        {
            if(i%j==0)
            {
                return false;
            }
        }
        return true;
    }

}
