package java03;

import java.util.Arrays;

public class StickSort {
    public static void main(String[] args) {
		int[] a={6,2,7,3,4,8,1,9,5};
		Sort(a);
	}
    private static void Sort(int[] a){    	
    	int count=0;//用来统计结果次数
    	for(int i=1;i<a.length;i++){   
    		int cnt=0;//用来统计比较的次数
    		boolean swap=false;//用来标记是否进行了交换
    		for(int j=i;j>0;j--){ //从第i个位置往前比较
    			
    			if(a[j]<a[j-1]){//如果前一个大于后一个，就交换并且cnt自增一次，将swap设置为true，以免跳出
    				swap(a,j,j-1);
    				cnt++;
    				System.out.println("比较了"+cnt+"次");
    				swap=true;
    			}
    			if(!swap){//如果不发生交换，则跳出循环
    				break;
    			}
    		}
    		
    		count++;
    		System.out.println("第"+count+"次结果："+Arrays.toString(a));
    		}
    	}
    
    private static void swap(int[] a,int i,int j){
    	int t=a[i];
    	a[i]=a[j];
    	a[j]=t;
    }
}
