package java11;

public class Numprinter implements Runnable{
	private int num;

	public Numprinter(int num) {
		this.num = num;
	}

	@Override
	public void run() {
		System.out.print(num+" ");
	}
	
}
