package homework7;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
public class CopyFile {
	public static void main(String[] args) {
        String sourcePath = "C:\\Users\\LENOVO\\Desktop\\德育分";
        String targetPath = "C:\\Users\\LENOVO\\Desktop\\新建文件夹";

        File sourceDirectory = new File(sourcePath);
        File targetDirectory = new File(targetPath);
        if (copyDirectory(sourceDirectory, targetDirectory)) {
            System.out.println("文件夹复制成功。");
        } else {
            System.out.println("文件夹复制失败。");
        }
    }

    public static boolean copyDirectory(File source, File target) {
        if (source.isDirectory()) {
            if (!target.exists()) {
                boolean isCreated = target.mkdirs();
                if (!isCreated) {
                    System.out.println("创建目标目录失败：" + target.getPath());
                    return false;
                }
            }
            File[] files = source.listFiles();
            if (files != null) {
                for (File file : files) {
                    File targetFile = new File(target, file.getName());
                    if (!copyDirectory(file, targetFile)) {
                        return false;
                    }
                }
            }
            return true;
        } else {
            try {
                copyFile(source, target);
            } catch (IOException e) {
                System.out.println("复制文件失败：" + e.getMessage());
                return false;
            }
            return true;
        }
    }

    public static void copyFile(File sourceFile, File targetFile) throws IOException {
        try (FileInputStream in = new FileInputStream(sourceFile);
             FileOutputStream out = new FileOutputStream(targetFile)) {
            byte[] buffer = new byte[1024];
            int length;
            while ((length = in.read(buffer)) > 0) {
                out.write(buffer, 0, length);
            }
        }
    }
	
}
