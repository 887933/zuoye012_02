package java50practice;

import java.util.Scanner;

public class java47 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		for(int i=0;i<7;i++){
			System.out.print("输入一个数字：");
			int n=sc.nextInt();
			if(n<1||n>50){
				System.out.println("输入不合法");
			}else{
				for(int j=0;j<n;j++){
					System.out.print("* ");
				}
				System.out.println("");
			}
		}
	}
}
