package homework6;

import java.util.HashMap;
import java.util.Set;

public class java5 {
	public static void main(String[] args) {
		int[] a= {3,4,2,7};
		int[] b= {2,9,0,4,6};
		System.out.println(distinct(a,b));
	}

	private static Set<Integer> distinct(int[] a, int[] b) {
		HashMap<Integer,Integer> temp=new HashMap<Integer, Integer>();
		for(int i:a) {
			temp.put(i,null);
		}
		for(int i:b) {
			temp.put(i,null);
		}
		return temp.keySet();
	}
}
