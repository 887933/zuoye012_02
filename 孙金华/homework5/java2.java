package homework5;

import java.util.ArrayList;
import java.util.Collections;

public class java2 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,"张无忌-男-15","周芷若-女-14", 
        "赵敏-女-13","张强-男-20","张三丰-男-100","张翠山-男-40",
        "张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
        ArrayList<String> males=male(list);
        ArrayList<String> females=female(list);
        ArrayList<String> combined = new ArrayList<>();
        combined.addAll(males);
        combined.addAll(females);
        System.out.println("过滤后的演员名单：" + combined);
    }

    private static ArrayList<String> male(ArrayList<String> list) {
        ArrayList<String> male= new ArrayList<>();
        for (String actor:list) {
            String[] parts=actor.split("-");
            if (parts[1].equalsIgnoreCase("男")&&parts[0].length()==3&&male.size()<3)
            	male.add(actor);
        }
        return male;
    }

    private static ArrayList<String> female(ArrayList<String> list) {
        ArrayList<String> female=new ArrayList<>();
        for (String actor:list) {
            String[] parts=actor.split("-");
            if (parts[1].equalsIgnoreCase("女")&&parts[0].startsWith("林"))
                female.add(actor);
        }
        female.remove(0);
        return female;
}
}