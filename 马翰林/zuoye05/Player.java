package zuoye05;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.TreeMap;

public class Player implements Comparable<Player>{
    @Override
    public String toString() {
        return "Player{" +
                "name='" + name + '\'' +
                ", poker=" + poker +
                '}';
    }

    private String name;
    private ArrayList<String> poker=new ArrayList<String>();
  private TreeMap<Integer,String> treeMap=new TreeMap<>();

    public Player() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getTreeMao() {
        return poker;
    }

    public void setPoker(ArrayList<String> poker) {
        this.poker = poker;
    }

    @Override
    public int compareTo(Player o) {
        return 0;
    }

    public TreeMap<Integer, String> getTreeMap() {
        ArrayList<Integer> key=new ArrayList<>(this.treeMap.keySet());
        Collections.sort(key, new Comparator<Integer>() {

            @Override
            public int compare(Integer o1, Integer o2) {
                return o1-o2;
            }
        });
        return treeMap;
    }

    public void setTreeMap(TreeMap<Integer, String> treeMap) {
        this.treeMap = treeMap;
    }
}
