package homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import java.util.stream.Collectors;

public class java04_RandomCallName2 {
	public static void main(String[] args) throws IOException {
		/*
		 * 作业2：使用生成的50个男生50个女生姓名，每个姓名占一行
		 * 显示名称和性别，并打印男女比例
		 * 70%概率点到男生
		 * 30%概率点到女生
		 * 
		 */
		String gender = null;
		int totalTimes = 100,males = 0;
		for(int i=0;i<totalTimes;i++) {
			gender = randomCallName2("names.txt");
			if(gender.equals("男")) {
				males++;
			}
		}
		System.out.println("男女比例为 男：女="+males+"："+(totalTimes-males));
	}
	
	private static String randomCallName2(String string) throws IOException {
		FileReader fr = new FileReader(string);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		ArrayList<String> list = new ArrayList<String>();
		while((line = br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		Random r = new Random();
		int num = r.nextInt(10)+1;
		ArrayList<String> names = new ArrayList<String>();
		if(num>=1&&num<=7) {
			names = (ArrayList<String>)list.stream().filter(t->"男".equals(t.split("-")[1])).collect(Collectors.toList());
		}else if(num>=8&&num<=10) {
			names = (ArrayList<String>)list.stream().filter(t->"女".equals(t.split("-")[1])).collect(Collectors.toList());
		}
		
		num = r.nextInt(names.size());
		String[] LuckyOne = names.get(num).split("-");
		System.out.println(LuckyOne[0]+" "+LuckyOne[1]);
		return LuckyOne[1];
	}
}
