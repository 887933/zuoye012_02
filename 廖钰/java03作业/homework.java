package homework;

import java.util.ArrayList;
import java.util.Random;

public class homework {
	public static void main(String[] args) {
		//作业：接口和抽象类的区别
		//作业：随机红包
		randomRedPocket();
	}

	private static void randomRedPocket() {
		User u0 = new User("aaa","Manager",300);
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr = u0.send(200,3);
		User u1 = new User("bbb","Member",50);
		User u2 = new User("ccc","Member",50);		
		User u3 = new User("ddd","Member",50);
		u1.receive(arr);
		u2.receive(arr);
		u3.receive(arr);
		System.out.println(u0.getUsertype()+" "+u0.getUsername()+"的余额为："+u0.getMoney());
		System.out.println(u1.getUsertype()+" "+u1.getUsername()+"的余额为："+u1.getMoney());
		System.out.println(u2.getUsertype()+" "+u2.getUsername()+"的余额为："+u2.getMoney());
		System.out.println(u3.getUsertype()+" "+u3.getUsername()+"的余额为："+u3.getMoney());
	}
	
	
}
