package homework;

import java.util.Random;

public class Diloscate_02 {
	public static void main(String[] args){
		int[][] arr = {{1, 2, 3}, {4, 5, 6, 7}, {8, 9, 10}};
		Random rd=new Random();
		int temp;
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				int x=rd.nextInt(arr.length-1);
				int y=rd.nextInt(arr[i].length-1);
				temp=arr[i][j];
				arr[i][j]=arr[x][y];
				arr[x][y]=temp;
			}
		}
		for(int i=0;i<arr.length;i++){
			for(int j=0;j<arr[i].length;j++){
				System.out.print(arr[i][j] + " ");
			}
			System.out.println();
		}
	}
}
