package zuoye719;

import java.util.ArrayList;
import java.util.Scanner;

public class demo4 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入第一个字符串:");
	String str1=sc.nextLine();
	System.out.println("请输入第二个字符串:");
	String str2=sc.nextLine();
	System.out.println("最长相同的字符串:");
	System.out.println(common(str1,str2));

}
private static String common(String str1, String str2) {	
	int maxCommon=0;
	String longgeststr="";
	boolean find=false;
	
	for(int i=0;i<str1.length();i++){
		for(int j=1;i+j<=str1.length();j++){
		  String str3=str1.substring(i, i+j);
			
			if(str2.contains(str3)&&str3.length()>maxCommon){
				maxCommon=str3.length();
				longgeststr=str3;
				find=true;
			}
			
		}
	}
	if(find==false){
		return "没有找到连续相同的子字符串!";
	}
	return longgeststr;
}
}
