package demo1;
//定义方法复制数组当中的任意位置的内容，转化为新的数组
//* 	源数组：int[] arr={1,2,3,4,5,6,7,8,9};
//* 			复制2，5这个位置的数组的内容
//* 			int[] arr={3,4,5};
import java.util.Arrays;

public class java11 {
	public static void main(String[] args) {
		int[] arr = {1,2,3,4,5,6,7,8};
		change(arr,2,5);
	}

	private static void change(int[] arr, int m, int k) {
		// TODO Auto-generated method stub
		int[] arr1=new int[k-m];
		for(int i= 0;i<arr1.length;i++){
			arr1[i] = arr[i+m];
		}
		System.out.println(Arrays.toString(arr1));
	}

}
