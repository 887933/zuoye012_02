package demo;

import java.util.Random;
import java.util.Scanner;

public class HomeWork13 {
	public static void main(String[] args){
//		定义方法复制数组当中的任意位置的内容，转化为新的数组
//		 * 	源数组：int[] arr={1,2,3,4,5,6,7,8,9};
//		 * 			复制2，5这个位置的数组的内容
//		 * 			int[] arr={3,4,5};
		int[] arr={1,2,3,4,5,6,7,8,9};
		int[] arr2=copy(arr);
		for(int i=0;i<arr2.length;i++){
			System.out.println(arr2[i]);
		}

	}
	public static int[] copy(int[] arr){
		
		Scanner sc=new Scanner(System.in);
		Random r=new Random();
		System.out.println("请输入要复制几个数");
		int x=sc.nextInt();
		int[] re_arr=new int[x];
		for(int i=0;i<x;i++){//随机复制
			int y=r.nextInt(arr.length);
			re_arr[i]=arr[y];
		}
		
		return re_arr;
	}

}
