package demo;

public class Student {
	private String name;
	private int id;
	private int score1;
	private int score2;
	private int score3;
	private double avg;
	public double getAvg() {
		return avg;
	}
	public void setAvg(double avg) {
		this.avg = avg;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getScore1() {
		return score1;
	}
	public void setScore1(int score1) {
		this.score1 = score1;
	}
	public int getScore2() {
		return score2;
	}
	public void setScore2(int score2) {
		this.score2 = score2;
	}
	public int getScore3() {
		return score3;
	}
	public void setScore3(int score3) {
		this.score3 = score3;
	}
	public Student(String name, int id, int score1, int score2, int score3, double avg) {
		super();
		this.name = name;
		this.id = id;
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
		this.avg = avg;
	}
	public Student(String name, int id, int score1, int score2, int score3) {
		super();
		this.name = name;
		this.id = id;
		this.score1 = score1;
		this.score2 = score2;
		this.score3 = score3;
	}
	@Override
	public String toString() {
		return "Student [name=" + name + ", id=" + id + ", score1=" + score1 + ", score2=" + score2 + ", score3="
				+ score3 + ", avg=" + avg + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(avg);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + score1;
		result = prime * result + score2;
		result = prime * result + score3;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Student other = (Student) obj;
		if (Double.doubleToLongBits(avg) != Double.doubleToLongBits(other.avg))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (score1 != other.score1)
			return false;
		if (score2 != other.score2)
			return false;
		if (score3 != other.score3)
			return false;
		return true;
	}
	
	
}
