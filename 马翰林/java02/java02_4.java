package java02;

import java.util.Scanner;

//循环作业3：  求平方根
public class java02_4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        if (a<0||a>100) {
            System.out.println("输入数字不规范亲重新输入");
        }
        else
        {
            for(int i=1;i<=10;i++){
                if(i*i==a)
                {
                    System.out.println(a+"的平方根是"+i);
                }
                else if(a<(i+1)*(i+1)&&i*i<a){
                    System.out.println(a+"的平方根在"+i+"和"+(i+1)+"之间");
                }

            }

        }
    }

}
