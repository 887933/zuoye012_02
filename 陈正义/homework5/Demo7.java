package homework5;
//我们使用[0-9] [a-z][A-Z]范围内来表示字符串
//示例1：
//输入ppRYYGrrYBR2258
//	YrR8RrY
//输出：
//Yes 8
//
//
//	输入:ppRYYGrrYB225
//	YrR8RrY
//NO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class Demo7 {
    public static void main(String[] args) {
        String long_a = "ppRYYGrrYBR2258";
        String short_a = "YrR8RrY";
        String long_b = "ppRYYGrrYB225";
        panduan(short_a, long_a);
        panduan(short_a, long_b);
    }

    public static void panduan(String short_a, String long_a) {
        boolean b = true;
        int[] shortCounts = new int[256];
        for (char c : short_a.toCharArray()) {
            shortCounts[c]++;
        }

        for (char c : short_a.toCharArray()) {
            int countInLong = 0;
            for (char ccc : long_a.toCharArray()) {
                if (c == ccc) {
                    countInLong++;
                }
            }
            if (countInLong < shortCounts[c]) {
                b = false;
                break;
            }
        }

        int e = long_a.length() - short_a.length();
        if (b) {
            System.out.println("YES " + e);
        } else {
            System.out.println("NO");
        }
    }
}