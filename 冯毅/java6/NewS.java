package java07;

public class NewS {
	public static void newS(String s){
		char[] cc=s.toCharArray();
		for(int i = 0;i<cc.length;i++){
			if(cc[i]>='A'&&cc[i]<='Y'){
				cc[i]=(char)(cc[i]+33);
			}
			else if(cc[i]=='Z'){
				cc[i]='a';
			}
			else if(cc[i]>='a'&&cc[i]<='c'){
				cc[i]='2';
			}
			else if(cc[i]>='d'&&cc[i]<='f'){
				cc[i]='3';
			}
			else if(cc[i]>='g'&&cc[i]<='i'){
				cc[i]='4';
			}
			else if(cc[i]>='j'&&cc[i]<='l'){
				cc[i]='5';
			}
			else if(cc[i]>='m'&&cc[i]<='o'){
				cc[i]='6';
			}
			else if(cc[i]>='p'&&cc[i]<='s'){
				cc[i]='7';
			}
			else if(cc[i]>='t'&&cc[i]<='v'){
				cc[i]='8';
			}
			else if(cc[i]>='w'&&cc[i]<='z'){
				cc[i]='9';
			}
		}
		String newS=String.valueOf(cc);
		System.out.println(newS);
	}
}
