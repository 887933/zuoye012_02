package javazuoye03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class zuoye01 {
	public static void main(String[] args) {
		ArrayList<String> arr=new ArrayList();
		HashMap<String,Integer> map=new HashMap<>();
		Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		for(String a: arr)
		{
			String[] parts=a.split(",");
			String name = parts[0];
			int age =Integer.parseInt(parts[1]);
			if(age>24)
			{
				map.put(name, age);
			}
			}
		for(Map.Entry<String, Integer> entry : map.entrySet())
		{
			String key=entry.getKey();
			Integer value=entry.getValue();
			System.out.println("姓名"+key+"年龄"+value);
		}
	}
}
