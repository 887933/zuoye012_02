package demo1;
//输出所有两位数，三位数，四位数，五位数的回文数
public class java04 {
	public static void main(String[] args) {
		for(int i=0;i<=100000;i++){
			if(i<100){
				if(i/10==i%10){
					System.out.println(i);
				}
			}else if(i>=100 &&i<1000){
				if(i/100==i%10){
					System.out.println(i);
				}
			}else if(i>=1000 &&i<10000){
				if(i/1000==i%10 && i/100%10==i%100/10){
					System.out.println(i);
				}
			}else if(i>=10000 &&i<100000){
				if(i/10000==i%10 && i/1000%10==i%100/10){
					System.out.println(i);
				}
			}
		}
	}
}
