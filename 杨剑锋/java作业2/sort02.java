package homework;

import java.util.Arrays;
/*1.选择增量序列：首先确定一个增量序列，通常初始增量较大，然后逐渐减小至1。
2.按增量分组：将整个数组分成若干个子序列，每个子序列相隔某个增量的距离。
3.插入排序：对每个子序列分别进行插入排序。
4.缩小增量：逐步减小增量，重复步骤2和3，直至增量为1。
5.最后一次插入排序：当增量减至1时，对整个数组进行一次直接插入排序.*/
public class sort02 {
	public static void main(String[] args) {
		int[] a={4,6,3,8,23,59,34,12,65,1};
		sort(a);
	}

	private static void sort(int[] a) {
		int dk=a.length/2;
		while(dk>=1){
			for(int i=dk;i<a.length;i++){
				if(a[i]<a[i-dk]){
					int j;
					int x=a[i];
					a[i]=a[i-dk];
					for(j=i-dk;j>=0&&x<a[j];j=j-dk){
						a[j+dk]=a[j];
					}
					a[j+dk]=x;
				}
			}
			dk=dk/2;
		}
		System.out.println(Arrays.toString(a));
	}
}
