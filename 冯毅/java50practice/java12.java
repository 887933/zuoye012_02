package java50practice;

import java.util.Scanner;

public class java12 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入利润（单位：万元）：");
		double pro=sc.nextDouble();
		double bone=0;
		if(pro<=10){
			bone=pro*0.1;
		}
		else{
			bone+=10*0.1;
			if(pro<=20){
				bone+=(pro-10)*0.075;
			}
			else{
				bone+=10*0.075;
				if(pro<=40){
					bone+=(pro-20)*0.05;
				}
				else{
					bone+=20*0.05;
					if(pro<=60){
						bone+=(pro-40)*0.03;
					}
					else{
						bone+=20*0.03;
						if(pro<=100){
							bone+=(pro-60)*0.015;
						}
						else{
							bone+=40*0.015+(pro-100)*0.01;
						}
					}
				}
			}
		}
		System.out.println(bone+"万元");
	}
}
