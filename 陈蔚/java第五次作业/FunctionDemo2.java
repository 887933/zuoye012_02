package zuoye724;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class FunctionDemo2 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100"
		, "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-男-41","林婷-女-22","林立-女-23");
		
		List<String> collect1 = list.stream().filter(new FunctionDemo2()::MaleLimit).limit(3).map(t->t.split("-")[0]).collect(Collectors.toList());
		
		List<String> collect2 = list.stream().filter(new FunctionDemo2()::FeMaleLimit).skip(1).map(t->t.split("-")[0]).collect(Collectors.toList());
		 ArrayList<String> newlist=new ArrayList<>(collect1);
		newlist.addAll(collect2);
		System.out.println(newlist);
		
	}
	public boolean MaleLimit(String t){
		return "男".equals(t.split("-")[1])&&t.split("-")[0].length()==3;
		
	}
	public boolean FeMaleLimit(String t){
		
		return "女".equals(t.split("-")[1])&&t.split("-")[0].charAt(0)=='林';
		
	}
}
