package demo;

import java.util.Scanner;

public class HomeWork12 {
	public static void main(String[] args){
//作业：定义方法
//		 * 1.判断数组当中某一个数是否存在，如果存在
//		，则返回true,否则返回false		
		int[] arr={1,2,3,4,5,6,7};
		Scanner sc=new Scanner(System.in);
		int x=sc.nextInt();
		if(judge(arr,x)){
			System.out.println("存在");
		}else{
			System.out.println("不存在");
		}
	}
	public static boolean judge(int[] arr,int x){
		for(int i=0;i<arr.length;i++){
			if(arr[i]==x){
				return true;
			}
		}
		return false;
	}

}
