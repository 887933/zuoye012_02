package java11;

public class make extends Thread {
	@Override
	public void run() {
		while(true){
			synchronized(product.lock){
				if(product.count>3){
					break;
				}else{
					if(!product.flag){
						//生产
						for(int i=1;i<=5;i++){
							System.out.println("正在生产第"+i+"个产品");
						}
						System.out.println("第"+product.count+"次生产完成");
						product.flag=true;
						product.lock.notify();
					}else{
						//等待
						try {
							product.lock.wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}

