package java12;
import java.util.concurrent.ExecutorService;  
import java.util.concurrent.Executors;  
import java.util.function.IntConsumer;  

public class FizzBuzzMain {
	public static void main(String[] args) {  
		int n = 15;  
		FizzBuzz fizzBuzz = new FizzBuzz(n);  
		ExecutorService executor = Executors.newFixedThreadPool(4);  

		// 线程A: 输出 "fizz"  
		Runnable fizzTask = () -> {  
			try {  
				fizzBuzz.fizz(() -> System.out.print("fizz "));  
			} catch (InterruptedException e) {  
				Thread.currentThread().interrupt();  
			}  
		};  

		// 线程B: 输出 "buzz"  
		Runnable buzzTask = () -> {
			try {  
				fizzBuzz.buzz(() -> System.out.print("buzz "));  
			} catch (InterruptedException e) {  
				Thread.currentThread().interrupt();  
			}  
		};  

		// 线程C: 输出 "fizzbuzz"  
		Runnable fizzbuzzTask = () -> {  
			try {  
				fizzBuzz.fizzbuzz(() -> System.out.print("fizzbuzz "));  
			} catch (InterruptedException e) {  
				Thread.currentThread().interrupt();  
			}  
		};  
		Runnable numberTask = () -> {  
			try {  
				fizzBuzz.number(x -> System.out.print(x + " "));  
			} catch (InterruptedException e) {  
				Thread.currentThread().interrupt();  
			}  
		};
		executor.submit(fizzTask);  
		executor.submit(buzzTask);  
		executor.submit(fizzbuzzTask);  
		executor.submit(numberTask);  
		executor.shutdown();  
	}  
}
