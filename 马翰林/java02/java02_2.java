package java02;
//for 循环作业1：  输出所有两位数，三位数，四位数，五位数的回文数      12321 是回文  10101 是   12345不是
public class java02_2 {
    public static void main(String[] args) {
        for (int i =10; i <100; i++) {
            if(i/10==i%10){
                System.out.println(i+"是回文");
            }
        }
        for (int i =100; i <1000; i++) {
            if(i/100==i%100%10){
                System.out.println(i+"是回文");
            }
        }
        for (int i =1000; i <10000; i++) {
            if(i/1000==i%1000%100%10&&i%1000/100==i%1000%100/10){
                System.out.println(i+"是回文");
            }
        }
        for (int i =10000; i <100000; i++) {
            if(i/10000==i%10000%1000%100%10&&i%10000/1000==i%10000%1000%100/10){
                System.out.println(i+"是回文");
            }
        }
    }
}
