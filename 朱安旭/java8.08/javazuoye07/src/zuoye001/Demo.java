package zuoye001;

import java.util.concurrent.ArrayBlockingQueue;

public class Demo {
	public static void main(String[] args) {
		ArrayBlockingQueue<String> array=new ArrayBlockingQueue<>(1);
		Cooker c=new Cooker(array);
		Foodie f=new Foodie(array);
		c.start();
		f.start();
	}
}
