package homework2;

import java.util.Arrays;

//ϣ������
public class java2 {
	public static void main(String[] args) {
		int[] arr= {12,23,46,23,34,45,30,7,9,5,7,34};
        shellsort(arr);
        System.out.println(Arrays.toString(arr));
	}
    public static void shellsort(int[] arr) {
        int n = arr.length;
        int t = 1;
        while (t < n) {
            t = 2 * t + 1;
        }
        for (int gap = t; gap > 0; gap = (gap - 1) / 2) {
            for (int i = gap; i < n; i++) {
                int temp = arr[i];
                int j;
                for (j = i; j >= gap && arr[j - gap] > temp; j -= gap) {
                    arr[j] = arr[j - gap];
                }
                arr[j] = temp;
            }
        }
    }

}
