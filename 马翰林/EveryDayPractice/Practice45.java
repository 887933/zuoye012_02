package EveryDayPractice;

import java.util.Scanner;

public class Practice45 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int i= sc.nextInt();
        if(isPrime(i))
        {
            if(i%9==0)
            {
                System.out.println("1");
            }
            else {
                System.out.println("0个");
            }
        }
        else {
            System.out.println("不是素数");
        }

    }
    private static boolean isPrime(int i) {
        for(int j=2;j<i;j++)
        {
            if(i%j==0)
            {
                return false;
            }
        }
        return true;
    }
}
