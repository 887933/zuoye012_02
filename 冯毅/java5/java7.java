package java06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;

public class java7 {
	public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		Collections.addAll(a, 1,5,1,6,4,9,5,5,3,6,4);
		System.out.println(a);
		System.out.println(kickSameNum(a));
	}
	public static TreeSet<Integer> kickSameNum(ArrayList<Integer> a){
		TreeSet<Integer> ti=new TreeSet<Integer>();
		for(Integer i:a){
			ti.add(i);
		}
		return ti;
	}
}
