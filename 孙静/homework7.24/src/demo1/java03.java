package demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collectors;

public class java03 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入字符串：");
		String s = sc.nextLine();
		ArrayList<Character> list = new ArrayList<>();
		char[] ch = s.toCharArray();
		for(char c:ch){
			list.add(c);
		}
		Scanner sc1 = new Scanner(System.in);
		System.out.println("请输入次数");
		int count = sc1.nextInt();
		//stream
		list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))//对数据计次
		.entrySet().stream().filter(entry->entry.getValue()==count)//通过entrySet可以获得次数value
		.map(Entry::getKey)//通过map改变输出，只要键
		.forEach(System.out::print);
		
		//方法引用
		TreeMap<Character, Integer> hashMap = treeMap(ch);
		hashMap.entrySet().stream().filter(entry->entry.getValue()==count)
		.map(Entry::getKey).forEach(System.out::print);

	}
	public static TreeMap<Character, Integer> treeMap(char[] ch) {
		TreeMap<Character,Integer> hashMap = new TreeMap<>();
		for(char c:ch){
			if(hashMap.containsKey(c)){
				int value = hashMap.get(c);
				hashMap.put(c, value+1);
				
			}else{
				hashMap.put(c, 1);
			}
		}
		return hashMap;
	}
}
