package Hwork;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class No_2 {
    public static void main(String[] args) throws IOException, InterruptedException {
        HashMap<String, Integer> map = new HashMap<>();
        BufferedReader br = new BufferedReader(new FileReader("user.txt"));
        Scanner sc = new Scanner(System.in);
        String str;
        while ((str = br.readLine()) != null) {
            String[] arr = str.split("=");
            map.put(arr[0], Integer.parseInt(arr[1]));
        }
        System.out.println(map);
        int count = 0;
        try {
            while (true) {
                String name = sc.nextLine().trim(); // 添加.trim()以消除可能残留的换行符
                if (map.containsKey(name)) {
                    System.out.println("用户名正确，请输入密码");
                    int pwd = sc.nextInt();
                    sc.nextLine(); // 读取换行符
                    if (map.get(name) == pwd) {
                        System.out.println("密码正确，登录成功");
                        break;
                    } else {
                        System.out.println("密码错误，请重新输入");
                        count++;
                    }
                } else {
                    System.out.println("输入错误，请重新输入");
                    count++;
                }
                if (count == 3) {
                    Date date = new Date();
                    System.out.println("请等一分钟后重试");
                    TimeUnit.SECONDS.sleep(10); // 修改为60秒
                    count = 0; // 重置计数器
                }
            }

        } catch (InputMismatchException e) {
            System.out.println("请输入数字");
        }
        br.close();
        sc.close(); // 关闭Scanner
    }
}

