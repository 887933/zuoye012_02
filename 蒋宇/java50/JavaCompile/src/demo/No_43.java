package demo;

public class No_43 {
	public static void main(String[] args) {
        int high = 7;
        int sum = (high + 1) / 2;
        for (int i = 2; i <= high + 1; i++) {
            sum += high * Math.pow(high + 1, i - 2) * (int) ((high + 1) / 2);
        }
        System.out.println(sum);
	}

}
