package zuoye729;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;


public class CallNameDemo2 {
	public static void main(String[] args) throws Exception {
		File file=new File("bili.txt");//该文件记录男女比例
		if(!file.exists()){
			file.createNewFile();
		}
	   Random r=new Random();
	  //50个男生，50个女生的集合
	   ArrayList<Student> list=new ArrayList<>();
	   //男女生比例集合
	   ArrayList<String> list1=new ArrayList<>();
	   //50个男生的集合
	   ArrayList<Student> male=new ArrayList<>();
	   //50个女生的集合
	   ArrayList<Student> female=new ArrayList<>();
	   int malecount=0;
	   int femalecount=0;
	   BufferedReader br=new BufferedReader(new FileReader("name.txt")); 
	   String line=null;
	   while((line=br.readLine())!=null){
			String[] split = line.split("-");
		    list.add(new Student(split[0],split[1],Integer.parseInt(split[2])));	   
		}
	      br.close();
	     // System.out.println("男女生集合:");
		//  System.out.println(list);
		  //打乱
		  Collections.shuffle(list);
		  for(int i=0;i<list.size();i++){
			  if("男".equals(list.get(i).toString().split("-")[1])){
				  male.add(list.get(i));
			  }
			  if("女".equals(list.get(i).toString().split("-")[1])){
				  female.add(list.get(i));
			  }
		  }
		  System.out.println("男生集合:");
		  System.out.println(male);
		  System.out.println("女生集合:");
		  System.out.println(female);
		  //1-100的随机数
		 int index=r.nextInt(list.size())+1;
		 //0-49的随机数作为男生集合和女生集合的下标
		  int i=r.nextInt(50);
		  System.out.println("幸运同学:");
		 if(index<=70){
			  malecount++;
			  System.out.println(male.get(i).getName()+"-"+male.get(i).getGender());	  
		  }
		 if(index>70&&index<=100){
			  femalecount++;
			  System.out.println(female.get(i).getName()+"-"+female.get(i).getGender());	  
		  }
			  BufferedReader br1=new BufferedReader(new FileReader("bili.txt"));
		   	   String line1=null;
		       System.out.println("男女生比例:");
		   	   while((line1=br1.readLine())!=null){
		   	      malecount+=Integer.parseInt(line1.split(":")[0]);
		   	      femalecount+=Integer.parseInt(line1.split(":")[1]);  
		   	      String str=(malecount+":"+femalecount).toString();  
		   	      System.out.println(str);
		   	    }
		   	    br1.close();
     
        StringBuilder sb=new StringBuilder();
        sb.append(malecount).append(":").append(femalecount);
       // System.out.println(sb.toString());
        list1.add(sb.toString());
        System.out.println(list1);
        BufferedWriter bw=new BufferedWriter(new FileWriter("bili.txt"));
		for (String s : list1) {
			bw.write(s);
			bw.newLine();
		}
		bw.close();
		
		
}
}
