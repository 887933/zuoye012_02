package Strings;

public class Buzz extends Thread{
	int printBuzz;
	
	public Buzz(int printBuzz) {
		super();
		this.printBuzz = printBuzz;
	}

	@Override
	public void run() {
		synchronized (Demo.class) {
			if(printBuzz%5==0&&printBuzz%3!=0){
				System.out.println("buzz");
			}
			
		}
		
	}

}
