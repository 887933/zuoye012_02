package zuoye08;

import com.sun.source.tree.Tree;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

public class zuoye01 {
    public static void main(String[] args) throws IOException {
        TreeMap<Integer,String> map=new TreeMap<>();
        BufferedReader br = new BufferedReader(new FileReader("sort.txt"));
        String line=br.readLine();
       while(line!=null){
          System.out.println(line);
          String[] str=line.split("\\.");
          map.put(Integer.parseInt(str[0]),str[1]);
          line=br.readLine();
       }
        System.out.println(map);
        BufferedWriter bw = new BufferedWriter(new FileWriter("sort.txt"));
        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            bw.write(entry.getKey() + "." + entry.getValue());
            bw.newLine();
        }

       br.close();
        bw.close();
    }

}
