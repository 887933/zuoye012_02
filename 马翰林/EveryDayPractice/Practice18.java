package EveryDayPractice;
public class Practice18 {
    public static void main(String[] args) {
        // 定义队伍成员
        String[] teamA = {"a", "b", "c"};
        String[] teamB = {"x", "y", "z"};

        // 可能的配对
        String[][] possibleMatches = {
                {"b", "x"}, // a 不与 x 比赛，所以 b 或 c 必须与 x 比赛
                {"a", "y"}, {"b", "y"}, {"c", "y"}, // a, b, c 都可以与 y 比赛
                {"a", "z"}, {"b", "z"} // c 不与 z 比赛，所以 a 或 b 必须与 z 比赛
        };

        // 遍历可能的配对
        for (int i = 0; i < possibleMatches.length; i++) {
            for (int j = i + 1; j < possibleMatches.length; j++) {
                for (int k = j + 1; k < possibleMatches.length; k++) {
                    // 确保所有选手都不同
                    if (isUnique(possibleMatches[i][0], possibleMatches[j][0], possibleMatches[k][0])
                            && isUnique(possibleMatches[i][1], possibleMatches[j][1], possibleMatches[k][1])) {
                        // 输出组合
                        System.out.println("Match 1: " + possibleMatches[i][0] + " vs " + possibleMatches[i][1]);
                        System.out.println("Match 2: " + possibleMatches[j][0] + " vs " + possibleMatches[j][1]);
                        System.out.println("Match 3: " + possibleMatches[k][0] + " vs " + possibleMatches[k][1]);
                        System.out.println("-----------");
                    }
                }
            }
        }
    }

    // 检查选手是否都不同
    private static boolean isUnique(String... players) {
        for (int i = 0; i < players.length; i++) {
            for (int j = i + 1; j < players.length; j++) {
                if (players[i].equals(players[j])) {
                    return false;
                }
            }
        }
        return true;
    }

}