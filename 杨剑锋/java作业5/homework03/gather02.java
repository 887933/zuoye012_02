package homework03;

import java.util.ArrayList;
import java.util.Collections;

public class gather02 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		int i=0,j=0;
		ArrayList<String> name=new ArrayList<String>();
		for(String a:list){
			String[] arr=a.split("-");
			String regex=".{3}";
			if(arr[0].matches(regex)&&arr[1].matches("[男]")&&++i<=3){
				name.add(arr[0]);
			}
			if(arr[0].matches("^林[\u4e00-\u9fa5]+$")&&j++>0&&arr[1].matches("[女]")){
				name.add(arr[0]);
			}
		}
		System.out.println("过滤后的男演员和女演员姓名:"+name);
	}
}
