public class Practice36 {
    public static void rotate(int[] nums, int k) {
        if (nums == null || nums.length == 0 || k < 0) {
            throw new IllegalArgumentException("Invalid input");
        }

        int n = nums.length;
        k = k % n; // In case the rotation count is greater than array length
        reverse(nums, 0, n - 1); // Reverse the whole array
        for (int num : nums) {
            System.out.print(num + " ");
        }
        System.out.println();
        reverse(nums, 0, k - 1); // Reverse the first k elements
        for (int num : nums) {
            System.out.print(num + " ");
        }
        System.out.println();
        reverse(nums, k, n - 1); // Reverse the rest of the array
    }

    private static void reverse(int[] nums, int start, int end) {
        while (start < end) {
            int temp = nums[start];
            nums[start] = nums[end];
            nums[end] = temp;
            start++;
            end--;
        }
    }

    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 6, 7};
        int m = 4; // Rotate by m positions
        rotate(arr, m);
        for (int num : arr) {
            System.out.print(num + " ");
        }
    }
}