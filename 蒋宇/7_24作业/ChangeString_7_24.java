package homework;

import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Scanner;

public class ChangeString_7_24 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串:");
        String str = sc.nextLine();
        LinkedHashMap<Character, Character> map = new LinkedHashMap<>();
        
        str.chars().mapToObj(c -> (char) c)
                .forEach(a -> {
                    char result = a;
                    if (Character.isUpperCase(a)) {
                        result = a == 'Z' ? 'A' : (char) (Character.toLowerCase(a) + 1);
                    } else if (Character.isLowerCase(a)) {
                        if (a >= 'a' && a <= 'c') {
                            result = '2';
                        } else if (a >= 'd' && a <= 'f') {
                            result = '3';
                        } else if (a >= 'g' && a <= 'i') {
                            result = '4';
                        } else if (a >= 'j' && a <= 'l') {
                            result = '5';
                        } else if (a >= 'm' && a <= 'o') {
                            result = '6';
                        } else if (a >= 'p' && a <= 's') {
                            result = '7';
                        } else if (a >= 't' && a <= 'v') {
                            result = '8';
                        } else if (a >= 'w' && a <= 'z') {
                            result = '9';
                        }
                    }
                    map.put(a, result);
                });

        System.out.print("转换结果为: ");
        map.forEach((k, v) -> System.out.print(v));
    }


    
    
    
//    for (int i = 0; i <arr.length ; i++) {
//      if (arr[i] >= 'A' && arr[i]<='Z') {
//        arr[i]= (char) (arr[i]+33);
//      } else if (arr[i]>='a'&&arr[i]<='c') {
//        arr[i]='2';
//      }else if (arr[i]>='d'&&arr[i]<='f') {
//        arr[i]='3';
//      }else if (arr[i]>='g'&&arr[i]<='i') {
//        arr[i]='4';
//      }else if (arr[i]>='j'&&arr[i]<='l') {
//        arr[i]='5';
//      }else if (arr[i]>='m'&&arr[i]<='o') {
//        arr[i]='6';
//      }else if (arr[i]>='p'&&arr[i]<='s') {
//        arr[i]='7';
//      }else if (arr[i]>='t'&&arr[i]<='v') {
//        arr[i]='8';
//      }else if (arr[i]>='w'&&arr[i]<='z') {
//        arr[i]='9';
//      }else {
//        arr[i]=arr[i];
//      }
//    }
//
//    System.out.println(arr);
    
    
}
