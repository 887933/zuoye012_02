package zuoye6;

public class ThreadA  extends Thread{
	int num;
	public ThreadA(int num) {
		super();
		this.num = num;
	}

	@Override
	public void run() {
		synchronized (FizzBuzz.lock) {
			if(num%3==0&&num%5==0){
				FizzBuzz.flag=true;
			}
			if(!FizzBuzz.flag){
			if(num%3==0){
				FizzBuzz.fizz();
			}	 
		}
		}

	}
}
