package homework;

import java.util.Scanner;

public class MaxNumber {
	public static void main(String[] args){
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入三个数");
	int a=sc.nextInt();
	int b=sc.nextInt();
	int c=sc.nextInt();
	System.out.println("最大的数为："+max(a,b,c));
}
	public static int max(int a,int b,int c){
		int max = a;
		if(a<b){
			if(b>c){
				max=b;
			}
			else{
				max=c;
			}
		}
		else if(a<c){
			max=c;
		}
		return max;
	}
}
