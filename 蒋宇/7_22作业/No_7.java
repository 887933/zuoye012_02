package homework;

import java.util.ArrayList;
import java.util.Scanner;

public class No_7 {
	public static void main(String[] args) {
//		我们使用[0-9] [a-z][A-Z]范围内来表示字符串
//		示例1：
//		输入ppRYYGrrYBR2258
//			YrR8RrY
//		输出：
//		Yes 8
//
//
//	 	输入:ppRYYGrrYB225
//			YrR8RrY
//		NO
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入两个字符串，先输入大串，再输入小串");
		String str1=sc.next();
		String str2=sc.next();
		int num=str1.length()-str2.length();
		char[] c1 = str1.toCharArray();
		char[] c2 = str2.toCharArray();
		ArrayList<Character> a=new ArrayList<Character>();
		ArrayList<Character> b=new ArrayList<Character>();
		boolean sign=true;
		for(char aa:c1){
			a.add(aa);
		}
		for(char aa:c2){
			b.add(aa);
		}
		for(Character cc:b){
			if(a.contains(cc)){
				a.remove(Character.valueOf(cc));
			}else{
				sign=false;
				System.out.println("NO");
				break;
			}
		}
		System.out.println(a);
		if(sign){
			System.out.println("Yes "+num);
		}

	}

}
