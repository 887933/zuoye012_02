package print;

public class AlternatePrint {
    private static final Object lock = new Object();
    private static int currentNumber = 1;
    private static char currentChar = 'A';
    private static boolean printNumber = true; // Flag to control which thread should print

    public static void main(String[] args) {
        Thread numberThread = new Thread(() -> {
            while (currentNumber <= 52) {
                synchronized (lock) {
                    while (!printNumber) { // Wait until it's this thread's turn
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    System.out.print(currentNumber);
                    System.out.print(currentNumber+1); // Print number
                    currentNumber+=2;
                    
                    printNumber = false;  // Switch turn to the character thread
                    lock.notify(); // Notify the character thread
                }
            }
        });

        Thread charThread = new Thread(() -> {
            while (currentChar <= 'Z') {
                synchronized (lock) {
                    while (printNumber) { // Wait until it's this thread's turn
                        try {
                            lock.wait();
                        } catch (InterruptedException e) {
                            Thread.currentThread().interrupt();
                        }
                    }
                    System.out.print(currentChar);  // Print character
                    currentChar++;
                    printNumber = true;  // Switch turn to the number thread
                    lock.notify(); // Notify the number thread
                }
            }
        });

        numberThread.start();
        charThread.start();
        
        try {
            numberThread.join();
            charThread.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
