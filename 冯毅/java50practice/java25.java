package java50practice;

import java.util.Scanner;

public class java25 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("输入一个五位数：");
		String s="";
		while(true){
			s=sc.next();
			if(s.length()==5){
				break;
			}
			System.out.println("输入不合法重新输入");
			sc.nextLine();
		}
		char[] ch = s.toCharArray();
		boolean b=false;
		for(int i=0,j=ch.length-1;i<=j;i++,j--){
			if(ch[i]!=ch[j]){
				b=true;
			}
		}
		if(b){
			System.out.println("这个数不是回文");
		}else{
			System.out.println("这个数是回文");
		}
	}
}
