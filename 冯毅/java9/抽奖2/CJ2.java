package java11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CJ2 extends Thread{
	static ArrayList<Integer> cj;
	static ArrayList<Integer> cj1=new ArrayList<>();
	static ArrayList<Integer> cj2=new ArrayList<>();
	static {
        List<Integer> valuesToAdd = Arrays.asList(10,6,20,50,100,200,500,800,2,80,300,700);
        cj = new ArrayList<>(valuesToAdd); 
       
    
    }
	Lock lock=new ReentrantLock();
	public void run(){
		while(true){
			lock.lock();
			try {
				Random r=new Random();
				int pi=r.nextInt(cj.size());
				Integer integer = cj.get(pi);
				cj.remove(pi);
				System.out.println(getName()+"产生了一个"+integer+"元大奖");
				if(getName().equals("抽奖箱1")){
					cj1.add(integer);
				}
				else if(getName().equals("抽奖箱2")){
					cj2.add(integer);
				}
				if(cj.size()==0){
					if(getName().equals("抽奖箱1")){
						int count=cj1.size();
						int max=0;
						int sum=0;
						for(Integer i:cj1){
							if(i>max){
								max=i;
							}
							sum+=i;
						}
						System.out.println(getName()+"总共产生了"+count+"个大奖，最高金额为"+max+"元，总计金额为"+sum+"元");
					}
					else if(getName().equals("抽奖箱2")){
						int count=cj2.size();
						int max=0;
						int sum=0;
						for(Integer i:cj2){
							if(i>max){
								max=i;
							}
							sum+=i;
						}
						System.out.println(getName()+"总共产生了"+count+"个大奖，最高金额为"+max+"元，总计金额为"+sum+"元");
					}
					break;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{lock.unlock();}
			
		}
	}
}
