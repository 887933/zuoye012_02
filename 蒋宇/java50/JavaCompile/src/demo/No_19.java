package demo;

public class No_19 {
	public static void main(String[] args) {
//		打印出如下图案（菱形）
//	    *
//	   *** 
//	  ***** 
//	 ******* 
//	  ***** 
//	   *** 
//	    * 
//	程序分析：先把图形分成两部分来看待，前四行一个规律，
//	后三行一个规律，利用双重 for循环，第一层控制行，第二层控制列。
		int a = 5;
		//正等腰三角形
		for (int i = 1; i < a; i++) {
		    for (int l = a; l > i; l--) {
		        System.out.print(" ");
		    }
		    for (int j = 0; j < 2*i-1; j++) {
		        System.out.print("*");
		    }
		    System.out.println();
		}
		//倒等腰三角形
		for (int i = a-1; i > 0; i--) {
		    for (int k = a; k >= i; k--) {
		        System.out.print(" ");
		    }
		    for (int j = 2*i-3; j > 0; j--) {
		        System.out.print("*");
		    }
		    System.out.println();
		}

	}

}
