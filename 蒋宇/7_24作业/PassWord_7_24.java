package homework;

import java.util.LinkedHashMap;
import java.util.Scanner;

public class PassWord_7_24 {
	 public static void main(String[] args) {
	        System.out.println("请输入密码：");
	        Scanner scanner = new Scanner(System.in);
	        String str = scanner.next();

	        LinkedHashMap<Character, Character> map = str.chars()                     // 将字符串转换为IntStream
	                .mapToObj(c -> (char) c)                                          // 将每个字符映射为Character对象
	                .collect(LinkedHashMap::new,                                       // 使用collect收集为LinkedHashMap
	                        (m, c) -> {
	                            char encryptedChar = encryptChar(c);                  // 加密字符
	                            m.put(c, encryptedChar);                               // 放入map
	                        },
	                        LinkedHashMap::putAll);                                    // 合并所有元素

	        System.out.println("加密后的密码为：");
	        map.forEach((key, value) -> System.out.print(value));                      // 打印加密后的密码
	    }

	    // 加密字符的方法
	    private static char encryptChar(char c) {
	        if (c >= 'A' && c <= 'Z') {
	            char c1 = Character.toLowerCase(c);
	            return (char) (c1 + 1);
	        } else if (c >= '0' && c <= '9') {
	            return c;
	        } else if (c >= 'a' && c <= 'c') {
	            return '2';
	        } else if (c >= 'd' && c <= 'f') {
	            return '3';
	        } else if (c >= 'g' && c <= 'i') {
	            return '4';
	        } else if (c >= 'j' && c <= 'l') {
	            return '5';
	        } else if (c >= 'm' && c <= 'o') {
	            return '6';
	        } else if (c >= 'p' && c <= 's') {
	            return '7';
	        } else if (c >= 't' && c <= 'v') {
	            return '8';
	        } else if (c >= 'w' && c <= 'z') {
	            return '9';
	        } else if (c == 'Z') {
	            return 'a';
	        }
	        return c;  // 如果出现其他字符，原样返回
	    }


}
