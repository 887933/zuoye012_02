package java11;

public class NUM extends Thread{
	@Override
	public void run() {
		while(true){
			synchronized (Tools.lock) {
				if(Tools.count==53){
					Tools.lock.notify();
					break;
				}else{
					if(Tools.flag){
						Tools.count++;
						if(Tools.count<53){
							System.out.print(Tools.count+" ");
						}
						if(Tools.count%2==0){
							Tools.flag=false;
							Tools.lock.notify();
						}
					}else{
						try {
							Tools.lock.wait();
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
}
