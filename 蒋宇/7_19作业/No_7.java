package demo;

public class No_7 {

	public static void main(String[] args) {
		int decimal = 10;
		String binary = decimalToBinary(decimal);
		System.out.println("十进制数 " + decimal + " 转换为二进制数为: " + binary);
	}

	public static String decimalToBinary(int decimal) {
		StringBuilder binary = new StringBuilder();
		while (decimal > 0) {
			int remainder = decimal % 2;
			binary.insert(0, remainder); // 将余数插入到字符串的最前面
			decimal = decimal / 2;
		}
		return binary.toString();
	}

}
