package homework03;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;


public class gather01 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		Collections.addAll(list, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		HashMap<String, Integer> map =new HashMap<String, Integer>();
		for(String arr:list){
			String[] a=arr.split(",");
			int age=Integer.parseInt(a[1]);
			if (age>24){
				map.put(a[0], age);
			}
		}
		System.out.println(map);
	}
}
