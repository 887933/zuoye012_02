package homework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

public class homework {
	public static void main(String[] args) {
		//作业1：斗地主优化：看牌有序
		//Doudizhu();
		
		
		/*
		 *  1.创建一个arraList集合,并添加以下字符串,字符串中前面是姓名 ,后面是年龄
		 *  "zhangsn,23"
		 *  "lisi,24"
		 *  "wangwu,25"
		 *  保留年龄大于24岁的,并将结果收集到map集合中,姓名为建,年龄为值  
		 *	ArrayList<String> arr=new ArrayList();
		 *	Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		 */
//		ArrayList<String> arr=new ArrayList();
//		Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
//		HashMap<String,Integer> map = new HashMap<String,Integer>();
//		ArrayToMap(map,arr);
//		System.out.println(map);
		
		/*
		 *  2.
		 *  一个集合中
		 *  分别存储了6个男演员
		 *  和6名女演员.
		 *  男演员只要名字为3个子的前面三人
		 *  女演员只要姓林的,并且不要第一个
		 *  把过滤后的男演员和女演员姓名结合在一起   
			ArrayList<String> list = new ArrayList<String>();
			Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		 */
//		ArrayList<String> list = new ArrayList<String>();
//		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
//		String names = ChooseActors(list);
//		System.out.println("names="+names);
		
		 /*  3.   
		 *  "dasdsawqeqw"  1 
		 *   输出:
		 *   e 
		 *   "dasdsawqeqw"  2
		 *   输出:
		 *   d
		 *   a
		 *   s
		 *   w
		 *   q
		 */
		
//		Scanner sc = new Scanner(System.in);
//		String input = "dshfadfnahd";//sc.nextLine();
//		int count = 2;
//		getCharByCount(input, count);
		
		/*
		 *  4.把任意字符串加密显示   
		 *  YUANzhi1987
		 *  加密后为
		 *  zvbo9441987
		 *  加密规则:
		 *   1.如果是大写字母变成小写字母,往后移动一位
		 *   2.小写字母通过收集输入发对应键盘来进行加密
		 *   1-1
		 *   abc-2
		 *   def-3
		 *   ghi-4
		 *   jkl-5
		 *   mno-6
		 *   pqrs-7
		 *   tuv-8
		 *   wxyz-9
		 *   0-0
		 *   3.数字不变
		 */
//		String input = "YUANzhi19!7";
//		String output = StringEncryption(input);
//		System.out.println(output);
		
		/*
			5. hashmap去重  
			合并 输出数组两个数组中都有元素则去除
			[1,2,3]
			[3,4,5,2]
			得到[1,4,5]
			
		 */
		int[] arr1 = {1,2,3,5}, arr2 = {3,4,5,2,6};
		System.out.println(eraseTheSame(arr1,arr2));
		
		
		
	

		/*
			6.ArrayList去重 
		*/
		
		
		
		
		/*
			7.我们使用[0-9] [a-z][A-Z]范围内来表示字符串
			示例1：
			输入ppRYYGrrYBR2258
				YrR8RrY
			输出：
			Yes 8
		
		 	输入:ppRYYGrrYB225
				YrR8RrY
			NO
		 */
		
		
	}

	private static Set<Integer> eraseTheSame(int[] arr1, int[] arr2) {
		HashMap<Integer,Integer> map = new HashMap<Integer,Integer>();
		int j=0;
		for(int i:arr1) {
			if(map.get(i)==null) {
				map.put(i, 1);
			}else {
				j = map.get(i)+1;
				map.put(i, j);
			}
		}
		for(int i:arr2) {
			if(map.get(i)==null) {
				map.put(i, 1);
			}else {
				j = map.get(i)+1;
				map.put(i, j);
			}
		}
		Set<Integer> set = map.keySet();
		ArrayList<Integer> remove = new ArrayList<>();
		for(int i:set) {
			if(map.get(i)>1) {
				remove.add(i);
			}
		}
		for(int i:remove) {
			map.remove(i);
		}
		Set<Integer> returnSet = map.keySet();
		return returnSet;
	}

	private static String StringEncryption(String input) {
		String output = "";
		char[] chararr = input.toCharArray();
		for(char c:chararr) {
			if(c>='A'&&c<='Z') {
				output += (char)Character.toLowerCase(c+1);
			}else if(c>='a'&&c<='z') {
				int count = 0;
				switch(c) {
				case 'a':case 'b':case 'c':
					count = 2;
					break;
				case 'd':case 'e':case 'f':
					count = 3;
					break;
				case 'g':case 'h':case 'i':
					count = 4;
					break;
				case 'j':case 'k':case 'l':
					count = 5;
					break;
				case 'm':case 'n':case 'o':
					count = 6;
					break;
				case 'p':case 'q':case 'r':case 's':
					count = 7;
					break;
				case 't':case 'u':case 'v':
					count = 8;
					break;
				case 'w':case 'x':case 'y':case 'z':
					count = 9;
					break;
				}
				output += count;
			}else if(c>='0'&&c<='9') {
				output += c;
			}else {
				return "含有非法字符！";
			}
		}
		return output;
	}

	private static void getCharByCount(String input, int count) {
		HashMap<Character,Integer> map = new HashMap<Character,Integer>();
		for(int i=0;i<input.length();i++) {
			char c = input.charAt(i);
			if(map.get(c)==null) {
				map.put(c, 1);
			}else if(map.get(c)!=null) {
				map.put(c, map.get(c)+1);
			}
		}
		System.out.println(map);
		Set<Character> keyset = map.keySet();
		for(Character c:keyset) {
			if(map.get(c)==count) {
				System.out.println(c);
			}
		}
	}

	private static String ChooseActors(ArrayList<String> list) {
		String names="",name,gender,age;
		String[] strings;
		int flagMale = 0,flagFemale = 0,flagFirstName=0;
		for(int i=0;i<list.size();i++) {
			strings = list.get(i).split("-");
			name = strings[0];
			gender = strings[1];
			age = strings[2];
			if(gender.equals("男")) {
				if(name.length()==3&&flagMale<3) {
					if(flagFirstName==0) {
						flagFirstName++;
					}else {
						names += ",";
					}
					names += name;
					flagMale ++;
				}
				
			}else {
				
				if(name.contains("林")) {
					if(flagFemale<=0) {
						flagFemale++;
						continue;
					}else {
						if(flagFirstName==0) {
							flagFirstName++;
						}else {
							names += ",";
						}
						names += name;
					}
					
				}
				
			}
			
		}
		return names;
	}

	private static void ArrayToMap(HashMap<String, Integer> map, ArrayList<String> arr) {
		String key;
		int value;
		String[] strings;
		for(int i=0;i<arr.size();i++) {
			strings = arr.get(i).split(",");
			key = strings[0];
			value = Integer.parseInt(strings[1]);
			if(value>=24) {
				map.put(key, value);
			}
		}
	}

	private static void Doudizhu() {
		ArrayList<String> poker=new ArrayList<String>();
		String[] colors= {"♥","♠","♦","♣"};
		String[] numbers = {"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("大王");
		poker.add("小王");
		
		for(String number:numbers) {
			for(String color:colors) {
				poker.add(color+number);
			}
		}
		
		Collections.shuffle(poker);

		Player p1 = new Player();p1.setName("Wang");
		Player p2 = new Player();p2.setName("Li");
		Player p3 = new Player();p3.setName("Liu");
		ArrayList<String> dipai = new ArrayList<String>();
		//摸牌
		for(int i=0;i<poker.size();i++) {
			String p = poker.get(i);
			if(i>=51) {
				dipai.add(p);
			}else if(i%3==0) {
				p1.getPoker().add(p);
			}else if(i%3==1) {
				p2.getPoker().add(p);
			}else if(i%3==2) {
				p3.getPoker().add(p);
			}
		}
		
		//使用hashmap键值对排序
		HashMap<String,Integer> map = new HashMap<>();
		
		for(int i=1;i<=numbers.length;i++) {
			map.put(numbers[i-1], i);
		}
		
		//看牌前排序
//		Collections.sort(p1.getPoker(),new Comparator<String>() {
//
//			@Override
//			public int compare(String o1, String o2) {
//				if(o1.charAt(1)!=o2.charAt(1)) {
//					for(int i=0;i<8;i++) {
//						
//					}
//				}
//				return 0;
//			}
//			
//		});
		
		//看牌
		System.out.println(p1.getPoker());
		System.out.println(p2.getPoker());
		System.out.println(p3.getPoker());
		System.out.println("底牌："+dipai);
	}
}
