package demo1;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;
//排序
public class Sort01 {
	public static void main(String[] args) throws IOException {
		BufferedReader br= new BufferedReader(new FileReader("sort.txt"));
		
		ArrayList<String> list = new ArrayList<>();
		String line;
		while((line =br.readLine())!=null){
			System.out.println(line);
			list.add(line);
		}
		Collections.sort(list);
		BufferedWriter bw= new BufferedWriter(new FileWriter("sort.txt"));
		for(String s:list){
			bw.write(s);
			bw.newLine();
			System.out.println(s);
		}
		br.close();
		bw.close();
	}

}
