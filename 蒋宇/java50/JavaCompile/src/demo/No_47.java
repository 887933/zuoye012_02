package demo;

import java.util.ArrayList;
import java.util.Scanner;

public class No_47 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入7个1-50以内的整数:");
		ArrayList<Integer> list=new ArrayList<>();
		for(int i=0;i<7;i++){
			int num=sc.nextInt();
			if(num<1 || num>50){
				System.out.println("请重输");
				i--;
			}else{
				list.add(num);
			}
		}
		for(int j=0;j<list.size();j++){
			for(int k=0;k<list.get(j);k++){
				System.out.print("*");
			}
			System.out.println("");
		}
	}
}
