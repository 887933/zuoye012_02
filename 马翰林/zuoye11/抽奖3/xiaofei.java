package zuoye11;

public class xiaofei extends Thread{

    public void run() {
        synchronized (desk.lock) {
            while (desk.counter != 0) {
                if (desk.flag == true) {
                    System.out.println("我消费了一个");
                    desk.counter--;
                    desk.flag = false;
                    desk.lock.notify();
                } else {
                    try {
                        desk.lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
        }
    }
}
