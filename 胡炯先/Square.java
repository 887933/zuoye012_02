package homework;

import java.util.Scanner;

public class Square {
	public static void main(String[] args){
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		for(int i=1;i<num/2;i++){
			if(i*i==num){
				System.out.println(num + "的平方根是" + i);
				break;
			}
			else if(i*i>num){
				System.out.println(num + "的平方根是" + i +"-"+ (i-1) + "之间");
				break;
			}
		}
	}
}
