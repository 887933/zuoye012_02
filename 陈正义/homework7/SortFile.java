package homework7;

import java.io.*;
import java.nio.file.*;
import java.util.Arrays;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SortFile {
    public static void main(String[] args) {
        String inputPath = "C:\\Users\\LENOVO\\Desktop\\新建文件夹\\新建文件夹\\ceshi.txt";
        String outputPath = inputPath;
        try (Stream<String> lines = Files.lines(Paths.get(inputPath))) {
            String sortedData = lines
                    .flatMap(line -> Arrays.stream(line.split("-")))
                    .mapToInt(Integer::parseInt)
                    .sorted()
                    .mapToObj(String::valueOf)
                    .collect(Collectors.joining("-"));
            Files.write(Paths.get(outputPath), sortedData.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
