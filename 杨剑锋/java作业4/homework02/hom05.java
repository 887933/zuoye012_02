package homework02;

public class hom05 {
	 public static void main(String[] args) {
	        String original = "abcdefg";
	        String reversed = reversePart(original, 1, 5); 
	        System.out.println(reversed);
	    }

	    public static String reversePart(String str, int start, int end) {
	        if (str == null || str.length() <= 1 || start < 0 || end >= str.length() || start > end) {
	            return str;
	        }

	        StringBuilder sb = new StringBuilder(str);
	        while (start < end) {
	            char temp = sb.charAt(start);
	            sb.setCharAt(start, sb.charAt(end));
	            sb.setCharAt(end, temp);
	            start++;
	            end--;
	        }
	        return sb.toString();
	    }
}
