package java9;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Random;

public class CallStudent02 {
	public static void main(String[] args) throws Exception {
		BufferedReader br=new BufferedReader(new FileReader("student.txt"));
		BufferedReader br2=new BufferedReader(new FileReader("studentcount.txt"));
		ArrayList<Student> studentnan=new ArrayList<Student>();
		ArrayList<Student> studentnv=new ArrayList<Student>();
		String line;
		String line1;
		while((line=br.readLine())!=null){
			String[] str=line.split("-");
			if(str[1].equals("男")){
				studentnan.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
			}
			else{
				studentnv.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
			}
		}

		Random rd=new Random();
		int weight=rd.nextInt(11);
		int countnan=0;
		int countnv=0;



		if(weight<7){

			while((line1=br2.readLine())!=null){
				String[] str=line1.split("=");
				if(str[0].equals("countnan")){
					countnan=Integer.parseInt(str[1])+1;
				}
				if(str[0].equals("countnv")){
					countnv=Integer.parseInt(str[1]);
				}
			} 

			int number=rd.nextInt(studentnan.size());
			System.out.println(studentnan.get(number).getName()+"-男,打印男生和女生的比例为 "+countnan+":"+countnv);
		}
		else{
			while((line1=br2.readLine())!=null){
				String[] str=line1.split("=");
				if(str[0].equals("countnan")){
					countnan=Integer.parseInt(str[1]);
				}
				if(str[0].equals("countnv")){
					countnv=Integer.parseInt(str[1])+1;
				}
			}
			br2.close();
			int number=rd.nextInt(studentnv.size());
			System.out.println(studentnv.get(number).getName()+"-女,打印男生和女生的比例为 "+countnan+":"+countnv);
		}
		BufferedWriter bw=new BufferedWriter(new FileWriter("studentcount.txt"));
		bw.write("countnan=" + countnan+"\n");  
        bw.newLine();  
        bw.write("countnv=" + countnv+"\n");  
        bw.close();
        br.close();
		br2.close();
	}
}
