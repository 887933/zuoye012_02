package demo04;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

public class DouDiZhu {
	public static void main(String[] args) {
		//准备牌
		ArrayList<Integer> poker=new ArrayList<Integer>();
		String[] colors={"♠","♥","♣","♦"};
		String[] numbers={"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
//		poker.add("大王");
//		poker.add("小王");
		
		TreeMap<String,Integer> map=new TreeMap<String,Integer>();
		Integer s=3;
		map.put("大王", 1);
		map.put("小王", 2);
		for(int i=0;i<numbers.length;i++){
			for(int j=0;j<colors.length;j++){
				map.put(colors[j]+numbers[i],s++);
				
			}
		}
		Set<Entry<String, Integer>> entrySet = map.entrySet();
		for(Entry<String, Integer> aa:entrySet){
			poker.add(aa.getValue());
		}
		
		
//		for(String number:numbers){
//			for(String color:colors){
//				poker.add(color+number);
//			}
//		}
//		System.out.println(poker);
		//洗牌
		Collections.shuffle(poker);
		System.out.println(poker);
//		
		Player p1=new Player();p1.setName("老王");
		Player p2=new Player();p2.setName("老李");
		Player p3=new Player();p3.setName("老六");
		ArrayList<Integer> dipai=new ArrayList<Integer>();
		//摸牌
		for(int i=0;i<poker.size();i++){
			//获取每一张的牌
			Integer p = poker.get(i);
			if(i>=51){
				dipai.add(p);
			}else if(i%3==0){
				p1.getPoker().add(p);
			}else if(i%3==1){
				p2.getPoker().add(p);
			}else if(i%3==2){
				p3.getPoker().add(p);
			}
		}
		Collections.sort(p1.getPoker());
		Collections.sort(p2.getPoker());
		Collections.sort(p3.getPoker());
		Collections.sort(dipai);
		Set<Entry<String, Integer>> entrySet1 = map.entrySet();
		//看牌
		//将数字牌转换成对应的牌面，并打印玩家的牌
		System.out.print(p1.getName()+"的牌是：");
        for (Integer card : p1.getPoker()) {
            for (Entry<String, Integer> entry : entrySet1) {
                if (entry.getValue().equals(card)) {
                    System.out.print(entry.getKey() + " ");
                }
            }
        }
        System.out.println();
        System.out.print(p2.getName()+"的牌是：");

        for (Integer card : p2.getPoker()) {
            for (Entry<String, Integer> entry : entrySet1) {
                if (entry.getValue().equals(card)) {
                    System.out.print(entry.getKey() + " ");
                }
            }
        }
        System.out.println();
        System.out.print(p3.getName()+"的牌是：");
        for (Integer card : p3.getPoker()) {
            for (Entry<String, Integer> entry : entrySet1) {
                if (entry.getValue().equals(card)) {
                    System.out.print(entry.getKey() + " ");
                }
            }
        }
        System.out.println();

        System.out.print("底牌: ");
        for (Integer card : dipai) {
            for (Entry<String, Integer> entry : entrySet1) {
                if (entry.getValue().equals(card)) {
                    System.out.print(entry.getKey() + " ");
                }
            }
        }
	}
}