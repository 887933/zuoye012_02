package homework3;

import java.util.ArrayList;
import java.util.Random;
class Manager extends User {
    public Manager(String name, int money) {
        super(name, money);
    }

    public ArrayList<Integer> send(int totalMoney, int count) {
        ArrayList<Integer> redList = new ArrayList<>();
        int leftMoney = super.getMoney();
        if (totalMoney > leftMoney) {
            System.out.println("����");
            return redList;
        }
        
        super.setMoney(leftMoney - totalMoney);
        int rest = totalMoney;
        while (count > 1) {
            int avg = rest / count;
            int min = 1;
            int max = avg * 2;
            int price = new Random().nextInt(max) + min;
            redList.add(price);
            rest -= price;
            count--;
        }
        
        int last = rest;
        redList.add(last);
        return redList;
    }

    public void show() {
        System.out.println("Manager: " + getName() + ", Money: " + getMoney());
    }
}
