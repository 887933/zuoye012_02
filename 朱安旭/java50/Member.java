package demo01;

import java.util.ArrayList;
import java.util.Random;

public class Member extends Usr {
	public Member(String username, int moeny) {
		super(username,moeny);
	}
	public Member() {
	}
	
	public void receive(ArrayList<Integer> redList){
		int index = new Random().nextInt(redList.size());
		//根据索引  ，从集合中删除这个元素 ，得到删除得红包
		Integer delta = redList.remove(index);
		//当前成员自己本来有多少钱
		int money=this.getMoeny()+delta;
		this.setMoeny(money);
	}
	
	
}
