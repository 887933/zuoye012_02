package demo;

import java.util.Scanner;

public class No_29 {
	public static void main(String[] args) {
		//求一个3*3矩阵对角线元素之和
		//程序分析：利用双重for循环控制输入二维数组，
		//再将a[i][i]累加后输出。
		Scanner sc=new Scanner(System.in);
        int[][] a=new int[3][3];
        int sum = 0;
        System.out.println("请输入数组的值");
        for(int i=0;i<a.length;i++){
        	for(int j=0;j<a[i].length;j++){
        		a[i][j]=sc.nextInt();
        		sum+=a[i][j];
        	}
        }
        System.out.println(sum);
        
	}

}
