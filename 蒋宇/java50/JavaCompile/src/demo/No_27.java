package demo;

public class No_27 {
	public static void main(String[] args) {
		//求100之内的素数
		for(int i=1;i<100;i++){
			if(judge(i)){
				System.out.println(i);
			}
		}
	}
	private static boolean judge(int i) {
		if(i==1||i%2==0&&i!=2){
			return false;
		}
		for(int j=2;j<=Math.sqrt(i);j++){
			if(i%j==0){
				return false;
			}
		}
		return true;
		
	}

}
