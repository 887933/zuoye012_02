package homework12;

public class procedureA07 extends Thread{
	static int num=0;
	static final Object lock = new Object(); 
	@Override
	public void run() {
		for(int i=1;i<=52;){
			synchronized (lock) {
				if(num==2){
					try {
						lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
					System.out.print(i);
					i++;
					num++;
					lock.notify();
				}
			}
		}
	}
}
