package homework12;
//.子进程先执行三次，主进程再执行五次，然后这个过程执行三次
public class homework05 {
	public static void main(String[] args) {
		for(int i=0;i<3;i++){
			for(int j=0;j<3;j++){
				new Thread(new Runnable() {
					
					@Override
					public void run() {
						System.out.println("子程序");
					}
				}).start();
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int j=0;j<5;j++){
				System.out.println("主程序");
			}
		}
	}
}
