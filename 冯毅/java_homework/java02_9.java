package dame;

import java.util.Random;

public class java02_9 {
    public static void main(String[] args) {
		int[] arr={1,2,3,4,5};
		Random r=new Random();
		for(int i=arr.length-1;i>0;i--){
			int index=r.nextInt(i+1);
			int temp=arr[i];
			arr[i]=arr[index];
			arr[index]=temp;
		}
		System.out.print("打乱后的数组为：");
		for(int i=0;i<arr.length;i++){
			System.out.print(arr[i]);
		}
	}
}
