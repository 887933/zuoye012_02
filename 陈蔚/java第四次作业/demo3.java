package zuoye719;

import java.util.ArrayList;
import java.util.Scanner;
//随机输入字符串，判断是否是回文
public class demo3 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入字符串:");
	String str=sc.nextLine();
	ArrayList<Character> list=new ArrayList<>();
	for(int i=0;i<str.length();i++){
	char ch=str.charAt(i);
	list.add(ch);
	}
	boolean ishuiwen=true;
	for(int left=0,right=list.size()-1;left<right;left++,right--){
		if(list.get(left)!=list.get(right)){
			ishuiwen=false;
		}
	}
	if(ishuiwen){
		System.out.println(str+"是回文");
	}else{
		System.out.println(str+"不是回文");
	}
}
}
