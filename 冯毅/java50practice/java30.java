package java50practice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class java30 {
	public static void main(String[] args) {
		
		ArrayList<Integer> nums=new ArrayList<>(Arrays.asList(1,6,9,45,2,98,7,55));
		nums.sort(null);
		System.out.println("原数组：");
		System.out.println(nums);
		System.out.print("输入一个数字:");
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		nums.add(num);
		nums.sort(null);
		System.out.println("新数组：");
		System.out.println(nums);
	}
}
