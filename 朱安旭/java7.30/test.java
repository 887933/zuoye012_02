package demo01;

import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.channels.FileLock;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class test {
    public static void main(String[] args) throws IOException, Exception {
        // 获取当前时间日期
        LocalDateTime currentDateTime = LocalDateTime.now();

        // 定义日期时间格式化器（可选）
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
//
//        // 格式化日期时间（可选）
       //String formattedDateTime = currentDateTime.format(formatter);

        // 输出当前时间日期
        System.out.println("当前时间日期: " + currentDateTime);
        File lockFile = new File("program.lock");
        FileLock lock = new RandomAccessFile(lockFile, "rw").getChannel().tryLock();
		if (lock != null) {
		    System.out.println("程序运行中...");

		    // 模拟程序运行10秒钟
		    Thread.sleep(10000);

		    // 释放文件锁
		    lock.release();
		    System.out.println("程序运行结束，释放文件锁。");

		    // 删除文件锁
		    lockFile.delete();
		}else {
            System.out.println("程序已经在运行中，请稍后再试。");
        }
    } 
}