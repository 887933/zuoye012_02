package demo04;

import java.util.ArrayList;

public class Player {
	private String name;
	private ArrayList<Integer> poker=new ArrayList<Integer>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<Integer> getPoker() {
		return poker;
	}
	public void setPoker(ArrayList<Integer> poker) {
		this.poker = poker;
	}
	@Override
	public String toString() {
		return "Player [name=" + name + ", poker=" + poker + "]";
	}
	
}
