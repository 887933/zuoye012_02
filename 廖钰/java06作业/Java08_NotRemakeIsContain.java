package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Java08_NotRemakeIsContain {
	public static void main(String[] args) {
		/*
			7.我们使用[0-9] [a-z][A-Z]范围内来表示字符串
			示例1：
			输入ppRYYGrrYBR2258
				YrR8RrY
			输出：
			Yes 8
		
		 	输入:ppRYYGrrYB225
				YrR8RrY
			NO
		 */
		String input1 = "ppRYYGrrYBR225899dkwu8";
		String input2 = "YrR8RrY99";
		isContain(input1,input2);
				
	}
	
	private static void isContain(String input1, String input2) {
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		char[] addarr = input1.toCharArray();
		char[] deletearr = input2.toCharArray();
		
		for(char c:addarr) {
			if(map.get(c)==null) {
				map.put(c, 1);
			}else {
				int j=map.get(c);
				map.put(c, j+1);
			}
		}
		
		for(char c:deletearr) {
			if(map.get(c)==null) {
				System.out.println("NO");
				return;
			}else if(map.get(c)==1){
				map.remove(c);
			}else {
				int j = map.get(c);
				map.put(c, j-1);
			}
		}
		int sum=0;
		Set<Character> keyset = map.keySet();
		for(char c:keyset) {
			sum += map.get(c);
		}
		System.out.println("YES "+sum);
	}
}
	