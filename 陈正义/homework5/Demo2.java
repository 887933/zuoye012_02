//package homework5;
//2.
//*  一个集合中
//*  分别存储了6个男演员
//*  和6名女演员.
//*  男演员只要名字为3个字的前面三人
//*  女演员只要姓林的,并且不要第一个
//*  把过滤后的男演员和女演员姓名结合在一起   
//	ArrayList<String> list = new ArrayList<String>();
//Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", 
//"张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35",
//"谢广坤-女-41","林婷-女-22","林立-女-23");

package homework5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Demo2 {
    public static void main(String[] args) {
        // 创建集合并添加演员信息
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list,
                "张无忌-男-15", "周芷若-女-14", "赵敏-女-13",
                "张强-男-20", "张三丰-男-100", "张翠山-男-40",
                "张良-男-35", "王二麻子-男-35",
                "谢广坤-女-41", "林婷-女-22", "林立-女-23");

        // 创建男演员和女演员的完整信息列表
        List<String> maleActors = new ArrayList<>();
        List<String> femaleActors = new ArrayList<>();

        // 遍历原始列表，筛选出符合条件的男演员和女演员的完整信息
        for (String actorInfo : list) {
            String[] parts = actorInfo.split("-");
            String name = parts[0];
            if (parts[1].equals("男") && name.length() == 3) {
                // 男演员，名字为3个字
                maleActors.add(actorInfo); // 添加完整信息
            } else if (parts[1].equals("女") && name.startsWith("林") && !name.equals("周芷若")) {
                // 女演员，姓林，且不是第一个
                femaleActors.add(actorInfo); // 添加完整信息
            }
        }

        // 合并男演员和女演员的完整信息列表
        List<String> combinedList = new ArrayList<>(maleActors);
        combinedList.addAll(femaleActors);

        // 输出结果
        System.out.println("过滤后的演员名单：");
        for (String actor : combinedList) {
            System.out.print(actor+"  ");
        }
    }
}