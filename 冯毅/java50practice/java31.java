package java50practice;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Scanner;

public class java31 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入数组长度：");
		int length=sc.nextInt();
		ArrayList<Integer> nums=new ArrayList<>();
		for(int i=0;i<length;i++){
			System.out.print("输入一个数字：");
			int num=sc.nextInt();
			nums.add(num);
		}
		nums.sort(new Comparator<Integer>() {

			@Override
			public int compare(Integer o1, Integer o2) {
				return o2-o1;
			}
		});
		System.out.println("排序后的数组：");
		System.out.println(nums);
	}
}
