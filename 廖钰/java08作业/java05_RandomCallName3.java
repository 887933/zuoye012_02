package homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;

public class java05_RandomCallName3 {
	public static void main(String[] args) throws IOException {
		/*
		 * 作业3：
		 * 每次打印一个同学的名字
		 * 每逢3的倍数只显示 张三彤X
		 * 
		 */
		int totalTimes = 100;
		for(int i=0;i<totalTimes;i++) {
			if((i+1)%3==0) {
				System.out.println("张三珊怡");
			}else {
				randomCallName1("names.txt");				
			}
		}
	}
	
	private static void randomCallName1(String string) throws IOException {
		FileReader fr = new FileReader(string);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		ArrayList<String> list = new ArrayList<String>();
		while((line = br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		Random r = new Random();
		int num = r.nextInt(list.size());
		String name = list.get(num);
		System.out.println(name.split("-")[0]);
	}
}
