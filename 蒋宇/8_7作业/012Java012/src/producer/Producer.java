package producer;

public class Producer extends Thread {  
    private BaoZi bz;  
  
    public Producer(BaoZi bz) {  
        super();  
        this.bz = bz;  
    }  
  
    @Override  
    public void run() {  
        while (true) { 
            synchronized (bz) { 
                while (bz.count == bz.MAX_CAPACITY) { 
                    try {  
                        bz.wait(); 
                    } catch (InterruptedException e) {  
                        Thread.currentThread().interrupt(); // 恢复中断状态  
                        return; // 退出线程  
                    }  
                }  
  
                // 生产包子  
                System.out.println("包子铺正在做包子");  
                bz.count++; // 增加包子数量  
                bz.flag = true; // 标记有包子可吃（尽管这个标志在简单场景下可能不是必需的）  
                bz.notify(); // 唤醒可能正在等待的消费者  
  
                
            }  
  
            
            // try { Thread.sleep(1000); } catch (InterruptedException e) { ... }  
        }  
    }  
}  
  

