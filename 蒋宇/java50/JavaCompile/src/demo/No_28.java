package demo;

import java.nio.channels.SelectableChannel;
import java.util.Arrays;

public class No_28 {
	public static void main(String[] args) {
		// 对10个数进行排序
		// 程序分析：可以利用选择法，即从后9个比较过程中，
		// 选择一个最小的与第一个元素交换， 下次类推，
		// 即用第二个元素与后8个进行比较，并进行交换。
		int[] arr = { 12, 65, 89, 43, 2, 87, 49, 62, 77, 93 };
		//BubbleSort(arr);
		selectSort(arr);

	}

	public static void BubbleSort(int[] arr) {
		// 冒泡排序最优解
		
		while(true){
			int last = 0;
			for (int i = 0; i < arr.length - 1; i++) {
				
				if (arr[i] > arr[i + 1]) {
					swap(arr, i, i + 1);
					last=i;
				}
			}
            if(last==0){
				break;
			}
		
		}
		System.out.println(Arrays.toString(arr));
	}
	//选择排序12, 65, 89, 43, 2, 87, 49, 62, 77, 93
//	public static void selectSort(int[] arr){
//		for(int k=0;k<arr.length-1;k++){//0-8
//			int j=0;
//			int sign=arr[k];
//			boolean x = false;
//			for(int i=k;i<arr.length-1;i++){
//				if(arr[i+1]<sign){
//					sign=arr[i+1];
//					j=i+1;
//					x=true;
//				}
//			}
//			if(x){
//				swap(arr,k,j);
//			}
//			
//		}
//		System.out.println(Arrays.toString(arr));
//	}
	public static void selectSort(int[] arr){
		for(int k=0;k<arr.length;k++){//0-8
			int j=k;
			
			for(int i=k;i<arr.length;i++){
				if(arr[i]<arr[j]){
					j=i;
				}
			}
			swap(arr,k,j);
			
		}
		System.out.println(Arrays.toString(arr));
	}

	private static void swap(int[] a, int i, int j) {
		int t = a[i];
		a[i] = a[j];
		a[j] = t;
	}

}
