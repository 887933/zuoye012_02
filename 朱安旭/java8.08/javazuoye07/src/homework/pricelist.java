package homework;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Random;
public class pricelist {
	static ArrayList<Integer> list=new ArrayList<>();
	static{
		list.add(10);
		list.add(6);
		list.add(20);
		list.add(50);
		list.add(100);
		list.add(200);
		list.add(500);
		list.add(800);
		list.add(2);
		list.add(80);
		list.add(300);
		list.add(700);
	}
	public synchronized static Integer outprice(){
		if(list.isEmpty())
		{
			return 0;
		}
		Random r=new Random();
		int number=r.nextInt(list.size());
		Integer price=list.remove(number);
		return price;
	}
}
