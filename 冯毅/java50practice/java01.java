package java50practice;

import java.util.Scanner;

public class java01 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入月数：");
		int month=sc.nextInt();
		System.out.println("第"+month+"个月有"+getRabbit(month)+"对兔子.");
	}
	public static long getRabbit(int month){
		if(month==1||month==2){
			return 1;
		}
		long[] r=new long[month+1];
		r[1]=1;
		r[2]=1;
		for(int i=3;i<month+1;i++){
			r[i]=r[i-1]+r[i-2];
		}
		return r[month];
	}
}
