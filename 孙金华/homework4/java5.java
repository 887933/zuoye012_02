package homework4;
//将一个字符串进行反转，可以指定部分反转
public class java5 {
	public static void main(String[] args) {
		String a="qwertyuwr";
		System.out.println(flip(a,2,5));
	}
	public static String flip(String a,int left,int right) {
		char[] arr=new char[a.length()];
		for(int i=0,n=0;i<a.length();i++) {
			if(i<left||i>right) {
				arr[i]=a.charAt(i);
			}
			else {
				arr[i]=a.charAt(right-n);
				n++;
			}
		}
		return new String(arr);
	}
	public static String flip(String a) {
		char[] arr=new char[a.length()];
		for(int i=0;i<a.length();i++) {
			arr[i]=a.charAt(a.length()-1-i);
		}
		return new String(arr);
	}
}
