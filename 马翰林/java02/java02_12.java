package java02;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class java02_12 {
    public static void main(String[] args) {
        ArrayList<student> students=new ArrayList<>();
        Scanner sc=new Scanner(System.in);
        addStudentName(students);
        printStudentName(students);
        randomStudentName(students);
    }
    public static  void printStudentName(ArrayList<student> students){
        for(int i=0;i<students.size();i++){
            student s=students.get(i);
            System.out.println("第"+(i+1)+"个学生名称:"+s.getName()+"年龄"+s.getAge());
        }
    }
    public static void addStudentName(ArrayList<student> students) {
        Scanner sc=new Scanner(System.in);
        System.out.println("请输入要添加的学生数量：");
        int numStudents = sc.nextInt();
        sc.nextLine(); // 清除输入缓冲区的换行符

        for (int i = 0; i < numStudents; i++) {
            System.out.println("请输入第" + (i + 1) + "个学生的名称：");
            String name = sc.nextLine();
            System.out.println("请输入第" + (i + 1) + "个学生的年龄：");
            int age = sc.nextInt();
            sc.nextLine();
            student s = new student(name,age);
            students.add(s);
        }
    }
    public static String randomStudentName(ArrayList<student> students){
        int index= new Random().nextInt(students.size());
        student s=students.get(index);
        return "幸运儿"+s.getAge()+s.getName();
    }
}
