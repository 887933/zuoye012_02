package hom05;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
/*作业2：文本文件中有以下的数据
2-1-9-4-7-8

将数据进行排序后写会去
1-2-4-7-8-9*/

public class homework01 {
	public static void main(String[] args) throws Exception {
		
		BufferedReader fos=new BufferedReader(new FileReader("read.txt"));
		ArrayList<String> list = new ArrayList<>();
		String line;
		while((line=fos.readLine())!=null){
			list.add(line);
		}
		String[] lines=list.get(0).split("-");
		Arrays.sort(lines);
		StringBuilder str = new StringBuilder();
		for(int i=0;i<lines.length;i++){
			str.append(lines[i]);
			if(i!=lines.length-1){
				str.append('-');
			}
		}
		FileWriter fis=new FileWriter("read.txt");
		fis.write(str.toString());
		fis.close();
	}
}
