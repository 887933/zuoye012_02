package java07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class java6 {
	public static void main(String[] args) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		Collections.addAll(a, 1,5,1,6,4,9,5,5,3,6,4);
		 TreeSet<Integer> collect = a.stream().distinct().collect(Collectors.toCollection(TreeSet::new));
		collect.forEach(System.out::print);
	}
}
