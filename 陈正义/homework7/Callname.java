package homework7;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import homework2.Student;
//键盘录入学生姓名和年龄，年龄还要为18-25岁，超出这个范围，则抛出异常，需要重新录入，一直到录入正确为止.
//比如：年龄超出范围，了如果年龄的时候录入的abc情况也不行
public class Callname {
	public static void main(String args[]) {
		ArrayList<Student>list=new ArrayList<Student>();
		setStudentName(list);
		printStudentName(list);
		Student st=callName(list);
		System.out.println("幸运儿:"+st.getName()+" 年龄"+st.getAge()+" id:"+st.getId());
	}

	private static Student callName(ArrayList<Student> list) {
		Random r=new Random();
		int i=r.nextInt(list.size());
		Student s=list.get(i);
		return s;
	}

	private static void printStudentName(ArrayList<Student> list) {
		for(int i=0;i<list.size();i++) {
			Student s=list.get(i);
			System.out.println("学生"+(i+1)+"学生姓名:"+s.getName()+" 学生年龄"+s.getAge()+" id:"+s.getId());
		}
	}

	private static void setStudentName(ArrayList<Student> list) {
	    Scanner sc = new Scanner(System.in);
	    System.out.println("请输入学生数量：");
	    int num = sc.nextInt();
	    for (int i = 0; i < num; i++) {
	        System.out.println("请输入学生姓名：");
	        String name = sc.next();
	        int age = 0;
	        boolean validAge = false;
	        while (!validAge) {
	            System.out.println("请输入学生年龄（18-25岁）：");
	            if (sc.hasNextInt()) {
	                age = sc.nextInt(); 
	                if (age >= 18 && age <= 25) {
	                    validAge = true;
	                } else {
	                    System.out.println("年龄超出范围，请重新输入！");
	                }
	            } else {
	                System.out.println("输入包含非数字字符，请重新输入年龄！");
	                sc.next(); 
	            }
	        }
	        int id = i + 1;
	        Student s = new Student(id, age, name);
	        list.add(s);
	    }
	    sc.close();
	}
}
