package demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class java01 {
	public static void main(String[] args) {
		ArrayList<Person> list=new ArrayList();
		list.add(new Person("张三",21));
		list.add(new Person("李四",24));
		list.add(new Person("丁真",26));
		list.add(new Person("珍珠",27));
		list.add(new Person("锐刻五",30));
        //stream
		list.stream()
        .filter(person -> person.getAge() > 24)
        .collect(Collectors.toList()).forEach(System.out::print);
		//方法引用
		System.out.println("");
		list.stream().filter(java01::compare)
		.collect(Collectors.toList()).forEach(System.out::print);
		}

	public static boolean compare(Person p){
		return p.getAge()>24;
	}
		
}



