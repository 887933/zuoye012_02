package demo;

import java.util.Scanner;

public class No_44 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一个大于2的偶数:");
		int num=sc.nextInt();
		if(num<=2 || num%2!=0){
			System.out.println("你输入的是奇数");
		}else{
			for(int i=2;i<num;i++){
				if(isPrime(i)){
					for(int j=2;j<num;j++){
						if(isPrime(j)){
							if(i+j==num){
								System.out.println(num+"="+i+"+"+j);
							}
						}
					}
				}
			}

	}

}

	private static boolean isPrime(int number) {
		// TODO Auto-generated method stub
		for (int j = 2; j <= Math.sqrt(number); j++) {
            if (number % j == 0) return false;
        }
        return true;
	}

	}
