package demo1;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class java05 {
	

	public static void main(String[] args) {
		HashMap<Integer, Integer> hashMap = new HashMap<>();
		int[] arr1 = new int[]{2,3,4,5};
		int[] arr2 = new int[]{1,2,3};
		int count=0;
		Arrays.stream(arr1).forEach(num->hashMap.put(num,hashMap.getOrDefault(num, 0)+1));
		Arrays.stream(arr2).forEach(num->hashMap.put(num,hashMap.getOrDefault(num, 0)+1));
		
		
		//stream
		System.out.print("[");
		hashMap.entrySet().stream()
		.filter(entry->entry.getValue()==1)
		.map(Map.Entry::getKey).forEach(new java05()::print);
		System.out.print("]");
		
		System.out.println("");
		
		
		//����
		System.out.print("[");
		hashMap.entrySet().stream().filter(java05::judge)
		.map(Map.Entry::getKey).forEach(new java05()::print);
		 System.out.print("]");
		 
		 

	}
	 int count=0;
	public  void print(Integer c){
		
		if(count==0){
			System.out.print(c);
			count=1;
		}else{
			System.out.print(","+c);
		}
	}
		public  static boolean judge(Entry<Integer, Integer> entry){
			return entry.getValue()==1;
		}
}
