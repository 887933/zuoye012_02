package java9;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Random;

public class CallStudent01 {
	public static void main(String[] args) throws Exception {
		BufferedReader b=new BufferedReader(new FileReader("student.txt"));
		ArrayList<Student> student=new ArrayList<Student>();
		String line;
		while((line=b.readLine())!=null){
			String[] str=line.split("-");
			student.add(new Student(str[0],Integer.parseInt(str[2]),str[1]));
		}
		b.close();
		Random rd=new Random();
		int number=rd.nextInt(100)+1;
		System.out.println(student.get(number).getName());
	}
}
