package homework03;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

/*  YUANzhi1987
*  加密后为
*  zvbo9441987
*  加密规则:
	 *   1.如果是大写字母变成小写字母,往后移动一位
	 *   2.小写字母通过收集输入发对应键盘来进行加密
	 *   1-1
	 *   abc-2
	 *   def-3
	 *   ghi-4
	 *   jkl-5
	 *   mno-6
	 *   pqrs-7
	 *   tuv-8
	 *   wxyz-9
	 *   0-0
	 *   3.数字不变
	 */

public class gather04 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入加密前密码：");
		String front=sc.next();
		HashMap<Character, Character> map = new HashMap<Character, Character>();
		map.put('a', '2'); map.put('b', '2'); map.put('c', '2');
        map.put('d', '3'); map.put('e', '3'); map.put('f', '3');
        map.put('g', '4'); map.put('h', '4'); map.put('i', '4');
        map.put('j', '5'); map.put('k', '5'); map.put('l', '5');
        map.put('m', '6'); map.put('n', '6'); map.put('o', '6');
        map.put('p', '7'); map.put('q', '7'); map.put('r', '7'); map.put('s', '7');
        map.put('t', '8'); map.put('u', '8'); map.put('v', '8');
        map.put('w', '9'); map.put('x', '9'); map.put('y', '9'); map.put('z', '9');
		StringBuilder stringBuilder = new StringBuilder();
		char[] arr=front.toCharArray();
		for(int i=0;i<arr.length;i++){
			if(arr[i]>='A'&&arr[i]<='Z'){
				arr[i]=(char) (arr[i]+33);
				stringBuilder.append(arr[i]);
			}else if(arr[i]>='a'&&arr[i]<='z'){
				arr[i]=map.get(arr[i]);
				stringBuilder.append(arr[i]);
			}else{
				stringBuilder.append(arr[i]);
			}
		}
		System.out.println("加密后："+stringBuilder);
	}
}
