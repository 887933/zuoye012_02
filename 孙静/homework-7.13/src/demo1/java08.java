package demo1;
//你妈妈每天给你2.5元，你都会存起来，但是当这一天是
//*  存前的第五天或者是5的倍数的时候，你就会去花6元钱，
//*  多久你能存到100元
public class java08 {
	public static void main(String[] args) {
		float getMoney = (float) 2.5;
		float allMoney =(float) 0.0;
		int day=0;
		while(allMoney<100){
			day++;
			if(day%5==0){
				allMoney += getMoney-6.0;
			}else{
				allMoney += getMoney;
			}
			
		}
		System.out.println("第"+day+"天可以存够100元");
		System.out.println("一共是"+allMoney+"元");
	}

}
