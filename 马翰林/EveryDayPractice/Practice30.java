package EveryDayPractice;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

public class Practice30 {
     public static void main(String[] args) {
         ArrayList<Integer> list = new ArrayList<>();
         list.add(1);
         list.add(2);
         list.add(6);
         list.add(10);
         list.add(55);
         Scanner sc = new Scanner(System.in);
         int n = sc.nextInt();
         for (int i = 0; i < list.size(); i++) {
             if(list.get(i)<n&&list.get(i+1)>n) {
                 list.add(i+1,n);
             }
         }
         System.out.println(list);
     }
}
