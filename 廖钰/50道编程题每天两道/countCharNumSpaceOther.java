package dailyProgram;

import java.util.Scanner;

public class countCharNumSpaceOther {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		char c;
		int i=0,character=0,number=0,space=0,other=0;
		String str = sc.nextLine();
		str += '\n';
		while(str.charAt(i) != '\n') {
			c = str.charAt(i);
			if(c>='a'&&c<='z') {
				character++;
			}else if(c>='A'&&c<='Z') {
				character++;
			}else if(c>='0'&&c<='9') {
				number++;
			}else if(c==' '){
				space++;
			}else {
				other++;
			}
			i++;
		}
		System.out.println("有"+character+"个英文字母，"+number+"个数字，"+space+"个空格，"+other+"个其他字符。");
	}
}
