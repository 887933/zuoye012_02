package java08;

import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class java07 {
	public static void main(String[] args) throws Exception {
		Random r=new Random();
		String strXingURL="https://hanyu.baidu.com/shici/detail?from=kg1&highlight=&pid=0b2f26d4c0ddb3ee693fdb1137ee1b0d&srcid=51369";
		String boyMingURL="http://mingzi.jb51.net/qiming/nanhai/68814860866.html";
		String girlMingURL="http://mingzi.jb51.net/qiming/nvhai/68815554142.html";
		String xingContet=webCarawler(strXingURL);
		String boymingContet=webCarawler(boyMingURL);
		String girlmingContet=webCarawler(girlMingURL);
		ArrayList<String> listXing=getDataXing(xingContet,"(.{4})(，|。)");
		ArrayList<String> listboyMing=getDataMing(boymingContet,"(..、){4}..");
		ArrayList<String> listgirlMing=getDataMing(girlmingContet,"(..、){4}..");
		ArrayList<String> xing=execDataXing(listXing);
	//	System.out.println(xing);
		ArrayList<String> boyming=execDataMing(listboyMing);
	//	System.out.println(boyming);
		ArrayList<String> girlming=execDataMing(listgirlMing);
	//	System.out.println(girlming);
		ArrayList<String> boyxingMing=getInfo(xing,boyming,50);
		for(int i=0;i<boyxingMing.size();i++){
			String newS=boyxingMing.get(i)+"-男-"+String.valueOf(r.nextInt(11)+20);
			boyxingMing.set(i, newS);
		}
		System.out.println(boyxingMing);
		ArrayList<String> girlxingMing=getInfo(xing,girlming,50);
		for(int i=0;i<girlxingMing.size();i++){
			String newS=girlxingMing.get(i)+"-女-"+String.valueOf(r.nextInt(8)+18);
			girlxingMing.set(i, newS);
		}
		System.out.println(girlxingMing);
		FileWriter fw=new FileWriter("boynames.txt");
		for (String string : boyxingMing) {
			fw.write(string);
			fw.write("\n");
			fw.flush();
		}
		FileWriter fw2=new FileWriter("girlnames.txt");
		for (String string : girlxingMing) {
			fw2.write(string);
			fw2.write("\n");
			fw2.flush();
		}
		fw.close();
		fw2.close();
	}
	
	private static ArrayList<String> getInfo(ArrayList<String> xing, ArrayList<String> ming, int count) {
		HashSet<String> set=new HashSet<>();
		while(true){
			if(set.size()==count){
				break;
			}
			Collections.shuffle(xing);
			Collections.shuffle(ming);
			set.add(xing.get(0)+ming.get(0));
		}
		return new ArrayList(set);
	}
	
	
	private static ArrayList<String> execDataXing(ArrayList<String> listXing) {
		ArrayList<String>  xing=new ArrayList<>();
		for (String string : listXing) {
			//赵钱孙李，或者周吴郑王。
			for(int i=0;i<string.length()-1;i++){
				char charAt = string.charAt(i);
				xing.add(charAt+"");
			}
		}
		return xing;
	}
	
	
	private static ArrayList<String> execDataMing(ArrayList<String> listMing) {
		ArrayList<String>  ming=new ArrayList<>();
		for (String string : listMing) {
			//哲睿、益昌、夏明、胜才、浚龙
			String[] split = string.split("、");
			for (String string2 : split) {
				ming.add(string2);
			}
		}
		return ming;
	}
	
	
	private static ArrayList<String> getDataXing(String xingContet, String regex) {
		ArrayList<String> list=new ArrayList<>();
		//按照正则的规则生成
		Pattern pattern = Pattern.compile(regex);
		//按照 pater规则，找到str当中获取的数据
		Matcher matcher = pattern.matcher(xingContet);
		while(matcher.find()){
			String group = matcher.group();
			if(!group.contains("em")){
				list.add(group);
			}
		}
		return list;
	}
	
	private static ArrayList<String> getDataMing(String xingContet, String regex) {
		ArrayList<String> list=new ArrayList<>();
		//按照正则的规则生成
		Pattern pattern = Pattern.compile(regex);
		//按照 pater规则，找到str当中获取的数据
		Matcher matcher = pattern.matcher(xingContet);
		while(matcher.find()){
			String group = matcher.group();
				list.add(group);
		}
		return list;
	}
	
	
	private static String webCarawler(String strXingURL) throws Exception {
		StringBuilder sb=new StringBuilder();
		//创建一个url对象
		URL url =new URL(strXingURL);
		//连接上这个网站，确保网络通常
		URLConnection openConnection = url.openConnection();
		InputStream inputStream = openConnection.getInputStream();
		//读取数据
		InputStreamReader isr=new InputStreamReader(inputStream,"utf-8");
		int ch;
		while((ch=isr.read())!=-1){
			sb.append((char)ch);
		}
		isr.close();
		return sb.toString();
	}
}
