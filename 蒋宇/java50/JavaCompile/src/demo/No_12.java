package demo;

import java.util.Scanner;

public class No_12 {
	public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("请输入当月利润I（万元）：");
        double profit = input.nextDouble();
        
        long bonus = 0; // 奖金定义为长整型
        
        if (profit <= 10) {
            bonus = (long) (profit * 0.1 * 10000); // 奖金计算
        } else if (profit <= 20) {
            bonus = (long) (10 * 0.1 * 10000 + (profit - 10) * 0.075 * 10000);
        } else if (profit <= 40) {
            bonus = (long) (10 * 0.1 * 10000 + 10 * 0.075 * 10000 + (profit - 20) * 0.05 * 10000);
        } else if (profit <= 60) {
            bonus = (long) (10 * 0.1 * 10000 + 10 * 0.075 * 10000 + 20 * 0.05 * 10000 + (profit - 40) * 0.03 * 10000);
        } else if (profit <= 100) {
            bonus = (long) (10 * 0.1 * 10000 + 10 * 0.075 * 10000 + 20 * 0.05 * 10000 + 20 * 0.03 * 10000 + (profit - 60) * 0.015 * 10000);
        } else {
            bonus = (long) (10 * 0.1 * 10000 + 10 * 0.075 * 10000 + 20 * 0.05 * 10000 + 20 * 0.03 * 10000 + 40 * 0.015 * 10000 + (profit - 100) * 0.01 * 10000);
        }
        
        System.out.println("应发放奖金总数为：" + bonus + " 元");
    }

}
