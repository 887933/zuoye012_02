package java09;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

public class java04 {
	public static void main(String[] args) throws Exception {
		BufferedReader brb=new BufferedReader(new InputStreamReader(new FileInputStream("boynames.txt")));
		BufferedReader brg=new BufferedReader(new InputStreamReader(new FileInputStream("girlnames.txt")));
		ArrayList<String> boyAndgirl=new ArrayList<>();
		ArrayList<String> girl=new ArrayList<>();
		Scanner sc=new Scanner(System.in);
		String b;
		String g;
		while((b=brb.readLine())!=null){
			boyAndgirl.add(b);
		}
		while((g=brg.readLine())!=null){
			girl.add(g);
		}
			
			Collections.shuffle(girl);
			ArrayList<String> newgirl=new ArrayList<>();
			int p=0;
			while(p<15){
				newgirl.add(girl.get(p));
				p++;
			}
			for(String ss:newgirl){
				boyAndgirl.add(ss);
			}
			BufferedReader brr=new BufferedReader(new InputStreamReader(new FileInputStream("counter.txt")));
			HashMap<String, Integer> counter=new HashMap<>();
			String cs;
			while((cs=brr.readLine())!=null){
				String[] split = cs.split(":");
				counter.put(split[0], Integer.parseInt(split[1]));
			}
			Random r=new Random();
			int pi=r.nextInt(boyAndgirl.size());
			Collections.shuffle(boyAndgirl);
			String luck = boyAndgirl.get(pi);
			String sex = luck.split("-")[1];
			if(sex.equals("女")){
				Integer value = counter.get("girlcount");
				counter.put("girlcount",value+1);
			}
			else if (sex.equals("男")){
				Integer value = counter.get("boycount");
				counter.put("boycount",value+1);
			}
			Integer allcount = counter.get("count");
			System.out.print("第"+(allcount+1)+"次点名："+luck+"  男女比列："+counter.get("boycount")+":"+counter.get("girlcount"));
			counter.put("count", allcount+1);
			OutputStreamWriter osw=new OutputStreamWriter(new FileOutputStream("counter.txt"));
			Set<Entry<String, Integer>> entrySet = counter.entrySet();
			int i=0;
			for(Entry<String, Integer> e:entrySet){
				String sss=e.getKey()+":"+e.getValue();
				if(i<entrySet.size()-1){
					osw.write(sss);
					osw.write("\n");
				}
				else{
					osw.write(sss);
				}
				i++;
			}
			brb.close();
			brg.close();
			brr.close();
			osw.close();
	}
}
