package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;
import java.util.concurrent.Callable;

public class No_3 implements Callable<Integer>{
    ArrayList<Integer> a;
	
	public No_3(ArrayList<Integer> a) {
		super();
		this.a = a;
	}

	

	@Override
	public Integer call() throws Exception {
		ArrayList<Integer> b=new ArrayList<>();
		int sum=0;
		while(true){
			synchronized (No_3.class) {
				 
				if(a.size()==0){
					
					System.out.println(Thread.currentThread().getName()+"总共产生了"+b.size()+"个大奖，最高金额为"+Collections.max(b)+"元，总计金额为"+sum+"元");
					break;
				}else{
					int nextInt = new Random().nextInt(a.size());
					sum+=a.get(nextInt);
					System.out.println(Thread.currentThread().getName()+"产生了一个"+a.get(nextInt)+"元大奖");
					b.add(a.get(nextInt));
					a.remove(nextInt);
					
				}
			}
			Thread.sleep(100);
		}
		if(b.size()==0){
			return null;
		}else{
			
		}
		return Collections.max(b);
	}
}

