package demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class java01 {
	public static void main(String[] args) {
		ArrayList<Person> list=new ArrayList();
		list.add(new Person("张三",21));
		list.add(new Person("李四",24));
		list.add(new Person("丁真",26));
		list.add(new Person("珍珠",27));
		list.add(new Person("锐刻五",30));

		List<Person> filteredList = list.stream()
                .filter(person -> person.getAge() > 24)
                .collect(Collectors.toList());
			
			// 对筛选后的列表进行排序
			Collections.sort(filteredList, new Comparator<Person>() {
			@Override
			public int compare(Person o1, Person o2) {
				int result = o2.getAge() - o1.getAge();
				if (result == 0) {
					result = o1.getName().compareTo(o2.getName());
				}
					return result;
				}
			});
			
			// 将排序后的列表添加到HashMap中
			HashMap<String, Integer> map = new HashMap<>();
			for (Person person : filteredList) {
				map.put(person.getName(), person.getAge());
			}
			
			// 打印HashMap中的内容
			map.forEach((name, age) -> System.out.print(name + ": " + age));
			}
	}


