package homework6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Stream;

public class Demo9 {
    public static void main(String[] args) {
        ArrayList<String> list = new ArrayList<>();
        Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-30", "张翠山-男-40", "张良-男-35", "王二麻子-男-35", "谢广坤-女-41", "林婷-女-22", "林立-女-23");
        Stream<Actors> boy = list.stream()
        .filter(s -> "男".equals(s.split("-")[1]) && s.split("-")[0].length() == 3)
        .map(Actors::new);
        Stream<Actors> girl = list.stream()
        .filter(s -> "女".equals(s.split("-")[1]) && s.split("-")[0].startsWith("林"))
        .skip(1).map(Actors::new);

        Stream.concat(boy, girl).forEach(System.out::println);
    }
}

class Actors {
    private String name;
    private int age;
    private String sex;
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public String getSex() {
        return sex;
    }
    public void setSex(String sex) {
        this.sex = sex;
    }
    @Override
    public String toString() {
        return "Actors [name=" + name + ", age=" + age + ", sex=" + sex + "]";
    }
    public Actors(String str) {
        String[] parts = str.split("-");
        this.name = parts[0];
        this.sex = parts[1];
        this.age = Integer.parseInt(parts[2]);
    }
    public Actors() {
    }
}
  