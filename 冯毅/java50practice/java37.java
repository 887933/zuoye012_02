package java50practice;

import java.util.ArrayList;
import java.util.Scanner;

public class java37 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入n:");
		int n=sc.nextInt();
		ArrayList<Integer> people=new ArrayList<>();
		for(int i=1;i<=n;i++){
			people.add(i);
		}
		System.out.println(people);
		int count = 0; 
        int index = 0; 
        while (people.size() > 1) {
            count++;
            if (count == 3) {
                people.remove(index);
                count = 0; 
                if (index >= people.size()) {
                    index = 0;
                }
            } else {
                index++;
                if (index >= people.size()) {
                    index = 0;
                }
            }
        }
        
        System.out.println("最后剩下的人的编号: " + people.get(0));
	}
}
