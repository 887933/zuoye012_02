package HomeWork;
import java.io.*;
import java.nio.file.*;
import java.util.*;

public class RandomCallName03 {
    private static String FILE_Path = "src/HomeWork/fw.txt";
    private static String SPECIAL_NAME = "张三";
    private static int counter = 0;

    public static void main(String[] args) {
        try {
            List<String> names = Files.readAllLines(Paths.get(FILE_Path));
            // 每三次输出“张三”
            for (int i = 0;i < 10;i++){
            if (++counter % 3 == 0) {
                System.out.println(SPECIAL_NAME);
            } else {
                List<String> otherNames = new ArrayList<>(names);
                otherNames.remove(SPECIAL_NAME);
                if (!otherNames.isEmpty()) {
                    Random random = new Random();
                    String randomName = otherNames.get(random.nextInt(otherNames.size()));
                    String[] parts = randomName.split("-");
                    String name = parts[0];
                    System.out.println(name);
                } else {
                    System.out.println("班级里只有张三一个人！");
                }
            }
            }
        } catch (IOException e) {
            System.out.println("文件读取错误：" + e.getMessage());
        }
    }
}

