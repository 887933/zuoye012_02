package java50practice;

import java.util.ArrayList;
import java.util.Scanner;

public class java36 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("你想输入几个数：");
		int count=sc.nextInt();
		ArrayList<Integer> ai=new ArrayList<>();
		for(int i=0;i<count;i++){
			System.out.print("输入：");
			int num=sc.nextInt();
			ai.add(num);
		}
		System.out.print("原数组："+ai);
		System.out.print("你想让前面多少个往后移动（提示：要小于等于原数组长度的一半）：");
		int m=sc.nextInt();
		ArrayList<Integer> copyai=new ArrayList<>();
		for(int i=0;i<m;i++){
			copyai.add(ai.get(0));
			ai.remove(0);
		}
		ai.addAll(copyai);
		System.out.println(ai);
	}
}
