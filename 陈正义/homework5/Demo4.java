//4.把任意字符串加密显示   
//	 *  YUANzhi1987
//	 *  加密后为
//	 *  zvbo9441987
//	 *  加密规则:
//		 *   1.如果是大写字母变成小写字母,往后移动一位
//		 *   2.小写字母通过收集输入发对应键盘来进行加密
//		 *   1-1
//		 *   abc-2
//		 *   def-3
//		 *   ghi-4
//		 *   jkl-5
//		 *   mno-6
//		 *   pqrs-7
//		 *   tuv-8
//		 *   wxyz-9
//		 *   0-0
//		 *   3.数字不变
package homework5;

import java.util.ArrayList;
import java.util.Scanner;

public class Demo4 {
	public static void main(String[] args) {
		System.out.println("请输入一个字符串：");
		Scanner sc=new Scanner(System.in);
		String str=sc.next();
		encrypt(str);
	}

	private static void encrypt(String str) {
		// TODO Auto-generated method stub
		ArrayList<Character> list =new ArrayList<Character>();
		for( int i=0;i<str.length();i++) {
			list.add(str.charAt(i));
		}
		for(int k=0;k<list.size();k++) {
			if(list.get(k)>='A'&&list.get(k)<'Z') {
				char num=(char)(list.get(k)+1);
				list.set(k, num);
			}
			else if(list.get(k)=='Z') {
				list.set(k, 'A');
			}
			else if(list.get(k)>='a'&&list.get(k)<='c')
            {
               list.set(k, (char)(list.get(k)+2));
            }
			else if(list.get(k)>'c'&&list.get(k)<='f')
            {
            	list.set(k, (char)(list.get(k)+3));
            }
			else if(list.get(k)>'f'&&list.get(k)<='i')
            {
            	list.set(k, (char)(list.get(k)+4));
            }
			else if(list.get(k)>='j'&&list.get(k)<='l')
            {
            	list.set(k, (char)(list.get(k)+5));
            }
			else if(list.get(k)>='m'&&list.get(k)<='o')
            {
            	list.set(k, (char)(list.get(k)+6));
            }
			else if(list.get(k)>='p'&&list.get(k)<='s')
            {
            	list.set(k, (char)(list.get(k)+7));
            }
			else if(list.get(k)>='t'&&list.get(k)<='y')
            {
                list.set(k, (char)(list.get(k)+8));
            }
			else if (list.get(k)>='w'&&list.get(k)<='z')
            {
            	list.set(k, (char)(list.get(k)+9));
            }
			else if(list.get(k)>='0'&&list.get(k)<='9') {
            	list.set(k, list.get(k));
            }
			
		}
		System.out.println("加密后："+list);
		
		
		
	}
}
