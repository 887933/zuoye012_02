package HomeWork;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class RandomCallName02 {
    public static void main(String[] args) {
        List<String> maleStudents = new ArrayList<>();
        List<String> femaleStudents = new ArrayList<>();
        List<String> calledStudents = new ArrayList<>();
        int maleCount = 0;
        int femaleCount = 0;

        try (BufferedReader br = new BufferedReader(new FileReader("src/HomeWork/fw.txt"))) {
            String line;
            while ((line = br.readLine()) != null) {
                String[] parts = line.split("-");
                if (parts.length == 3) {
                    String name = parts[0];
                    String gender = parts[1];
                    if (gender.equals("男")) {
                        maleStudents.add(name + "-" + gender);
                    } else if (gender.equals("女")) {
                        femaleStudents.add(name + "-" + gender);
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("读取学生数据时出错。");
            return;
        }

        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            String selectedStudent;
            if (random.nextInt(100) < 70) {
                selectedStudent = maleStudents.get(random.nextInt(maleStudents.size()));
                maleCount++;
            } else {
                selectedStudent = femaleStudents.get(random.nextInt(femaleStudents.size()));
                femaleCount++;
            }
            calledStudents.add(selectedStudent);

            System.out.println("点到的同学: " + selectedStudent);
            System.out.println("已点到的所有同学:");
            for (String student : calledStudents) {
                System.out.println(student);
            }

            System.out.println("男生和女生的比例为 " + maleCount + "：" + femaleCount);
            System.out.println("------------");
        }
    }
}