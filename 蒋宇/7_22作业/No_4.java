package homework;

import java.util.Arrays;
import java.util.Scanner;

public class No_4 {
	public static void main(String[] args) {
//		把任意字符串加密显示   
//		 *  YUANzhi1987
//		 *  加密后为
//		 *  zvbo9441987
//		 *  加密规则:
//			 *   1.如果是大写字母变成小写字母,往后移动一位
//			 *   2.小写字母通过收集输入发对应键盘来进行加密
//			 *   1-1
//			 *   abc-2
//			 *   def-3
//			 *   ghi-4
//			 *   jkl-5
//			 *   mno-6
//			 *   pqrs-7
//			 *   tuv-8
//			 *   wxyz-9
//			 *   0-0
//			 *   3.数字不变
		Scanner sc=new Scanner(System.in);
		String str1=sc.next();
		System.out.println(str1);
		String str2;
		char[] c1 = str1.toCharArray();
//		for(int i=0;i<c1.length;i++){
//			System.out.println(c1[i]);
//		}
		for(int i=0;i<c1.length;i++){
			if(Character.isUpperCase(c1[i])){
				if(c1[i]=='Z'){
					c1[i]='a';
					continue;
				}
				c1[i]=(char) (Character.toLowerCase(c1[i])+1);
			}
			else if(Character.isLowerCase(c1[i])){
            	System.out.println("请输入任意一个小写字母或者01");
				str2=sc.next();
				char input=str2.charAt(0);
				if(input=='1'){
					c1[i]='1';
				}else if(input>='a'&&input<='c'){
					c1[i]='2';
				}else if(input>='d'&&input<='f'){
					c1[i]='3';
				}else if(input>='g'&&input<='i'){
					c1[i]='4';
				}else if(input>='j'&&input<='l'){
					c1[i]='5';
				}else if(input>='m'&&input<='o'){
					c1[i]='6';
				}else if(input>='p'&&input<='s'){
					c1[i]='7';
				}else if(input>='t'&&input<='v'){
					c1[i]='8';
				}else if(input>='w'&&input<='z'){
					c1[i]='9';
				}else if(input=='0'){
					c1[i]='0';
				}else{
					System.out.println("输入错误，请重新输入");
					break;
				}
			}
            
		}
		String modifiedString = String.valueOf(c1);
		System.out.println("加密后的字符串为：" + modifiedString);
	}

}
