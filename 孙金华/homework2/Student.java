package homework2;

public class Student {
	private int age;
	private int id;
	private String name;
	public Student() {
	}
	public Student(int id,int age,String name) {
		this.id=id;
		this.name=name;
		this.age=age;
	}
	public int getId() {
		return this.id;
	}
	public int getAge() {
		return this.age;
	}
	public String getName() {
		return this.name;
	}
	public void setId(int id) {
		this.id=id;
	}
	public void setAge(int age) {
		this.age=age;
	}
	public void setName(String name) {
		this.name=name;
	}
}
