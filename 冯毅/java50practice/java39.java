package java50practice;

import java.util.Scanner;

public class java39 {
	public static void main(String[] args) {
		System.out.println("请输入n");
		Scanner sc=new Scanner(System.in);
		int n=sc.nextInt();
		if(n%2==0){
			System.out.println("结果为："+oushu(n));
		}else{
			System.out.println("结果为："+jishu(n));
		}
	}
	public static double jishu(int n){
		double count=0;
		for(double i=1;i<=n;i=i+2){
			count+=1/i;
		}
		return count;
	}
	public static double oushu(int n){
		double count=0;
		for(double i=2;i<=n;i=i+2){
			count+=1/i;
		}
		return count;
	}
}
