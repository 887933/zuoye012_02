package java50practice;

import java.util.Scanner;

public class java05 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入成绩:");
		int score=sc.nextInt();
		if(score>100||score<0){
			System.out.println("输入不合法，成绩应为0-100之间！");
		}
		else{
			char grade=(score>=90)?'A':((score>=60&&score<=89)?'B':'C');
			System.out.print("你的成绩等级为："+grade);
		}
	}
}
