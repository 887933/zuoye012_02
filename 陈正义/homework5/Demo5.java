package homework5;
//hashmap去重  
//	合并 输出数组两个数组中都有元素则去除
//	[1,2,3]
//	[3,4,5,2]
//	得到[1,4,5]
import java.util.ArrayList;
import java.util.HashMap;
public class Demo5 {
	public static void main(String[] args) {
		HashMap<Integer,Integer> map = new HashMap<>();
        ArrayList<Integer> list = new ArrayList<>();
        map.put(1,0);
        map.put(2,0);
        map.put(3,0);
        HashMap<Integer,Integer> map1 = new HashMap<>();
        map1.put(3,0);
        map1.put(4,0);
        map1.put(5,0);
        map1.put(2,0);
        for(Integer key:map.keySet()) {
            Integer value = map.get(key);
          if(map1.containsKey(key)) {
               map1.remove(key);
          }
          else
              map1.put(key,value);
        }
        for(Integer key:map1.keySet()) {
            list.add(key);
        }
        System.out.println(list);
	}
}
