package homework3;

import java.util.ArrayList;
import java.util.Random;

public class Member extends User{
	public Member(String username, int moeny) {
		super(username,moeny);
	}
	public Member() {
	}
	public void receive(ArrayList<Integer> redList){
		Random r=new Random();
		int index = r.nextInt(redList.size());
		Integer delta = redList.remove(index);
		int money=this.getMoeny()+delta;
		this.setMoeny(money);
	}
}
