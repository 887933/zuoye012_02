package dailyProgram;

import java.util.Scanner;

public class scoreGrade {
	public static void main(String[] args) {
		int score;
		while(true) {
			System.out.println("请输入成绩（0-100）：");
			Scanner sc= new Scanner(System.in);
			score = sc.nextInt();
			if(score>100||score<0) {
				continue;
			}else {
				break;
			}
		}
		System.out.println(score>=60?(score>=90?"A":"B"):"C");
	}
}
