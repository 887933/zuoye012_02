package HomeWork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class No_4 {

    public static void main(String[] args) {
        String sourceFolder = "d:\\3";
        String destinationFolder = "d:\\new";

        try {
            copyFolder(new File(sourceFolder), new File(destinationFolder));
            System.out.println("Folder copied successfully.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void copyFolder(File sourceFolder, File destinationFolder) throws IOException {
        if (sourceFolder.isDirectory()) {
            if (!destinationFolder.exists()) {
                destinationFolder.mkdirs();
            }

            File[] files = sourceFolder.listFiles();
            if (files != null) {
                for (File file : files) {
                    File destinationFile = new File(destinationFolder, file.getName());
                    if (file.isDirectory()) {
                        copyFolder(file, destinationFile);
                    } else {
                        FileInputStream fis = new FileInputStream(file);
                        FileOutputStream fos = new FileOutputStream(destinationFile);
                        byte[] buffer = new byte[1024];
                        int length;
                        while ((length = fis.read(buffer)) > 0) {
                            fos.write(buffer, 0, length);
                        }
                        fis.close();
                        fos.close();
                    }
                }
            }
        }
    }

}
