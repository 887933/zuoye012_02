package demo;

import java.util.Scanner;

public class No_15 {
	public static void main(String[] args) {
//		输入三个整数x,y,z，请把这三个数由小到大输出。
//		程序分析：我们想办法把最小的数放到x上，先将x与y进行比较，
//		如果x>y则将x与y的值进行交换，然后再用x与z进行比较，
//		如果x>z则将x与z的值进行交换，这样能使x最小。
		Scanner sc=new Scanner(System.in);
		int x=sc.nextInt();
		int y=sc.nextInt();
		int z=sc.nextInt();
		int temp=x;
		if(x>y&&x>z&&y>z){
			System.out.println(z);
			System.out.println(y);
			System.out.println(x);
		}else if(x>y&&x>z&&z>y){
			System.out.println(y);
			System.out.println(z);
			System.out.println(x);
		}else if(y>x&&y>z&&x>z){
			System.out.println(z);
			System.out.println(x);
			System.out.println(y);
		}else if(y>x&&y>z&&z>x){
			System.out.println(x);
			System.out.println(z);
			System.out.println(y);
		}else if(z>x&&z>y&&y>x){
			System.out.println(x);
			System.out.println(y);
			System.out.println(z);
		}else if(z>x&&z>y&&x>y){
			System.out.println(y);
			System.out.println(x);
			System.out.println(z);
		}
		

	}

}
