package homework05;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;


/*作业2：
在文件中准备用户名和密码，user.txt
每个用户输入用户名和密码输入三次后，被锁1分钟，如果再次输入三次后错误，将被冻结
 */
public class homework02 {
	public static void main(String[] args) throws IOException, InterruptedException {
        File file = new File("user.txt");
        ArrayList<user> userList = readUsersFromFile(file);

        Scanner sc = new Scanner(System.in);
        boolean loggedIn = false;
        user currentUser = null;

        here:while (!loggedIn) {
            System.out.println("请输入用户名:");
            String username = sc.nextLine();
            System.out.println("请输入密码:");
            String password = sc.nextLine();

            currentUser = findUserByUsername(userList, username);
            if (currentUser != null) {
                if (currentUser.getCount() == 2) {
                    System.out.println("由于多次输入错误，您的账户已被锁定，请等待10秒。");
                    Thread.sleep(10000);
                    System.out.println("解除封禁");
                }
                if (6-currentUser.getCount() < 2) {
                    System.out.println("由于多次输入错误，您的账户已被冻结");
                    break here;
                }
                if (currentUser.getPassword().equals(password)) {
                    System.out.println("登录成功" + username);
                    loggedIn = true;
                } else {
                	if(currentUser.getCount()<2){
	                    currentUser.setCount(currentUser.getCount() + 1);
	                    System.out.println("登录失败!请重新登陆" + (3 - currentUser.getCount()) + "次");
                	}else{
                		currentUser.setCount(currentUser.getCount() + 1);
	                    System.out.println("登录失败!请重新登陆" + (6 - currentUser.getCount()) + "次");
                	}
                }
            } else {
                System.out.println("用户名不存在或被冻结，请重新输入。");
            }
        }
        sc.close();
    }

    private static ArrayList<user> readUsersFromFile(File file) throws IOException {
        ArrayList<user> userList = new ArrayList<>();
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        while ((line = br.readLine()) != null) {
            String[] parts = line.split("=");
            String username = parts[0];
            String password = parts[1];
            user users = new user(username, password, 0);
            userList.add(users);
        }
        br.close();
        return userList;
    }
    private static user findUserByUsername(ArrayList<user> userList, String username) {
        for (user user : userList) {
            if (user.getUsername().equals(username)) {
                return user;
            }
        }
        return null;
    }
}
