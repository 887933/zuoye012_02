package demo1;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.TreeMap;
import java.util.Map.Entry;
import java.util.function.Function;
import java.util.stream.Collectors;

public class java06 {

	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		int[] arr1 = new int[]{1,2,3,4,5};
		int[] arr2 = new int[]{1,2,3,8,9};
		for(int a:arr1){
			list.add(a);
		}
		for(int a:arr2){
			list.add(a);
		}
		System.out.print("[");
		list.stream().collect(Collectors.groupingBy(Function.identity(),Collectors.counting()))
		.entrySet().stream().filter(entry->entry.getValue()==1).map(Entry::getKey)
		.forEach(new java06()::print);
		System.out.print("]");
		
		System.out.println("");
		//����
		System.out.print("[");
		TreeMap<Integer, Integer> hashMap = treeMap(arr1,arr2);
		hashMap.entrySet().stream().filter(java06::judge)
		.map(Entry::getKey).forEach(new java06()::print);
		System.out.print("]");
		
		
	}
	public static TreeMap<Integer, Integer> treeMap(int[] ch1, int[] ch2) {
		TreeMap<Integer,Integer> hashMap = new TreeMap<>();
		for(int c:ch1){
			if(hashMap.containsKey(c)){
				int value = hashMap.get(c);
				hashMap.put(c, value+1);
				
			}else{
				hashMap.put(c, 1);
			}
		}
		for(int c:ch2){
			if(hashMap.containsKey(c)){
				int value = hashMap.get(c);
				hashMap.put(c, value+1);
				
			}else{
				hashMap.put(c, 1);
			}
		}
		return hashMap;
	}
	public static boolean judge(Entry<Integer,Integer> entry){
		return entry.getValue()==1;
		
	}
	int count = 0;
	public void print(int n){
		if(count ==0){
			System.out.print(n);
			count =1;
		}else{
			System.out.print(","+n);
		}
	}
}
