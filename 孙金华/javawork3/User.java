package homework3;

public class User {
	private String username;
	private int moeny;
	public User() {
	}
	public User(String username,int moeny) {
		this.moeny=moeny;
		this.username=username;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public int getMoeny() {
		return moeny;
	}
	public void setMoeny(int moeny) {
		this.moeny = moeny;
	}
	public void show(){
		System.out.println("我叫:"+username+",我有多少钱:"+moeny);
	}
}
