package shellsort;

import java.util.Arrays;

public class shellsort {
public static void main(String[] args) {
	/*希尔排序:将数组按步长来交换，第一次设定步长为数组长度的一半，
	 * 将索引设置为数组长度的一半，从数组长度的一半对应的元素arr[arr.length/2]开始按arr.length/2步长往前计数，找到该位置对应的元素，若前者小于后者则交换
	 * 随后索引往后推进重复上述操作，直到索引到达数组的最后一个元素*/
	int[] arr={7,6,9,3,1,5,2,4};
	shellsort(arr);
	
}

private static void shellsort(int[] arr) {

	for(int gap=arr.length/2;gap>0;gap=gap/2){		
         for(int i=gap;i<arr.length;i++){
        	 int j=i;
        	 while(j>=gap&&arr[j-gap]>arr[j]){
        		 swap(arr,j,j-gap);
        		
        		 j-=gap;
        	 }
        	 
         }
         System.out.println(Arrays.toString(arr)); 
	}
 
}

private static void swap(int[] arr, int j, int i) {

	int temp=arr[i];
	arr[i]=arr[j];
	arr[j]=temp;
}


}
