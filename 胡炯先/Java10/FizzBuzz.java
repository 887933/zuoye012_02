package java12;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.IntConsumer;

public class FizzBuzz extends Thread{
	private int n;  
	private AtomicInteger count = new AtomicInteger(1);  
	private final ReentrantLock lock = new ReentrantLock();  
	private final Condition fizzCond = lock.newCondition();  
	private final Condition buzzCond = lock.newCondition();  
	private final Condition numberCond = lock.newCondition();  

	public FizzBuzz(int n) {  
		this.n = n;  
	}  

	// printFizz.run() 输出 "fizz"  
	public void fizz(Runnable printFizz) throws InterruptedException {  
		for (int i = 1; i <= n; ) {  
			lock.lock();  
			try {  
				while (count.get() % 3 == 0 && count.get() % 5 != 0 && count.get() <= n) {  
					printFizz.run();  
					count.incrementAndGet();  
					numberCond.signal();  
				}  
				fizzCond.await();  
			} finally {  
				lock.unlock();  
			}  
		}  
	}  

	// printBuzz.run() 输出 "buzz"  
	public void buzz(Runnable printBuzz) throws InterruptedException {  
		for (int i = 1; i <= n; ) {  
			lock.lock();  
			try {  
				while (count.get() % 5 == 0 && count.get() % 3 != 0 && count.get() <= n) {  
					printBuzz.run();  
					count.incrementAndGet();  
					numberCond.signal();  
				}  
				buzzCond.await();  
			} finally {  
				lock.unlock();  
			}  
		}  
	}  

	// printFizzBuzz.run() 输出 "fizzbuzz"  
	public void fizzbuzz(Runnable printFizzBuzz) throws InterruptedException {  
		for (int i = 1; i <= n; ) {  
			lock.lock();  
			try {  
				while (count.get() % 3 == 0 && count.get() % 5 == 0 && count.get() <= n) {  
					printFizzBuzz.run();  
					count.incrementAndGet();  
					numberCond.signal();  
				}  
				fizzCond.signal();  
				buzzCond.signal();  
			} finally {  
				lock.unlock();  
			}  
		}  
	}  

	// printNumber.accept(x) 当 x 既不是 3 的倍数也不是 5 的倍数时，输出 x  
	public void number(IntConsumer printNumber) throws InterruptedException {  
		for (int i = 1; i <= n; ) {  
			lock.lock();  
			try {  
				while (count.get() % 3 != 0 && count.get() % 5 != 0 && count.get() <= n) {  
					printNumber.accept(count.get());  
					count.incrementAndGet();  
					fizzCond.signal();  
					buzzCond.signal();  
				}  
				numberCond.await();  
			} finally {  
				lock.unlock();  
			}  
		}  
	}  
}

