package homework11;

public class Foodie extends Thread {
	private Desk desk;
	public Foodie(Desk desk) {
		this.desk=desk;
	}

	@Override
	public void run() {
		while(true){
			synchronized(desk.getLock()){
				if(desk.getCount()==0){
					break;
				}else{
					if(desk.isFlag()){
						//有
						System.out.println("吃货正在吃汉堡包");
						desk.setFlag(false);
						desk.getLock().notify();
						desk.setCount(desk.getCount()-1);
					}else{
						//没有 等待
						try {
							desk.getLock().wait();
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
	}
	
}