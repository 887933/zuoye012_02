package demo;

public class No_41 {
	public static void main(String[] args) {
        int peach = findOriginalPeach();
        System.out.println("海滩上原来最少有" + peach + "个桃子");
    }

	public static int findOriginalPeach() {
        int peach = 1;
        boolean flag = false;
        while(true) {
            int remain = peach;
            for(int i = 0; i < 5; i++) {
                if((remain - 1) % 5 == 0) {
                    remain = (remain - 1) / 5 * 4;
                } else {
                    flag = true;
                    break;
                }
            }
            if(!flag) {
                break;
            }
            peach++;
            flag = false;
        }
        return peach;
    }
}
