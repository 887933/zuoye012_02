package demo;

import java.util.Scanner;

public class No_26 {
	public static void main(String[] args) {
//		请输入星期几的第一个字母来判断一下是星期几，
//		如果第一个字母一样，则继续 判断第二个字母。
//		程序分析：用情况语句比较好，如果第一个字母一样
//		，则判断用情况语句或if语句判断第二个字母。
		//Monday,Tuesday,Wednesday,Thursday,
		//Friday,Saturday,Sunday
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入星期几");
		String w=sc.next();
		char[] a=w.toCharArray();
		switch (a[0]){
		case 'M':
			System.out.println("今天是星期一");
			break;
		case 'T':
			if(a[1]=='u'){
				System.out.println("今天是星期二");
				break;
			}else if(a[1]=='h'){
				System.out.println("今天是星期四");
				break;
			}
		case 'W':
			System.out.println("今天是星期三");
			break;
		case 'F':
			System.out.println("今天是星期五");
			break;
		case 'S':
			if(a[1]=='a'){
				System.out.println("今天是星期六");
				break;
			}else if(a[1]=='u'){
				System.out.println("今天是星期天");
				break;
			}
		}

	}

}
