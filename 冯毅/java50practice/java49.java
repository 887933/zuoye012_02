package java50practice;

public class java49 {
	 public static void main(String[] args) {
	        String bs = "wsksogjcsfkwoasscsplplpcs";
	        String ss = "cs";
	        int count = countSubstrings(bs, ss);
	        System.out.println( ss + "������" + count + " ��");
	    }

	    public static int countSubstrings(String mainStr, String subStr) {
	        if (mainStr == null || subStr == null || subStr.length() == 0) {
	            return 0;
	        }
	        int count = 0;
	        int fromIndex = 0;
	        while (fromIndex < mainStr.length()) {
	            int foundIndex = mainStr.indexOf(subStr, fromIndex);
	            if (foundIndex == -1) {
	                break;
	            }
	            count++;
	            fromIndex = foundIndex + 1; 
	        }

	        return count;
	    }
}
