package homework;

import java.util.Random;
import java.util.Scanner;

//1.判断数组当中某一个数是否存在，如果存在，则返回true,否则返回false
public class Way1 {
	public static void main(String args[]) {
		int[] arr=new int[6];
		Random r=new Random();
		for(int j=0;j<6;j++) {
			int a = r.nextInt(10);
			arr[j]=a;
		}
		System.out.println(equals(arr));
	}
	public static boolean equals(int[] arr) {
		System.out.println("请输入一个数：");
		Scanner sc =new Scanner(System.in);
		int num=sc.nextInt();
		for(int i=0;i<arr.length-1;i++) {
			if(num==arr[i]) {
				return true;
			}
		}
		return false;
	}

}
