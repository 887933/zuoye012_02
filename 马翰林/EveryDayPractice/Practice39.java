package EveryDayPractice;

import java.util.Scanner;

public class Practice39 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        if (n % 2 == 0) {
            ou(n);
        } else {
            ji(n);
        }

    }

    public static void ou(int n) {
        double x = 0;
        for (int i = 2; i <= n; i+=2)
        {
            x += 1.0 / i;
        }
        System.out.println(x);
    }
    public static void ji(int n)
    {
        double x=0;
        for(int i=1;i<=n; i+=2) {
            x += 1.0/ i;
        }
        System.out.println(x);
    }
}
