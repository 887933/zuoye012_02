package zuoye4;

public class Consumer extends Thread {
@Override
public void run() {
      while(true){
    	  synchronized (Share.lock) {
			if(Share.count==0){
				Share.i=0;
				Share.lock.notify();
			}else{
				if(Share.flag){
					for(;Share.count>0;Share.count--){
					Share.i++;
					System.out.println("消费者消费了第"+Share.i+"个产品");
					Share.flag=false;
				}
				}else{
					try {
						Thread.sleep(800);
						Share.lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}
      }


}
}
