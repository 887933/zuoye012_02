package demo1;
//求1-100之间的奇数和，并把求和的结果在控制台打印
public class java02 {
	public static void main(String[] args) {
		int count = 0,sum=0;
		while(count<=100){
			if(count%2 !=0){
				sum+=count;
			}
			count++;
		}
		System.out.println(sum);
	}

}
