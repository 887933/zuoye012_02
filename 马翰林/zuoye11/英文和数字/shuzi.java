package zuoye11;

public class shuzi extends  Thread{
    public static final   Object  lock = new Object();
    public  static boolean flag = false;
    public  void  run() {
        synchronized (lock) {
            int n = 0;
            for (int i = 1; i <= 52; i++) {
                n++;
                if (n%2!=0) {
                    System.out.print(i+" ");
                    lock.notify();
                }
                if (n%2==0) {
                    try {
                        System.out.print(i+" ");
                        flag = true;
                        lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            lock.notifyAll();
        }
    }
}
