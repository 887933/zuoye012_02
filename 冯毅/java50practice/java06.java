package java50practice;

import java.util.Scanner;

public class java06 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入第一个数:");
		int num1=sc.nextInt();
		System.out.print("输入第二个数:");
		int num2=sc.nextInt();
		int max = Math.max(num1, num2);
		int min = Math.min(num1, num2);
		int originalmax=max;
		int originalmin=min;
		while(true){
			if(max%min==0){
				break;
			}else{
				int y=max%min;
				max=min;
				min=y;
			}
		}
		System.out.println("最大公约数为："+min);
		System.out.println("最小公倍数为："+(originalmax*originalmin)/min);
	}
}
