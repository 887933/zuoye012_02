package homework11;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class DrawBox9 extends Thread{
	Prize prize;
	List<Integer> list=new ArrayList<Integer>();
	int count=0,sum=0;
	public DrawBox9( Prize prize) {
		super();
		this.prize = prize;
	}
	public DrawBox9() {
	}
	@Override
	public void run() {
		while(prize.getCount()>0) {
			
			try {
				int num= Prize.lottery(prize);
				list.add(num);
				if(num!=-1) {
				count++;
				}
				System.out.println("抽奖箱7产生一个"+num+"元大奖");
				sleep(100);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		int max=Collections.max(list);
		for(int i:list) {
			sum+=i;
		}
		System.out.println(" 抽奖箱7：总共产生了"+count+"个大奖，最高金额为"+max+"，总计金额为"+sum+"元");
	}
}
