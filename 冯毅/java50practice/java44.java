package java50practice;

import java.util.ArrayList;
import java.util.Scanner;

public class java44 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入n:");
        int n = sc.nextInt();
        checkGoldbach(n);
    }

    private static void checkGoldbach(int n) {
       
            
            boolean foundPair = false;
            for (int i = 2; i <= n / 2; i++) {
                if (isPrime(i) && isPrime(n - i)&&n%2==0) {
                    System.out.println(n + " = " + i + " + " + (n - i));
                    foundPair = true;
                }
            }
            if (!foundPair) {
                System.out.println("输入不合法");
            }
        
    }

    private static boolean isPrime(int number) {
        if (number <= 1) return false;
        if (number == 2) return true;
        if (number % 2 == 0) return false;

        for (int i = 3; i * i <= number; i += 2) {
            if (number % i == 0) {
                return false;
            }
        }
        return true;
    }
}
