package homework6;
//hashmap去重  
//	合并 输出数组两个数组中都有元素则去除
//	[1,2,3]
//	[3,4,5,2]
//	得到[1,4,5]
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;
public class Demo5 {
	public static void main(String[] args) {		
		ArrayList<Integer> list = new ArrayList<>();
		ArrayList<Integer> list2 = new ArrayList<>();
		Collections.addAll(list, 1,2,3);
		Collections.addAll(list2, 3,4,5,2);
		list.addAll(list2);
		Map<Integer, Long> map = list.stream().collect(Collectors.groupingBy(s->s, Collectors.counting()));
		Set<Entry<Integer, Long>> entrySet = map.entrySet();
		ArrayList<Integer> newNum=new ArrayList<Integer>();
		for(Entry<Integer, Long> i:entrySet){
			if(i.getValue()==1){
				newNum.add(i.getKey());
			}
		}
		System.out.println(newNum);

	}
}
