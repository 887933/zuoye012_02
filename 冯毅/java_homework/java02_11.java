package dame;

import java.util.Scanner;

public class java02_11 {
    public static void main(String[] args) {
    	System.out.print("请输入一个数：");
    	Scanner sc=new Scanner(System.in);
    	int s=sc.nextInt();
		int[] arrA={1,2,3,4,5,6,7};
		boolean c=isExist(s,arrA);
		if(c==true){
			System.out.println("这个数存在");
		}
		else{
			System.out.println("这个数不存在");
		}
	}
    public static boolean isExist(int num,int[] arr){   	
    	boolean b=false;
    	for(int i=0;i<arr.length;i++){
    		if(num==arr[i]){
    			b=true;
    			break;
    		}    		
    	}
    	return b;
    }
}
