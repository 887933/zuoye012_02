package homework4;

import java.util.Scanner;

public class Demo2 {
	public static void main(String[] args) {
		System.out.println("请输入一段字符串：");
		Scanner sc=new Scanner(System.in);
	        String str = sc.next(); 
	        int number = 0; 
	        boolean c=false;
	        for(int i=0;i<str.length();i++) {
	        	if(str.charAt(i)=='-') {
	        		c=true;
	        		i++;
	        	}
	        	if(str.charAt(i)<'0'||str.charAt(i)>'9'||str.charAt(0)=='0') {
	        		System.out.println("请输入有效整数！");
	        		break;
	        	}
	        	if(i>=10) {
	        		System.out.println("位数超过了10位！");
	        		break;
	        	}
	        	number=number*10+(str.charAt(i)-'0');
	        }
	        if(c==true) {
	        	number=-number;
	        }
	        System.out.println("转换后的整数是: " + number);
	}
}
