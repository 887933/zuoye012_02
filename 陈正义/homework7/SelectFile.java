package homework7;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

public class SelectFile {
	  public static void main(String[] args) {
	        File directory = new File("C:\\Users\\LENOVO\\Desktop");
	        Map<String, Integer> fileCounts = new HashMap<>();
	        Select(directory, fileCounts);
	        for (Map.Entry<String, Integer> entry : fileCounts.entrySet()) {
	            System.out.println(entry.getKey() + ":" + entry.getValue() + "��");
	        }
	    }

	    private static void Select(File directory, Map<String, Integer> fileCounts) {
	        File[] files = directory.listFiles();
	        if (files != null) {
	            for (File file : files) {
	                if (file.isDirectory()) {
	                    Select(file, fileCounts);
	                } else {
	                    String extension = getExtension(file);
	                    fileCounts.put(extension, fileCounts.getOrDefault(extension, 0) + 1);
	                }
	            }
	        }
	    }

	    private static String getExtension(File file) {
	        String name = file.getName();
	        int i = name.lastIndexOf('.');
	        if (i > 0 && i < name.length() - 1) {
	            return name.substring(i).toUpperCase();
	        }
	        return ""; 
	    }
}
