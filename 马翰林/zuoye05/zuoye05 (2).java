package javazuoye06.zuoye05;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class zuoye05 {
    public static void main(String[] args) {
        HashMap<Integer,Integer> map = new HashMap<>();
        ArrayList<Integer> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add(3);
        map.put(1,0);
        map.put(2,0);
        map.put(3,0);
        HashMap<Integer,Integer> map1 = new HashMap<>();
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(3);
         list1.add(4);
         list1.add(5);
         list1.add(2);
        map1.put(3,0);
        map1.put(4,0);
        map1.put(5,0);
        map1.put(2,0);
        for(Integer key:map.keySet()) {
            Integer value = map.get(key);
          if(map1.containsKey(key)) {
               map1.remove(key);
          }
          else
              map1.put(key,value);
        }
        for(Integer key:map1.keySet()) {
            list.add(key);
        }
        List<Integer> list2 = Stream.concat(list1.stream(),list.stream()).distinct().sorted().toList();
     System.out.println(list2);
    }
}
