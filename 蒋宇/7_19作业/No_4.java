package demo;

import java.util.ArrayList;

public class No_4 {
	

	    public static void main(String[] args) {
	        String str1 = "dajlskldjsalkdjsalkda";
	        String str2 = "djaskldjlsalk";

	        findLongestCommonSubstrings(str1, str2);
	    }

	    public static void findLongestCommonSubstrings(String str1, String str2) {
	        int length1 = str1.length();
	        int length2 = str2.length();
	        int[][] dp = new int[length1 + 1][length2 + 1];
	        int maxLength = 0;
	        ArrayList<String> commonSubstrings = new ArrayList<>();

	        for (int i = 1; i <= length1; i++) {
	            for (int j = 1; j <= length2; j++) {
	                if (str1.charAt(i - 1) == str2.charAt(j - 1)) {
	                    dp[i][j] = dp[i - 1][j - 1] + 1;
	                    if (dp[i][j] > maxLength) {
	                        maxLength = dp[i][j];
	                        commonSubstrings.clear();
	                        commonSubstrings.add(str1.substring(i - maxLength, i));
	                    } else if (dp[i][j] == maxLength) {
	                        commonSubstrings.add(str1.substring(i - maxLength, i));
	                    }
	                }
	            }
	        }

	        System.out.println("相同的最长的字符串长度为：" + maxLength);
	        System.out.println("相同的最长的字符串为：");
	        for (String substring : commonSubstrings) {
	            System.out.println(substring);
	        }
	    }
	

}
