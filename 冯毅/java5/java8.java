package java06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

public class java8 {
	public static void main(String[] args) {
		String s="YrR8RrY";
		String s1="ppRYYGrrYBR2258";
		String s2="ppRYYGrrYB225";
		int k1=isRight(s, s1);
		if(k1==1){
			System.out.println("yes");
		}else{
			System.out.println("no");
			}
		int k2=isRight(s, s2);
		if(k2==1){
			System.out.println("yes");
		}else{
			System.out.println("no");
			}
	}	
	public static int isRight(String s,String s2){
		  HashMap<Character, Integer> h = new HashMap<Character, Integer>();
		  char[] c=s.toCharArray();
		  char[] c6=s2.toCharArray();
		  ArrayList<Character> cs2=new ArrayList<Character>();
		  for(char cc:c6){
			  cs2.add(cc);
		  }
		  for(char cc:c){
			  if(h.containsKey(cc)){
				  int value=h.get(cc);
				  h.put(cc, value+1);
			  }
			  else{
				  h.put(cc, 1);
			  }
		  }
		  Set<Entry<Character, Integer>> entrySet = h.entrySet();
		   ArrayList<Character> ac = new ArrayList<Character>();
		  for(Entry<Character, Integer> e:entrySet){
			  ac.add(e.getKey());
		  }
		  for(int i=0;i<ac.size();i++){
			  Character co = ac.get(i);
			  if(!cs2.contains(co)){
				  return -1;
			  }
			  else{
				  int count=Collections.frequency(cs2, co);
				  if(count<h.get(co)){
					  return -1;
				  }
			  }
		  }
		  return 1;
	}
}
