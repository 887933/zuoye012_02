package java11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class CJ extends Thread{
	static ArrayList<Integer> cj;
	static {
        List<Integer> valuesToAdd = Arrays.asList(10,6,20,50,100,200,500,800,2,80,300,700);
        cj = new ArrayList<>(valuesToAdd); 
    
    }
	Lock lock=new ReentrantLock();
	public void run(){
		while(true){
			lock.lock();
			try {
				Random r=new Random();
				int pi=r.nextInt(cj.size());
				Integer integer = cj.get(pi);
				cj.remove(pi);
				System.out.println(getName()+"产生了一个"+integer+"元大奖");
				if(cj.size()==0){
					break;
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{lock.unlock();}
			
		}
	}
	
}
