package dailyProgram;

public class PrimeNumberBetween101to200 {
	public static void main(String[] args) {
		for(int i=101;i<=200;i++) {
			if(isPrimeNumber(i)) {
				System.out.println(i+"是素数");
			}
		}
	}

	private static boolean isPrimeNumber(int i) {
		for(int j=2;j<i/2;j++) {
			if(i%j==0) {
				return false;
			}
		}
		return true;
	}
}
