package java07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Map.Entry;
import java.util.stream.Collectors;

public class java3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
        System.out.print("�����ַ�����");
        String s = sc.nextLine(); 
        Map<Character, Long> map = s.chars().mapToObj(c -> (char) c)
        .collect(Collectors.groupingBy(c -> c, Collectors.counting())); 
        Comparator<Map.Entry<Character, Long>> valueComparator = new Comparator<Map.Entry<Character, Long>>() {
            @Override
            public int compare(Map.Entry<Character, Long> o1, Map.Entry<Character, Long> o2) {
                return Long.compare(o1.getValue(), o2.getValue());
            }
        };
			List<Map.Entry<Character, Long>> list = new ArrayList<Map.Entry<Character,Long>>(map.entrySet());
			Collections.sort(list,valueComparator);
			list.stream().forEach(System.out::println);
	}
}
