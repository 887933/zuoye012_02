package zuoye6;

public class ThreadB extends Thread{
	int num;
	public ThreadB(int num) {
		super();
		this.num = num;
	}

	@Override
	public void run() {
		synchronized (FizzBuzz.lock) {
			if(!FizzBuzz.flag){
				if(num%5==0){
					FizzBuzz.buzz();
				}
			}
		}

	}
}
