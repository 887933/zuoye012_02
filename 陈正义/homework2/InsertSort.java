package homework2;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
/*1.从第二个元素开始，依次对每个元素进行排序。
 *2.取出当前元素，与已排序序列中的元素从后向前进行比较。
 *3.如果当前元素比已排序序列中的元素小，则将已排序序列中的元素依次后移一位，为当前元素腾出插入位置。
 *4.将当前元素插入到正确的位置。
 *5.重复以上步骤。
 */
public class InsertSort {
	public static void main(String[] args) {
		ArrayList<Integer> list =new ArrayList<Integer>();
		System.out.println("请输入数组长度：");
		Scanner sc=new Scanner(System.in);
		int number =sc.nextInt();
		Random r=new Random();
		for(int i=0;i<number;i++) {
			int digit=r.nextInt(100);
			list.add(digit);
		}
		sc.close();
		System.out.println("排序前：");
		for(int i=0;i<list.size();i++) {
			System.out.print(list.get(i)+"  ");
		}
		for(int i=1;i<list.size();i++) {
			int t=list.get(i);
			int j=i-1;
			while(j>=0&&t<list.get(j)) {
					list.set(j+1, list.get(j));
					j--;
			}
			list.set(j+1, t);
		}	
		System.out.println(" ");
		System.out.println("排序后：");
		for(int i=0;i<list.size();i++) {
			System.out.print(list.get(i)+"  ");
		}
	}

}
