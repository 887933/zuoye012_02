package java08;

import java.io.File;

public class java03 {
	public static void main(String[] args) {
		File f=new File("abc");
		deleteFile(f);
	}
	private static void deleteFile(File src) {
		File[] listFiles = src.listFiles();
		if(listFiles!=null){
			for (File file : listFiles) {
				if(file.isDirectory()){
					
					deleteFile(file);
				}
				
				else{
					file.delete();
				}
		   }
			src.delete();
		}
		
	}

}
