package producer;

public class Customer extends Thread {  
    private BaoZi bz;  
  
    public Customer(BaoZi bz) {  
        this.bz = bz;  
    }  
  
    @Override  
    public void run() {  
        while (true) {  
            synchronized (bz) {  
                while (bz.count == 0) {  
                    try {  
                        bz.wait();  
                    } catch (InterruptedException e) {  
                        Thread.currentThread().interrupt();  
                        return;  
                    }  
                }  
                if (bz.flag) {  
                    System.out.println("吃货正在吃包子");  
                    bz.flag = false;  
                    bz.count--;  
                    bz.notify();  // 通知生产者或其他等待的消费者  
                }  
            }  
        }  
    }  
}