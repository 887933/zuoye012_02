package homework4;

import java.util.ArrayList;
import java.util.Scanner;

//键盘录入1-100之间的整数，并添加到集合中，直到集合数据总和超过200为止
public class java1 {
	public static void main(String[] args) {
		Scanner s=new Scanner(System.in);
		ArrayList<Integer> list=new ArrayList<Integer>();
		while(true) {
			int sum=0;
			for(int i=0;i<list.size();i++) {
				sum+=list.get(i);
			}
			if(sum>=200) {
				break;
			}
			System.out.print("输入一个整数：");
			int a=s.nextInt();
			list.add(a);
		}
		System.out.println(list.toString());
	}
}
