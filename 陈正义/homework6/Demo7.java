package homework6;

import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;
import java.util.stream.Collectors;

//我们使用[0-9] [a-z][A-Z]范围内来表示字符串
//示例1：
//输入ppRYYGrrYBR2258
//	YrR8RrY
//输出：
//Yes 8
//
//
//	输入:ppRYYGrrYB225
//	YrR8RrY
//NO
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class Demo7 {
    public static void main(String[] args) {
		String s="YrR8Rr11Y";
		String s1="ppRYYGrrYBR122158";
		String s2="ppRYYGrrYB21125";
		int f = isRight( s, s1);
		if(f==1){
			System.out.println("yes    "+(s1.length()-s.length()));
		}else{
			System.out.println("no");
		}
		int f2=isRight( s, s2);
		if(f2==1){
			System.out.println("yes"+(s2.length()-s.length()));
		}else{
			System.out.println("no");
		}
	}
	public static int isRight(String s,String s2){
		Map<Character, Long> c1 = s.chars().mapToObj(c->(char) c).collect(Collectors.groupingBy(c->c,Collectors.counting()));
		Map<Character, Long> c2 = s2.chars().mapToObj(c->(char) c).collect(Collectors.groupingBy(c->c,Collectors.counting()));
		Set<Entry<Character, Long>> e = c1.entrySet();
		for(Entry<Character, Long> e1:e){
			boolean con = c2.containsKey(e1.getKey());
			if(!con){
				return -1;
			}
			if(c2.get(e1.getKey())<e1.getValue()){
				return -1;
			}
		}
		return 1;
	}
}