package java50practice;

import java.util.ArrayList;
import java.util.Scanner;

public class java35 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入数组长度：");
		int length=sc.nextInt();
		ArrayList<Integer> ins=new ArrayList<>();
		for(int i=0;i<length;i++){
			System.out.println("输入第"+(i+1)+"个数");
			int num=sc.nextInt();
			ins.add(num);
		}
		System.out.println("原数组：");
		System.out.println(ins);
		ArrayList<Integer> copyins=new ArrayList<>(ins);
		copyins.sort(null);
		int max=copyins.get(length-1);
		int min=copyins.get(0);
		int maxindex = ins.indexOf(max);
		int minindex=ins.indexOf(min);
		int ins_0=ins.get(0);
		int ins_last=ins.get(length-1);
		ins.set(0, max);
		ins.set(length-1, min);
		ins.set(maxindex, ins_0);
		ins.set(minindex, ins_last);
		System.out.println(ins);
	}
}
