package demo;

import java.util.Arrays;
import java.util.Scanner;

public class No_30 {
	public static void main(String[] args) {
//		有一个已经排好序的数组。现输入一个数，
//		要求按原来的规律将它插入数组中。
//		程序分析：首先判断此数是否大于最后一个数，
//		然后再考虑插入中间的数的情况，插入后此元素之后的数，
//		依次后移一个位置。
		Scanner sc=new Scanner(System.in);
		int[] x={1,4,7,10,13,16,19,22,25};
		System.out.println("请输入你要插入的数");
		int y=sc.nextInt();
		x=insert(x,y);
		System.out.println(Arrays.toString(x));

	}

	private static int[] insert(int[] x,int y) {
		// 1,4,7,10,13,16,19,22,25
		int[] b=new int[x.length+1];
		int sign=0;
		for(int i=0;i<x.length;i++){
			if(y<x[i]){
				sign=i;
				break;
			}
		}
		for(int i=0;i<x.length+1;i++){
			if(i<sign){
				b[i]=x[i];
			}else if(i==sign){
				b[i]=y;
			}else{
				b[i]=x[i-1];
			}
		
			
		}
		return b;
	}

}
