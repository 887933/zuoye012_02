package zuoye11;

import java.util.concurrent.atomic.AtomicIntegerArray;

public class zuoye08 {
  public static void main(String[] args) throws InterruptedException {
      AtomicIntegerArray results = new AtomicIntegerArray(10);
      Thread[] threads = new Thread[10];

      for (int i = 0; i < 10; i++) {
          int start = 1 + i * 10;
          int end = 10 + i * 10;
          threads[i] = new jiafa(start, end, results, i);
          threads[i].start();
      }

      for (Thread thread : threads) {
          try {
              thread.join();
          } catch (InterruptedException e) {
              e.printStackTrace();
          }
      }

      // 计算所有线程结果的总和
      int totalSum = 0;
      for (int i = 0; i < 10; i++) {
          totalSum += results.get(i);
      }

      System.out.println("Total Sum: " + totalSum);
  }
}
