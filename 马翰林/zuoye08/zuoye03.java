package zuoye08;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.Buffer;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class zuoye03 {
    public static void main(String[] args) throws Exception {
        String strXingURL="https://hanyu.baidu.com/shici/detail?from=kg1&highlight=&pid=0b2f26d4c0ddb3ee693fdb1137ee1b0d&srcid=51369";
        String mingXingURL="http://www.haoming8.cn/mingzi/25024.html";
        String strXingURLnv="https://hanyu.baidu.com/shici/detail?from=kg1&highlight=&pid=0b2f26d4c0ddb3ee693fdb1137ee1b0d&srcid=51369";
        String mingXingURLnv="http://www.haoming8.cn/mingzi/35783.html";
        BufferedWriter name=new BufferedWriter(new FileWriter("name.txt"));
        BufferedReader count=new BufferedReader(new FileReader("count.txt"));
        int num=Integer.parseInt(count.readLine());
        //读取网页中的整个网页的内容
        String xingContet=webCarawler(strXingURL);
//		System.out.println(xingContet);
        String mingContet=webCarawler(mingXingURL);
//		System.out.println(mingContet);
        String xingContet2=webCarawler(strXingURLnv);
        String mingContet2=webCarawler(mingXingURLnv);
        //正则表达式
        //赵钱孙李，周吴郑王。
        ArrayList<String> listXing=getDataXing(xingContet,"(.{4})(，|。)");
//		System.out.println(listXing);
        //哲睿、益昌、夏明、胜才、浚龙
        ArrayList<String> listMing=getDataMing(mingContet,"(..、){4}..");
//		System.out.println(listMing);
        ArrayList<String> listXingnv=getDataXing(xingContet2,"(.{4})(，|。)");
        ArrayList<String> listMingnv=getDataMing(mingContet2,"(..、){4}..");
        //处理数据
        ArrayList<String> xing=execDataXing(listXing);
        //System.out.println(xing);

        ArrayList<String> ming=execDataMing(listMing);
        //System.out.println(ming);

        //合起来
        //System.out.println("男");
        HashMap<String,Integer> xingMing=getInfo(xing,ming,50);
        for (Map.Entry<String, Integer> entry : xingMing.entrySet()) {
            if (entry.getValue()>20&&entry.getValue()<30) {
          //      System.out.println(entry.getValue() + "岁" + entry.getKey());
            }
        }
       // System.out.println("--------");
     //   System.out.println("女");
        ArrayList<String> xingnv=execDataXing(listXingnv);
        ArrayList<String> mingnv=execDataMing(listMingnv);
        HashMap<String,Integer> xingmingnv=getInfo(xingnv,mingnv,50);
        for (Map.Entry<String, Integer> entry : xingmingnv.entrySet()) {
            if (entry.getValue()>18&&entry.getValue()<25) {
          //      System.out.println(entry.getValue() + "岁" + entry.getKey());
            }
        }
        FileWriter fw=new FileWriter("names.txt");
        for (String string : xingMing.keySet()) {
            fw.write(string + "-男-" + xingMing.get(string));
            fw.write("\n");
            fw.flush();
        }
        for (String string2 : xingmingnv.keySet()) {
            fw.write(string2 + "-女-" + xingmingnv.get(string2));
            fw.write("\n");
            fw.flush();
        }
   FileReader fr=new FileReader("names.txt");
        BufferedReader br=new BufferedReader(fr);
        String read=br.readLine();
        ArrayList<String> arr=new ArrayList<>();
        while (read!=null) {
            arr.add(read);
            read=br.readLine();
        }
        Collections.sort(arr);
     //System.out.println(arr);
     Random rd=new Random();
     int c=rd.nextInt(arr.size());
     String n=arr.get(c).split("-")[0];
     System.out.println("第"+num+"点名"+n);
     FileWriter fw1=new FileWriter("count.txt");
     int num2=num+1;
     fw1.write(Integer.toString(num2));
        fw.close();
       fw1.close();
       count.close();
    }

    private static HashMap<String,Integer> getInfo(ArrayList<String> xing, ArrayList<String> ming, int count) {
        HashMap<String,Integer> map=new HashMap<>();
        while(true){
            if(map.size()==count){
                break;
            }
            Random a=new Random();
            int b=a.nextInt(100);
            Collections.shuffle(xing);
            Collections.shuffle(ming);
            map.put(xing.get(0)+ming.get(0),b);
        }
        return map;
    }

    private static ArrayList<String> execDataMing(ArrayList<String> listMing) {
        ArrayList<String>  ming=new ArrayList<>();
        for (String string : listMing) {
            //哲睿、益昌、夏明、胜才、浚龙
            String[] split = string.split("、");
            for (String string2 : split) {
                ming.add(string2);
            }
        }
        return ming;
    }

    private static ArrayList<String> execDataXing(ArrayList<String> listXing) {
        ArrayList<String>  xing=new ArrayList<>();
        for (String string : listXing) {
            for(int i=0;i<string.length()-1;i++){
                char charAt = string.charAt(i);
                xing.add(charAt+"");
            }
        }
        return xing;
    }

    private static ArrayList<String> getDataMing(String xingContet, String regex) {
        ArrayList<String> list=new ArrayList<>();
        //按照正则的规则生成
        Pattern pattern = Pattern.compile(regex);
        //按照 pater规则，找到str当中获取的数据
        Matcher matcher = pattern.matcher(xingContet);
        while(matcher.find()){
            String group = matcher.group();
            list.add(group);
        }
        return list;
    }

    private static ArrayList<String> getDataXing(String xingContet, String regex) {
        ArrayList<String> list=new ArrayList<>();
        //按照正则的规则生成
        Pattern pattern = Pattern.compile(regex);
        //按照 pater规则，找到str当中获取的数据
        Matcher matcher = pattern.matcher(xingContet);
        while(matcher.find()){
            String group = matcher.group();
            if(!group.contains("em")){
                list.add(group);
            }
        }
        return list;
    }

    private static String webCarawler(String strXingURL) throws Exception {
        StringBuilder sb=new StringBuilder();
        //创建一个url对象
        URL url =new URL(strXingURL);
        //连接上这个网站，确保网络通常
        URLConnection openConnection = url.openConnection();
        InputStream inputStream = openConnection.getInputStream();
        //读取数据
        InputStreamReader isr=new InputStreamReader(inputStream,"utf-8");
        int ch;
        while((ch=isr.read())!=-1){
            sb.append((char)ch);
        }
        isr.close();
        return sb.toString();
    }

}

