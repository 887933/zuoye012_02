package homework;
//for 循环作业1：  输出所有两位数，三位数，四位数，五位数的回文数      12321 是回文  10101 是   12345不是
public class For1 {
	public static void main(String args[]) {
		for(int i=10;i<100000;i++) {
			if(getNumber(i)==true) {
				System.out.println(i);
			}
		}
/*		for(int i=10;i<100000;i++) {
			if(i<100) {
				if(i/10==i%10) {
					System.out.println(i);
				}
			}
			else if(i<1000&&i>=100) {
				if(i/100==i%10) {
					System.out.println(i);
				}
			}
			else if(i>=1000&&i<10000) {
				if(i/1000==i%10&&(i/100)%10==(i%100)/10) {
					System.out.println(i);
				}
			}
			else {
				if(i/10000==i%10&&(i/1000)%10==(i%100)/10) {
					System.out.println(i);
			}
		}
	}*/
	}
	//charAt用于返回字符串中指定索引处的字符。
	public static  boolean getNumber(int x){
		String str=Integer.toString(x);
		
		for(int i=0;i<str.length()/2;i++) {
		if(str.charAt(i)!=str.charAt(str.length()-1-i)) {
			return false;
		}
	}
		return true;
	}
}
