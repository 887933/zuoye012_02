package zuoye719;

import java.util.ArrayList;
import java.util.Scanner;

public class demo5 {
public static void main(String[] args) {
	Scanner sc=new Scanner(System.in);
	System.out.println("请输入字符串:");
	String str=sc.nextLine();
	ArrayList<Character> list=new ArrayList<>();
	for(int i=0;i<str.length();i++){
	char ch=str.charAt(i);
	list.add(ch);
	}
	
	System.out.println("请输入字符串起始位置:");
	int strstart=sc.nextInt();
	System.out.println("请输入字符串终止位置:");
	int strend=sc.nextInt();
	swap(list,strstart,strend);
	   System.out.println("反转后的数组为:");
		for(int i=0;i<list.size();i++){
			System.out.print(list.get(i));
		}
/*	
	for(int left=0,right=list.size()-1;left<right;left++,right--){
		swap(list,left,right);
	
	}
    System.out.println("反转后的数组为:");
	for(int i=0;i<list.size();i++){
		System.out.print(list.get(i));
	}
*/
	
}

private static void swap(ArrayList<Character> list,int i,int j) {
    if(i<0||j>list.size()||i>=j){
    	System.out.println("请输入合理的反转位置!");
    }
	for(int n=i;i<j;i++,j--){
	Character temp=list.get(i);
	list.set(i, list.get(j));
    list.set(j, temp);
    }
	
}
}
