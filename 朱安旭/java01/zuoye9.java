package zuoye;

import java.util.Random;

public class zuoye9 {
	public static void main(String[] args)
	{
		int[] arr={1,2,3,4,5};
		change(arr);
		for(int num :arr)
		{
			System.out.print(num+" ");
		}
	}
	
	private static void change(int[] arr){
		Random rnd=new Random();
		for(int i = arr.length-1;i>0;i--)
		{
			int index=rnd.nextInt(i+1);
			int temp=arr[index];
			arr[index]=arr[i];
			arr[i]=temp;
		}
	}
}
