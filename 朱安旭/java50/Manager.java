package demo01;

import java.util.ArrayList;
import java.util.Random;

public class Manager extends Usr {
	public Manager(String username, int moeny) {
		super(username,moeny);
	}
	public Manager() {
	}
	public ArrayList<Integer> send(int totalMoeny,int count){
		ArrayList<Integer>  redList=new ArrayList<Integer>();
		//群主得先看自己钱够不够
		int leftMoney=this.getMoeny();
		if(totalMoeny>leftMoney){
			System.out.println("余额不足");
			return redList;//返回null
		}
		//扣钱
		this.setMoeny(leftMoney-totalMoeny);
		//发红包 ，分成n等分
		int avg=totalMoeny/count;
		Random rnd = new Random();
		int []arr = new int[count];
		arr[0]=rnd.nextInt(totalMoeny)+1;
		redList.add(arr[0]);
		int last = totalMoeny-arr[0];
		//除开零头 先放一样得钱
		for(int i=1;i<count-1;i++){
			avg=last/count-i;
			arr[i] =(int) (rnd.nextInt(avg*2)+1);
			redList.add(arr[i]);
			 last = last-arr[i];
		}
		redList.add(last); 
		return redList;
	}

}
