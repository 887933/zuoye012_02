package EveryDayPractice;

import java.util.Arrays;
import java.util.Scanner;

public class Practice34 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("请输入三个数：");
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int c = scanner.nextInt();

        int[] numbers = {a, b, c};

        // 对数组进行排序
        Arrays.sort(numbers);

        // 输出排序后的结果
        System.out.println("排序后的结果为：");
        System.out.println(numbers[0] + ", " + numbers[1] + ", " + numbers[2]);

    }
}