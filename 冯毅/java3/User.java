package java04;

public class User {
    String name;
    public User(String name, int money) {
		super();
		this.name = name;
		this.money = money;
	}
	int money;
    public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getMoney() {
		return money;
	}
	public void setMoney(int money) {
		this.money = money;
	}	
	public void show(){
		System.out.println("我叫:"+getName()+",我有多少钱:"+money);
	}
}
