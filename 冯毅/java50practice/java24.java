package java50practice;

import java.util.Scanner;

public class java24 {
	public static void main(String[] args) {
		System.out.println("输入一个数字：");
		Scanner sc=new Scanner(System.in);
		String s="";
		while(true){
			s=sc.next();
			if(s.length()<=5&&Integer.parseInt(s)>0){
				break;
			}
			System.out.println("输入不合法，请重新输入");
			sc.nextLine();
		}
		char[] ch = s.toCharArray();
		System.out.println("它是"+s.length()+"位数");
		for(int i=ch.length-1;i>=0;i--){
			if(i>0){
				System.out.print(ch[i]+",");
			}else{
				System.out.print(ch[i]);

			}
		}
	}
}
