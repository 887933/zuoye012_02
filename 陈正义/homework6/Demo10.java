package homework6;

import java.util.Scanner;
import java.util.Arrays;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
public class Demo10 {
	 public static void main(String[] args) {
	        Scanner sc = new Scanner(System.in);
	        System.out.println("请输入一个字符串：");
	        String str = sc.nextLine();
	        System.out.println("请输入出现次数：");
	        int num = sc.nextInt();
	        sc.close();
	        String[] strArray = str.split("", -1);
	        Map<String, Long> frequencyMap = Arrays.stream(strArray)
	            .collect(Collectors.groupingBy(String::toString, Collectors.counting()));
	        Set<String> set = frequencyMap.entrySet().stream()
	            .filter(entry -> entry.getValue() == num) 
	            .map(Map.Entry::getKey)
	            .collect(Collectors.toSet());
	        System.out.println("出现次数为 " + num + " 的字符有：" + set);
	    }
}
 interface StreamProcessor {
    Set<Character> process(Stream<String> stream, int frequency);
}