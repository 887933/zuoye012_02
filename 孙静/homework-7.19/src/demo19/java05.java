package demo19;

import java.util.Scanner;

public class java05 {
	public static void main(String[] args) {
		toReverse();
	}

	@SuppressWarnings("resource")
	private static void toReverse() {
		// TODO Auto-generated method stub
		boolean judge = false;//定义judge判断是否有要反转的部分
		int count = 0;//判断反转的位置
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入字符串：");
		String s = sc.nextLine();
		char[] arr = s.toCharArray();//将它装进数组
		Scanner sr = new Scanner(System.in);
		System.out.println("请输入需要反转的部分：");
		String s1 = sr.nextLine();
		char[] arr1 = s1.toCharArray();
		for(int i =0;i<arr.length;i++){
			if(arr[i] == arr1[0]){
				for(int j = 0;j<arr1.length;j++){
					judge = (arr[i+j] == arr1[j]);//判断是否和反转部分连续且一样
					count = i;
				}
			}
		}
		StringBuilder b = new StringBuilder(s1);//StringBuilder类可以更改字符
		b = b.reverse();//反转
		String c = b.toString();//再变成字符串
		if(judge == true){
			StringBuilder sb = new StringBuilder(s);
			sb.replace(count, count+c.length(), c);//将字符串的第n到n+j位替换为c字符串
			System.out.println(sb.toString());//打印
		}
		
	}

}
