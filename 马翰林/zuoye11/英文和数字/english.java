package zuoye11;

public class english extends Thread{
    public void run(){
        synchronized (shuzi.lock) {
            for (char c = 'A'; c <= 'Z'; c++) {

                 if (shuzi.flag){
                     System.out.print(c);
                    shuzi.flag=false;
                    shuzi.lock.notify();
                }
                else {
                    try {
                        shuzi.lock.wait();
                    } catch (InterruptedException e) {
                        throw new RuntimeException(e);
                    }
                }
            }
            shuzi.lock.notifyAll();
        }
    }
}
