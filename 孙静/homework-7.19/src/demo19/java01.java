package demo19;

import java.util.ArrayList;
import java.util.Scanner;

public class java01 {
	public static void main(String[] args) {

		getSum();
	}

	private static void getSum() {
		// TODO Auto-generated method stub
		int sum = 0;
		ArrayList<Integer> list =new ArrayList<>();
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入0-100数字");
		while(true){

			String s = sc.nextLine();
			int i = Integer.parseInt(s);
			if(i<0 || i>100){
				System.out.print("不符合，请重新输入");
				continue;
			}
			
			list.add(i);
			sum += i;
			if(sum>200){
				break;
			}
		}
		System.out.print("[");
		for (Integer num:list){
			
			System.out.print(num+" ");
			
		}
		System.out.print("]");
		System.out.println("sum="+sum);
	}


}
