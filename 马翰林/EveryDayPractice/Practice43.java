package EveryDayPractice;

public class Practice43 {
     public static void main(String[] args) {
               int totalOddNumbers = 0;

               // 一位数的奇数个数
               totalOddNumbers += 4; // 1, 3, 5, 7

               // 二位数的奇数个数
               totalOddNumbers += calculatePossibleCombinations(1, 4);

               // 三位数的奇数个数
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 4);

               // 四位数的奇数个数
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 8 * 4);
               //五位数的奇数个数
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 8 * 8 * 4);
               //6位
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 8 * 8 * 8*4);
               //7位
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 8* 8* 8* 8*4);
               totalOddNumbers += calculatePossibleCombinations(7, 8 * 8* 8* 8* 8* 8*4);
               System.out.println("Total odd numbers possible: " + totalOddNumbers);
     }
          private static int calculatePossibleCombinations(int leadingDigitOptions, int remainingCombinations) {
               return leadingDigitOptions * remainingCombinations;
          }

}
