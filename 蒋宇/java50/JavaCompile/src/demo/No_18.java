package demo;

public class No_18 {
	public static void main(String[] args) {
//		两个乒乓球队进行比赛，各出三人。甲队为a,b,c三人，乙队为x,y,z三人。
//		已抽签决定比赛名单。有人向队员打听比赛的名单。a说他不和x比，
//		c说他不和x,z比，请编程序找出三队赛手的名单。 
//		for(char a='x';a<='z';a++){
//			for(char b='x';a<='z';b++){
//				if(a!=b){
//					for(char c='x';a<='z';c++){
//						if(a!=c&&b!=c){
//							if(a!='x'&&c!='x'&&c!='z'){
//								System.out.println("a:"+a);
//								System.out.println("b:"+b);
//								System.out.println("c:"+c);
//							}
//						}
//					}
//				}
//				
//			}
//		}
		for(char a='x';a<='z';a++){
            //第二个循环 定义b循环x-y
            for (char b='x';b<='z';b++){
                //在第二层循环中需要判断 a!=b ，按照上面图示理解，a和b 不可能同时对上一个对手
                if(a!=b){
                    //第三个循环 定义c循环x-y
                    for(char c ='x';c<='z';c++){
                        //在第三层循环中判断 a！=c   b!=c，按照上面图示理解，a，b ，c不可能同时对上一个对手
                        if(a!=c&&b!=c){
                            //  根据题目已知条件排除 a!='x'&&c!='x'&&c!='z'
                            if(a!='x'&&c!='x'&&c!='z'){
                                //排除不可能的，打印出最终结果
                                System.out.println("a:"+a);
                                System.out.println("b:"+b);
                                System.out.println("c:"+c);
                            }
                        }
 
                    }
                }
            }
        }
	}
}