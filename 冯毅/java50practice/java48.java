package java50practice;

import java.util.ArrayList;
import java.util.Scanner;

public class java48 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("输入一个四位整数：");
		int num=sc.nextInt();
		String ns=String.valueOf(num);
		ArrayList<Integer> nums=new ArrayList<>();
		char[] charArray = ns.toCharArray();
		for(Character c:charArray){
			nums.add(Integer.parseInt(String.valueOf(c)));
		}
		for(int i=0;i<nums.size();i++){
			nums.set(i, (nums.get(i)+5)%10);
		}
		int p0=nums.get(0);
		int p1=nums.get(1);
		int p2=nums.get(2);
		int p3=nums.get(3);
		nums.set(0, p3);
		nums.set(1, p2);
		nums.set(2, p1);
		nums.set(3, p0);
		System.out.println(nums.get(0)+""+nums.get(1)+""+nums.get(2)+""+nums.get(3));
	}
}
