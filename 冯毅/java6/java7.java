package java07;

import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

public class java7 {
	public static void main(String[] args) {
		String s="YrR8RrY";
		String s1="ppRYYGrrYBR2258";
		String s2="ppRYYGrrYB225";
		int f = isRight( s, s1);
		if(f==1){
			System.out.println("yes    "+(s1.length()-s.length()));
		}else{
			System.out.println("no");
		}
		int f2=isRight( s, s2);
		if(f2==1){
			System.out.println("yes"+(s2.length()-s.length()));
		}else{
			System.out.println("no");
		}
	}
	public static int isRight(String s,String s2){
		Map<Character, Long> c1 = s.chars().mapToObj(c->(char) c).collect(Collectors.groupingBy(c->c,Collectors.counting()));
		Map<Character, Long> c2 = s2.chars().mapToObj(c->(char) c).collect(Collectors.groupingBy(c->c,Collectors.counting()));
		Set<Entry<Character, Long>> e = c1.entrySet();
		for(Entry<Character, Long> e1:e){
			boolean con = c2.containsKey(e1.getKey());
			if(!con){
				return -1;
			}
			if(c2.get(e1.getKey())<e1.getValue()){
				return -1;
			}
		}
		return 1;
	}
}
