package dailyProgram03;

import java.util.Scanner;

public class InterestToPrize {
	public static void main(String[] args) {
		long prize=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入利润：");
		long interest = sc.nextLong();
		if(interest<100000) {
			prize = (long)(interest*0.1);
		}else if(interest<200000) {
			prize = 10000+(long)((interest-100000)*0.075);
		}else if(interest<400000) {
			prize = 17500+(long)((interest-200000)*0.05);
		}else if(interest<600000) {
			prize = 27500+(long)((interest-400000)*0.03);
		}else if(interest<1000000) {
			prize = 33500+(long)((interest-600000)*0.015);
		}else{
			prize = 39500+(long)((interest-1000000)*0.001);
		}
		System.out.println(prize);
	}
}
