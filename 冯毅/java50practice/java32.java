package java50practice;

import java.util.Scanner;

public class java32 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		long num=0;
		String strn="";
		while(true){
			System.out.print("输入一个大于七位的数字：");
			num=sc.nextLong();
			strn=String.valueOf(num);
			if(strn.length()<7){
				System.out.println("输入不合法请重新输入！");
				sc.nextLine();
			}else{
				break;
			}
		}
		char[] charArray = strn.toCharArray();
		int p=4;
		for(int i=charArray.length-4;i>=charArray.length-8;i--){
			System.out.println("第"+p+"位是"+charArray[i]);
			p++;
		}
	}
}
