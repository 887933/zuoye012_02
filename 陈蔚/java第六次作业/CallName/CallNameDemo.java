package CallName;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;



public class CallNameDemo {
	public static void main(String[] args) {
		
		ArrayList<Student> list=new ArrayList<>();
		Scanner sc=new Scanner(System.in);
		Student students=new Student();
		addStudent(list);
		print(list);
		randomStudent(list);

	}
	public static void addStudent(ArrayList<Student> list){
		Scanner sc=new Scanner(System.in);
		
		for(int i=0;i<3;i++){
			Student students=new Student();
	        System.out.println("请输入第"+(i+1)+"个学生的名字:");
	        students.name=sc.next();
	        while (true) {  
                try {  
                    System.out.println("请输入第" + (i + 1) + "个学生的年龄:");  
                    int age = sc.nextInt();  
                    students.setAge(age); 
                    break; 
                } catch (AgeException e) {  
                    System.out.println("年龄不符合规定范围,请重新输入!");         
                } catch (InputMismatchException e) {  
                	System.out.println("年龄输入错误，请输入一个整数！");  	
                    sc.nextLine();
                }  
            }  
			 students.id+=1;
	            
			list.add(students);
		
		}

	}

	public static void print(ArrayList<Student> list){

		for(int i=0;i<list.size();i++){
			
			Student s=list.get(i);
			System.out.println("第"+(i+1)+"个学生名称为:"+s.name+",年龄为:"+s.age);
		}

	}
		
	public static void randomStudent(ArrayList<Student>list){
		Random r=new Random();
	    int num=r.nextInt(list.size());
	    Student s=list.get(num);
	    System.out.println("幸运儿是:"+s.name);
		
	}	
	}
