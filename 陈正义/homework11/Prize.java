package homework11;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class Prize {
	private int count=12;
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	private ArrayList<Integer> prize = new ArrayList<Integer>(Arrays.asList(10, 6, 20, 50, 100, 200, 500, 800, 2, 80, 300, 700));
	
	
	public ArrayList<Integer> getPrize() {
		return prize;
	}

	public  void setPrize(ArrayList<Integer> prize) {
		this.prize = prize;
	}

	public Prize(int count,ArrayList<Integer> prize ) {
		super();
		this.count = count;
		this.prize=prize;
	}
	public Prize( ) {
		
	}
	public static synchronized Integer lottery(Prize prize) {
		if(prize.getCount()>0) {
		Random r=new Random();
		int num=r.nextInt(prize.getPrize().size());
		int p= prize.getPrize().get(num);
		prize.getPrize().remove(num);
		int count=prize.getCount()-1;
		prize.setCount(count);;
		return p;
	}
		return -1;
	}
}
