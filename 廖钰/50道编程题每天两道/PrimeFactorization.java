package dailyProgram;

import java.util.ArrayList;
import java.util.Scanner;

public class PrimeFactorization {
	public static void main(String[] args) {
		System.out.println("请输入要质因数分解的数：");
		Scanner sc= new Scanner(System.in);
		int num = sc.nextInt();
		System.out.println("质因数分解结果为"+num+"="+primeFactorize(num));
	}

	private static String primeFactorize(int num) {
		int i=2,divided = num;
		ArrayList<Integer> arr = new ArrayList<Integer>();
		arr.add(1);
		while(divided!=1) {
			if(divided%i==0) {
				divided /= i;
				arr.add(i);
			}else {
				i++;
			}
		}
		String str = "";
		for(int l=0;l<arr.size();l++) {
			str += String.valueOf(arr.get(l));
			if(l!=arr.size()-1) {
				str += "*";
			}
		}
		return str;
	}
}
