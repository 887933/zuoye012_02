package demo;

import java.util.Arrays;
import java.util.Scanner;

public class No_2 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String a = scanner.next();
		int b = parstInt(a);
		System.out.println("a的值=" + b);
	}

	private static int parstInt(String a) {
		if(a.length()<1||a.length()>10){
			System.out.println("字符串长度错误");
			return -1;
		}
		if(a.charAt(0)=='0'){
			System.out.println("字符串不能以0开头");
			return -1;
		}
		for(int i=0;i<a.length();i++){
			if(a.charAt(i)>'0'&&a.charAt(i)<'9'){
				continue;
			}else{
				System.out.println("字符串只能是数字");
				return -1;
			}
		}
		char[] arr=a.toCharArray();
		int sum=0;
		for(int i=0;i<arr.length;i++){
			int num=(int)arr[i]-48;
			sum+=num*Math.pow(10,arr.length-i-1);
		}
		return sum;
		
	}
	
}
