package homework8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

public class work1 {
	public static void main(String[] args) throws IOException {
		FileInputStream fis=new FileInputStream("sort.txt");
		BufferedReader br=new BufferedReader(new InputStreamReader(fis,"utf-8"));
		ArrayList<String> arr=new ArrayList<String>();
		String temp=null;
		while((temp=br.readLine())!=null) {
			if(!temp.isEmpty())
			    arr.add(temp);
		}
		for(int i=0;i<arr.size()-i;i++) {
			for(int j=1;j<arr.size()-i;j++) {
				if(arr.get(j).charAt(0)<arr.get(j-1).charAt(0)) {
					temp=arr.get(j);
					arr.set(j,arr.get(j-1));
					arr.set(j-1,temp);
				}
			}
		}
		System.out.println(arr);
		FileOutputStream fos=new FileOutputStream("sort.txt");
		BufferedWriter bw=new BufferedWriter(new OutputStreamWriter(fos,"utf-8"));
		for(String i:arr) {
			bw.write(i);
			bw.newLine();
		}
		br.close();
		bw.close();
	}
}
