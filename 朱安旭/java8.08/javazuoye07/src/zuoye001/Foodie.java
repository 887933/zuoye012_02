package zuoye001;

import java.util.concurrent.ArrayBlockingQueue;

public class Foodie extends Thread{
	private ArrayBlockingQueue<String> list;

	public Foodie(ArrayBlockingQueue<String> list) {
		super();
		this.list = list;
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		while(true){
			try{
				Thread.sleep(100);
				String take=list.take();
				System.out.println("消费者吃掉了一个"+take);
				}catch(InterruptedException e){
					e.printStackTrace();
				}
		}
	}
}
