package homework05;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
/*作业2： 生成的50个男生，50个女生.每一个信息占用一行。
运行效果：
70%的概率点到男生。
30%的概率点到女生.
就是随机点名：
第一次运行会打印一个同学: 只显示名称和性别慕毅翌-男,打印男生和女生的比例为 1：0
第二次运行会打印一个同学: 只显示名称和性别生宇邦-男,打印男生和女生的比例为 2：0
第3次运行会打印一个同学: 只显示名称和性别山妙婧-女,打印男生和女生的比例为 2：1
第4次运行会打印一个同学: 只显示名称和性别生宇邦-男,打印男生和女生的比例为 3：1
第5次运行会打印一个同学: 只显示名称和性别生宇邦-男,打印男生和女生的比例为 4：1
第6次运行会打印一个同学: 只显示名称和性别生宇邦-男,打印男生和女生的比例为 5：1
第7次运行会打印一个同学: 只显示名称和性别林婉莹-女,打印男生和女生的比例为 5：2
第8次运行会打印一个同学: 只显示名称和性别生宇邦-男,打印男生和女生的比例为 6：2和性别z
第9次运行会打印一个同学: 只显示名称和性别鲍珊怡-女,打印男生和女生的比例为 6：3*/

public class homework04 {
	public static void main(String[] args) throws IOException {
		ArrayList<Student> list=new ArrayList<>();
		BufferedReader br=new BufferedReader(new FileReader("fw1.txt"));
		String line;
		//生宇邦-男-21-1
		while((line=br.readLine())!=null){
			String[] arr = line.split("-");
			list.add(new Student(arr[0], arr[1], Integer.parseInt(arr[2]),Double.parseDouble(arr[3]) ));
		}
		br.close();
		double weight=0;
		for(Student student:list){
			weight=weight+student.getWeight();
		}
		double[] arr=new double[list.size()];
		int j=0;
		for(Student student:list){
			arr[j]=student.getWeight()/weight;
			j++;
		}
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i]+arr[i-1];
		}
		System.out.println(Arrays.toString(arr));
		double number=Math.random();
		System.out.println(number);
		int result = -Arrays.binarySearch(arr, number)-1;
		System.out.println(result);
		Student student=list.get(result);
		String name=student.getName();
		String gender=student.getGender();
		System.out.println(name+"-"+gender);
		BufferedReader fr=new BufferedReader(new FileReader("proportion.txt"));
		String lin=fr.readLine();
		fr.close();
		String[] a=lin.split(":");
		int maleCount=Integer.parseInt(a[0]);
		int femaleCount=Integer.parseInt(a[1]);
		if(gender.equals("男")){
			maleCount++;
		}else{
			femaleCount++;
		}
		System.out.println("男生和女生的比例为："+maleCount+":"+femaleCount);
		String ma=Integer.toString(maleCount);
		String fe=Integer.toString(femaleCount);
		BufferedWriter fw=new BufferedWriter(new FileWriter("proportion.txt"));
		fw.write(ma+":"+fe);
		fw.close();
	}
}
