package java08;

import java.io.File;
import java.util.HashMap;

public class java04 {
	public static void main(String[] args) throws ArrayIndexOutOfBoundsException{
		File src=new File("D:\\��ѵ\\java\\�μ�\\012Java008");
		HashMap<String, Integer> fmap=new HashMap<>();
		HashMap<String, Integer> findfile = findfile(src,fmap);
		System.out.println(findfile);
		
	}
	

	private static HashMap<String, Integer> findfile(File src,HashMap<String, Integer> fmap) throws ArrayIndexOutOfBoundsException{
		File[] listFiles = src.listFiles();
		
		if(listFiles!=null){
			for (File file : listFiles) {
				if(file.isFile()){
					String name = file.getName();
					String extension = getExtension(name);
					if(extension!=null){
					if(fmap.containsKey(extension)){
						Integer value = fmap.get(extension);
						fmap.put(extension, value+1);
					}
					else{
						fmap.put(extension, 1);
					}
					}
				}else{
                     findfile(file, fmap);
				}
		}
		}
		return fmap;
	}
	private static String getExtension(String filename) {
        int lastDotIndex = filename.lastIndexOf('.');
        if (lastDotIndex > 0 && lastDotIndex < filename.length() - 1) {
            return filename.substring(lastDotIndex + 1).toLowerCase();
        }
        return null;
    }

}
