package demo1;
//插入排序
import java.util.Arrays;

public class java01 {
    public static void main(String[] args) {
        //定义数组
        int[] arr = {99, 55, 2, 3, 9, 10, 22, 34, 67, 89, 69, 92};
        //未排序序列的开头位置
        int index = 1;
        sort(arr, index);
        System.out.println(Arrays.toString(arr));
       
    }
 

    public static void sort(int[] arr, int index) {
   
        int target = index;
        int current = arr[index];
        for (int i = index - 1; i >= 0; i--) {
            if (arr[i] > current) {
                arr[i + 1] = arr[i];
                target = i;
            } else {  
                break;
            }
        }
        //插入
        arr[target] = current;
        index++;
        if (index < arr.length) {
            sort(arr, index);
        }
    }
}