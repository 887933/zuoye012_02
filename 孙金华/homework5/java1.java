package homework5;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class java1 {
	public static void main(String[] args) {
		ArrayList<String> arr=new ArrayList<>();
		Collections.addAll(arr,"张三,21","李四,24","丁真,26","珍珠,27","锐刻五,30");
		HashMap<String,Integer> result=new HashMap<String, Integer>();
		for(String temp:arr) {
			String[] a=temp.split(",");
			Integer b=Integer.valueOf(a[1]);
			if(b>24)
				result.put(a[0],b);
		}
		System.out.println(result);
	}
}
