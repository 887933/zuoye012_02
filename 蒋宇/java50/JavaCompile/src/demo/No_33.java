package demo;

public class No_33 {
	public static void main(String[] args) {
		//     1
		//    1 1
		//   1 2 1
		//  1 3 3 1
		// 1 4 6 4 1
		//1 5 10 10 5 1
		for(int i=1;i<=6;i++){
			for(int j=6-i;j>=0;j--){
				System.out.print(" ");
			}
			for(int j=1;j<=i;j++){
				System.out.print(getNum(i,j));
				System.out.print(" ");
			}
			System.out.println("");
		}
	}
	public static int getNum(int i,int j){
		if(j==1||j==i){
			return 1;
		}
		return getNum(i-1,j-1)+getNum(i-1,j);
	}
}
