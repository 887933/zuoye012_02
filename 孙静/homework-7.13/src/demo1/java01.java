package demo1;

import java.util.Scanner;

////作业1:输入三个数，求最大值
public class java01 {
	@SuppressWarnings("resource")
	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		System.out.println("请输入三个数");
		int[] number = new int[3];
		int max=0;
		
		for(int i =0;i<3;i++){
			number[i]=sc.nextInt();
			if(max<number[i]){
				max=number[i];
			}
		}
		System.out.println(max);
	}
}
