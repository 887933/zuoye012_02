package homework11;
//作业1：抽奖金额
//集合：[10,6,20,50,100,200,500,800,2,80,300,700]
//
//两个抽奖箱，抽奖箱1，抽奖箱2.
// 打印控制台的格式：
//  	每次抽出一个奖项随机打印一个只:
//  抽奖箱1产生一个10元大奖
//抽奖箱2产生一个500元大奖
//抽奖箱1产生一个20元大奖
//抽奖箱1产生一个200元大奖
//抽奖箱2产生一个800元大奖
public class Demo1 {
	public static void main(String[] args) {
		Prize prize=new Prize();
		DrawBox1 d1=new DrawBox1(prize);
		DrawBox2 d2=new DrawBox2(prize);
		d1.start();
		d2.start();
	}
}
