package java09;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Scanner;

public class java02 {
	public static void main(String[] args) throws Exception, FileNotFoundException {
		BufferedReader br=new BufferedReader(new InputStreamReader(new FileInputStream("user.txt"),"GBK"));
		String s;
		HashMap<String, Integer> hm=new HashMap<>();
		while((s=br.readLine())!=null){
			String[] split = s.split("=");
			hm.put(split[0], Integer.parseInt(split[1]));
		}
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入用户名：");
		String username=sc.next();
		if(hm.get(username)==null){
			System.out.println("用户名不存在");
		}
		else{
			System.out.print("请输入密码：");
			int pasw=sc.nextInt();
			if(pasw==hm.get(username)){
				System.out.println("成功登陆");
			}
			else{
				int count=0;
				boolean b=true;
				while(b){
					if(count==3){
						long start = System.currentTimeMillis();
						long end = System.currentTimeMillis();
						while((end-start)/1000<10){
							System.out.println("输入错误三次，请等待十秒");
							sc.next();
							 end = System.currentTimeMillis();
						}
						count++;
					}
					else if(count==6){
						System.out.println("输入错误次数过多，账号被冻结");
						break;
					}
					else{
						System.out.print("密码错误，请重新输入：");
						pasw=sc.nextInt();
						if(pasw==hm.get(username)){
							b=true;
							System.out.println("成功登陆");
							break;
						}
						else{
							count++;
						}
					}
				}
			}
		}
	}
}
