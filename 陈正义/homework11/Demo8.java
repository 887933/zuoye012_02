package homework11;
//4.写两个线程，一个线程打印1~ 52，另一个线程打印A~Z，打印顺序是12A34B…5152Z
public class Demo8 {
	static Object lock=new Object();
	static char a='A';
	public static void main(String[] args) {
		
		
		for(int i=1;i<=26;i++) {
			int n=i;
			Thread t1=new Thread(()->{
				int number=n;
				synchronized (lock) {
					System.out.println(number*2-1);
					System.out.println(number*2);
					
					
					
				}
			});
			Thread t2=new Thread(()->{
				
				char s=(char)(a+n-1);
				synchronized (lock) {
					System.out.println(s);
				}
				
			});
			
			t1.start();
			try {
				t1.sleep(100);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			t2.start();
			try {
				t1.join();
				t2.join();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
}
