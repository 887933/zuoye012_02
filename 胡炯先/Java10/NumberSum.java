package java13;

import java.util.ArrayList;

import java.util.concurrent.atomic.AtomicInteger;  

public class NumberSum {  

	private static final int THREAD_COUNT = 10;  
	private static final AtomicInteger totalSum = new AtomicInteger(0);  

	public static void main(String[] args) throws InterruptedException {  
		Thread[] threads = new Thread[THREAD_COUNT];  
		for (int i = 0; i < THREAD_COUNT; i++) {  
			final int startIndex = i * 10 + 1;  
			final int endIndex = startIndex + 9;  

			threads[i] = new Thread(() -> {  
				int sum = 0;  
				for (int j = startIndex; j <= endIndex; j++) {  
					sum += j;  
				}  
				totalSum.addAndGet(sum);  
			});  

			threads[i].start();  
		}  

		for (Thread thread : threads) {  
			thread.join();  
		}  

		System.out.println("Total sum: " + totalSum.get());  
	}  
}

