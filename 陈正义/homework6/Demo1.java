//1.创建一个arraList集合,并添加以下字符串,字符串中前面是姓名 ,后面是年龄
//	 *  "zhangsn,23"
//	 *  "lisi,24"
//	 *  "wangwu,25"
//	 *  保留年龄大于24岁的,并将结果收集到map集合中,姓名为建,年龄为值  
//
//ArrayList<String> arr=new ArrayList();
//Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");

package homework6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;
public class Demo1 {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ArrayList<String> arr=new ArrayList<String>();
		Collections.addAll(arr, "张三,21", "李四,24", "丁真,26", "珍珠,27", "锐刻五,30");
		
		Map<String, Integer> map= arr.stream()
		.filter(s->Integer.parseInt(s.split(",")[1])>24)
		.collect(Collectors.toMap(s->s.split(",")[0], s->Integer.parseInt(s.split(",")[1])));
		System.out.println(map);
	}
}