package homework;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Java05_RemakeStringEncryptionNot {
	public static void main(String[] args) {
		/*
		 *  4.把任意字符串加密显示   
		 *  YUANzhi1987
		 *  加密后为
		 *  zvbo9441987
		 *  加密规则:
		 *   1.如果是大写字母变成小写字母,往后移动一位
		 *   2.小写字母通过收集输入发对应键盘来进行加密
		 *   1-1
		 *   abc-2
		 *   def-3
		 *   ghi-4
		 *   jkl-5
		 *   mno-6
		 *   pqrs-7
		 *   tuv-8
		 *   wxyz-9
		 *   0-0
		 *   3.数字不变
		 */
		
		String input = "YUANzhi19!7";
		String output = StringEncryption(input);
		System.out.println(output);
	}
	
	private static String StringEncryption(String input) {
		String output = "";
		char[] chararr = input.toCharArray();
		for(char c:chararr) {
			if(c>='A'&&c<='Z') {
				output += (char)Character.toLowerCase(c+1);
			}else if(c>='a'&&c<='z') {
				int count = 0;
				switch(c) {
				case 'a':case 'b':case 'c':
					count = 2;
					break;
				case 'd':case 'e':case 'f':
					count = 3;
					break;
				case 'g':case 'h':case 'i':
					count = 4;
					break;
				case 'j':case 'k':case 'l':
					count = 5;
					break;
				case 'm':case 'n':case 'o':
					count = 6;
					break;
				case 'p':case 'q':case 'r':case 's':
					count = 7;
					break;
				case 't':case 'u':case 'v':
					count = 8;
					break;
				case 'w':case 'x':case 'y':case 'z':
					count = 9;
					break;
				}
				output += count;
			}else if(c>='0'&&c<='9') {
				output += c;
			}else {
				return "含有非法字符！";
			}
		}
		return output;
	}
}
