package homework02;

import java.util.HashMap;

public class hom08 {
	public static void main(String[] args) {
        String s = "zahdsab";
        System.out.println(lengthOfLongestSubstring(s));
    }

    public static int lengthOfLongestSubstring(String s) {
        if (s == null || s.length() == 0) {
            return 0;
        }

        int start = 0;
        int maxLen = 0;
        int len = s.length();       
        HashMap<Character, Integer> map = new HashMap<>();

        for (int i = 0; i < len; i++) {
            if (map.containsKey(s.charAt(i)) && map.get(s.charAt(i)) >= start) {
                start = map.get(s.charAt(i)) + 1;
            } else {
                maxLen = Math.max(maxLen, i - start + 1);
            }
            map.put(s.charAt(i), i);
        }

        return maxLen;
    }
}
