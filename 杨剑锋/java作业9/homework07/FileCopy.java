package homework07;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

public class FileCopy extends Thread{
	private String sourceFilePath;
	private String destination;
	public FileCopy(String sourceFilePath, String destination) {
		super();
		this.sourceFilePath = sourceFilePath;
		this.destination = destination;
	}
	@Override
	public void run() {
		try {
			File sourceFile=new File(this.sourceFilePath);
			File destinationFolder=new File(this.destination);
			long fileSize=sourceFile.length();
			long copiedSize=0;
			FileInputStream fis = new FileInputStream(sourceFile);
			FileOutputStream fos = new FileOutputStream(destinationFolder);
			byte[] ch=new byte[1024];
			int bytesRead;
			while((bytesRead=fis.read(ch))!=-1){
				fos.write(ch,0,bytesRead);
				copiedSize+=bytesRead;
				double progress=(double)copiedSize/fileSize*100;
				double round = Math.round(progress);
				System.out.println(sourceFile.getName()+"�Ѹ���"+round+"%");
			}
			fis.close();
			fos.close();
			System.out.println(sourceFile.getName()+"�������");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
