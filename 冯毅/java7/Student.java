package java08;

public class Student {
    private int id;
    private String name;
    private int age;
    public int getId()
    {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAge() {
        return age;
    }
    public void setAge(int age)throws AgeException {
    	if(age<18||age>25){
    		throw new AgeException();
    	}
        this.age = age;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public boolean equals(Student t) {
        if(this.getAge()==t.getAge()){
            return true;
        }
        else
            return false;
    }
    public Student(String name, int age) {
        this.name=name;
        this.age=age;
    }
    public Student(int id, String name, int age) {
        this.id = id;
        this.name=name;
        this.age=age;
    }
    public Student() {

    }

	@Override
	public String toString() {
		return "Student [name=" + name + ", age=" + age + "]";
	}

	


}
