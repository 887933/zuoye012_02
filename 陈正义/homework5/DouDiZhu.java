package homework5;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;

public class DouDiZhu {
	public static void main(String[] args) {
		//准备牌
		ArrayList<String> poker=new ArrayList<String>();
		String[] colors= {"♥","♠","♦","♣"};
		String [] numbers= {"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("大王");
		poker.add("小王");
		for(String number:numbers) {
			for(String color:colors) {
				poker.add(color+number);
			}
		}
		System.out.println(poker);
		//洗牌
		Collections.shuffle(poker);
		System.out.println(poker);
		Player p1=new Player();
		Player p2=new Player();
		Player p3=new Player();
		p1.setName("老王");
		p2.setName("老李");
		p3.setName("老刘");
		ArrayList<String> dipai =new ArrayList<String>();
		for(int i=0;i<poker.size();i++) {
			String p=poker.get(i);
			if(i>=51) {
				dipai.add(p);
			}else if(i%3==0) {
				p1.getPoker().add(p);
			}else if(i%3==1) {
				p2.getPoker().add(p);
			}else if(i%3==2) {
				p3.getPoker().add(p);
			}
		}
		 sortPokers(p1);
	        sortPokers(p2);
	        sortPokers(p3);
		//看牌
		System.out.println(p1.getName()+"的牌："+p1.getPoker());
		System.out.println(p2.getName()+"的牌："+p2.getPoker());
		System.out.println(p3.getName()+"的牌："+p3.getPoker());
		System.out.println("底牌："+dipai);
		
		
		
		
		
		
		
		
	}

	private static void sortPokers(Player player) {
		// TODO Auto-generated method stub
	     Collections.sort(player.getPoker(), new Comparator<String>() {
	            @Override
	            public int compare(String card1, String card2) {
	                String[] ranks = { "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K", "A", "2", "小王", "大王"};
	                int index1 = -1, index2 = -1;
	                for (int i = 0; i < ranks.length; i++) {
	                    if (ranks[i].equals(card1.substring(1))) index1 = i;
	                    if (ranks[i].equals(card2.substring(1))) index2 = i;
	                }
	                if (card1.equals("大王") || card1.equals("小王")) index1 = ranks.length - 1;
	                if (card2.equals("大王") || card2.equals("小王")) index2 = ranks.length - 1;
	                return Integer.compare(index1, index2);
	            }
	        });
	}
}
