package homework;
//循环作业3：  求平方根
import java.util.Scanner;

public class For3 {
	public static void main(String args[]) {
		System.out.println("请输入一个数");
		Scanner sc = new Scanner(System.in);
		int num=sc.nextInt();
		//circulate作为标签使用
		circulate:for(int i=0;i<=num;i++) {
			if(i*i==num) {
				System.out.println(num+"的算术平方根为:"+i);
				break circulate;
			}
			else if(i*i>num){
				System.out.println(num+"的算术平方根在"+(i-1)+"~"+i+"之间");
				break circulate;
			}
		}
	}

}
