package CallName;

import java.util.InputMismatchException;

public class Student {
  String name;
   int age;
   int id;
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) throws AgeException{
	if(age<18||age>25){
		throw new AgeException();
	}
	this.age = age;
}

public Student(String name, int age, int id) {
	super();
	this.name = name;
	this.age = age;
	this.id = id;
}
public Student(){
	
}
	
}
