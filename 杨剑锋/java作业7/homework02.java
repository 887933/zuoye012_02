package hom05;

import java.io.File;

//作业1：在当前模块下创建文件夹abc，再在 abc文件中创建文件a.txt
public class homework02 {
	public static void main(String[] args) {
		File file=new File("abc");
		if(!file.exists()){
			boolean result=file.mkdirs();
			if(result){
				System.out.println("目录创建成功");
			}else{
				System.out.println("目录创建失败");
			}
		}
		File f1=new File(file,"a.txt");
		if(!f1.exists()){
			try {
				boolean result = f1.createNewFile();
				if(result){
					System.out.println("文件创建成功");
				}else{
					System.out.println("文件创建失败");
				}
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			}
		}
	}
}
