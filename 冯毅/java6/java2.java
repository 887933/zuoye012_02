package java07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.stream.Stream;

public class java2 {
	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		Stream<String> boy = list.stream().filter(s->"男".equals(s.split("-")[1])&&s.split("-")[0].length()==3).map(s->s.split("-")[0]);
		Stream<String> girl = list.stream().filter(s->"女".equals(s.split("-")[1])&&s.split("-")[0].charAt(0)=='林').skip(1).map(s->s.split("-")[0]);
		Stream.concat(boy, girl).forEach(System.out::println);
	}
}
