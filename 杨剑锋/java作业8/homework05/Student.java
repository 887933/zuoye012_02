package homework05;

public class Student {
	private String name;
	private String gender;
	private int age;
	private double weight;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public Student(String name, String gender, int age, double weight) {
		super();
		this.name = name;
		this.gender = gender;
		this.age = age;
		this.weight = weight;
	}
	public Student() {
	}
	@Override
	public String toString() {
		return name + "-" + gender + "-" + age + "-" +weight;
	}
}
