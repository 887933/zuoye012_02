package zuoye9;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class FileCopyThread extends Thread{
	private String  sourceFile;
	private String destFile;
	public FileCopyThread(String sourceFile, String destFile) {
		super();
		this.sourceFile = sourceFile;
		this.destFile = destFile;
	}
	public FileCopyThread(){

	}
	@Override
	public void run() {

		try{
			File file=new File(sourceFile);
			FileInputStream fis=new FileInputStream(sourceFile);
			FileOutputStream fos=new FileOutputStream(destFile);
			long Filesize =sourceFile.length();
			long copysize=0;

			byte[] buffer=new byte[4096];
			int b;
			while((b=fis.read(buffer))!=-1){
				fos.write(buffer, 0, b);
				copysize+=b;
                 
				int progress= (int)((copysize)/(Filesize*100));
                 if(progress%10==0){
				System.out.println(file.getName()+"文件已复制"+progress+"%");
                 }
                 if(progress>=100){
                	 System.out.println(file.getName()+"复制完成!");
                	 break;
                 }
			}
			

		}catch(Exception e){
			e.printStackTrace();
		}








	} 











}
