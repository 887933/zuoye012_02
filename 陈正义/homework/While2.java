package homework;
//whilez作业2：高度8000m，有一张足够大的纸，厚度为0.001m，请问，折叠多少次，不低于8000m?
public class While2 {
	public static void main(String args[]) {
		double thickness=0.001;
		int i=0;
		while(thickness<8000) {
			thickness=thickness*2;
			i++;
		}
		System.out.println("折叠"+i+"次，不低于8000m");
	}

}
