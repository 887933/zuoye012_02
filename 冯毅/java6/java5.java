package java07;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class java5 {
	public static void main(String[] args) {
		ArrayList<Integer> a1=new ArrayList<Integer>();
		Collections.addAll(a1,1,5,6,7,2,9,3);
		ArrayList<Integer> a2=new ArrayList<Integer>();
		Collections.addAll(a2,1,2,3,4,5);
		Map<Integer, Long> map = Stream.concat(a1.stream(), a2.stream())
		.collect(Collectors.groupingBy(c->c,Collectors.counting()));
		Set<Entry<Integer, Long>> entrySet = map.entrySet();
		ArrayList<Integer> newNum=new ArrayList<Integer>();
		for(Entry<Integer, Long> i:entrySet){
			if(i.getValue()==1){
				newNum.add(i.getKey());
			}
		}
		System.out.println(newNum);
	}
}
