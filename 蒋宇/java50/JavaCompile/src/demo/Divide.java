package demo;

import java.util.ArrayList;
import java.util.Scanner;

public class Divide {
	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入你想分解质因数的数");
		int num=sc.nextInt();
		String k=num+"=";
		for(int i=2;i<num;){
			if(judge(i)){
				if(num%i==0){
					num/=i;
					k=k+i+"*";
				}else{
					i++;
					continue;
				}
			}else{
				i++;
			}
		}
		k+=num;
		System.out.println(k);
	
		
	}
	
	private static boolean judge(int i) {
		if(i==1||i%2==0&&i!=2){
			return false;
		}
		for(int j=2;j<=Math.sqrt(i);j++){
			if(i%j==0){
				return false;
			}
		}
		return true;
		
	}

}
