package zuoye3;

import java.util.ArrayList;
import java.util.Random;

public class PrizeList{
     static ArrayList<Integer> list=new ArrayList<>();

	 static{
		 list.add(10);
		 list.add(6);
		 list.add(20);
		 list.add(50);
		 list.add(100);
		 list.add(200);
		 list.add(500);
		 list.add(800);
		 list.add(2);
		 list.add(80);
		 list.add(300);
		 list.add(700);
	 }
	public synchronized Integer drawPrize(){
		if(list.isEmpty()){
			return null;
		}
		Random r=new Random();
		int index=r.nextInt(list.size());
		Integer prize = list.remove(index);	
		return prize;
		
		
	}
	
}
