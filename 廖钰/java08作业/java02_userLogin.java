package homework;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class java02_userLogin {
	public static void main(String[] args) throws IOException, ParseException {
		/*
		 * 作业2：在文件中准备用户名和密码
		 * 每个用户名和密码输入错误三次后，锁定十秒钟；如果再次输入三次后错误，将账号冻结
		 * 
		 */
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入用户名：");
		String username = sc.next();
		userLogin(username);
		sc.close();
	}
	
	private static void userLogin(String username) throws IOException, ParseException {
		Scanner sc = new Scanner(System.in);
		BufferedReader br= new BufferedReader(new FileReader("user.txt"));
		String line = null;
		ArrayList<String> list= new ArrayList<String>(); 
		while((line=br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		
		//挨个判断是哪个账号
		boolean isFindUser = false;
		for(int i=0;i<list.size();i++) {
			String[] strings = list.get(i).split(",");
			int errorTimes = Integer.parseInt(strings[2]);
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			
			//纪录上次登录/登出时间，用于计算账号锁定时间
			Date date = (Date) sdf.parse(strings[3]);
			
			//判断是否找到用户，找到则将isFindUser置true
			if(strings[0].equals(username)) {
				isFindUser = true;
				
				if(strings[4].equals("1")) {
					System.out.println("该账号已被冻结！请联系客服申请解冻！");
					break;
				}
				
				//密码判断循环
				if(errorTimes<3) {
					for(;;) {
						//输入密码
						System.out.print("请输入密码：");
						String password = sc.next();
						
						if(strings[1].equals(password)) {
							System.out.println("登陆成功！");
							//修改list中的内容
							date = new Date(System.currentTimeMillis());
							String time = sdf.format(date);
							list.set(i, strings[0]+","+strings[1]+","+0+","+time+","+0);
							//break
							break;
							
						}else if(errorTimes<2){
							errorTimes++;
							System.out.println("密码错误，请重新输入！你还有"+(3-errorTimes)+"次机会");
							
						}else if(errorTimes==2) {
							System.out.println("错误次数太多，账号锁定10秒！");
							//修改list中的内容
							date = new Date(System.currentTimeMillis()+10L*1000);
							String time = sdf.format(date);
							list.set(i, strings[0]+","+strings[1]+","+3+","+time+","+0);
							//break
							break;
						}
						
					}
					
				//锁定后三次机会密码判断
				}else {
					Date date2 = new Date(System.currentTimeMillis());
					if(date.compareTo(date2)>0) {
						System.out.println("账号锁定中，请稍后登录！");
						break;
					}
					
					for(;;) {
						//输入密码
						System.out.print("请输入密码：");
						String password = sc.next();
						if(strings[1].equals(password)) {
							System.out.println("登陆成功！");
							//修改list中的内容
							date = new Date(System.currentTimeMillis());
							String time = sdf.format(date);
							list.set(i, strings[0]+","+strings[1]+","+0+","+time+","+0);
							//break
							break;
							
						}else if(errorTimes<5){
							errorTimes++;
							System.out.println("密码错误，请重新输入！你还有"+(6-errorTimes)+"次机会");
							
						}else if(errorTimes==5) {
							System.out.println("错误次数太多，账号已冻结！");
							//修改list中的内容
							date = new Date(System.currentTimeMillis());
							String time = sdf.format(date);
							list.set(i, strings[0]+","+strings[1]+","+6+","+time+","+1);
							//break
							break;
							
						}
						
					}
					
				}
			}
			//如果已经找到过用户，不再检查后续账号，直接跳出循环
			if(isFindUser) {
				break;
			}
		}
		if(!isFindUser) {
			System.out.println("该账号不存在！");
		}
		
		sc.close();
		br.close();
		
		//将文件内容重新写入
		FileWriter fw = new FileWriter("user.txt");
		BufferedWriter out = new BufferedWriter(fw);
		for(int i=0;i<list.size();i++) {
			out.write(list.get(i));
			out.newLine();
		}
		out.close();
	}
}
