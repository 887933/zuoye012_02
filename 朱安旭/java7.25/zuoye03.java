package demo01;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.function.Function;
import java.util.stream.Collectors;

public class zuoye03 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		ArrayList<Character> list=new ArrayList<>();
		HashMap<Character,Integer> arr=new HashMap<>();
		System.out.println("请输入一串字符串");
		String str;
		str = sc.next();
		char[] ch=str.toCharArray();

		for(char c : ch)
		{
			list.add(c);
		}
		list.stream().forEach(t->arr.compute(t,(k,v)->(v == null)? 1 : v+1));
		System.out.println("请输入想要查询出现了几次的字符");
		int number = sc.nextInt();
		list.stream().filter(t->arr.get(t)==number).forEach(s->System.out.print(s));
}
}