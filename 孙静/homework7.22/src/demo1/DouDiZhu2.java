package demo1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class DouDiZhu2 {
	public static void main(String[] args) {
		//准备牌
		ArrayList<String> poker=new ArrayList<String>();
		String[] colors={"♠","♣","♦","♥"};
		String[] numbers={"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("大王");
		poker.add("小王");
		
		for(String number:numbers){
			for(String color:colors){
				poker.add(color+number);
			}
		}
		System.out.println(poker);
		//洗牌
		Collections.shuffle(poker);
		System.out.println(poker);
		
		Player p1=new Player();p1.setName("老王");
		Player p2=new Player();p2.setName("老李");
		Player p3=new Player();p3.setName("老六");
		
		ArrayList<String> dipai=new ArrayList<String>();
		//摸牌
		for(int i=0;i<poker.size();i++){
			//获取每一张的牌
			String p = poker.get(i);
			if(i>=51){
				dipai.add(p);
			}else if(i%3==0){
				p1.getPoker().add(p);
			}else if(i%3==1){
				p2.getPoker().add(p);
			}else if(i%3==2){
				p3.getPoker().add(p);
			}
		}
		//看牌
		
		Collections.sort(p1.getPoker(),new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				//
				int result=o1.charAt(1)-o2.charAt(1);
				return result;
			}
		});
		Collections.sort(p2.getPoker(),new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				//
				int result=o1.charAt(1)-o2.charAt(1);
				return result;
			}
		});
		Collections.sort(p3.getPoker(),new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				//
				int result=o1.charAt(1)-o2.charAt(1);
				return result;
			}
		});
		
//		System.out.println(p1.getPoker());
		System.out.println(p1.getName()+"的牌:"+p1.getPoker());
		System.out.println(p2.getName()+"的牌:"+p2.getPoker());
		System.out.println(p3.getName()+"的牌:"+p3.getPoker());
		System.out.println("底牌:"+dipai);
	}
}
