package java02;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class java_11 {

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<>();
        list.add(10);
        list.add(20);
        list.add(30);
        System.out.print(list);
        String a = arr(list);
        System.out.println(a);
    }

    public static String arr(ArrayList<Integer> list) {
        StringBuilder a = new StringBuilder("{");
        for (int i = 0; i < list.size(); i++) {
        a.append(list.get(i));
        if(i != list.size()-1){
                a.append("@");
        }
        }
        a.append("}");
    return a.toString();
    }
}
