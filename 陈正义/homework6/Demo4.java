//4.把任意字符串加密显示   
//	 *  YUANzhi1987
//	 *  加密后为
//	 *  zvbo9441987
//	 *  加密规则:
//		 *   1.如果是大写字母变成小写字母,往后移动一位
//		 *   2.小写字母通过收集输入发对应键盘来进行加密
//		 *   1-1
//		 *   abc-2
//		 *   def-3
//		 *   ghi-4
//		 *   jkl-5
//		 *   mno-6
//		 *   pqrs-7
//		 *   tuv-8
//		 *   wxyz-9
//		 *   0-0
//		 *   3.数字不变
package homework6;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
public class Demo4 {
	public static void main(String[] args) {
		System.out.println("请输入一个字符串：");
		Scanner sc=new Scanner(System.in);
		String str=sc.next();
		char[] arr=str.toCharArray();
		List<Character> list=new ArrayList<Character>();
		for(char i:arr) {
			list.add(i);
		}
		List<Character> ch=  list.stream().map(c->{if(c>='A'&&c<='Z') {
			return (char)(c=='z'?'A':c+1);
			}else if (c >= 'a' && c <= 'c') {
                return '2';
            } else if (c > 'c' && c <= 'f') {
                return '3';
            } else if (c > 'f' && c <= 'i') {
                return '4';
            } else if (c >= 'j' && c <= 'l') {
                return '5';
            } else if (c >= 'm' && c <= 'o') {
                return '6';
            } else if (c >= 'p' && c <= 's') {
                return '7';
            } else if (c >= 't' && c <= 'y') {
                return '8';
            } else if (c >= 'w' && c <= 'z') {
                return '9';
            } else if (c >= '0' && c <= '9') {
                return c;
            } else {
                return c; 
            }
        }).collect(Collectors.toList());
		System.out.println(ch);
	}
}
