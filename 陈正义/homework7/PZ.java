package homework7;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class PZ {
	public static void main(String[] args) throws Exception {
		String strXingURL="https://hanyu.baidu.com/shici/detail?from=kg1&highlight=&pid=0b2f26d4c0ddb3ee693fdb1137ee1b0d&srcid=51369";
		String MmingXingURL="https://baijiahao.baidu.com/s?id=1709777057514537851";
		String FmingXingURL="http://m.mingzi.jb51.net/qiming/baobao/22476.html";

		//读取网页中的整个网页的内容
		String xingContet=webCarawler(strXingURL);
//		System.out.println(xingContet);
		String FmingContet=webCarawler(FmingXingURL);
//		System.out.println(mingContet);
		String MmingContet=webCarawler(MmingXingURL);
		
		//正则表达式
		//赵钱孙李，周吴郑王。
		ArrayList<String> listXing=getDataXing(xingContet,"(.{4})(，|。)");
//		System.out.println(listXing);
		//舒苒：
		ArrayList<String> FlistMing=getDataMing(FmingContet,"..：");
//		System.out.println(listMing);
//		京墨、凌霄、乐允、嘉述
		ArrayList<String> MlistMing=getDataMing(FmingContet,"(..、){3}..");
		//处理数据   
		ArrayList<String> xing=execDataXing(listXing);
		System.out.println(xing);
		
		ArrayList<String> Fming=FexecDataMing(FlistMing);
		System.out.println(Fming);
		ArrayList<String> Mming=MexecDataMing(MlistMing);
		System.out.println(Mming);
		//合起来
		ArrayList<String> FxingMing=getInfo(xing,Fming,50);
		System.out.println(FxingMing);
		ArrayList<String> MxingMing=getInfo(xing,Mming,50);
		System.out.println(MxingMing);
		ArrayList<String> listnan1=mingData(MxingMing,"男");
		ArrayList<String> listnv1=mingData(FxingMing,"女");
		listnan1.addAll(listnv1);
		System.out.println(listnan1);
		FileWriter fw=new FileWriter("names.txt");
		for (String string : listnan1) {
			fw.write(string);
			fw.write("\n");
			fw.flush();
		}
		fw.close();
	}
	private static ArrayList<String> mingData(ArrayList<String> mxingMing, String string) {
		// TODO Auto-generated method stub
		Random r=new Random();
		ArrayList<String> xing=new ArrayList<>();
		for(String k: mxingMing){
			
				xing.add(k+"-"+string+"-"+(r.nextInt(8)+18));
			
		}// TODO Auto-generated method stub
		return xing;
	}
	private static ArrayList<String> getInfo(ArrayList<String> xing, ArrayList<String> ming, int count) {
		HashSet<String> set=new HashSet<>();
		while(true){
			if(set.size()==count){
				break;
			}
			Collections.shuffle(xing);
			Collections.shuffle(ming);
			set.add(xing.get(0)+ming.get(0));
		}
		return new ArrayList(set);
	}

	private static ArrayList<String> MexecDataMing(ArrayList<String> listMing) {
		ArrayList<String>  ming=new ArrayList<>();
		for (String string : listMing) {
			//哲睿、益昌、夏明、胜才、浚龙
			String[] split = string.split("、");
		
			for (String string2 : split) {
				ming.add(string2);
			}
		}
		return ming;
	}
	private static ArrayList<String> FexecDataMing(ArrayList<String> listMing) {
		ArrayList<String>  ming=new ArrayList<>();
		for (String string : listMing) {
			//哲睿、益昌、夏明、胜才、浚龙
			String[] split = string.split("：");
		
			for (String string2 : split) {
				ming.add(string2);
			}
		}
		return ming;
	}

	private static ArrayList<String> execDataXing(ArrayList<String> listXing) {
		ArrayList<String>  xing=new ArrayList<>();
		for (String string : listXing) {
			//赵钱孙李，或者周吴郑王。
			for(int i=0;i<string.length()-1;i++){
				char charAt = string.charAt(i);
				xing.add(charAt+"");
			}
		}
		return xing;
	}

	private static ArrayList<String> getDataMing(String xingContet, String regex) {
		ArrayList<String> list=new ArrayList<>();
		//按照正则的规则生成
		Pattern pattern = Pattern.compile(regex);
		//按照 pater规则，找到str当中获取的数据
		Matcher matcher = pattern.matcher(xingContet);
		while(matcher.find()){
			String group = matcher.group();
			if(group.indexOf('》')==-1){
				list.add(group);
				}
		}
		return list;
	}

	private static ArrayList<String> getDataXing(String xingContet, String regex) {
		ArrayList<String> list=new ArrayList<>();
		//按照正则的规则生成
		Pattern pattern = Pattern.compile(regex);
		//按照 pater规则，找到str当中获取的数据
		Matcher matcher = pattern.matcher(xingContet);
		while(matcher.find()){
			String group = matcher.group();
			if(!group.contains("em")){
				list.add(group);
			}
		}
		return list;
	}

	private static String webCarawler(String strXingURL) throws Exception {
		StringBuilder sb=new StringBuilder();
		//创建一个url对象
		URL url =new URL(strXingURL);
		//连接上这个网站，确保网络通常
		URLConnection openConnection = url.openConnection();
		InputStream inputStream = openConnection.getInputStream();
		//读取数据
		InputStreamReader isr=new InputStreamReader(inputStream,"utf-8");
		int ch;
		while((ch=isr.read())!=-1){
			sb.append((char)ch);
		}
		isr.close();
		return sb.toString();
	}
}
