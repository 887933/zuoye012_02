package homework7;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Set;

public class work3 {
	public static void main(String[] args) {
		File src=new File("f:\\");
		countfile(src);
	}
	public static void countfile(File src) {
		HashMap<String,Integer> map=new HashMap<String, Integer>();
		part(src,map);
		Set<String> key=map.keySet();
		for(String i:key) {
			System.out.println(i+"文件数量:"+map.get(i));
		}
	}
	public static void part(File src,HashMap<String,Integer> map) {
		if(src.isDirectory()) {
			File[] files=src.listFiles();
			if(files!=null) {
				if(files.length==0) {
					return;
				}
				else {
					for(File i:files) {
						part(i,map);
					}
				}
			}
		}
		else {
			String name=src.getName();
			if(name.contains(".")) {
				String[] names=name.split("\\.");
				if(map.containsKey(names[1])) {
					map.put(names[1],map.get(names[1])+1);
				}
				else {
					map.put(names[1],1);
				}
			}
			else {
				String temp="无后缀";
				if(map.containsKey(temp)) {
					map.put(temp,map.get(temp)+1);
				}
				else {
					map.put(temp,1);
				}
			}
		}
	}
}
