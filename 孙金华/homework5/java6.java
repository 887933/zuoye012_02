package homework5;

import java.util.ArrayList;

public class java6 {
	public static void main(String[] args) {
		int a[]= {1,4,2,7,0};
		int b[]= {2,8,0,2,1,9};
		System.out.println(distinct(a,b));
	}
	public static ArrayList<Integer> distinct(int[] a,int[] b){
		ArrayList<Integer> result=new ArrayList<Integer>();
		for(int i:a) {
			if(!result.contains(i))
				result.add(i);
		}
		for(int i:b) {
			if(!result.contains(i))
				result.add(i);
		}
		return result;
	}
}
