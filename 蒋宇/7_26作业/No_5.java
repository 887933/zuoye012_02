package HomeWork;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;

public class No_5 {
	public static void main(String[] args) {
        File f1 = new File("fnew.txt");
        FileInputStream fis = null;
        FileOutputStream fos = null;
        
        try {
            fis = new FileInputStream(f1);
            byte[] a = new byte[(int) f1.length()];
            fis.read(a);
            
            String str = new String(a);
            String[] split = str.split("-");
            Integer[] so = new Integer[split.length];
            
            for (int i = 0; i < split.length; i++) {
                int parseInt = Integer.parseInt(split[i]);
                so[i] = parseInt;
            }
            
            Arrays.sort(so);
            
            fos = new FileOutputStream(f1);
            String sortedData = Arrays.toString(so).replaceAll(",","-");
            
            fos.write(sortedData.getBytes());
            
            System.out.println("Data sorted and written back to the file successfully.");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fis != null) {
                    fis.close();
                }
                if (fos != null) {
                    fos.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
		
		

