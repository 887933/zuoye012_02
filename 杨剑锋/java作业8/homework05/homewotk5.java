package homework05;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;


/*作业3： 生成的50个男生，50个女生.每一个信息占用一行。

就是随机点名：
第一次运行会打印一个同学: 只显示名称慕毅翌,
第2次运行会打印一个同学: 只显示名称鲍珊怡,
第3次运行会打印一个同学: 只显示名张三珊怡,
第4次运行会打印一个同学: 只显示名生宇邦珊怡,
第5次运行会打印一个同学: 只显示名林婉莹珊怡,
第6次运行会打印一个同学: 只显示名张三珊怡,*/

public class homewotk5 {
	public static void main(String[] args) throws IOException {
		ArrayList<Student1> list = new ArrayList<>();
		BufferedReader fr=new BufferedReader(new FileReader("fw2.txt"));
		String lin;
		while((lin=fr.readLine())!=null){
			String[] arr=lin.split("-");
			list.add(new Student1(arr[0],arr[1],Integer.parseInt(arr[2])));
		}
		fr.close();
		Random r=new Random();
		int num=list.size();
		int index=r.nextInt(num);
		Student1 student=list.get(index);
		String name=student.getName();
		System.out.println(name);
		list.remove(index);
		BufferedWriter fw=new BufferedWriter(new FileWriter("fw2.txt"));
		for(Student1 s:list){
			String str=s.toString();
			fw.write(str);
			fw.newLine();
		}
		fw.close();
	}
}
