package dame;

import java.util.Scanner;

public class java02_12 {
    public static void main(String[] args) {
    	int[] arr={1,2,3,4,5,6,7,8,9};
    	System.out.print("请输入第一个个数字：");
    	Scanner sc=new Scanner(System.in);
    	int num1=sc.nextInt();
    	System.out.print("请输入第二个个数字：");
    	int num2=sc.nextInt();
    	int[] arrB=copyArr(num1,num2,arr);
    	for(int i=0;i<arrB.length;i++){
    		System.out.print(arrB[i]);
    	}
	}
    public static int[] copyArr(int num1,int num2,int[] arr){
    	int num3=num2-num1+1;
    	int[] arr2=new int[num3];
    	int k=0;
    	for(int i=num1;i<num2+1;i++){
    		arr2[k]=arr[i];
    		k++;
    	}
    	return arr2;
    }
}
