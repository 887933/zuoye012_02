package homework8;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Scanner;

public class Login {
	public static void main(String[] args) throws IOException {
		ArrayList<String[]> list=new ArrayList<String[]>();
		FileInputStream fis=new FileInputStream("abc\\user.txt");
		BufferedReader br=new BufferedReader(new InputStreamReader(fis,"utf-8")); 
		String line;
		 while ((line = br.readLine()) != null) {
             String[] user = line.split("=");
             list.add(user);
         }
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入姓名：");
		String name=sc.nextLine();
		System.out.println("请输入密码：");
		String password=sc.nextLine();
		int login=0;
		DateTimeFormatter.ofPattern("HH:mm:ss");
		br.close();
		for(int i=0;i<list.size();i++) {
			
			
			if(list.get(i)[0].equals(name)) {
				login++;
				String str2=list.get(i)[3].toString();
				LocalTime time2=LocalTime.parse(str2);
				LocalTime time1=LocalTime.now();
				long secondsBetween = ChronoUnit.SECONDS.between(time2, time1);
				System.out.println(secondsBetween );
				if(secondsBetween<20&&Integer.parseInt(String.valueOf(list.get(i)[2].charAt(0)))==3){
					System.out.println("不允许在此时间段登录！");
					break;
				}
				if(Integer.parseInt(String.valueOf(list.get(i)[2].charAt(0)))>=6) {
					System.out.println("账号已被冻结！");
					break;
				}
				if(list.get(i)[1].equals(password)) {
					
					if(Integer.parseInt(String.valueOf(list.get(i)[2].charAt(0)))<7) {
						if(Integer.parseInt(String.valueOf(list.get(i)[2].charAt(0)))<4) {
							System.out.println("登陆成功！");
							
							break;
							}else {
								if(secondsBetween>20) {
									System.out.println("登陆成功！");
									break;
								}
								
							}
					}
					
				}else {
					int count=Integer.parseInt(list.get(i)[2])+1;
					String[] arr=list.get(i);
				 	arr[2]= String.valueOf(count);
				 	arr[3]=LocalTime.now().toString();
				 	list.set(i, arr);
	                 System.out.println("密码错误！");
	                 break;
				}
			}
		}
		if(login==0) {
			System.out.println("没有该用户!");
		}
		for (String[] array : list) {
		    for (String element : array) {
		        System.out.print(element + " ");
		    }
		    System.out.println();
		}

		try (FileOutputStream fos = new FileOutputStream("abc\\user.txt");
	             OutputStreamWriter osw = new OutputStreamWriter(fos, "UTF-8");
	             BufferedWriter bw = new BufferedWriter(osw)) {

	            for (String[] user : list) {
	                bw.write(String.join("=", user));
	                bw.newLine(); 
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	}
}
