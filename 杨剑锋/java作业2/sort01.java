package homework;

import java.util.Arrays;
/*文字描述：
 * 1.选择第二个数为插入数，从插入数开始依次向前比较找到大于等于插入元素的位置，
 * 	如果找到了合适的插入位置，就将该位置之后的所有元素向后移动一位，腾出空间插入新元素。
 * 2.对剩余未排序的元素重复上述步骤，直到所有元素都被插入到正确的位置。
*/
public class sort01 {
	public static void main(String[] args) {
		int[] a={4,6,3,8,23,59,34,12,65,1};
		sort(a);
	}

	private static void sort(int[] a) {
		for(int i=1;i<a.length;i++){
			int inser=a[i];
			int index=i-1;
			while(index>=0&&inser<a[index]){
				a[index+1]=a[index];
				index--;
			}
			a[index+1]=inser;
		}
		System.out.println(Arrays.toString(a));
	}
}
