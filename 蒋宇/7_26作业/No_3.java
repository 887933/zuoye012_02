package HomeWork;

import java.io.File;

public class No_3 {
	public static void main(String[] args) {
//		统计任意一个文件夹中（包含子文件夹），每种文件的个数并打印
//		打印格式如下：
//		Txt:50个
//		Doc:20个
//		Java:50个
//		Class:50个
//		Png:10个
//		.....
		String str="e:\\eclipse\\Teacher_code";
		File f1=new File(str);
		File[] listFiles = f1.listFiles();
		int[] a=new int[5];
		a=findType(listFiles);
		for(int b:a){
			System.out.println(b);
		}

	}

	private static int[] findType(File[] listFiles) {
		// TODO Auto-generated method stub
		int[] arr={0,0,0,0,0};
//		 countTXT=arr[0];
//		 countDOC=arr[1];
//		 countJAVA=arr[2];
//		 countCLASS=arr[3];
//		 countPNG=arr[4];
		for(File a:listFiles){
			if (a!=null) {
				if (a.isFile()) {
					if (a.getName().endsWith(".txt")) {
						arr[0]++;
					}
					if (a.getName().endsWith(".doc")) {
						arr[1]++;
					}
					if (a.getName().endsWith(".java")) {
						arr[2]++;
					}
					if (a.getName().endsWith(".class")) {
						arr[3]++;
					}
					if (a.getName().endsWith(".png")) {
						arr[4]++;
					}
				} else {
					int[] b={0,0,0,0,0};
					b=findType(a.listFiles());
					for(int i=0;i<b.length;i++){
						arr[i]+=b[i];
					}
				} 
			}
		}
		return arr;
	}
}
