package homework6;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

public class java1 {
	public static void main(String[] args) {
		ArrayList<String> arr=new ArrayList<>();
		Collections.addAll(arr,"张三,21","李四,24","丁真,26","珍珠,27","锐刻五,30");
		Map<String,Integer> result=arr.stream()
		.collect(Collectors.toMap(t->t.split(",")[0],t->Integer.parseInt(t.split(",")[1])));
		System.out.println(result);
	}
}
