package demo19;

import java.util.Scanner;

public class Java04 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("请输入字符串1：");
        String s = sc.nextLine();
        char[] c = s.toCharArray();
        
        System.out.println("请输入字符串2：");
        String s1 = sc.nextLine();
        char[] c1 = s1.toCharArray();
        
        int count = 0, num = 0, m = 0;
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < c1.length; j++) {
                while (i < c.length && j < c1.length && c[i] == c1[j]) {
                    count++;
                    i++;
                    j++;
                }
                if (count > num) {
                    num = count;
                    m = i - 1; // 设置为上一个匹配字符的索引
                }
                count = 0; // 重置 count
                // 不需要手动设置 i 和 j
            }
        }
        System.out.println(num);
        for(int i = m;i<m+num;i++){
        	System.out.println(c[i]);
        }
    }
}
