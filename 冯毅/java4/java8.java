package java05;

import java.util.Scanner;

public class java8 {
public static void main(String[] args) {
	Scanner sc = new Scanner(System.in);
	System.out.print("请输入字符传：");
	String s=sc.next();
	System.out.println("不含有重复的最长子串的最大长度为："+lengthOfLongestSubstring(s));
}
public static int lengthOfLongestSubstring(String s) {
    if (s == null || s.isEmpty()) return 0;

    int n = s.length();
    int start = 0;
    int maxLength = 0;
    boolean[] c = new boolean[26]; 

    for (int end = 0; end < n; end++) {
        while (c[s.charAt(end) - 'a']) {
            c[s.charAt(start) - 'a'] = false; 
            start++; 
        }
        c[s.charAt(end) - 'a'] = true; 
        maxLength = Math.max(maxLength, end - start + 1); 
    }

    return maxLength;
}
}
