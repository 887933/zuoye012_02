package demo;

import java.util.Scanner;

public class No_32 {
	public static void main(String[] args) {
		//取一个整数a从右端开始的4～7位。
		//程序分析：可以这样考虑：
		//(1)先使a右移4位。
		//(2)设置一个低4位全为1,其余全为0的数。可用~(~0<<4)
		//(3)将上面二者进行&运算。
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一个数(大于7位)");
		int num=sc.nextInt();
		num/=1000;
		int result=num%10000;
		System.out.println(result);
	}

}
