package demo1;

import java.util.ArrayList;

public class Player {
	private String name;
	private ArrayList<String> poker=new ArrayList<String>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public ArrayList<String> getPoker() {
		return poker;
	}
	public void setPoker(ArrayList<String> poker) {
		this.poker = poker;
	}
	@Override
	public String toString() {
		return "Player [name=" + name + ", poker=" + poker + "]";
	}
	
}
