package homework;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class java06_RandomCallName4 {
	public static void main(String[] args) throws IOException {
		/* 
		 * 作业4：
		 * 名单里十个学生，每轮每个同学只能点一次
		 */
		ArrayList<Integer> roll = new ArrayList<Integer>();
		for(int i=0;i<10;i++) {
			roll.add(i);
		}
		int totalTimes = 100;
		for(int i=0;i<totalTimes;i+=10) {
			System.out.println("====第"+(i/10+1)+"轮====");
			Collections.shuffle(roll);
			for(int j=0;j<10;j++) {
				int num = roll.get(j);
				randomCallName3("names.txt",num);
			}
		}
	}
	
	private static void randomCallName3(String string, int num) throws IOException {
		FileReader fr = new FileReader(string);
		BufferedReader br = new BufferedReader(fr);
		String line = null;
		ArrayList<String> list = new ArrayList<String>();
		while((line = br.readLine())!=null&&line.length()>0) {
			list.add(line);
		}
		String name = list.get(num);
		System.out.println(name.split("-")[0]);
	}
}
