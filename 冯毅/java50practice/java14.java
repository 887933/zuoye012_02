package java50practice;

import java.beans.Statement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.util.Scanner;

public class java14 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.print("请输入日期（格式：2015-08-06）:");
		String date=sc.next();
		String[] split = date.split("-");
		int year=Integer.parseInt(split[0]);
		int mon=Integer.parseInt(split[1]);
		int day=Integer.parseInt(split[2]);
		LocalDate doy = LocalDate.of(year, mon, day);
		int i = doy.get(ChronoField.DAY_OF_YEAR);
		System.out.println(year+"年"+mon+"月"+day+"日是一年中的第"+i+"天");
	}
}
