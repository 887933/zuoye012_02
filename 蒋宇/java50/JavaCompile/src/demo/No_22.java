package demo;

public class No_22 {
	public static void main(String[] args) {
		//利用递归方法求5!。
//		程序分析：递归公式：fn=fn_1*4!
		int i=5;
		System.out.println(getMutiply(i));

	}
	public static long getMutiply(long i){
		if(i==1){
			return 1;
		}
		return i*getMutiply(i-1);
	}

}
