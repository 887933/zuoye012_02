package zuoye726;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Demo6 {
	public static void main(String[] args) throws Exception {
		String strXingURL="https://hanyu.baidu.com/shici/detail?from=kg1&highlight=&pid=0b2f26d4c0ddb3ee693fdb1137ee1b0d&srcid=51369";
		String malemingURL="http://mingzi.jb51.net/qiming/nanhai/68814860866.html";
        String femalemingURL="http://www.haoming8.cn/mingzi/25022.html";
		
        String xingContent=webCarawLer(strXingURL);
        String malemingContent=webCarawLer(malemingURL);
        String femalemingContent=webCarawLer(femalemingURL);
        
		ArrayList<String> listXing=getData(xingContent,"(.{4})(，|。)");
		ArrayList<String> listmaleMing=getData(malemingContent,"(..、){4}..");
		ArrayList<String> listfemaleMing=getData(femalemingContent,"(..、){4}..");
	    
		ArrayList<String> xing=execDataXing(listXing);
		ArrayList<String> maleming=execDataMing(listmaleMing,"男");
		ArrayList<String> femaleming=execDataMing(listfemaleMing,"女");
	    ArrayList<String> mxingMing=getInfo(xing,maleming,50);
	    ArrayList<String> fmxingMing=getInfo(xing,femaleming,50);
	    List<String> collect = Stream.concat(mxingMing.stream(), fmxingMing.stream()).collect(Collectors.toList());
	    
	   System.out.println(collect);
	    FileWriter fw=new FileWriter("name.txt");
	    for(String str1:collect){
	    	fw.write(str1);
	    	fw.write("\n");
	    	fw.flush();
	    }
	    
	fw.close();
	}

	private static ArrayList<String> getData(String Content, String regex) {
          ArrayList<String> list=new ArrayList<>();
          Pattern compile=Pattern.compile(regex);
          Matcher matcher = compile.matcher(Content);
          while(matcher.find()){
        	  String group=matcher.group();
        	  if(group.indexOf('e')==-1){
        		  list.add(group);
        	  }
          }
		
		return list;
	}

	private static ArrayList<String> execDataMing(ArrayList<String> listMing, String string) {
       Random r=new Random();
       ArrayList<String> ming=new ArrayList<>();
       for(String s:listMing){
    	   String[] split = s.split("、");
    	   for(String ss:split){
    		   ming.add(ss+"-"+string+"-"+(r.nextInt(7)+18));
    	   }
       }
       return ming;
		
	}

	private static ArrayList<String> getInfo(ArrayList<String> xing, ArrayList<String> ming,int count) {
		HashSet<String> set=new HashSet<>();
		while(true){
			if(set.size()==count){
				break;
			}
			Collections.shuffle(xing);
			Collections.shuffle(ming);
	    
	        set.add(xing.get(0)+ming.get(0));
	   
		}
		return new ArrayList<String>(set);
	}

	private static ArrayList<String> execDataXing(ArrayList<String> listXing) {
		ArrayList<String>  xing=new ArrayList<>();
		for (String string : listXing) {
			//赵钱孙李，或者周吴郑王。
			for(int i=0;i<string.length()-1;i++){
				char charAt = string.charAt(i);
				xing.add(charAt+"");
			}
		}
		return xing;
	}
	
	private static String webCarawLer(String strXingURL) throws Exception {
		StringBuilder sb=new StringBuilder();
		//创建一个url对象
		URL url =new URL(strXingURL);
		//连接上这个网站，确保网络通常
		URLConnection openConnection = url.openConnection();
		InputStream inputStream = openConnection.getInputStream();
		//读取数据
		InputStreamReader isr=new InputStreamReader(inputStream,"utf-8");
		int ch;
		while((ch=isr.read())!=-1){
			sb.append((char)ch);
		}
		isr.close();
		return sb.toString();
	}
	
}
