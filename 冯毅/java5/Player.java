package java06;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class Player {
	private String name;
	private TreeMap<Integer,String> tm= new TreeMap<Integer,String>();
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public TreeMap<Integer, String> getTm() {
		return tm;
	}
	public void setTm(TreeMap<Integer, String> tm) {
		this.tm = tm;
	}
	@Override
	public String toString() {
		return "Player [name=" + name + ", tm=" + tm + "]";
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((tm == null) ? 0 : tm.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Player other = (Player) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (tm == null) {
			if (other.tm != null)
				return false;
		} else if (!tm.equals(other.tm))
			return false;
		return true;
	}
	
	
	
	
	
}
