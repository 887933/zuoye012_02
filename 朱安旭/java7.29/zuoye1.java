package demo01;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.Random;
import java.util.Scanner;

public class zuoye1 {
	    public static void main(String[] args) throws IllegalArgumentException,InputMismatchException,Exception{
	        ArrayList<Student> list=new ArrayList<Student>();
	        Scanner sc=new Scanner(System.in);
	        System.out.print("请输入一共多少个学生");
	        int num=0;
	        boolean validAge = false;
    		while(!validAge){
    		try{
    			 num=sc.nextInt();
    			if(num<0){throw new IllegalArgumentException("输入错误，请输入整数");}
    			validAge = true;}	
    		catch (InputMismatchException e) {
                System.out.println("请输入一个有效的整数年龄");
                sc.nextLine();
    		}
    			catch(IllegalArgumentException e){
    			System.out.print("个数有误，请重新输入");
    			sc.nextLine();
    		} 
    		catch (Exception e) {
                System.out.println("输入错误，请输入一个整数");
                sc.nextLine();
    		}
    		}
	            storageName(list,num);
	        for(int i=0;i<num;i++){
	            System.out.println("第"+(i+1)+"个学生的名字是"+list.get(i).name+"年龄是"+list.get(i).age+"岁");
	        }
	        luck(list);
	    }
	    private static void storageName(ArrayList<Student> list, int x) throws ArrayIndexOutOfBoundsException{
			// TODO Auto-generated method stub
	    	Scanner sc=new Scanner(System.in);
			for(int i=0;i<x;i++){
	            Student s=new Student();
	            System.out.print("请输入第"+(i+1)+"个名称");
				s.setName(sc.next());
	            System.out.print("请输入第"+(i+1)+"个年龄");
	            boolean validAge = false;
	    		while(!validAge){
	    		try{
	    			s.setAge(sc.nextInt());
	    			if(s.getAge()<18||s.getAge()>25){
	    			throw new IllegalArgumentException("年龄超出有效范围，请输入合法的年龄。");
	    		}else {validAge = true;}
	    		}catch(IllegalArgumentException e){
	    			System.out.print("年龄有误，请重新输入");
	    			sc.nextLine();
	    		}
	    		}
	            
	            s.id=i+1;
	            list.add(s);
		}
	 
	        }
	    public static void luck(ArrayList<Student> list){
	        Random r=new Random();
	        int num=r.nextInt(list.size());
	        System.out.println("幸运儿的姓名是"+list.get(num).getName()+"年龄是"+list.get(num).getAge()+"岁");
	    }
	}
