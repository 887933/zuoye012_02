package zuoye724;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FunctionDemo6 {
public static List<Integer> collect1;
public static List<Integer> collect2;
	public static void main(String[] args) {
	     int []arr1 = {1,2,3};
	     int []arr2= {3,4,5,2};
	     collect1 = Arrays.stream(arr1).boxed().collect(Collectors.toList());
	     collect2 = Arrays.stream(arr2).boxed().collect(Collectors.toList());
		 List<Integer> result =new ArrayList<>();	
		 Stream<Integer> stream1 = collect1.stream().filter(new FunctionDemo6()::iscontains1);
		 Stream<Integer> stream2 = collect2.stream().filter(new FunctionDemo6()::iscontains2);
	     result=Stream.concat(stream1, stream2).collect(Collectors.toList());
	     System.out.println(result);
	}
	public boolean iscontains1(Integer t){
		return !collect2.contains(t);
	}
	public boolean iscontains2(Integer t){
		return !collect1.contains(t);
		    
	}

}

