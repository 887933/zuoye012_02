package java06;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.TreeMap;

public class Poker {
	public static void main(String[] args) {
		ArrayList<String> poker=new ArrayList<String>();
		ArrayList<Integer> ppp=new ArrayList<Integer>();
		String[] colors={"♠","♣","♦","♥"};
		String[] numbers={"2","A","K","Q","J","10","9","8","7","6","5","4","3"};
		poker.add("大王");
		poker.add("小王");
		for(String number:numbers){
			for(String color:colors){
				poker.add(color+number);
			}
		}
		for(int i=1;i<55;i++){
			ppp.add(i);
		}
//		System.out.println(ppp);
		Collections.shuffle(ppp);
		System.out.println(poker);
		TreeMap<Integer,String> tm= new TreeMap<Integer,String>();
		for(int i=0;i<54;i++){
			tm.put(i+1,poker.get(i));
		}
		System.out.println(tm);
//		Collections.shuffle(poker);
//		System.out.println(poker);
		Player p1 = new Player();
		p1.setName("老王");
		Player p2 = new Player();
		p2.setName("老李");
		Player p3 = new Player();
		p3.setName("老马");
		ArrayList<String> dipai=new ArrayList<String>();
		for(int i=0;i<poker.size();i++){
		    Integer n = ppp.get(i);
			if(i>=51){
				dipai.add(tm.get(n));
			}else if(i%3==0){
				p1.getTm().put(n,tm.get(n));
			}else if(i%3==1){
				p2.getTm().put(n, tm.get(n));
			}else if(i%3==2){
				p3.getTm().put(n, tm.get(n));
			}
		}
		System.out.println(p1.getName()+"的牌："+p1.getTm());
		System.out.println(p2.getName()+"的牌："+p2.getTm());
		System.out.println(p3.getName()+"的牌："+p3.getTm());
		System.out.println("底牌："+dipai);
	}
}
