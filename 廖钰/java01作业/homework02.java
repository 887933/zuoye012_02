package homework;

import java.util.Random;
import java.util.Scanner;

public class homework02 {
	public static void main(String[] args) {
		System.out.println("====输入三个数求最大值====");
		System.out.println(max(4,6,9));
		
		System.out.println("====while作业1：求1-100所有奇数和====");
		sumOfOddNumber(100);
		
		System.out.println("====while作业2：纸的厚度0.001m，求对折多少次达到8000m====");
		paperFoldTo8000();
		
		System.out.println("====for循环作业1：输出所有两位数、三位数、四位数、五位数的回文数====");
		forLoop01();
		
		System.out.println("====for循环作业2:1-100，打印所有个位是7、十位是7或者7的倍数的数====");
		forLoop02();
		
		System.out.println("====for循环作业3：键盘输入一个数字，求平方根，若能正好算出整数，如16的平方根为4，则输出4，否则输出平方根在哪个范围里，例如20的平方根在4-5之间====");
		forLoop03();
		
		System.out.println("====for循环作业4：输入一个1-100的数字，判断是否为质数====");
		forLoop04();

		System.out.println("====作业1：每天存2.5元零钱，当存五天花6元，问多少天能存到100元。====");
		break01();
		
		System.out.println("====随机打乱数组输出顺序====");
		int [] intarr= {1,2,3,4,5,6,7,8,9};
		randomOrder(intarr);

		System.out.println("====随机打乱二维数组输出顺序====");
		int[][] intarr2 = {{1,2,3},{4,5,6,7},{8,9,10}};
		randomOrder2(intarr2);

		System.out.println("====查询一个数是否存在于数组中====");
		int[] intarr3 = {1,3,5,7,8,10,12};
		System.out.println(isNumberExist(intarr3));
		
		System.out.println("====将原数组的2到5截取出来创建一个新数组====");
		int[] intarr4 = {1,2,3,4,5,6,7,8,9};
		int[] intarr5 = cutToNewArray(intarr4,2,5);
	}
	
	private static int[] cutToNewArray(int[] intarr, int a, int b) {
		int length = b-a,i=0;
		int[] newIntarr = new int[length];
		System.out.print("{");
		while(i<length) {
			newIntarr[i] = intarr[a+i];
			System.out.print(newIntarr[i]);
			if(i!=length-1) {
				System.out.print(", ");
			}
			i++;
		}
		System.out.println("}");
		return newIntarr;
	}

	private static boolean isNumberExist(int[] intarr) {
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入要查询的数字：");
		int num = sc.nextInt(),i=0;
		while(i<intarr.length) {
			if(intarr[i]==num) {
				return true;
			}
			i++;
		}
		return false;
	}

	private static void randomOrder2(int[][] intarr) {
		Random r = new Random();
		int i=0,j=0,x=0,y=0;
		while(i<intarr.length) {
			j=0;
			while(j<intarr[i].length) {
				x = r.nextInt(intarr.length);
				y = r.nextInt(intarr[x].length);
				int temp = 0;
				temp = intarr[i][j];
				intarr[i][j] = intarr[x][y];
				intarr[x][y] = temp;
				j++;
			}
			i++;
		}
		i = 0;
		j = 0;
		System.out.print("{");
		while(i<intarr.length) {
			j=0;
			System.out.print("{");
			while(j<intarr[i].length) {
				System.out.print(intarr[i][j]);
				if(j!=intarr[i].length-1) {
					System.out.print(", ");
				}
				j++;
			}
			System.out.print("}");
			if(i!=intarr.length-1) {
				System.out.print(", ");
			}
			i++;
		}
		System.out.println("}");
	}

	private static void randomOrder(int[] intarr) {
		Random r = new Random();
		int i =0, j=0;
		while(i<intarr.length) {
			j = r.nextInt(intarr.length);
			int temp = 0;
			temp = intarr[i];
			intarr[i] = intarr[j];
			intarr[j] = temp;
			i++;
		}
		i = 0;
		System.out.print("{");
		while(i<intarr.length) {
			System.out.print(intarr[i]+", ");
			i++;
		}
		System.out.println("}");
	}

	private static void break01() {
		double money=0;
		int day=0;
		while(money<100) {
			day++;
			money+=2.5;
			if(day%5==0) {
				money-=6;
			}
		}
		System.out.println("需要存"+day+"天");
	}

	private static void forLoop04() {
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入一个数字：");
		int number = sc.nextInt();
		for(int i=2;i<=number;i++) {
			if(number%i==0&&i!=number) {
				System.out.println(number+"是合数");
				break;
			}else if(number%i==0&&i==number) {
				System.out.println(number+"是质数");
			}
		}
	}

	private static void forLoop03() {
		Scanner sc = new Scanner(System.in);
		System.out.print("请输入一个数字：");
		int num = sc.nextInt();
		for(int i=1;;i++) {
			if(i*i==num) {
				System.out.println(num+"的平方根是"+i);
				break;
			}else if(i*i>num) {
				System.out.println(num+"的平方根在"+(i-1)+"和"+i+"之间");
				break;
			}
		}
	}

	private static void forLoop02() {
		for(int i=1;i<=100;i++) {
			if(i%10==7) {
				System.out.println(i);
			}else if(i/10==7) {
				System.out.println(i);
			}else if(i%7==0) {
				System.out.println(i);
			}
		}
	}

	private static void forLoop01() {
		for(int i=10;i<100;i++) {
			if(i/10==i%10) {
				System.out.println(i);
			}
		}
		for(int i=100;i<1000;i++) {
			if(i/100==i%10) {
				System.out.println(i);
			}
		}
		for(int i=1000;i<10000;i++) {
			if(i/1000==i%10&&(i%1000)/100==(i%100)/10) {
				System.out.println(i);
			}
		}
		for(int i=10000;i<100000;i++) {
			if(i/10000==i%10&&(i%10000)/1000==(i%100)/10) {
				System.out.println(i);
			}
		}
	}

	private static void paperFoldTo8000() {
		double i = 0.001;
		int times=0;
		while(i<8000) {
			i*=2;
			times++;
		}
		System.out.println("需要对折"+times+"次才能达到8000米的厚度");
	}

	private static void sumOfOddNumber(int top) {
		int x=1,sum=0;
		while(x<=top) {
			if(x%2==1){
				sum+=x;
			}
			x++;
		}
		System.out.println(sum);
	}

	public static int max(int x,int y,int z) {
		if(x>y) {
			if(x>z) {
				return x;
			}else {
				return y;
			}
		}else {
			if(y>z) {
				return y;
			}else {
				return z;
			}
		}
	}
}
