package java50practice;

public class java41 {
	public static void main(String[] args) {
        int peaches = 1; 
        while (true) {
            if (isValid(peaches, 5)) {
                System.out.println("海滩上原来最少有 " + peaches + " 个桃子。");
                break;
            }
            peaches++;
        }
    }

    private static boolean isValid(int peaches, int rounds) {
        for (int i = 0; i < rounds; i++) {
            if (peaches <= 1) {
                return false; 
            }
            peaches = (peaches - 1) / 5 * 4; 
        }
        return true;
    }
}
