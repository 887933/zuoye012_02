package demo1;

public class Person {
	private String name;
	private int age;
	private String gender;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	
	public String getGender() {
		return gender;
	}
	public Person(String name, String gender, int age) {
		super();
		this.name = name;
		this.age = age;
		this.gender = gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public Person(String name, int age) {
		super();
		this.name = name;
		this.age = age;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return  " ["+name + ","+age+"]";
	}
	public Object startsWith(String string) {
		// TODO Auto-generated method stub
		return null;
	}

}
