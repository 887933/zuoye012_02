package homework12;

public class Cooker extends Thread {
	@Override
	public void run() {
		while(true){
			synchronized (Desk.lock) {
				if(Desk.count==0){
					break;
				}
				if(!Desk.flag){
					Desk.flag=true;
					System.out.println("生产者生产一个汉堡");
					Desk.lock.notify();
				}else{
					try {
						Desk.lock.wait();
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
