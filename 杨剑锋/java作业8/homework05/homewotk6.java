package homework05;
/*作业4：生成的50个男生，50个女生.每一个信息占用一行。
准备一个文件，里面有是十个学生。
每一轮学生只能被点到一次，程序运行10次，算第一轮。
第11次运行的时候，不需要手动操作本地文件，要求程序自动开始第二轮点名
下面是我的文件内存：
慕毅翌-男-24
生宇邦-男-21
段雨晗-男-21
葛棋文-男-22
申瑾磊-男-19
钟义辰-男-24
官剑珲-男-21
侯鸿畅-男-22
黎铂宇-男-21
端致浩-男-18*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;


public class homewotk6 {
	public static void main(String[] args) throws IOException {
		ArrayList<Student> list = new ArrayList<>();
		BufferedReader br=new BufferedReader(new FileReader("student.txt"));
		String line;
		while((line=br.readLine())!=null){
			String[] arr = line.split("-");
			list.add(new Student(arr[0], arr[1], Integer.parseInt(arr[2]),Double.parseDouble(arr[3]) ));
		}
		br.close();
		double weight=0;
		for (Student student : list) {
			weight=weight+student.getWeight();
		}
		double[] arr=new double[list.size()];
		int index=0;
		for (Student student : list) {
			arr[index]=student.getWeight()/weight;
			index++;
		}
		for(int i=1;i<arr.length;i++){
			arr[i]=arr[i]+arr[i-1];
		}
		double number = Math.random();
		int result = -Arrays.binarySearch(arr, number)-1;
		System.out.println(list.get(result));
		Student student = list.get(result);
		student.setWeight(0);
		BufferedWriter bw=new BufferedWriter(new FileWriter("student.txt"));
		for (Student s : list) {
			if(weight==1){
				s.setWeight(1);
			}
			bw.write(s.toString());
			bw.newLine();
		}
		bw.close();
		if(weight==1){
			System.out.println("该轮结束");
		}
	}
}
