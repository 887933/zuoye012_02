package demo;

import java.util.ArrayList;
import java.util.List;

public class No_37 {
    public static void main(String[] args) {
        int n = 10; // 总人数
        List<Integer> circle = new ArrayList<>();
        
        for (int i = 1; i <= n; i++) {
            circle.add(i);
        }

        int idx = 0;
        while (circle.size() > 1) {
            idx = (idx + 2) % circle.size(); // 计算要删除的人的索引
            circle.remove(idx);
        }

        System.out.println("最后留下的人的编号是：" + circle.get(0));
    }
}

