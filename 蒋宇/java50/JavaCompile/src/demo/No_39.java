package demo;

import java.util.Scanner;

public class No_39 {
	public static void main(String[] args) {
		//编写一个函数，输入n为偶数时，调用函数求1/2+1/4+...+1/n,
		//当输入n为奇数时，调用函数1/1+1/3+...+1/n(利用指针函数) 
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		getResult(num);
	}

	private static void getResult(int num) {
		// TODO Auto-generated method stub
		double sum = 0;
		if(num%2==0){
			for(int i=2;i<=num;i+=2){
				sum+=(double)1/i;
			}
		}
		if(num%2==1){
			for(int i=1;i<=num;i+=2){
				sum+=(double)1/i;
			}
		}
		System.out.println(sum);
	}

}
