package HomeWork;

import java.io.File;
import java.nio.file.Files;

public class No_2 {
	public static void main(String[] args) {
		File f = new File("d:\\a\\b\\c");
		f.mkdirs();
		deleteFiles(f);
	}

	public static void deleteFiles(File file) {
		if (file.isDirectory()) {
			File[] files = file.listFiles();
			if (files != null) {
				for (File f : files) {
					deleteFiles(f);
				}
			}
		}
		
		// 删除文件或文件夹
		if (file.delete()) {
			System.out.println("Deleted: " + file.getAbsolutePath());
		} else {
			System.out.println("Failed to delete: " + file.getAbsolutePath());
		}
	}


}
