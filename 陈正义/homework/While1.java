package homework;
//while作业1： 求1-100之间的奇数和，并把求和的结果在控制台打印
public class While1 {
	public static void main(String args[]) {
		int i=1,sum=0;
		while(i<100) {
			if(i%2!=0) {
				sum=sum+i;
			}
			i++;
		}
		System.out.println("1-100之间的奇数和为："+sum);
	}

}
