package homework05;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class homework01 {
	public static void main(String[] args) throws IOException {
		ArrayList<String> list = new ArrayList<>();
		BufferedReader br=new BufferedReader(new FileReader("sort.txt"));
		String lin;
		while((lin=br.readLine())!=null){
			list.add(lin);
		}
		br.close();
		System.out.println(list);
		List<String> collect = list.stream().sorted()
		.collect(Collectors.toList());
		BufferedWriter bw=new BufferedWriter(new FileWriter("sort.txt"));
		for (String s : collect) {
			bw.write(s);
			bw.newLine();
		}
		bw.close();
	}
}
