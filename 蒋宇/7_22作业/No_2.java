package homework;

import java.util.ArrayList;
import java.util.Collections;

public class No_2 {
//	一个集合中
//	 *  分别存储了6个男演员
//	 *  和6名女演员.
//	 *  男演员只要名字为3个子的前面三人
//	 *  女演员只要姓林的,并且不要第一个
//	 *  把过滤后的男演员和女演员姓名结合在一起   
//		ArrayList<String> list = new ArrayList<String>();
//		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", 
	//"张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35",
	//"谢广坤-女-41","林婷-女-22","林立-女-23","刘亦菲-女-28");

	public static void main(String[] args) {
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> result = new ArrayList<String>();
        Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", 
        		"张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22",
        		"林立-女-23","刘亦菲-女-28");
        int countNan =0;
        int countNv =0;
        for(String a:list){
        	String[] split = a.split("-");
        	if(split[1].equals("男")&&split[0].length()==3&&countNan<3){
        		
        		result.add(split[0]);
        		countNan++;
        	}else if(split[1].equals("女")&&split[0].startsWith("林")){
        		if(countNv==0){
        			countNv++;
        		}else{
        			result.add(split[0]);
            		countNv++;
        		}
        		
        	}
        }
        System.out.println(result);
		
	}

}
