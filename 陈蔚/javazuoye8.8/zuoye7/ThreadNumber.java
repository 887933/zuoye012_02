package zuoye7;

public class ThreadNumber extends Thread{
	@Override
	public void run() {
		synchronized (Share.lock) {
			while(true){
				if(!Share.flag){
					if(Share.number<=52){
						System.out.print(Share.number++);
						System.out.print(Share.number++);
						Share.flag=true;
						Share.lock.notify();
					}
				}else{
					try {
						Share.lock.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}

	}


}
