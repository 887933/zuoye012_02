package homework03;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;

/*hashmap去重  
	合并 输出数组两个数组中都有元素则去除
	[1,2,3]
	[3,4,5,2]
	得到[1,4,5]*/

public class gather05 {
	public static void main(String[] args) {
		int[] a={1,2,3};
		int[] b={3,4,5,2};
		HashMap<Integer,Integer > Map = new HashMap<Integer,Integer >();
		for (int num : a) {
			Map.put(num, 0);
		}
		for (int num : b) {
			if (Map.containsKey(num)) {
				Map.remove(num);
			} else {
				Map.put(num, 0);
			}
		}
		Set<Entry<Integer, Integer>> entrySet = Map.entrySet();
		ArrayList<Integer> arrayList = new ArrayList<Integer>();
		for(Entry<Integer, Integer> e:entrySet){
			arrayList.add(e.getKey());
		}
		System.out.println(arrayList);
	}
}
