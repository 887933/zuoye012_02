package demo;

import java.util.Scanner;

public class No_25 {
	public static void main(String[] args) {
		// 一个5位数，判断它是不是回文数。即12321是回文数，
		// 个位与万位相同，十位与千位相同。
		System.out.println("请输入一个五位数");
		Scanner sc=new Scanner(System.in);
		int i=sc.nextInt();
		if (i % 10 == i / 10000 && (i / 10 % 10) == (i / 1000 % 10)){
			System.out.println("是回文数");
		}else{
			System.out.println("不是回文数");
		}


			 
	}
}
