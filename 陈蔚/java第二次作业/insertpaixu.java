package insertpaixu;

import java.util.Arrays;

public class insertpaixu {
public static void main(String[] args) {
	/*插入排序:一个无序数组，从数组的第二个元素开始，设置索引1和索引2，索引1负责推进，索引2负责后退，用来帮助元素逐一与前面的元素进行比较，
	 * 若前面的元素大于索引的元素，则交换，反之则不交换
	 * 1.在第一轮时，索引1和索引2都在arr[1]处，对应的元素为arr[1]，与arr[0]比较，后者大则交换，交换完成后，
	 * 随后索引1往后推进1位，并把索引2重新设置为索引1相同的位置
	 * 2.在第二轮时，索引1和索引2在arr[2]处，对应元素为arr[2]，与arr[0]和arr[1]比较，先与arr[1]比较，后者大则交换，
	 * 随后索引2后退至arr[1]的位置,索引2重新对应交换后的元素，与前面一个元素比较，比前面元素小则交换，直到前面没有元素为止
	 * 3.以此类推*/
	int[] arr={9,7,4,5,3,8,2,6,1};
	insertion(arr);
	
	
}

private static void insertion(int[] arr) {
	/*将索引1设为i，负责推进，索引2设为j，负责比较并交换*/
	int i,j;
	for(i=0;i<arr.length;i++){
		for(j=i;j>0;j--){
			if(arr[j]<arr[j-1]){
				swap(arr,j,j-1);
			}
		}
	}
	System.out.println(Arrays.toString(arr));
}

private static void swap(int[] arr, int i, int j) {
	int temp=arr[i];
	arr[i]=arr[j];
	arr[j]=temp;
}
}
