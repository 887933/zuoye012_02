package java13;

public class PrintSequence {
	private int number = 1;
	private int flag=0;
	private char letter = 'A';  
	private final Object lock = new Object();  
	private boolean numberTurn = true; // 控制打印顺序的标记  

	public void printNumbers() {  
		synchronized (lock) {  
			while (number <= 52 ) {  
				while (!numberTurn) {  
					try {  
						lock.wait();  
					} catch (InterruptedException e) {  
						Thread.currentThread().interrupt();  
						return;  
					}  
				}
				System.out.print(number);  
				number++;  
				if (number <= 52) {  
					System.out.print(number);  
					number++;  
				} 
				numberTurn = false;
				lock.notify(); // 通知等待的线程    
			}
		}
	}

	public void printLetters() {  
		synchronized (lock) {  
			while (letter <= 'Z') {  
				while (numberTurn) {  
					try {  
						lock.wait();  
					} catch (InterruptedException e) {  
						Thread.currentThread().interrupt();  
						return;  
					}  
				}  

				System.out.print(letter);  
				letter++;  
				numberTurn = true;  
                lock.notify();
			}  
		}  
	}  

	public static void main(String[] args) {  
		PrintSequence ps = new PrintSequence();  

		Thread t1 = new Thread(ps::printNumbers);  
		Thread t2 = new Thread(ps::printLetters);  

		t1.start();  
		t2.start();  
	}  
}
