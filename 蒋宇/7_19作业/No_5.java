package demo;

public class No_5 {
	public static void main(String[] args) {
        String str = "abcdefg";
        System.out.println("部分反转前：" + str);
        String reversedStr = reverseSubstring(str, 2, 5);
        System.out.println("部分反转后：" + reversedStr);
    }

    public static String reverseSubstring(String str, int start, int end) {
        StringBuilder sb = new StringBuilder(str);
        sb.replace(start, end + 1, new StringBuilder(str.substring(start, end + 1)).reverse().toString());
        return sb.toString();
    }

}
