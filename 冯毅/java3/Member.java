package java04;

import java.util.ArrayList;
import java.util.Random;

public class Member extends User {
	
	public Member(String name, int money) {
		super(name, money);
	}

	public void receive(ArrayList<Integer> redList){
		int index = new Random().nextInt(redList.size());
		//根据索引  ，从集合中删除这个元素 ，得到删除得红包
		Integer delta = redList.remove(index);
		//当前成员自己本来有多少钱
		int money=super.getMoney()+delta;
		super.setMoney(money);
	}
	
	public void show(){
		System.out.println("我叫:"+getName()+",我有多少钱:"+money);
	}
	
}
