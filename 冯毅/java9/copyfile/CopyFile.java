package java11;

import java.awt.FocusTraversalPolicy;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.concurrent.atomic.AtomicInteger;

public class CopyFile implements Runnable{
	 private final File s;
	 private final File d;
	 private final long total;
	 private final AtomicInteger cb;
	 private final String fileName;
	 public CopyFile(File s, File d, AtomicInteger cb, String fileName) {
		super();
		this.s = s;
		this.d = d;
		this.total=s.length();
		this.cb = cb;
		this.fileName = fileName;
	}
	@Override
	public void run() {
		try {
			FileInputStream fis=new FileInputStream(s);
			FileOutputStream fos=new FileOutputStream(d);
			byte[] b=new byte[1024];
			int len;
			try {
				while((len=fis.read(b))>0){
					fos.write(b, 0, len);
					cb.addAndGet(len);
					jindu();
				}
				System.out.println(fileName+"复制完成");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	int n=1;
	private void jindu(){
		double jd=(double)cb.get()/total*100;
		if(jd/10>=n){
			System.out.println(fileName+"复制了"+n+"0%");
			n++;
		}
	}
}
