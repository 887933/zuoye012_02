package homework;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class No_5 {
	public static void main(String[] args) {
//		hashmap去重  
// 		合并 输出数组两个数组中都有元素则去除
// 		[1,2,3]
// 		[3,4,5,2]
// 		得到[1,4,5]
		int[] arr1 = {1, 2, 3};
        int[] arr2 = {3, 4, 5, 2};
        
        HashMap<Integer, Boolean> map = new HashMap<>();
        
        for (int num : arr1) {
            map.put(num, true);
        }
        
        for (int num : arr2) {
            if (map.containsKey(num)) {
                map.remove(num);
            } else {
                map.put(num, true);
            }
        }
        
        List<Integer> result = new ArrayList<>(map.keySet());
        
        System.out.println(result);

	}

}
