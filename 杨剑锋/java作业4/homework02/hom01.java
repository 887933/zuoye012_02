package homework02;

import java.util.ArrayList;
import java.util.Scanner;

public class hom01 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		ArrayList<Integer> redlist=new ArrayList<>();
		int sum=0;
		while(true){
			System.out.println("请输入1-100整数：");
			int input=sc.nextInt();
			if(input>100||input<1){
				System.out.println("输入错误重新输入");
				continue;
			}
			redlist.add(input);
			sum+=input;
			if(sum>200){
				break;
			}
		}
		System.out.println("集合中的整数为：");
		System.out.println(redlist);
	}
}
