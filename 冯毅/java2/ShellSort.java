package java03;

import java.util.Arrays;

public class ShellSort {
    public static void main(String[] args) {
		int[] a={5,7,6,2,8,4,3,1};
		ShellSort(a);
	}
    public static void ShellSort(int[] a){
      int lip=a.length/2;//数组一半的长度作为起始标记点
	  int cnt=0;//记录排序次数
      while(lip>0){//当lip/2的结果小于零时，退出程序
    	  for(int i=lip;i<a.length;i++){//以lip作为起始标记点往后遍历
    		  int temp=a[i];
    		  int j;
    		  for(j=i;j>=lip&&a[j-lip]>temp;j-=lip){//对每一个分组进行插入排序
     			  a[j]=a[j-lip];
    		  }
    		  a[j]=temp;
    		  cnt++;
    		  System.out.println("第"+cnt+"次结果:"+Arrays.toString(a));
    	  }
    	  lip=lip/2;
      }
    }
    
}
