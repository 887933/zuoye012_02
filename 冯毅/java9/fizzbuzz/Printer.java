package java11;

public class Printer implements Runnable{
	private final String str;
	
	public Printer(String str) {
		this.str = str;
	}

	@Override
	public void run() {
		System.out.print(str+" ");
	}
	
}
