package homework;

import java.util.HashMap;
import java.util.Map;

public class CheckString_7_24 {
    public static void main(String[] args) {
        String str1 = "ppRYYGrrgsdgsYBR2258";
        String str2 = "YrR8RrYggsss";
        System.out.println(checkStrings(str1, str2));
        
    }

    public static String checkStrings(String str1, String str2) {
        Map<Character, Integer> charCountMap = str1.chars().mapToObj(c -> (char) c)
            .collect(HashMap::new, (map, c) -> map.put(c, map.getOrDefault(c, 0) + 1), HashMap::putAll);

        boolean isSubset = str2.chars().mapToObj(c -> (char) c)
            .allMatch(c -> charCountMap.containsKey(c) && charCountMap.get(c) > 0);

        return isSubset ? "YES " + (str1.length() - str2.length()) : "NO";
    }
}
