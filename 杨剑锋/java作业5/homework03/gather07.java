package homework03;

import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

/*我们使用[0-9] [a-z][A-Z]范围内来表示字符串
示例1：
输入ppRYYGrrYBR2258
	YrR8RrY
输出：
Yes 8


	输入:ppRYYGrrYB225
	YrR8RrY
NO*/

public class gather07 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String str1=sc.next();
		String str2=sc.next();
		HashMap<Character, Integer> hashMap = new HashMap<Character, Integer>();
		char[] a=str1.toCharArray();
		char[] b=str2.toCharArray();
		for(char e:a){
			if(hashMap.containsKey(e)){
				int value=hashMap.get(e)+1;
				hashMap.put(e, value);
			}else{
				hashMap.put(e, 1);
			}
		}
		for(char e:b){
			if(hashMap.containsKey(e)){
				int value=hashMap.get(e)-1;
				if(value==-1){
					System.out.println("NO");
					return;
				}
				hashMap.put(e, value);
			}else{
				System.out.println("NO");
				return;
			}
		}
		Set<Entry<Character, Integer>> entrySet = hashMap.entrySet();
		int num=0;
		for(Entry<Character, Integer> e:entrySet){
			num=num+e.getValue();
		}
		System.out.println("Yes "+num);
	}
}
