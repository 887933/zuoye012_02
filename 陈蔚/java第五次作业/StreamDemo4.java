package zuoye724;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
/*
 * 把任意字符串加密显示   
	 *  YUANzhi1987
	 *  加密后为
	 *  zvbo9441987
	 *  加密规则:
		 *   1.如果是大写字母变成小写字母,往后移动一位
		 *   2.小写字母通过收集输入发对应键盘来进行加密
		 *   1-1
		 *   abc-2
		 *   def-3
		 *   ghi-4
		 *   jkl-5
		 *   mno-6
		 *   pqrs-7
		 *   tuv-8
		 *   wxyz-9
		 *   0-0
		 *   3.数字不变*/
public class StreamDemo4 {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("请输入一个字符串:");
		String str=sc.nextLine();
		ArrayList<String> list=new ArrayList<>();
		Collections.addAll(list, "a-2","b-2","c-2","d-3","e-3","f-3","g-4","h-4","i-4","j-5",
				"k-5","l-5","m-6","n-6","o-6","p-7","q-7","r-7","s-7","t-8","u-8","v-8","w-9","x-9",
				"y-9","z-9","0-0","1-1","2-2","3-3","4-4","5-5","6-6","7-7","8-8","9-9");
		for(char c:str.toCharArray()){
			list.stream().filter(t->t.charAt(0)==c-'A'+'a').forEach(t->System.out.print((char)(t.charAt(0)+1)));
			list.stream().filter(t->t.charAt(0)==c).map(t->t.split("-")[1]).forEach(System.out::print);		
			}		
	}
}
