package HomeWork;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class CallNameReset {
	public static void main(String[] args) throws AgeException {
		ArrayList<Student> students = new ArrayList<Student>();
		addStudentName(students, 3);
		printStudentsName(students);
		String randName = randStudentName(students);
		System.out.println("幸运儿" + randName);
		HashMap<String,Integer> student=new HashMap<>();
	}

	public static void printStudentsName(ArrayList<Student> students) {
		for (int i = 0; i < students.size(); i++) {
			System.out.println("第" + (i + 1) + "个学生名称：" + students.get(i).getName());
		}

	}

	public static void addStudentName(ArrayList<Student> students, int x) {
	    Scanner sc = new Scanner(System.in);
	    int count = 0;

	    while (count != x) {
	        try {
	            System.out.println("Enter details for student " + (count + 1));
	            System.out.print("Name: ");
	            String name = sc.next();
	            System.out.print("Age: ");
	            int age = sc.nextInt();

	            Student s = new Student(name, age);
	            students.add(s);
	            count++;
	        } catch (AgeException e) {
	            System.out.println("Age input error. Please enter the details again.");
	            sc.nextLine(); // Clear the input buffer
	        }
	    }
	}


	public static String randStudentName(ArrayList<Student> students) {
		int index = new Random().nextInt(students.size());
		return students.get(index).getName();
	}

}
