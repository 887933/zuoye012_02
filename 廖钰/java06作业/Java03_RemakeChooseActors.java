package homework;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class Java03_RemakeChooseActors {
	public static void main(String[] args) {
		/*
		 *  2.
		 *  一个集合中
		 *  分别存储了6个男演员
		 *  和6名女演员.
		 *  男演员只要名字为3个子的前面三人
		 *  女演员只要姓林的,并且不要第一个
		 *  把过滤后的男演员和女演员姓名结合在一起   
			ArrayList<String> list = new ArrayList<String>();
			Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		 */
		ArrayList<String> list = new ArrayList<String>();
		ArrayList<String> names = new ArrayList<String>();
		Collections.addAll(list, "张无忌-男-15", "周芷若-女-14", "赵敏-女-13", "张强-男-20", "张三丰-男-100", "张翠山-男-40","张良-男-35","王二麻子-男-35","谢广坤-女-41","林婷-女-22","林立-女-23");
		
		List<String> male = list.stream()
								.filter(s->"男".equals(s.split("-")[1]))
								.limit(3)
								.collect(Collectors.toList());
		
		List<String> female = list.stream()
								.filter(s->"女".equals(s.split("-")[1]))
								.filter(s->s.startsWith("林"))
								.skip(1)
								.collect(Collectors.toList());
		
		male.stream().forEach(s->names.add(s));
		female.stream().forEach(s->names.add(s));
		
		System.out.println(names);
	}	
}
