package demo;

import java.util.Scanner;

public class No_3 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("请输入字符串用于判断");
		String a = scanner.next();
		if(isHuiWen(a)){
			System.out.println("该数是回文数");
		}else{
			System.out.println("不是回文数");
		}
	}

	private static boolean isHuiWen(String a) {
		int len=a.length();
		for(int i=0,j=a.length()-1;i<j;i++,j--){
			if(a.charAt(i)==a.charAt(j)){
				continue;
			}else{
				return false;
			}
		}
		return true;
		
	}

}
