package homework05;

public class user {
	private String username;
	private String password;
	private int count=0;
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	@Override
	public String toString() {
		return "Users [username=" + username + ", password=" + password + ", count=" + count + "]";
	}
	public user(String username, String password, int count) {
		super();
		this.username = username;
		this.password = password;
		this.count = count;
	}
	public user() {
	}
}
