package homework2;

import java.util.ArrayList;
/*8.随机点名案例

1.存储三个同学的姓名
2.查看所有同学的姓名
3.随机点名其中一人，打印到控制*/
import java.util.Random;
import java.util.Scanner;
public class CallName {
	public static void main(String args[]) {
		ArrayList<Student>list=new ArrayList<Student>();
		setStudentName(list);
		printStudentName(list);
		Student st=callName(list);
		System.out.println("幸运儿:"+st.getName()+" 年龄"+st.getAge()+" id:"+st.getId());
	}

	private static Student callName(ArrayList<Student> list) {
		Random r=new Random();
		int i=r.nextInt(list.size());
		Student s=list.get(i);
		return s;
	}

	private static void printStudentName(ArrayList<Student> list) {
		for(int i=0;i<list.size();i++) {
			Student s=list.get(i);
			System.out.println("学生"+(i+1)+"学生姓名:"+s.getName()+" 学生年龄"+s.getAge()+" id:"+s.getId());
		}
	}

	private static void setStudentName(ArrayList<Student> list) {
		Scanner sc=new Scanner(System.in);
		System.out.println("学生数量：");
		int num=sc.nextInt();
		for(int i=0;i<num;i++) {
		    System.out.println("请输入学生信息");
		    String name=sc.next();
		    int age=sc.nextInt();
		    int id=i+1;
		    sc.nextLine();
		    Student s= new Student(id,age,name);
		    list.add(s);
			}
		
		
	}
	

}