package hom05;

import java.io.File;
//作业2：删除一个多级文件夹
public class homework03 {
	public static void main(String[] args) {
		File file=new File("D:\\bbbb");
		if(file.exists()){
			deleteDirectory(file);
			System.out.println("目录已删除");
		}else{
			System.out.println("目录不存在");
		}
	}

	private static void deleteDirectory(File file) {
		File[] f1=file.listFiles();
		if(f1!=null){
			for(File f2:f1){
				if(f2.isDirectory()){
					deleteDirectory(f2);
				}else{
					f2.delete();
				}
			}
		}
		file.delete();
	}
}
