package dailyProgram;

import java.util.Scanner;

public class rabbitBirth {
	public static void main(String[] args) {
		System.out.println("你想知道第几个月兔子的数量？请输入：");
		Scanner sc= new Scanner(System.in);
		int mon = sc.nextInt();
		System.out.println("第"+mon+"个月的兔子对数为"+rabbitBirth(mon)+"对。");
	}

	private static int rabbitBirth(int mon) {
		if(mon == 1||mon == 2) {
			return 1;
		}
		return rabbitBirth(mon-1)+rabbitBirth(mon-2);
	}
}
