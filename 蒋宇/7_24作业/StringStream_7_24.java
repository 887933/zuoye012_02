package homework;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StringStream_7_24 {
    public static void main(String[] args) {
        String str = "aaabbc";
        // ʹ��Stream����ArrayList��HashMap
        List<Character> filteredChars = str.chars()
                .mapToObj(c -> (char)c)
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()))
                .entrySet().stream()
                .filter(entry -> entry.getValue() > 1)
                .map(Map.Entry::getKey)
                .collect(Collectors.toList());

        System.out.println(filteredChars);
    }
}