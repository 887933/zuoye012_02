package homework3;

import java.util.ArrayList;
import java.util.Random;
class Member extends User {
    public Member(String username, int money) {
        super(username, money);
    }

    public void receive(ArrayList<Integer> redList) {
        int index = new Random().nextInt(redList.size());
        Integer delta = redList.remove(index);
        int money = this.getMoney() + delta;
        this.setMoney(money);
    }

    public void show() {
        System.out.println("Member: " + getName() + ", Money: " + getMoney());
    }
}